#include <limits>
#include <QFileDialog>
#include <QTextStream>
#include <QPrintDialog>
#include <QPrintPreviewDialog>
#include <QPrinter>
#include <QPalette>
// #include <QMessageBox>
#include <QHeaderView>
#include <QInputDialog>
#include <QImageReader>
#include <QStatusBar>
#include "mainform.h"
#include "aboutform.h"
#include "spectralparsdialog.h"
#include "deconvform.h"
#include "arraymanager.h"
#include "datatable.h"
#include "proceditor.h"
#include "optionsDialog.h"
#include "expanddialog.h"
#include "2moment.cpp"
#include "cmatrix_utils.h"
#include <QDebug>
#include <QStyleFactory>
#include <QDesktopServices>
#include <QUrl>
#include <QtAlgorithms>
#include <QDialogButtonBox>
#include <QRadioButton>
#include <QButtonGroup>
#include <QListWidgetItem>
#include <QSvgRenderer>
#include <QMdiSubWindow>
#include <QSplitter>
#include "plotarea.h"
//#include <QSvgGenerator>

using namespace std;
using namespace libcmatrix;

void MainForm::init()
{
	fileNewWindow();
	mlDialog=new ModelessDialog(this);
        connect(data, SIGNAL(undoChanged(bool)), editUndoAction, SLOT(setEnabled(bool)));
	connect(data, SIGNAL(dataEmptified(bool)), this, SLOT(disactivateControls(bool)));
	initColourSchemeList();
	readSettings();
	if (QStyleFactory::keys().contains(data->global_options.style))
		QApplication::setStyle(QStyleFactory::create(data->global_options.style));
	datatable=NULL;
	deconvform=NULL;
	arraymanager=NULL;
	initAvailableProcList();
	initFileFilterList();
//	initCommandTable();
	QString prevproclist=QDir::homePath()+"/.gsim/proclist.prc";
	if (QFile::exists(prevproclist))
		loadProcFile(prevproclist);
	else {
		selectedProcList=availableProcList;
		updateProcTable();
		}
	updateProcToolBar();
	disactivateControls(true);
}

void MainForm::disactivateControls(bool dataEmpty)
{
		d1DockWidget->widget()->setEnabled(!dataEmpty);
		d2DockWidget->widget()->setEnabled(!dataEmpty);
		procDockWidget->widget()->setEnabled(!dataEmpty);
		infoDockWidget->widget()->setEnabled(!dataEmpty);

		fileSaveAction->setEnabled(!dataEmpty);
		fileExportPdfAction->setEnabled(!dataEmpty);
		fileCloseAction->setEnabled(!dataEmpty);
		filePrintAction->setEnabled(!dataEmpty);
		filePrintPreviewAction->setEnabled(!dataEmpty);
		fileReloadAction->setEnabled(!dataEmpty);
		fileWatchAction->setEnabled(!dataEmpty);
		fileNewWindowAction->setEnabled(!dataEmpty);
		fileExportVectorAction->setEnabled(!dataEmpty);
		
		fileMoveToMenu->setEnabled(!dataEmpty);

		editMenu->setEnabled(!dataEmpty);
		editCopyAction->setEnabled(!dataEmpty);

		viewRealAction->setEnabled(!dataEmpty);
		viewImagAction->setEnabled(!dataEmpty);
		viewReferencingAction->setEnabled(!dataEmpty);
		viewSetRangeAction->setEnabled(!dataEmpty);
		rulerMenu->setEnabled(!dataEmpty);

		analysisDeconvAction->setEnabled(!dataEmpty);
		analysisArrayAction->setEnabled(!dataEmpty);
		procMenu->setEnabled(!dataEmpty);

		commandLineEdit->setEnabled(!dataEmpty);
	
		windowsMenu->setEnabled(mdiArea->subWindowList().size()>1);
		fileMoveToMenu->setEnabled(mdiArea->subWindowList().size()>1);

		plotToolBar->setEnabled(!dataEmpty);
		procToolBar->setEnabled(!dataEmpty);
		if (dataEmpty)
			infoTextEdit->clear();
}


void MainForm::readSettings()
{
        QSettings settings("GSim", "GSim");
	if (settings.allKeys().isEmpty())
		return;
        settings.beginGroup("MainWindow");
	restoreState(settings.value("windows_geom").toByteArray());
        resize(settings.value("size", QSize(1024, 768)).toSize());
        move(settings.value("pos", QPoint(0, 0)).toPoint());
	working_directory=settings.value("dir").toString();
	if (working_directory.isEmpty())
		working_directory=QDir::homePath();
	ppSpinBox->setValue(settings.value("NoiseFactor",0).toInt());
	levelsSpinBox->setValue(settings.value("levels",6).toInt());
	plot3D->nlevels=levelsSpinBox->value();
	posnegComboBox->setCurrentIndex(settings.value("posneg",0).toInt());
	plot3D->pos_neg=posnegComboBox->currentIndex();
	floorSpinBox->setValue(settings.value("floor",5.0).toDouble());
	plot3D->floor=floorSpinBox->value();
	ceilingSpinBox->setValue(settings.value("ceiling",100.0).toDouble());
	plot3D->ceiling=ceilingSpinBox->value();
	multiplierSpinBox->setValue(settings.value("multiplier",2.0).toDouble());
	plot3D->multiplier=multiplierSpinBox->value();
	sliceComboBox->setCurrentIndex(settings.value("slice",0).toInt());
	QString colourSchemeName=settings.value("colourScheme","white").toString();
	ColourScheme* scPt=NULL;
	if (!colourSchemeList.size())
		qDebug()<<"colourSchemeList is empty!";
	for (int i=0; i<colourSchemeList.size(); i++) {
		if (colourSchemeName==colourSchemeList.at(i)->name)
			scPt=colourSchemeList.at(i);
	}
	if (!scPt)
		scPt=colourSchemeList.at(0); //set white scheme by default
	plot2D->setScheme(*scPt);
	plot3D->setScheme(*scPt);
        settings.endGroup();
}

void MainForm::writeSettings()
{
	QSettings settings("GSim", "GSim");
        settings.beginGroup("MainWindow");
        settings.setValue("size", size());
        settings.setValue("pos", pos());
	settings.setValue("dir", working_directory);
	settings.setValue("windows_geom", saveState());
	settings.setValue("NoiseFactor", ppSpinBox->value());
	settings.setValue("levels", levelsSpinBox->value());
	settings.setValue("posneg", posnegComboBox->currentIndex());
	settings.setValue("floor", floorSpinBox->value());
	settings.setValue("ceiling", ceilingSpinBox->value());
	settings.setValue("multiplier", multiplierSpinBox->value());
	settings.setValue("slice", sliceComboBox->currentIndex());
	settings.setValue("colourScheme", activePlot()->colourScheme.name);
        settings.endGroup();
}

void MainForm::destroy()
{
	delete data;
}

void MainForm::closeEvent(QCloseEvent *)
{
	QString prevproclist=QDir::homePath()+"/.gsim";
	QDir dir(prevproclist);
	bool ok;
	if (!dir.exists()) {
		ok=dir.mkpath(prevproclist);
		}
	prevproclist=QDir::homePath()+"/.gsim/proclist.prc";
	saveProcFile(prevproclist);
	writeSettings();
	if (arraymanager!=NULL)
		arraymanager->close();
	if (datatable!=NULL)
		datatable->close();
}

//void MainForm::initCommandTable()
//{
//Left for the future - too many problems
/*	commandTable.clear();
//	commandTable.push_back(commandPair("editproc",&MainForm::on_editProcButton_clicked));
//	QStringListModel* commandList=commandLineEdit->model();
//	QStringList clist=commandList->stringList();
//	foreach (commandPair p, commandTable)
//		clist<<p.name;
//	commandList->setStringList(clist);
*/
//}

void MainForm::initAvailableProcList()
{
	availableProcList.push_back(new SortRowsProc(data));
	availableProcList.push_back(new RearrangeProc(data));
	availableProcList.push_back(new DCOffsetProc(data));
	availableProcList.push_back(new SetSizeDirProc(data));
	availableProcList.push_back(new autoBruker(data));
	availableProcList.push_back(new DirShiftProc(data));
	availableProcList.push_back(new ShearingProc(data));
	availableProcList.push_back(new drawDiag(data));
	availableProcList.push_back(new t1DepGaussLB(data));
	availableProcList.push_back(new DirLBProc(data));
	availableProcList.push_back(new DirFtProc(data));
	availableProcList.push_back(new DirInvertProc(data));
	availableProcList.push_back(new DirPhaseProc(data));
	availableProcList.push_back(new autoPhaseProc(data));
	availableProcList.push_back(new t1DepPhaseProc(data));
    availableProcList.push_back(new TshearingProc(data));
	availableProcList.push_back(new magnSpectrumProc(data));
	availableProcList.push_back(new baselineCorrection(data));
	availableProcList.push_back(new binningDir(data));
	availableProcList.push_back(new IndirLBProc(data));
	availableProcList.push_back(new SetSizeIndirProc(data));
	availableProcList.push_back(new IndirFtProc(data));
	availableProcList.push_back(new IndirInvertProc(data));
	availableProcList.push_back(new IndirPhaseProc(data));
	availableProcList.push_back(new IndirShiftProc(data));
	availableProcList.push_back(new addNoiseProc(data));
	availableProcList.push_back(new smoothProc(data));
	availableProcList.push_back(new addSpectra(data));
	availableProcList.push_back(new appendSpectrum(data));
	availableProcList.push_back(new transposeProc(data));
	availableProcList.push_back(new cropProc(data));
	availableProcList.push_back(new loadFileProc(data));
	availableProcList.push_back(new saveFileProc(data));
	availableProcList.push_back(new ExternalCommandProc(data));
	availableProcList.push_back(new balanceIndir(data));
	availableProcList.push_back(new redorProc(data));
        availableProcList.push_back(new scale(data));
        availableProcList.push_back(new sum_sb(data));
#ifdef QT_SCRIPT_LIB
	availableProcList.push_back(new scriptProc(data));
#endif

	procActions.clear();
	QStringList clist;
	for (size_t i=0; i<availableProcList.size(); i++) {
		availableProcList.at(i)->setPlots(plot2D,plot3D,this);
		procActions.push_back(availableProcList.at(i)->action());
		procMenu->addAction(procActions[i]);
		connect(availableProcList[i], SIGNAL(changeInfo()), this, SLOT(callChangeInfo()));
		connect(availableProcList[i], SIGNAL(changeFilesList()), this, SLOT(updateFilesList()));
		clist<<availableProcList[i]->getCommand();
		}
//	QStringListModel* commandList=qobject_cast<QStringListModel*> (commandCompleter->model());
	QStringListModel* commandList=commandLineEdit->model();
	commandList->setStringList(clist);
}

void MainForm::initFileFilterList()
{
	fileFilterList.clear();
	fileFilterList.push_back(new MatlabFileFilter(data));
	fileFilterList.push_back(new SimpsonFileFilter(data));
	fileFilterList.push_back(new SpinsightFileFilter(data));
	fileFilterList.push_back(new VnmrFileFilter(data));
	fileFilterList.push_back(new XwinnmrFileFilter(data));
	fileFilterList.push_back(new XwinnmrprocFileFilter(data));
	fileFilterList.push_back(new SpinevolutionFileFilter(data));
	fileFilterList.push_back(new CastepFileFilter(data));
}

void MainForm::initColourSchemeList()
{
	colourSchemeList.clear();
	colourSchemeList.push_back(new whiteScheme());
	colourSchemeList.push_back(new blackScheme());
	colourSchemeList.push_back(new yellowScheme());
}

BaseProcessingFunc* MainForm::findFunc(QString name)
{
	BaseProcessingFunc* result=NULL;
	size_t n=availableProcList.size();
	for (size_t i=0; i<n; i++)
	{
		if (name==availableProcList.at(i)->getName())
			result=availableProcList.at(i);
	}
	return result;
}

void MainForm::updateProcTable()
{

//store previous parameters
	QHash<QString, QStringList> paras;
	int n=storedProcList.size();
	if (n>procTable->rowCount())
		n=procTable->rowCount(); //FIXME this avoid some crashes, however the nature of them is not clear.
	for (int i=0; i<n; i++) {
		QStringList entry=paras.value(storedProcList.at(i)->getName());
		if (procTable->item(i,0)->checkState()==Qt::Checked)
			entry<<"*";
		else
			entry<<" ";
		entry<<procTable->item(i,2)->text();
		paras[storedProcList.at(i)->getName()]=entry;
	}

	procTable->clear();
	n=selectedProcList.size();
	procTable->setRowCount(n);
	procTable->setColumnCount(4);
	procTable->horizontalHeader()->hide();
	for (size_t i=0; i<n; i++) {
		selectedProcList.at(i)->setPlots(plot2D, plot3D, this);
		connect (selectedProcList[i], SIGNAL(changeInfo()), this, SLOT (callChangeInfo()));
		connect (selectedProcList[i], SIGNAL(changeFilesList()), this, SLOT (updateFilesList()));
		selectedProcList.at(i)->placeToTable(procTable,i);
// restore parameters if were present
		QStringList l=paras.value(selectedProcList.at(i)->getName());
		if (!l.isEmpty()) {
			if (l.at(0)=="*")
				procTable->item(i,0)->setCheckState(Qt::Checked);
			else 
				procTable->item(i,0)->setCheckState(Qt::Unchecked);
			procTable->item(i,2)->setText(l.at(1));
			if(l.size()>2) {
				l.takeFirst();
				l.takeFirst();
			}
			paras[selectedProcList.at(i)->getName()]=l;
		}
	}
	procTable->resizeColumnToContents(0);
	procTable->setColumnWidth(3,24);
	procTable->resizeRowsToContents();
}

void MainForm::updateProcToolBar()
{
	procToolBar->clear();
	size_t n=data->global_options.procToolBarList.size();
	for (size_t i=0; i<n; i++) {
		BaseProcessingFunc* func=findFunc(data->global_options.procToolBarList.at(i));
		if (func!=NULL)
			procToolBar->addAction(func->action());
	}
	if (n)
		procToolBar->show();
	else
		procToolBar->hide();
}

void MainForm::callChangeInfo()
{
	update_info(data->getActiveCurve());
}

void MainForm::on_editProcButton_clicked()
{
    ProcEditor* pe = new ProcEditor(this);
    pe->exec();
    delete pe;
    updateProcTable();
    updateProcToolBar();
}

void MainForm::on_saveProcButton_clicked()
{
    QFileDialog file_dialog(
    			this,
			"Choose a file",
			 working_directory
                        );
    file_dialog.setFileMode(QFileDialog::AnyFile);
    file_dialog.setLabelText(QFileDialog::Accept,"Save");
    QCheckBox attachwindowbox("Attach processing to selected data files");
    if (data->size())
      	file_dialog.layout()->addWidget(&attachwindowbox);
    QString s;
    if (file_dialog.exec())
    	s = file_dialog.selectedFiles().at(0);
    if (s.isEmpty()) return;
    if (!s.endsWith(".prc"))
	s+=QString(".prc");
    saveProcFile(s);
    statusBar()->showMessage(QString("Proccesing list saved in file %1").arg(s),2000);
    if (attachwindowbox.isChecked()) {
	QStringList datalist,proclist;
	foreach (int i, data->selectedCurves) {
		datalist<<data->get_info(i)->filename;
		proclist<<s;
	}
	writeAttachedProcessing(datalist,proclist);
    }
}

void MainForm::saveProcFile(QString s)
{
     QFile file(s);
        if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
            return;

        QTextStream out(&file);
        int n=procTable->rowCount();
	for (size_t i=0; i<n; i++) {
		out<<selectedProcList.at(i)->getName()<<"\n";
		if (procTable->item(i,0)->checkState()==Qt::Checked)
			out<<"*\n";
		else
			out<<"\n";
		out<<procTable->item(i,2)->text()<<"\n";
	}
	file.close();
}

void MainForm::on_loadProcButton_clicked()
{
QString s = QFileDialog::getOpenFileName(
                    this,
                    "Choose a file",
                    working_directory,
                    "Processing lists (*.prc)");
     if (s.isEmpty()) return;
     loadProcFile(s);
     statusBar()->showMessage("Processing list is loaded",2000);
}


void MainForm::loadProcFile(QString s)
{
        QFile file(s);
        if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
            return;

	QStringList all;
    QTextStream in(&file);
    while (!in.atEnd()) 
        all << in.readLine();
 	file.close();
	interpreteProcList(all);
}

void MainForm::interpreteProcList(QStringList& all)
{	
	if (!all.size()%3) {
		QMessageBox::critical(this,"Proccesing list error", "Processing list has wrong length");
		return;
	}
	selectedProcList.clear();
	QStringList checks;
	QStringList options;
	size_t n=all.size()/3;
	for (size_t i=0; i<n; i++) {
		BaseProcessingFunc* func=findFunc(all.at(3*i));
		if (func==NULL)  //skip item if unrecognised
			continue;
		selectedProcList.push_back(func);
		checks.push_back(all.at(3*i+1));
		options.push_back(all.at(3*i+2));
		}
	updateProcTable();
	for (size_t i=0; i<checks.size(); i++) {
		if (checks.at(i)=="*")
			procTable->item(i,0)->setCheckState(Qt::Checked);
		procTable->item(i,2)->setText(options.at(i));
	}
}

void MainForm::readAttachedProcessing(QStringList& data_files, QStringList& proc_files)
{
	data_files.clear();
	proc_files.clear();
	QString store_name=QDir::homePath()+"/.gsim/attachedProcList";
	QFile file(store_name);
	if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
		return;
	QTextStream in(&file);
	while (!in.atEnd()) {
	QString line = in.readLine();
	QStringList entries=line.split(";");
	if (entries.size()!=2)
		continue;
	data_files<<entries.at(0);
	proc_files<<entries.at(1);
	}
	file.close();
}

void MainForm::writeAttachedProcessing(QStringList& data_files, QStringList& proc_files)//add contents
{
	QStringList prevData, prevProc;
	readAttachedProcessing(prevData, prevProc);
	QString store_name=QDir::homePath()+"/.gsim/attachedProcList";
	QFile file(store_name);
	if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
		return;
//check for dublicates
	foreach (QString file, data_files) {
		int index=prevData.indexOf(file);
		if (index>=0) {
			prevData.removeAt(index);
			prevProc.removeAt(index);
		}
	}
	prevData+=data_files;
	prevProc+=proc_files;
	while (prevData.size()>data->global_options.maxAttachedProc) {
		prevData.removeFirst();
		prevProc.removeFirst();
	}
	QTextStream out(&file);
	for (size_t i=0;i<prevData.size();i++)
		out << prevData.at(i)<< ";" <<prevProc.at(i)<< "\n";
	file.close();
}

void MainForm::removeAttachedProcessing(QString& data_file)//remove given file form attached list
{
	QStringList prevData, prevProc;
	readAttachedProcessing(prevData, prevProc);
	int i=prevData.indexOf(data_file);
	if (i>=0) {
		prevData.removeAt(i);
		prevProc.removeAt(i);
	}
	QString store_name=QDir::homePath()+"/.gsim/attachedProcList";
	QFile::remove(store_name);
	writeAttachedProcessing(prevData,prevProc);
}

void MainForm::checkAttachedProcessing(QStringList& data_files, QStringList& proc_files, QStringList& check_files)//check files from check_files list for possible attached processing
{
	int index=-1;
	foreach(QString checking, check_files){
		index=data_files.indexOf(checking);
		if (index>=0) {
			if (!QFile::exists(proc_files.at(index)))
				continue;//file has been moved/deleted
			QMessageBox msgBox;
			QPushButton *yesButton = msgBox.addButton(QMessageBox::Yes);
			QPushButton *noButton = msgBox.addButton(QMessageBox::No);
			QPushButton *disattachButton = msgBox.addButton(tr("Disattach"), QMessageBox::ActionRole);
			msgBox.setIcon(QMessageBox::Question);
			msgBox.setWindowIcon(QIcon(":/images/gsim.png"));
			msgBox.setDefaultButton(yesButton);
			msgBox.setText(QString("File %1 has attached processing stored in %2\nDo you like to load processing?").arg(checking).arg(proc_files.at(index)));
			msgBox.setWindowTitle(QString("Attached processing found"));
			msgBox.exec();
			if (msgBox.clickedButton() == yesButton)
				loadProcFile(proc_files.at(index));
			else if (msgBox.clickedButton() == noButton)
				continue;
			else if (msgBox.clickedButton() == disattachButton)
				removeAttachedProcessing(data_files[index]);
		}
	}
}

Plot2DWidget* MainForm::activePlot()
{
	if (!data->size())
		return plot2D;
	if (data->get_odata(data->getActiveCurve())->rows()>1)
		return plot3D;
	else
		return plot2D;
}

void MainForm::on_extractProcButton_clicked()
{
	const size_t k=data->getActiveCurve();
	if (data->get_info(k)->file_filter==NULL) {
		QMessageBox::critical(this,"Extraction failed","Data format is unset: extraction impossible");
		return;
		}
	QStringList list;
	data->get_info(k)->file_filter->extract(data->get_entry(k),list);
	if (!list.size()) {
		QMessageBox::critical(this,"Extraction failed","Processing list extraction is not supported for this file format");
		return;
	}
//	qDebug()<<list;
	interpreteProcList(list);
	statusBar()->showMessage("Processing operation have been extracted",2000);
}

void MainForm::on_reloadProcessButton_clicked()
{
	double xwinmin=activePlot()->xwinmin;
	double xwinmax=activePlot()->xwinmax;
	double ywinmax=activePlot()->ywinmax;
	double ywinmin=activePlot()->ywinmin;
	int type=data->get_info(data->getActiveCurve())->type;
	int type1=data->get_info(data->getActiveCurve())->type1;
	fileReload();
	on_processButton_clicked();
	if ((type!=data->get_info(data->getActiveCurve())->type) || (type1!=data->get_info(data->getActiveCurve())->type1))
		activePlot()->cold_restart(data);
	else {
		activePlot()->set_window_range(xwinmin,xwinmax,ywinmin,ywinmax);
		activePlot()->update();
	}
}


void MainForm::on_processButton_clicked()
{
	data->makeBackup();
	size_t n=procTable->rowCount();
	try {
		data->mutex.lock();
		for (size_t i=0; i<n; i++) {
			if (procTable->item(i,0)->checkState()==Qt::Checked)
				selectedProcList.at(i)->applyInt(i);
		}
		data->mutex.unlock();
		plot2D->cold_restart(data);
		plot3D->cold_restart(data);
		update_info(data->getActiveCurve());
		updateFilesList();
		statusBar()->showMessage("Active dataset has been processed",2000);
	}
	catch (MatrixException exc) {
		data->mutex.unlock();
		QApplication::restoreOverrideCursor();
		QMessageBox::critical(this, "Error", exc.what());
		editUndo();
	}
}

void MainForm::commandLineEdit_returnPressed()
{
	QStringList optlist;
	QString com=commandLineEdit->text();
	QString orig=com;
	bool toTable=false;
	if (com.startsWith("table:")) { //undocumented feature
		storedProcList=selectedProcList;
		selectedProcList.clear();
		toTable=true;
		com.remove("table:");
	}
	QStringList allc=com.split("+");
	data->makeBackup();
//	bool updateScreens=(allc.size()>1)? true: false;
	foreach(QString s, allc) {
		QStringList list=s.split(" ",QString::SkipEmptyParts);
		QAction* act=NULL;
//		vfpointer vfp=NULL;
		BaseProcessingFunc* func=NULL;
		if (!list.size())
			continue;	
		for (size_t i=0; i<procActions.size(); i++)
			if (list.at(0)==availableProcList.at(i)->getCommand()) {
				func=availableProcList.at(i);
				act=procActions.at(i);
			}
//		foreach (commandPair p, commandTable) {
//			if (list.at(0)==p.name)
//				vfp=p.function;
//		}
//		if (vfp) {
//			(*this.*vfp)();
//			continue;
//		}
		if (!act) {
			commandLineEdit->setText(QString("Command '%1' not found").arg(s));
			return;
		}
		if (toTable) {
			selectedProcList.push_back(func);
			if (list.size()>1)
				optlist<<list.at(1);
			else
				optlist<<"";
		}
		else {//don't execute if intended to go to table only
//			data->makeBackup();
			if (list.size()==1)
				act->trigger();
			else {
				func->toTable=false;
				connect (func, SIGNAL(stringIsReady()), func, SLOT(applyThroughMenu()));
				func->setOptions(list.at(1));
			}
		}
	}
//update option fields in the option table
	if (toTable) {
		if (optlist.size()!=selectedProcList.size())
			qDebug()<<"Problem with putting to table";
		updateProcTable();
		for (size_t i=0; i<optlist.size(); i++) {
			procTable->item(i,0)->setCheckState(Qt::Checked);
			if (optlist.at(i)!="")
				procTable->item(i,2)->setText(optlist.at(i));
		}
	}

//include last line in commandCompleter
//	QStringListModel* commandList=qobject_cast<QStringListModel*> (commandCompleter->model());
	QStringListModel* commandList=commandLineEdit->model();
	QStringList clist=commandList->stringList();
	if (clist.size()>availableProcList.size()+100)  //too long list should shortened
		clist.removeAt(availableProcList.size());
	clist<<orig;
	commandLineEdit->addToHistory(orig);
	commandList->setStringList(clist);
	commandLineEdit->clear();
}

void MainForm::fileNewWindow()
{
	static int wcounter=1;
	PlotArea* temparea=new PlotArea(this);
	QMdiSubWindow* subWindow=mdiArea->addSubWindow(temparea);
	mdiArea->setMinimumSize(QSize(160,120));
	mdiArea->setActiveSubWindow(subWindow);
	connect(temparea->plot2D, SIGNAL(openFileAsked(QString)), this, SLOT(fileOpen(QString)));
	connect(temparea->plot3D, SIGNAL(openFileAsked(QString)), this, SLOT(fileOpen(QString)));
	connect(temparea->plot2D, SIGNAL(onXaxisLabelClicked()), this, SLOT(cycChangeXAxis()));
	connect(temparea->plot3D, SIGNAL(onXaxisLabelClicked()), this, SLOT(cycChangeXAxis()));
	connect(temparea->plot3D, SIGNAL(onYaxisLabelClicked()), this, SLOT(cycChangeYAxis()));
//	update2d();
	subWindow->setWindowTitle(QString("Window%1").arg(wcounter++));
	subWindow->setWindowIcon(QIcon(":/images/gsim.png"));
	subWindow->raise();
	windowActivated(subWindow);
	subWindow->show(); // bug in Qt? see main.cpp for extra changes
}

void MainForm::windowActivated( QMdiSubWindow * wind)
{
	if (!wind) {
		if (!mdiArea->subWindowList().size())
			fileNewWindow();
		return;
	}
	PlotArea* temparea = qobject_cast<PlotArea*>(wind->widget());
	if (!temparea) {
		qDebug()<<"MainForm::windowActivated error";
		return;
	}
	data=temparea->data;
	plot2D=temparea->plot2D;
	plot3D=temparea->plot3D;
	plotarea=temparea->plotarea;

	plot3D->nlevels=levelsSpinBox->value();
	plot3D->pos_neg=posnegComboBox->currentIndex();
	plot3D->floor=floorSpinBox->value();
	plot3D->ceiling=ceilingSpinBox->value();
	plot3D->multiplier=multiplierSpinBox->value();

//experimental quality control
	plot3D->qstepx=qualitySpinBox->value();
	plot3D->qstepy=qualitySpinBox->value();

	foreach(BaseProcessingFunc* func, availableProcList) {//update pointers in Processing functions FIXME!
		func->data=temparea->data;
		func->setPlots(temparea->plot2D,temparea->plot3D,this);
	}
	foreach(BaseFileFilter* filter, fileFilterList) //update pointers in file filters list FIXME!
		filter->data=temparea->data;
//	fileMoveToMenu->clear();
	foreach (QAction* wa, fileMoveToMenu->actions()){
		disconnect(wa);
		fileMoveToMenu->removeAction(wa);
	}
//	windowsMenu->clear();
	foreach (QAction* wa, windowsMenu->actions()){
		disconnect(wa);
		windowsMenu->removeAction(wa);
	}
	foreach (QMdiSubWindow * w, mdiArea->subWindowList()) {
		QAction * wact=new QAction(w->windowTitle(),windowsMenu);
		if (w==mdiArea->activeSubWindow())
			wact->setText(wact->text().append(" *"));
		windowsMenu->addAction(wact);
		connect(wact,SIGNAL(triggered()),this,SLOT(windowsMenuCalled()));
		if (w!=wind) {
			QAction * act=new QAction(w->windowTitle(),fileMoveToMenu);
			fileMoveToMenu->addAction(act);
			connect(act,SIGNAL(triggered()),this,SLOT(fileMoveTo()));
		}
	}
	updateFilesList();
	disactivateControls(!data->size());
	whitewashButton->setChecked(plot2D->whitewash);
}

void MainForm::fileMoveTo()
{
	QString winname=qobject_cast<QAction*>(sender())->text();
	QMdiSubWindow* targetwin=NULL;
	foreach (QMdiSubWindow* w, mdiArea->subWindowList())
		if (w->windowTitle()==winname)
			targetwin=w;
	if (!targetwin)
		qDebug()<<"Error:Target window is not found";
//	qDebug()<<"Asked to move data"<<data->get_short_filename(data->getActiveCurve())<<"from"<<mdiArea->activeSubWindow()->windowTitle()<<"to"<<targetwin->windowTitle();
	QList<data_entry> movingdata;
	foreach (int i, data->selectedCurves)
		movingdata.push_back(*(data->get_entry(i))); //get all selected entry in temp array
	fileClose(); //delete all selected datasets
	mdiArea->setActiveSubWindow(targetwin);
	for (size_t i=0; i<movingdata.size(); i++) {
		movingdata[i].parent.hasParent=false; //forget parent information
		data->add_entry(movingdata.at(i));
	}
	data->updateAxisVectors();
	updateFilesList();
	plot2D->cold_restart(data);
	plot3D->cold_restart(data);
	disactivateControls(!data->size());
}

void MainForm::windowsMenuCalled()
{
	QString winname=qobject_cast<QAction*>(sender())->text();
	foreach (QMdiSubWindow* w, mdiArea->subWindowList())
		if (w->windowTitle()==winname)
			mdiArea->setActiveSubWindow(w);
}


void MainForm::fileOpenDialog()
{
	QFileDialog file_dialog(
    			this,
			"Choose a file",
			 working_directory
                        );
    file_dialog.setFileMode(QFileDialog::ExistingFiles);
    QStringList filters;
    QString all_ext;
    for (size_t i=0; i<fileFilterList.size(); i++) {
		if (fileFilterList.at(i)->canRead())
			all_ext+=fileFilterList.at(i)->extensions()+" ";
    }
    filters<<"Any known fileformats ("+all_ext+")";
    for (size_t i=0; i<fileFilterList.size(); i++) {
		if (fileFilterList.at(i)->canRead())
			filters.push_back(fileFilterList.at(i)->name()+" ("+fileFilterList.at(i)->extensions()+")");
    }	

    file_dialog.setNameFilters(filters);

    QCheckBox newwindowbox("Open in new window");
    if (data->size())
      	file_dialog.layout()->addWidget(&newwindowbox);

    QStringList list;
    if (file_dialog.exec())
    	list = file_dialog.selectedFiles();
   if (list.isEmpty()) return;
	QStringList attData, procData;
	readAttachedProcessing(attData, procData);
	checkAttachedProcessing(attData, procData,list);
    if (newwindowbox.isChecked()) {
	fileNewWindow();
	}
    const size_t len=list.size();
	for (size_t i=0; i<len; i++){
		QString name=list.at(i);
			if (name.isEmpty()) 
				return;
        QString filter=file_dialog.selectedNameFilter();
	try{
		fileOpen(name, filter);//0-any other x: x-1 filter in fileFilterList
	}
	catch(MatrixException exc) {
		QMessageBox::critical(this,"Reading error", exc.what());
		return;
	}
	}
	statusBar()->showMessage("File has been loaded",2000);
}

void MainForm::fileOpen(QString name,  QString filter, int flags)
{
    BaseFileFilter* fpointer=NULL;
	if (filter.contains("Any known fileformats")) {
		for (size_t i=0; i<fileFilterList.size(); i++)
            if (fileFilterList.at(i)->matching(name.toLatin1()))
				fpointer=fileFilterList.at(i);
	}
	else {
		for (size_t i=0; i<fileFilterList.size(); i++) {
			if (filter.contains(fileFilterList.at(i)->name()))
				fpointer=fileFilterList.at(i);
		}
	}

	if (fpointer==NULL) {
		QMessageBox::critical(this,"Error", "Appropriate file format is not found");
		return;
		}
	if (!QFile::exists(name)) {
		QMessageBox::critical(this,"Failed",QString("File %1 doesn't exist").arg(name));
		return;
	}
	data->anyFileOpen(name,fpointer,flags);
	if (!data->size())
		return;

	if (flags&OpenUpdateWorkingDirectory) {
		QDir dir;
		working_directory=dir.absoluteFilePath(name);
	}
	if (flags&OpenSetActive) {
		const size_t k=data->size()-1;
		data->setActiveCurve(k);
	}
	updateFilesList();
	const size_t K=data->getActiveCurve();

	if (data->get_odata(K)->rows()>1)
		plotarea->setCurrentIndex(1);
	else 
		plotarea->setCurrentIndex(0); //switch 1D/2D view
	if (flags&OpenFullRestart) {
    		plot2D->cold_restart(data);
		plot3D->cold_restart(data);
	}
	else
		activePlot()->update();
}

/*
void MainForm::fileSaveDir()
{
    if (!data->size()) return;
    QFileDialog file_dialog(
    			this,
			"Choose a directory",
			 working_directory
                        );
    file_dialog.setFileMode(QFileDialog::DirectoryOnly);
    QStringList filters;
	
	for (size_t i=0; i<fileFilterList.size(); i++)
		if (fileFilterList.at(i)->canWrite()) 
				filters.push_back(fileFilterList.at(i)->name()+" (*)");
    file_dialog.setFilters(filters);
    file_dialog.setLabelText(QFileDialog::Accept,"Save");
    QStringList s;
    if (file_dialog.exec())
    	s = file_dialog.selectedFiles();

   if (s.isEmpty()) 
	return;
   QDir dir;
   dir.mkpath(s[0]);
   dir.cd(s[0]);
   QString filter=file_dialog.selectedFilter();
//   working_directory=s.at(0);
   try {
	BaseFileFilter* fpointer=NULL;
	for (size_t i=0; i<fileFilterList.size(); i++) {
		if (filter.contains(fileFilterList.at(i)->name())) {
			fpointer=fileFilterList.at(i);
			break;
			}
	}

	if (fpointer==NULL) {
		QMessageBox::critical(this,"Error", "Appropriate file format is not found");
		return;
		}
    
	foreach (int k, data->selectedCurves) {
		QString name=dir.absolutePath()+"/"+data->get_short_filename(k);
		int save_fault=data->anyFileSave(k, name, fpointer);
		if (save_fault) {
			QMessageBox::critical(this, "Saving error", "Can't save data in "+name);	
		}
	}
   }
   catch(MatrixException exc) {	QMessageBox::critical(this,"Saving error", exc.what());
	return;
   }
   statusBar()->showMessage("Selected files have been saved",2000);
}
*/

void MainForm::fileSaveDialog()
{
    if (!data->size()) return;
    QFileDialog file_dialog(
    			this,
			"Choose a file",
			 working_directory
                        );
    file_dialog.setFileMode(QFileDialog::AnyFile);
    QStringList filters;
	
	for (size_t i=0; i<fileFilterList.size(); i++)
		if (fileFilterList.at(i)->canWrite()) 
				filters.push_back(fileFilterList.at(i)->name()+" (*)");
    file_dialog.setNameFilters(filters);
    file_dialog.setLabelText(QFileDialog::Accept,"Save");

    QStringList s;
    if (file_dialog.exec())
    	s = file_dialog.selectedFiles();

   if (s.isEmpty()) 
	return;
   QString filter=file_dialog.selectedNameFilter();
   try {
   	fileSave(s[0],filter);
   }
   catch(MatrixException exc) {
	QMessageBox::critical(this,"Saving error", exc.what());
	return;
   }
   statusBar()->showMessage("File has been saved",2000);
}

void MainForm::fileSave(QString& name, QString filter, int flags)
{
	BaseFileFilter* fpointer=NULL;
	for (size_t i=0; i<fileFilterList.size(); i++) {
		if (filter.contains(fileFilterList.at(i)->name())) {
			fpointer=fileFilterList.at(i);
			break;
			}
	}

	if (fpointer==NULL) {
		QMessageBox::critical(this,"Error", "Appropriate file format is not found");
		return;
		}
    
	if (!data->size()) return;
	const size_t k=data->getActiveCurve();
	int read_fault=data->anyFileSave(k, name, fpointer, flags);
	if (read_fault) {
		QMessageBox::critical(this, "Saving error", "Can't save data in "+name);	
	}
	else {
		if (!fpointer->savesToDir())
			name.truncate(name.lastIndexOf(QRegExp("[/\\\\]")));
		if (flags & SaveUpdateWorkingDirectory)
			working_directory=name;
		updateFilesList();
	}
}



void MainForm::curveSelected(int n)
{
	const size_t n_old=data->getActiveCurve();
	if (n<0 || n>=data->size())
		return;
	data->setActiveCurve(n);
	const size_t ni=data->get_odata(n)->rows();
	QPalette pal=filesListWidget->palette(); //change highlighting colour to the colour of the active curve 

if(ni<2){ //selected curve is 1d NMR
   plotarea->setCurrentIndex(0); 
   pal.setColor(QPalette::Highlight, plot2D->get_color(n));
   if (plot2D->get_color(n).red()+plot2D->get_color(n).green()+plot2D->get_color(n).blue()>384)
	pal.setColor(QPalette::HighlightedText,Qt::black);
   else
	pal.setColor(QPalette::HighlightedText,Qt::white);
   if (!data->global_options.mixFidSpec) {
		if (data->get_info(n)->type!=data->get_info(n_old)->type)
			plot2D->hot_restart();
		else
			plot2D->update();
	}
   else
   	plot2D->update();
}
else {
   plotarea->setCurrentIndex(1);
   if (!data->global_options.mixFidSpec) {
		if ((data->get_info(n)->type!=data->get_info(n_old)->type) || (data->get_info(n)->type1!=data->get_info(n_old)->type1))
			plot3D->hot_restart();
		else
			plot3D->update();
	}
   else
   	plot3D->update();
   if (plot3D->get_color(n)!=pal.color(QPalette::Window)) //prevent to use white on white, etc.
   	pal.setColor(QPalette::Highlight, plot3D->get_color(n));
   else
	pal.setColor(QPalette::Highlight, qApp->palette().color(QPalette::Highlight));
   if (plot3D->get_color(n).red()+plot3D->get_color(n).green()+plot3D->get_color(n).blue()>384)
	pal.setColor(QPalette::HighlightedText,Qt::black);
   else
	pal.setColor(QPalette::HighlightedText,Qt::white);
}
filesListWidget->setPalette(pal);
update_info(n);
setWindowTitle(QString("GSim - %1").arg(data->get_short_filename(n)));
}

void MainForm::filesSelectionChanged()
{
	QList<QListWidgetItem*> items=filesListWidget->selectedItems();
	data->selectedCurves.clear();
	for (size_t i=0; i<items.size(); i++) {
		data->selectedCurves.push_back(filesListWidget->row(items.at(i)));
	}
	qSort(data->selectedCurves.begin(),data->selectedCurves.end()); //FIXME: By some reason list is not sorted (Qt 4.3.1)
}

void MainForm::on_sticksButton_clicked()
{
    if (plot2D->visualMode>=2)
	plot2D->visualMode=0;
    else
	plot2D->visualMode++;
    QString vmname;
    switch (plot2D->visualMode){
	case 0:
		vmname=QString("curves");
		break;
	case 1:
		vmname=QString("sticks");
		break;
	case 2:
		vmname=QString("curves and points");
		break;
    }
    statusBar()->showMessage(QString("1D spectra will be shown as %1").arg(vmname),2000);
    plot2D->update();
}

void MainForm::on_whitewashButton_toggled(bool state)
{
    plot2D->whitewash=state;
    plot2D->update();
}

void MainForm::gridActiontoggled( bool mode )
{
  foreach (QMdiSubWindow* win, mdiArea->subWindowList()) {
   qobject_cast<PlotArea*>(win->widget())-> plot2D->is_xgrid=mode;
   qobject_cast<PlotArea*>(win->widget())-> plot2D->is_ygrid=mode;
   qobject_cast<PlotArea*>(win->widget())-> plot2D->update();
   qobject_cast<PlotArea*>(win->widget())->plot3D->is_xgrid=mode;
   qobject_cast<PlotArea*>(win->widget())->plot3D->is_ygrid=mode;
   qobject_cast<PlotArea*>(win->widget())-> plot3D->update(); //no needs to recalculate countours
 }
}

void MainForm::on_contourButton_toggled(bool mode) {
	foreach (QMdiSubWindow* win, mdiArea->subWindowList()) {
		qobject_cast<PlotArea*>(win->widget())->plot3D->isRaster=!mode;
		qobject_cast<PlotArea*>(win->widget())->plot3D->Update();
	}
}

void MainForm::on_projectionsButton_toggled(bool mode) {
	foreach (QMdiSubWindow* win, mdiArea->subWindowList()) {
		if (mode)
            qobject_cast<PlotArea*>(win->widget())->plot3D->hasProjections=true;
		else {
			qobject_cast<PlotArea*>(win->widget())->plot3D->deleteProjections();
			qobject_cast<PlotArea*>(win->widget())->plot3D->hasProjections=false;
		}
        //qobject_cast<PlotArea*>(win->widget())->plot3D->cold_restart(data); //BUG! data pointer is the same for all open windows - break the data!
        qobject_cast<PlotArea*>(win->widget())->plot3D->update();
	}
}

void MainForm::on_smartSideviewsButton_toggled(bool mode)
{
	foreach (QMdiSubWindow* win, mdiArea->subWindowList()) {
		if (mode)
			qobject_cast<PlotArea*>(win->widget())->plot3D->smartSideviews=true;
		else {
			qobject_cast<PlotArea*>(win->widget())->plot3D->createProjections();
			qobject_cast<PlotArea*>(win->widget())->plot3D->smartSideviews=false;
		}
		qobject_cast<PlotArea*>(win->widget())->plot3D->update();
	}
}

void MainForm::on_addLegendButton_clicked()
{
	if (plot3D->hasLegend)
		plot3D->hasLegend=false;
	else
		plot3D->hasLegend=true;
	plot3D->update();
}

/*
void MainForm::on_addLegendButton_clicked()
{
	const size_t k = data->getActiveCurve();
	if (data->get_odata(k)->rows()<2)
		return;
	QList<graph_object> graphics=data->get_graphics(k);
	int plotstep=plot3D->fontMetrics().height();
	int ypos=10;//initial y-position of the legend on the plot
	for (size_t i=0; i<plot3D->levels.size(); i++) { //the main loop over the number of levels
		bool show_level=true;
		graph_object line;
		line.type=GRAPHLINE;
		line.colour=plot3D->colourScheme.plot3DRasterColour.at(i);
		graph_object label;
		label.type=GRAPHTEXT;
		label.colour=plot3D->colourScheme.plot3DRasterColour.at(i);
		if (plot3D->isRaster) {
			if (i==plot3D->levels.size()-1)
				show_level=false;
			else if (plot3D->levels.at(i)<0 && plot3D->levels.at(i+1)>0)
				show_level=false;
			else
				label.maindata=QString("%1 <-> %2").arg(plot3D->levels.at(i)).arg(plot3D->levels.at(i+1));
		}
		else {
			plot3D->colourContours=true;
			label.maindata=QString("%1").arg(plot3D->levels.at(i));
		}
		double x1,x2,y1,y2,tx,ty;
		if (show_level) {
			plot3D->fromscreen_toreal(QPoint(100,ypos),x1,y1);
			plot3D->fromscreen_toreal(QPoint(140,ypos),x2,y2);
			plot3D->fromscreen_toreal(QPoint(160,ypos+plotstep/2),tx,ty);
			ypos+=plotstep;
			line.pos=QLineF(x1,y1,x2,y2);
			label.pos=QPointF(tx,ty);
			graphics.push_back(line);
			graphics.push_back(label);
		}
	}
	data->set_graphics(k,graphics);
	plot3D->update();
}
*/

void MainForm::moveActiontriggered(bool) {
	moveAction->setChecked(true);
	hscaleAction->setChecked(false);
	vscaleAction->setChecked(false);
	selectAction->setChecked(false);
	foreach (QMdiSubWindow* win, mdiArea->subWindowList()) {
		qobject_cast<PlotArea*>(win->widget())->plot2D->move_state=MOVEALL; //move all spectra
		qobject_cast<PlotArea*>(win->widget())->plot3D->move_state=MOVEALL; //move all spectra
		qobject_cast<PlotArea*>(win->widget())->plot2D->unselectAll();
		qobject_cast<PlotArea*>(win->widget())->plot3D->unselectAll();
	}
}

void MainForm::vscaleActiontriggered(bool) {
	moveAction->setChecked(false);
	hscaleAction->setChecked(false);
	vscaleAction->setChecked(true);
	selectAction->setChecked(false);
	foreach (QMdiSubWindow* win, mdiArea->subWindowList()) {
		qobject_cast<PlotArea*>(win->widget())->plot2D->move_state=VERTSCALING; //vertical scaling
		qobject_cast<PlotArea*>(win->widget())->plot3D->move_state=VERTSCALING;
		qobject_cast<PlotArea*>(win->widget())->plot2D->unselectAll();
		qobject_cast<PlotArea*>(win->widget())->plot3D->unselectAll();
	}
}

void MainForm::hscaleActiontriggered(bool) {
	moveAction->setChecked(false);
	hscaleAction->setChecked(true);
	vscaleAction->setChecked(false);
	selectAction->setChecked(false);
	foreach (QMdiSubWindow* win, mdiArea->subWindowList()) {
		qobject_cast<PlotArea*>(win->widget())->plot2D->move_state=HORSCALING;
		qobject_cast<PlotArea*>(win->widget())->plot3D->move_state=HORSCALING;
		qobject_cast<PlotArea*>(win->widget())->plot2D->unselectAll();
		qobject_cast<PlotArea*>(win->widget())->plot3D->unselectAll();
	}
}

void MainForm::selectActiontriggered(bool) {
	moveAction->setChecked(false);
	hscaleAction->setChecked(false);
	vscaleAction->setChecked(false);
	selectAction->setChecked(true);
	foreach (QMdiSubWindow* win, mdiArea->subWindowList()) {
		qobject_cast<PlotArea*>(win->widget())->plot2D->move_state=GRAPHSELECTION;
		qobject_cast<PlotArea*>(win->widget())->plot3D->move_state=GRAPHSELECTION;
	}
	activePlot()->is_leftbar=false;
	activePlot()->is_rightbar=false;
	activePlot()->update();
}

void MainForm::resetPlotActiontriggered(bool) {
	if (plotarea->currentIndex()==0)
		resetAll2d();
	else
		resetAll3d();
}

void MainForm::removeMarkersActiontriggered(bool) {
	Plot2DWidget* plot=activePlot();
	plot->is_leftbar=false;
	plot->is_rightbar=false;
	plot->update();
}

void MainForm::drawImageActiontriggered(bool)
{
	const size_t k=data->getActiveCurve(); 
	if (!activePlot()->is_leftbar) {
			QMessageBox::warning(this, "No markers found", "This function draws text at the main marker position.\nPlease, set main marker first");
			return;
		}
	QList<QByteArray> f= QImageReader::supportedImageFormats ();
	QString formats=QString("Images ("); 
	for (size_t i=0;i<f.size(); i++)
		formats+=QString("*.%1 ").arg(QString(f.at(i)));
	formats+=")";
	QString s = QFileDialog::getOpenFileName(
                    this,
                    "Choose a file",
                    working_directory,
                    formats);
	QPixmap pixmap=QPixmap(s);
	if (pixmap.isNull()){
		QMessageBox::critical(this, "Can't load image", "Image file cannot be loaded");
		return;
	}
	graph_object obj;
	obj.type=GRAPHIMAGE;
	obj.pos=QVariant(activePlot()->leftbarpos);
	obj.maindata=pixmap;
	QList<graph_object> graphics=data->get_graphics(k);
	graphics.push_back(obj);
	data->set_graphics(k, graphics);
	activePlot()->update();
}

void MainForm::drawTextActiontriggered(bool)
{
	if (!data->size())
		return;
	Plot2DWidget* plot=activePlot();
	const size_t k=data->getActiveCurve(); 

	if (!plot->is_leftbar) {
			QMessageBox::warning(this, "No markers found", "This function draws text at main marker position.\nPlease, set main marker first");
			return;
		}
	const Display_* disp=data->get_display(k);
	if (disp->xscale!=1.0 || disp->vscale!=1.0 || disp->xshift!=0.0 || disp->vshift!=0.0) {
		QMessageBox::warning(this, "Curve is scaled", "Active curve is scaled/shifted in either direction.\nDrawing operations are possible only for the fixed curve\nPlease, fix shifts/scalings using Edit->Fix shift/scalings menu");
			return;
	}
		graph_object obj;
		obj.type=GRAPHTEXT;
		obj.pos=QVariant(plot->leftbarpos);
		obj.maindata=QInputDialog::getText(this,"Input text", "Text:");
		QList<graph_object> graphics=data->get_graphics(k);
		graphics.push_back(obj);
		data->set_graphics(k, graphics);
		plot->update();
}

void MainForm::drawLineActiontriggered(bool)
{
	if (!data->size())
		return;
	const size_t k=data->getActiveCurve(); 
	Plot2DWidget* plot=activePlot(); 
	if ((!plot->is_leftbar)||(!plot->is_rightbar)) {
			QMessageBox::warning(this, "No markers found", "This function draws line between positions of the main and secondary markers.\nPlease, set markers first");
			return;
		}
	const Display_* disp=data->get_display(k);
	if (disp->xscale!=1.0 || disp->vscale!=1.0 || disp->xshift!=0.0 || disp->vshift!=0.0) {
		QMessageBox::warning(this, "Curve is scaled", "Active curve is scaled/shifted in either direction.\nDrawing operations are possible only for the fixed curve\nPlease, fix shifts/scalings using Edit->Fix shift/scalings menu");
			return;
	}
	graph_object obj;
	obj.type=GRAPHLINE;
	obj.pos=QLineF(plot->leftbarpos,plot->rightbarpos);
	QList<graph_object> graphics=data->get_graphics(k);
	graphics.push_back(obj);
	data->set_graphics(k, graphics);
	plot->update();
}

void MainForm::scale2D(double coeff)
{
	if (!data->size()) return;
	data->makeBackup();
	QList<int> list=data->selectedCurves;
	foreach (int k, list)
		data->set_odata(k,coeff*(*data->get_odata(k)));
	activePlot()->Update();
}

void MainForm::mult2dPressed()
{
	const double coeff=coeff2DSpinBox->value();
	scale2D(coeff);
	statusBar()->showMessage(QString("Intensity of the dataset has been multiplied by %1").arg(coeff),2000);
}

void MainForm::div2dPressed()
{
	const double coeff=1.0/coeff2DSpinBox->value();
	scale2D(coeff);
	statusBar()->showMessage(QString("Intensity of the dataset has been divided by %1").arg(1.0/coeff),2000);
}


void MainForm::update_info (const int k)
{
	if (k>=data->size()) return;
	infoTextEdit->clear();
	QStringList l=infoString(k).split("\n");
	foreach (QString s, l)
		infoTextEdit->append(s);
}

QString MainForm::infoString(const int k)
{
	QString out;
	const gsimFD* info=data->get_info(k);
	const size_t np=data->get_odata(k)->cols();
	const size_t ni=data->get_odata(k)->rows();
	if (data->size()==0) return QString("");
	out.append(QString("<u>File</u>: <i><font color=blue>")+info->filename+QString("</i></font><br>"));
	if (!info->title.isEmpty())
		out.append(QString("<u>Title</u>: <i><font color=blue>")+info->title+QString("</font></i><br>"));
	if (!info->pulprog.isEmpty())
		out.append(QString("<u>Pulse program</u>: <i><font color=blue>")+info->pulprog+QString("</font></i><br>"));
	QString s;
	if (info->type==FD_TYPE_FID) s="<b>FID </b>";
	else s="<b>spectrum </b>";
	if(ni>1) {
		if (info->type1==FD_TYPE_FID) s+="<b>x FID</b>";
		else s+="<b>x spectrum</b>";
	}
	s+=(data->get_display(k)->isRealOn)? QString(" (real)"):QString(" (imag)");
	out.append(QString("Data type: ")+s+"<br>");
	if(ni<2)
		out.append(QString("Number of points: <b>%1</b><br>").arg(np));
	else out.append(QString("Number of points: <b>%1 x %2</b><br>").arg(np).arg(ni));
	out.append(QString("Spectrum width: <b>%1 Hz</b><br>").arg(info->sw));
	if(ni>1) out.append(QString("Spectrum width (indirect): <b>%1 Hz</b><br>").arg(info->sw1));
	if (!info->processing.isEmpty()){
		out.append(QString("<b>Applied processing:</b><br>"));
		out.append(info->processing);
	}
	return out;
}


/* Add integral */
void MainForm::add_integral(){
      if (data->get_odata(data->getActiveCurve())->rows()>1) {
	add_integral_2D();
	return;
      }
      if (plot2D->is_leftbar && plot2D->is_rightbar) {
	data->makeBackup();
//        const size_t k=data->getActiveCurve();
	foreach (int k, data->selectedCurves) {
	if ((data->get_display(k)->xshift!=0) || (data->get_display(k)->vshift!=0) || (data->get_display(k)->xscale!=1) || (data->get_display(k)->vscale!=1)) {
		QMessageBox::critical(this,"Add integral","This function cannot be applied on dataset\nwhich has dynamic scaling/shift.\nPlease fix shift/scaling activating 'Edit->Fix shifts/scalings' or pressing 'F' key");
		return;
	}
        List<double> new_integralx;
        List<double> new_integraly;
	QVector<QString> message;
        double integ=0.0;

        size_t const full_len=data->size(k);

//FIXME Could it be substituted by plot2D->fromreal_toindex?
	size_t starti=full_len, endi=0;
        if (plot2D->leftbarpos.x() > plot2D->rightbarpos.x())
            for (size_t i=0; i<full_len; i++)
              if ((data->get_x(k,i) < plot2D->leftbarpos.x())&&(data->get_x(k,i) > plot2D->rightbarpos.x())) {
		new_integralx.push_back(data->get_x(k,i));
                integ+=data->get_value(k,0,i)+data->get_integrals(k).integral_lvl+data->get_integrals(k).integral_tlt*i/data->size(k);
                new_integraly.push_back(integ);
		if (i<starti) starti=i;
		if (i>endi) endi=i;
                }

        if (plot2D->leftbarpos.x() < plot2D->rightbarpos.x())
            for (size_t i=0; i<full_len; i++)
              if ((data->get_x(k,i) > plot2D->leftbarpos.x())&&(data->get_x(k,i) < plot2D-> rightbarpos.x())) {
		new_integralx.push_back(data->get_x(k,i));
                integ+=data->get_value(k,0,i)+data->get_integrals(k).integral_lvl+data->get_integrals(k).integral_tlt*i/data->size(k);
                new_integraly.push_back(integ);
		if (i<starti) starti=i;
		if (i>endi) endi=i;
                }
//save integral into "IntegList"
	QList<Array_> arrays=data->get_arrays(k);
	int q=-1;
	for (size_t i=0; i<arrays.size(); i++)
		if (arrays.at(i).name==QString("IntegList"))
			q=i;
	if (q<0) { //nothing found
		Array_ newarr("IntegList");
		arrays.push_back(newarr);
		q=arrays.size()-1;
		data->add_integral_scale(data->getActiveCurve(),1.0/integ); //first integral assigned to 1 on screen
	}     //now q contains the index of the IntegList
	arrays[q].data.push_back(integ);
	data->set_arrays(k,arrays);
	message=smoment(starti, endi, k);
        integralsInfoText->clear();
	const size_t slen=message.size();
	for(size_t i=0; i<slen; i++)
		integralsInfoText->append(message.at(i));
        data->add_integral(k, new_integralx, new_integraly);
//        data->add_integral_scale(k,0.1);
	}
         plot2D->Update();  
      }
      else {
	QMessageBox::information(this,"Cannot set integral", "This function calculates integral between markers positions.\nPlease, set markers first");
	}
}

void MainForm::add_integral_2D() {
	if ((!plot3D->is_leftbar) || (!plot3D->is_rightbar)) {
		QMessageBox::critical(this,"Add integral","Set markers at the corners of the integration area");
		return;
	}
	foreach (int k, data->selectedCurves) {
		if ((data->get_display(k)->xshift!=0) || (data->get_display(k)->vshift!=0) || (data->get_display(k)->xscale!=1) || (data->get_display(k)->vscale!=1)) {
			QMessageBox::critical(this,"Add integral","This function cannot be applied on dataset\nwhich has dynamic scaling/shift.\nPlease fix shift/scaling activating 'Edit->Fix shifts/scalings' or pressing 'F' key");
			return;
		}
		QVector<QString> message;
		size_t startx,endx,starty,endy;
		List<double> new_integralx;
		List<double> new_integraly;
        	double integ=0.0;
		plot3D->fromreal_toindex(k,plot3D->leftbarpos,startx,starty,false);
		plot3D->fromreal_toindex(k,plot3D->rightbarpos,endx,endy,false);
		if (startx>endx)
			std::swap(startx,endx);
		if (starty>endy)
			std::swap(starty,endy);
		Array_ arRR(QString("IntegRR%1").arg(data->integrals_size(k)));//integral, taken by row
		for (size_t i=starty; i<=endy; i++) {
			double integinrow=0.0;//value of the integral taken in row
			for (size_t j=startx; j<=endx; j++) {
				integ+=data->get_value(k,i,j);
				integinrow+=data->get_value(k,i,j);
			}
			arRR.data.push_back(integinrow);
		}

//save integrals to the array list
		QList<Array_> arrays=data->get_arrays(k);
		int q=-1;
		for (size_t i=0; i<arrays.size(); i++)
			if (arrays.at(i).name==QString("IntegList"))
			q=i;
		if (q<0) { //nothing found
			Array_ newarr("IntegList");
			arrays.push_back(newarr);
			q=arrays.size()-1;
			data->add_integral_scale(data->getActiveCurve(),1.0/integ); //first integral assigned to 1 on screen
		}     //now q contains the index of the IntegList
		arrays[q].data.push_back(integ);
		arrays.push_back(arRR);
		data->set_arrays(k,arrays);
		message.push_back(QString("<b>Total intensity:</b> %1").arg(integ));
        	integralsInfoText->clear();
		const size_t slen=message.size();
		for(size_t i=0; i<slen; i++)
			integralsInfoText->append(message.at(i));
		new_integralx.push_back(plot3D->leftbarpos.x());
		new_integralx.push_back(plot3D->rightbarpos.x());
		new_integraly.push_back(plot3D->leftbarpos.y());
		new_integraly.push_back(plot3D->rightbarpos.y());
		data->add_integral(k, new_integralx, new_integraly);
	}
	plot3D->update();
}

int MainForm::findIntegral(size_t k,double x)
{
	int ans=-1;
	size_t N=data->integrals_size(k);
	for (size_t i=0; i<N; i++) {
		double left=data->get_integral_x(k,i,0);
		double right=data->get_integral_x(k,i,data->integrals_size(k,i)-1);
		if (left>right)
			std::swap(left,right);
		if (x>left && x<=right) {
			ans=i;
			break;
		}
	}
	return ans;
}

int MainForm::findIntegral2D(size_t k,QPointF p)
{
	const size_t ninteg=data->integrals_size(k);
	int which=-1;
	for (size_t i=0; i<ninteg; i++){
		double rw=data->get_integral_x(k,i,1)-data->get_integral_x(k,i,0);
		double rh=data->get_integral_value(k,i,1)-data->get_integral_value(k,i,0);
		QRectF rect=QRectF(data->get_integral_x(k,i,0),data->get_integral_value(k,i,0),rw,rh).normalized();
		if (rect.contains(p))
			which=i;
	}
	return which;
}

void MainForm::on_calibrateButton_clicked()
{
	size_t k=data->getActiveCurve();
	if (data->get_odata(k)->rows()>1) {
		calibrate_integrals_2D();
		return;
	}
	if (!data->integrals_size(k)) {
		QMessageBox::critical(this, "Error", "No integrals found");
		return;
	}
	if (!plot2D->is_leftbar) {
		QMessageBox::information(this, "Marker not found", "This function uses the position\nof the main marker in order\nto find the relevant integral.\nPlease, set main marker within integral\nyou want to recalibrate");
		return;
	}
	double pos=plot2D->leftbarpos.x();
	int which=findIntegral(k, pos);
	if (which<0) {
		QMessageBox::critical(this, "Error", "Cannot determine integral for this marker position");
		return;
	}
	Array_ intar("empty");
	for (size_t i=0; i<data->get_arrays(k).size(); i++)
	if (data->get_arrays(data->getActiveCurve()).at(i).name==QString("IntegList"))
		intar=data->get_arrays(k)[i];
	double oldval=intar.data(which)*data->get_integral_scale(k);
	bool ok;
	double newval=QInputDialog::getDouble(this, "Calibrate integrals", "Type new value", oldval, 0.0, 9e36, 3, &ok);
	if (!ok){
		return;
	}
	data->add_integral_scale(k,newval/oldval*data->get_integral_scale(k));
	plot2D->Update();
}

void MainForm::calibrate_integrals_2D()
{
	const size_t k=data->getActiveCurve();
	const size_t ninteg=data->integrals_size(k);
	if (!ninteg) {
		QMessageBox::critical(this, "Error", "No integrals found");
		return;
	}
	if (!plot3D->is_leftbar) {
		QMessageBox::information(this, "Marker not found", "This function uses the position\nof the main marker in order\nto find the relevant integral.\nPlease, set main marker within integral\nyou want to recalibrate");
		return;
	}
	int which=findIntegral2D(k,plot3D->leftbarpos);
/*	for (size_t i=0; i<ninteg; i++){
		double rw=data->get_integral_x(k,i,1)-data->get_integral_x(k,i,0);
		double rh=data->get_integral_value(k,i,1)-data->get_integral_value(k,i,0);
		QRectF rect=QRectF(data->get_integral_x(k,i,0),data->get_integral_value(k,i,0),rw,rh).normalized();
		if (rect.contains(plot3D->leftbarpos))
			which=i;
	}
*/
	if (which<0) {
		QMessageBox::critical(this, "Error", "Cannot determine integral for this marker position");
		return;
	}
	Array_ intar("empty");
	for (size_t i=0; i<data->get_arrays(k).size(); i++)
	if (data->get_arrays(data->getActiveCurve()).at(i).name==QString("IntegList"))
		intar=data->get_arrays(k)[i];
	double oldval=intar.data(which)*data->get_integral_scale(k);
	bool ok;
	double newval=QInputDialog::getDouble(this, "Calibrate integrals", "Type new value", oldval, 0.0, 9e36, 3, &ok);
	if (!ok){
		return;
	}
	data->add_integral_scale(k,newval/oldval*data->get_integral_scale(k));
	plot3D->Update();
}

void MainForm::on_integralScaleButton_clicked()
{
	const size_t k=data->getActiveCurve();
	if (data->get_odata(k)->rows()>1) {
		QMessageBox::critical(this, "Error", "This function work only in 1D");
		return;
	}
	if (!data->integrals_size(k)) {
		QMessageBox::critical(this, "Error", "No integrals found");
		return;
	}

	promptDialog dial(this);
	QPushButton* upIntegScaleButton=new QPushButton(&dial);
	upIntegScaleButton->setText("Increase");
	upIntegScaleButton->setAutoRepeat(true);
	QPushButton* downIntegScaleButton=new QPushButton(&dial);
	downIntegScaleButton->setText("Decrease");
	downIntegScaleButton->setAutoRepeat(true);
	QHBoxLayout* lay1 = new QHBoxLayout;
	lay1->addWidget(upIntegScaleButton);
	lay1->addWidget(downIntegScaleButton);

	QPushButton* upIntegShiftButton=new QPushButton(&dial);
	upIntegShiftButton->setText("Increase");
	upIntegShiftButton->setAutoRepeat(true);
	QPushButton* downIntegShiftButton=new QPushButton(&dial);
	downIntegShiftButton->setText("Decrease");
	downIntegShiftButton->setAutoRepeat(true);
	QHBoxLayout* lay2 = new QHBoxLayout;
	lay2->addWidget(upIntegShiftButton);
	lay2->addWidget(downIntegShiftButton);

	QLabel* ll1=new QLabel;
	ll1->setText("Stretch/Expand:");
	QLabel* ll2=new QLabel;
	ll2->setText("Move up/down:");

        Integrals_ integ=data->get_integrals(k);

        QHBoxLayout* lay3 = new QHBoxLayout;
        QLabel* ll3=new QLabel;
        ll3->setText("Level");
        QDoubleSpinBox* lvlline = new QDoubleSpinBox;
        lvlline->setMaximum(std::numeric_limits<double>::max());
        lvlline->setMinimum(-1.0*std::numeric_limits<double>::max());
        lvlline->setSingleStep(plot2D->ywinmax/20000.0);
        lvlline->setValue(integ.integral_lvl);
        lay3->addWidget(ll3);
        lay3->addWidget(lvlline);

        QHBoxLayout* lay4 = new QHBoxLayout;
        QLabel* ll4=new QLabel;
        ll4->setText("Tilt");
        QDoubleSpinBox* tltline = new QDoubleSpinBox;
        tltline->setMaximum(std::numeric_limits<double>::max());
        tltline->setMinimum(-1.0*std::numeric_limits<double>::max());
        qDebug()<<"Winmax"<<plot2D->ywinmax;
        tltline->setSingleStep(plot2D->ywinmax/20000.0);
        tltline->setValue(integ.integral_tlt);
        lay4->addWidget(ll4);
        lay4->addWidget(tltline);
	
	dial.putWidget(ll1);
	dial.putLayout(lay1);
	dial.putWidget(ll2);
	dial.putLayout(lay2);
        dial.putLayout(lay3);
        dial.putLayout(lay4);

	connect(upIntegScaleButton,SIGNAL(clicked()),this,SLOT(upIntegScaleButtonClicked()));
	connect(downIntegScaleButton,SIGNAL(clicked()),this,SLOT(downIntegScaleButtonClicked()));
	connect(upIntegShiftButton,SIGNAL(clicked()),this,SLOT(upIntegShiftButtonClicked()));
	connect(downIntegShiftButton,SIGNAL(clicked()),this,SLOT(downIntegShiftButtonClicked()));
        connect(lvlline,SIGNAL(valueChanged(double)),this,SLOT(lvlChanged(double)));
        connect(tltline,SIGNAL(valueChanged(double)),this,SLOT(tltChanged(double)));
	dial.exec();
	disconnect(upIntegScaleButton,SIGNAL(clicked()),this,SLOT(upIntegScaleButtonClicked()));
	disconnect(downIntegScaleButton,SIGNAL(clicked()),this,SLOT(downIntegScaleButtonClicked()));
	disconnect(upIntegShiftButton,SIGNAL(clicked()),this,SLOT(upIntegShiftButtonClicked()));
	disconnect(downIntegShiftButton,SIGNAL(clicked()),this,SLOT(downIntegShiftButtonClicked()));
        disconnect(lvlline,SIGNAL(valueChanged(double)),this,SLOT(lvlChanged(double)));
        disconnect(tltline,SIGNAL(valueChanged(double)),this,SLOT(tltChanged(double)));
}

void MainForm::upIntegScaleButtonClicked()
{
	const size_t k=data->getActiveCurve();
	Integrals_ integ=data->get_integrals(k);
	integ.integral_vscale*=1.1;
	data->set_integrals(k,integ);
	plot2D->update();
}

void MainForm::downIntegScaleButtonClicked()
{
	const size_t k=data->getActiveCurve();
	Integrals_ integ=data->get_integrals(k);
	integ.integral_vscale/=1.1;
	data->set_integrals(k,integ);
	plot2D->update();
}

void MainForm::upIntegShiftButtonClicked()
{
	const size_t k=data->getActiveCurve();
	double range=max(real(*data->get_odata(k)))-min(real(*data->get_odata(k)));
	Integrals_ integ=data->get_integrals(k);
	integ.integral_vshift+=0.01*range;
	data->set_integrals(k,integ);
	plot2D->update();
}

void MainForm::downIntegShiftButtonClicked()
{
	const size_t k=data->getActiveCurve();
	double range=max(real(*data->get_odata(k)))-min(real(*data->get_odata(k)));
	Integrals_ integ=data->get_integrals(k);
	integ.integral_vshift-=0.01*range;
	data->set_integrals(k,integ);
	plot2D->update();
}



//	Integrals_ integ=data->get_integrals(k);
//	integ.integral_vshift=QInputDialog::getDouble(this,"Enter scale","Value:",integ.integral_vshift,-1e38,1e38,5);
//	data->set_integrals(k,integ);
//	plot2D->update();
//}

void MainForm::lvlChanged(double lvl)
{
    const size_t k=data->getActiveCurve();
    Integrals_ integ=data->get_integrals(k);
    integ.integral_lvl=lvl;
    data->set_integrals(k,integ);
    lvltltChanged();
}

void MainForm::tltChanged(double tlt)
{
    const size_t k=data->getActiveCurve();
    Integrals_ integ=data->get_integrals(k);
    integ.integral_tlt=tlt;
    data->set_integrals(k,integ);
    lvltltChanged();
}

void MainForm::lvltltChanged()
{
    qDebug()<<"Tlt/Lvl changed";
    const size_t k=data->getActiveCurve();
    Integrals_ integ=data->get_integrals(k);
    qDebug()<<"tlt:"<<integ.integral_tlt<<"lvl:"<<integ.integral_lvl;
    //find array and save it index as q
    QList<Array_> arrays=data->get_arrays(k);
    int q=-1;
    for (size_t i=0; i<arrays.size(); i++)
       if (arrays.at(i).name==QString("IntegList"))
            q=i;
    if (q<=-1){
        QMessageBox::critical(this,"Abort","Integral Array not found");
        return;
    }
    //check the length of the array
    if(arrays.at(q).data.size()!=integ.xintegrals.size()){
        QMessageBox::critical(this,"Abort","Mismatch between size of the array and integral curves");
        return;
    }
    //Change integral curves
    for (size_t i=0; i<integ.xintegrals.size(); i++){
        double valy=0.0;
        for (size_t j=0; j<integ.xintegrals.at(i).size(); j++){
        size_t pt=0;
        plot2D->fromreal_toindex(k,QPointF(integ.xintegrals.at(i)(j),0.0),pt,false);
        valy+=data->get_value(k,0,pt)+integ.integral_lvl+integ.integral_tlt*pt/data->size(k);
        integ.yintegrals[i](j)=valy;
      }
      arrays[q].data(i)=valy;
    }
    data->set_integrals(k,integ);
    data->set_arrays(k,arrays);
    plot2D->update();
}


void MainForm::on_ppButton_clicked()
{
//	size_t k=data->getActiveCurve();
	foreach (int k, data->selectedCurves) {
	if (data->get_odata(k)->rows()>1) {
		QMessageBox::critical(this,"Peak picking", "This function is valid for 1D data only");
		return;
	}
	if (!plot2D->is_leftbar) {
		QMessageBox::critical(this,"Peak picking","Please, set main marker above the noise level");
		return;
	}
	if ((data->get_display(k)->xshift!=0) || (data->get_display(k)->vshift!=0) || (data->get_display(k)->xscale!=1) || (data->get_display(k)->vscale!=1)) {
		QMessageBox::critical(this,"Peak picking","This function cannot be applied on dataset\nwhich has dynamic scaling/shift.\nPlease fix shift/scaling activating 'Edit->Fix shifts/scalings' or pressing 'F' key");
		return;
	}
	Array_ peaks("PeaksPos");
	int di=ppSpinBox->value();
	size_t np=data->get_odata(k)->cols();
	double th=plot2D->leftbarpos.y();
	double prevpt=-9e36, prevprevpt=-9e36;
	for (int i=0; i<np; i++) {
		complex pt=data->average_pt(k,i,di);
		if (real(pt)<th) {
			prevpt=-9e36;
			prevprevpt=-9e36;
			continue;
		}
		if ((real(pt)<prevpt)&&(prevpt>prevprevpt)) {
			peaks.data.push_back(data->get_x(k,i-1));
		}
			prevprevpt=prevpt;
			prevpt=real(pt); //going up
	}
	QList<Array_> arrays=data->get_arrays(k);
	for (size_t i=arrays.size(); i--;)
		if (arrays.at(i).name==QString("PeaksPos")) {
			arrays.removeAt(i);
			break;
		}
	Display_ disp=*data->get_display(k);	
	if (peaks.data.size()) {
		arrays.push_back(peaks);
		disp.hasPeaks=true;
	}
	else
		disp.hasPeaks=false;
	data->set_arrays(k,arrays);
	data->set_display(k,disp);
	plot2D->Update();
	}
}

void MainForm::on_manppButton_clicked()
{
//	size_t k=data->getActiveCurve();
	foreach(int k, data->selectedCurves){
	if (data->get_odata(k)->rows()>1) {
		QMessageBox::critical(this,"Manual peak picking", "This function is valid for 1D data only");
		return;
	}
	if (!plot2D->is_leftbar) {
		QMessageBox::critical(this,"Manual peak picking","Please, set main marker at the peak maximum");
		return;
	}
	if ((data->get_display(k)->xshift!=0) || (data->get_display(k)->vshift!=0) || (data->get_display(k)->xscale!=1) || (data->get_display(k)->vscale!=1)) {
		QMessageBox::critical(this,"Manual peak picking","This function cannot be applied on dataset\nwhich has dynamic scaling/shift.\nPlease fix shift/scaling activating 'Edit->Fix shifts/scalings' or pressing 'F' key");
		return;
	}
//find presaved arrays
	QList<Array_> arrays=data->get_arrays(k);
	int q=-1;
	for (size_t i=0; i<arrays.size(); i++)
		if (arrays.at(i).name=="PeaksPos")
			q=i;
//create new peak list
	Array_ peaks("PeaksPos");
	QList<double> temp;
//add old values if exist
	if (q>=0) {
		for (size_t j=0;j<arrays.at(q).data.size();j++)
			temp.push_back(arrays.at(q).data(j));
//		peaks.data=arrays.at(q).data;
		arrays.removeAt(q);
	}
//add new value
	temp.push_back(plot2D->leftbarpos.x());
	qSort(temp.begin(),temp.end(),qGreater<double>());
	for (size_t j=0;j<temp.size();j++)
		peaks.data.push_back(temp.at(j));

	Display_ disp=*data->get_display(k);	
	arrays.push_back(peaks);
	disp.hasPeaks=true;
	data->set_arrays(k,arrays);
	data->set_display(k,disp);
	plot2D->update();
}
}


void MainForm::on_snButton_clicked()
{
    foreach(int k, data->selectedCurves){
    if (data->get_odata(k)->rows()>1) {
        QMessageBox::critical(this,"S/N calculation", "This function is valid for 1D data only");
        return;
    }

    size_t np=data->get_odata(k)->cols();
//Use 10% intervals and find peak and best noise region
    double inten=-9e36, noise=9e36;
    size_t bestNsStart=0, bestNsEnd=0, maxPk=0, maxPkStart=0, maxPkEnd=0;
    size_t rg=np*0.1;

    QProgressDialog progress("Finding least noisy 10% region","Abort",0,np-rg-1,this);
    for (size_t i=0; i<np-rg-1; i++){
        progress.setValue(i);
        if (progress.wasCanceled())
            break;
        double estNoise=calcnoise(k,i,i+rg);
        if (estNoise<noise){
            noise=estNoise;
            bestNsStart=i;
            bestNsEnd=i+rg;
        }
    }
    progress.setValue(np-rg-1);
    for (size_t i=0; i<np; i++){
        if (inten<data->get_value(k,0,i)){
            inten=data->get_value(k,0,i);
            maxPk=i;
        }
    }
    maxPkStart=((maxPk-rg/2.0)>0.0)? maxPk-rg/2.0 : 0;
    maxPkEnd=((maxPk+rg/2.0)<np)? maxPk+rg/2.0 : np-1;


    promptDialog* dial= new promptDialog(this);

    QDoubleSpinBox* noiseStartBox = new QDoubleSpinBox(this);
    noiseStartBox->setDecimals(2);
    noiseStartBox->setSuffix(QString(" ")+plot2D->xaxis_label);
    noiseStartBox->setMinimum(data->get_x(k,np-1));
    noiseStartBox->setMaximum(data->get_x(k,0));
    double val=data->get_x(k,bestNsStart);
    noiseStartBox->setValue(val);

    QDoubleSpinBox* noiseEndBox = new QDoubleSpinBox(this);
    noiseEndBox->setDecimals(2);
    noiseEndBox->setSuffix(QString(" ")+plot2D->xaxis_label);
    noiseEndBox->setMinimum(data->get_x(k,np-1));
    noiseEndBox->setMaximum(data->get_x(k,0));
    val=data->get_x(k,0);
    noiseEndBox->setValue(val);

    QDoubleSpinBox* peakStartBox = new QDoubleSpinBox(this);
    peakStartBox->setDecimals(2);
    peakStartBox->setSuffix(QString(" ")+plot2D->xaxis_label);
    peakStartBox->setMinimum(data->get_x(k,np-1));
    peakStartBox->setMaximum(data->get_x(k,0));
    peakStartBox->setValue(data->get_x(k,maxPkStart));

    QDoubleSpinBox* peakEndBox = new QDoubleSpinBox(this);
    peakEndBox->setDecimals(2);
    peakEndBox->setSuffix(QString(" ")+plot2D->xaxis_label);
    peakEndBox->setMinimum(data->get_x(k,np-1));
    peakEndBox->setMaximum(data->get_x(k,0));
    peakEndBox->setValue(data->get_x(k,maxPkEnd));

    QHBoxLayout* l1= new QHBoxLayout;
    l1->addWidget(new QLabel("Noise region from"));
    l1->addWidget(noiseStartBox);
    l1->addWidget(new QLabel("to"));
    l1->addWidget(noiseEndBox);
    dial->putLayout(l1);

    QHBoxLayout* l2= new QHBoxLayout;
    l2->addWidget(new QLabel("Peak region from"));
    l2->addWidget(peakStartBox);
    l2->addWidget(new QLabel("to"));
    l2->addWidget(peakEndBox);
    dial->putLayout(l2);

    if (!dial->exec())
        break;

    size_t nsStart, nsEnd, pkStart, pkEnd;
    plot2D->fromreal_toindex(k,QPointF(noiseStartBox->value(),0.0),nsStart,false);
    plot2D->fromreal_toindex(k,QPointF(noiseEndBox->value(),0.0),nsEnd,false);
    plot2D->fromreal_toindex(k,QPointF(peakStartBox->value(),0.0),pkStart,false);
    plot2D->fromreal_toindex(k,QPointF(peakEndBox->value(),0.0),pkEnd,false);

    if(nsStart>nsEnd)
        std::swap(nsStart,nsEnd);
    if(pkStart>pkEnd)
        std::swap(pkStart,pkEnd);

//Calculate Noise

    noise = calcnoise(k,nsStart,nsEnd);

    inten=0.0;
    size_t pkPos=0;
//find Peak
    for (size_t i=pkStart; i<=pkEnd; i++)
        if (inten < data->get_value(k,0,i)){
            inten=data->get_value(k,0,i);
            pkPos=i;
        }
//Signal-to-noise output
    double sn=inten/(2.0*noise);
    QList<graph_object> graphics=data->get_graphics(k);
    graph_object g;
    g.type=GRAPHTEXT;
    g.maindata=QString("S/N=%1").arg(sn);
    g.pos=QPointF(data->get_x(k,pkPos),inten*1.01);
    graphics.push_back(g);
    data->set_graphics(k,graphics);
    plot2D->update();
    delete dial;
    }
}


double MainForm::calcnoise(size_t k,size_t start,size_t end)
{
    if ((end-start)%2) //should be odd number of points for symmetry in XY
        end--;
    double Y=0.0, Y2=0.0, XY=0.0;
    size_t mediumPt=start+(end-start)/2;
    double nn=(double) end-start+1;
    for (size_t i=start; i<=end; i++){
        Y+=data->get_value(k,0,i);
        Y2+=data->get_value(k,0,i)*data->get_value(k,0,i);
    }
    for (size_t i=0; i<mediumPt-start; i++)
        XY+=(data->get_value(k,0,mediumPt+i) - data->get_value(k,0,mediumPt-i))*i;

    double noise = sqrt((Y2 - (Y*Y + 3*XY*XY/(nn*nn - 1))/nn)/(nn - 1));
    //qDebug()<<"from"<<start<<"to"<<end<<"noise"<<noise;
    return noise;
}


/*
double MainForm::calcnoise(size_t k,size_t start,size_t end)
{
    double min=9e36;
    double max=-9e36;
    for (size_t i=start; i<=end; i++){
        double val=data->get_value(k,0,i);
        if (min>val) min=val;
        if (max<val) max=val;
    }
    double noise=abs(max-min);
    return noise;
}
*/

void MainForm::on_adjustScalesButton_clicked()
{
	List<double> maxs;
	size_t N=data->size();
	for (size_t k=0; k<N; k++) {
		maxs.push_back(max(real(*data->get_odata(k))));
	}
	double global_max=max(maxs);
	for (size_t k=0; k<N; k++) {
		data->set_odata(k,*data->get_odata(k)*global_max/maxs(k));
	}
	plot2D->cold_restart(data);
	plot3D->cold_restart(data);
}

void MainForm::on_sidebandsButton_clicked()
{
	foreach(int k, data->selectedCurves) {
	if (data->get_odata(k)->rows()>1) {
		QMessageBox::critical(this,"Sidebands picking", "This function is valid for 1D data only");
		return;
	}
	if (!plot2D->is_leftbar) {
		QMessageBox::critical(this,"Sidebands picking","Please, set main marker at the centreband");
		return;
	}

	double srHz=data->get_info(k)->spinrate;
//	qDebug()<<"SpinRate"<<srHz;

	promptDialog* dial= new promptDialog(this);
	QLineEdit* lined = new QLineEdit(dial);
	lined->setText("*");//default label
	QDoubleSpinBox* srBox = new QDoubleSpinBox(this);
	srBox->setDecimals(2);
	srBox->setMinimum(0.00);
	srBox->setMaximum(9999999999.00);
	srBox->setSuffix(" Hz");
	srBox->setValue(srHz);
	QDoubleSpinBox* errorBox=new QDoubleSpinBox(this);
	errorBox->setDecimals(2);
	errorBox->setMinimum(0.00);
	errorBox->setMaximum(9999999999.00);
	errorBox->setSuffix(" Hz");
	errorBox->setValue(srHz*0.02);//error is 2% of the spinning rate
	
	QCheckBox* createEnvelopeBox = new QCheckBox(dial);
	createEnvelopeBox ->setText("Create envelope?");
	dial->putLayout(hBlock(dial,  QString("Spinning rate"),srBox));
	dial->putLayout(hBlock(dial,  QString("Possible SR variation (+/-) "),errorBox));
	dial->putLayout(hBlock(dial,  QString("Input a label for sidebands"),lined));
	dial->putWidget(createEnvelopeBox);
	dial->exec();
	QString label=lined->text();
	srHz=srBox->value();
	double errorSR=errorBox->value(); //contains error in spinning rate measurements
	if (srHz<0.1){
		QMessageBox::critical(this,"Sidebands picking","Spinning rate is 0 Hz\nSet spinning rate in physical range");
		return;
	}
	bool createEnvelope=createEnvelopeBox->checkState()==Qt::Checked;
	delete dial;

	QList<int> envelopeIndexes;

	double srl=srHz; //spinning rate in local coordinates
	if (data->global_options.xfrequnits==UNITS_KHZ)
		srl/=1000.0;
	else if (data->global_options.xfrequnits==UNITS_PPM)
		srl/=data->get_info(k)->sfrq;
	double start=data->get_x(k,0);
	double end=data->get_x(k,data->get_odata(k)->cols()-1);
	if (start>end)
		std::swap(start,end);
	QList<graph_object> graphics=data->get_graphics(k);
	double treshold=(plot2D->is_rightbar)? plot2D->rightbarpos.y() : -9e-38;
	double curpos=plot2D->leftbarpos.x();
	size_t index;
	if (createEnvelope) {
		plot2D->fromreal_toindex(k,QPointF(curpos,0.0),index,true);
		envelopeIndexes.push_back(index);
	}
	curpos-=srl;
	int order=-1;//sideband's order
	while (curpos>start) {
		size_t curindex;
		int ptAround=int(fabs(order*errorSR/data->get_info(k)->sw*data->get_odata(k)->cols()));
		plot2D->fromreal_toindex(k,QPointF(curpos,0.0),curindex,true);
		int starti=(int(curindex)-ptAround)<0? 0: curindex-ptAround;
		int endi=(int(curindex)+ptAround)>=data->get_odata(k)->cols()? data->get_odata(k)->cols()-1:curindex+ptAround;
		index=curindex;
		for (size_t j=starti; j<=endi; j++) {
			if (data->get_value(k,0,j)>data->get_value(k,0,index))
				index=j;
		}
		if (data->get_value(k,0,index) < treshold)
			break;
		if (createEnvelope)
			envelopeIndexes.push_back(index);
		graph_object g;
		g.type=GRAPHTEXT;
		g.maindata=label;
		g.pos=QPointF(curpos,data->get_value(k,0,index));
		graphics.push_back(g);
		curpos-=srl;
		order--;
	}
	curpos=plot2D->leftbarpos.x()+srl;
	order=1;
	while (curpos<end) {
		size_t curindex;
		int ptAround=int(fabs(order*errorSR/data->get_info(k)->sw*data->get_odata(k)->cols()));
		plot2D->fromreal_toindex(k,QPointF(curpos,0.0),curindex,true);
		int starti=(int(curindex)-ptAround)<0? 0: curindex-ptAround;
		int endi=(int(curindex)+ptAround)>=data->get_odata(k)->cols()? data->get_odata(k)->cols()-1:curindex+ptAround;
		index=curindex;
		for (size_t j=starti; j<=endi; j++) {
			if (data->get_value(k,0,j)>data->get_value(k,0,index))
				index=j;
		}
		if (data->get_value(k,0,index) < treshold)
			break;
		if (createEnvelope)
			envelopeIndexes.push_back(index);
		graph_object g;
		g.type=GRAPHTEXT;
		g.maindata=label;
		g.pos=QPointF(curpos,data->get_value(k,0,index));
        graphics.push_back(g);
		curpos+=srl;
		order++;
	}
	data->set_graphics(k,graphics);
	if (createEnvelope) {
		data->mutex.lock();
		int centrebandIndex=envelopeIndexes.at(0);
		qSort(envelopeIndexes.begin(),envelopeIndexes.end());
		data_entry envel;
		envel.info.filename="envelope_"+label;
		envel.spectrum.create(1,envelopeIndexes.size());
		double x0=data->get_x(k,envelopeIndexes.at(0));
//		double xl=data->get_x(k,envelopeIndexes.last());
		envel.info.sw=srHz*envelopeIndexes.size(); //works only for Hz sacle here
		envel.info.spinrate=srHz;
		envel.info.ref=x0-envel.info.sw/2.0;
		envel.info.type=data->get_info(k)->type;
		envel.info.spinrate=data->get_info(k)->spinrate;
		for (size_t i=0; i<envelopeIndexes.size(); i++) {
			envel.spectrum(0,i)=data->get_odata(k,0,envelopeIndexes.at(i));
			if (centrebandIndex==envelopeIndexes.at(i)) {
				Array_ arr("Centreband");
				arr.data.push_back(i);
				envel.arrays.push_back(arr);
			}
		}
		data->add_entry(envel);
		data->updateAxisVectors();
		data->mutex.unlock();
//		updateFilesList();
	}
	}
	updateFilesList();
	plot2D->update();
}

void MainForm::deleteAllPp()
{
	size_t len=data->size();
	for (size_t k=0; k<len; k++) {
		QList<Array_> arrays=data->get_arrays(k);
			for (size_t i=arrays.size(); i--;)
				if (arrays.at(i).name==QString("PeaksPos"))
					arrays.removeAt(i);
		data->set_arrays(k,arrays);
		Display_ disp=*(data->get_display(k));
		disp.hasPeaks=false;
		data->set_display(k,disp);
	}
}

void MainForm::update2d(){
	plot3D->nlevels=levelsSpinBox->value();
	plot3D->pos_neg=posnegComboBox->currentIndex();
	plot3D->floor=floorSpinBox->value();
	plot3D->ceiling=ceilingSpinBox->value();
	plot3D->multiplier=multiplierSpinBox->value();

//experimental quality control
	plot3D->qstepx=qualitySpinBox->value();
	plot3D->qstepy=qualitySpinBox->value();
	plot3D->Update();
}

void MainForm::takeSlice()
{
	data->makeBackup();
	takeSlice(sliceComboBox->currentIndex());
}

void MainForm::takeSlice(const int n)
{
        if (!data->size()) return;
	size_t xi, yi;
//	size_t k=data->getActiveCurve();
	foreach (int k, data->selectedCurves) {
		switch (n) {
		case 0:
			if (plot3D->is_leftbar) {
			plot3D->fromreal_toindex(k,plot3D->leftbarpos,xi,yi);
			data->takeRow(k,yi);
			}
			else return;
			break;
		case 1:
			if (plot3D->is_leftbar) {
			plot3D->fromreal_toindex(k,plot3D->leftbarpos,xi,yi);
			data->takeCol(k,xi);
			}
			else return;
			break;
		case 2:
			data->takeHorProjection(k);
			break;
		case 3:
			data->takeVerProjection(k);
			break;
		case 4:
			data->takeHorPosSky(k);
			break;
		case 5:
			data->takeHorNegSky(k);
			break;
		case 6:
			data->takeVerPosSky(k);
			break;
		case 7:
			data->takeVerNegSky(k);
			break;
		case 8:
			data->takeHorTrain(k);
			break;
		case 9:
			data->takeStack(k);
			break;
		}
}
	updateFilesList();
	plot2D->cold_restart(data);
}

void MainForm::fileDelete(const int k)
{
	if (data==NULL) return;
	if (!data->size()) return;
	if (k>=int(data->size())) return;

	data->makeBackup();
	data->delete_curve(k);
	const size_t n=data->size();
	data->setActiveCurve(n-1);
	updateFilesList();
}

void MainForm::fileClose()
{
	if (!data->size()) return;
	QList<int> li=data->selectedCurves;
	const size_t N=li.size();
	for (size_t m=0; m<N; m++) {
		int k=li.at(m);
		if (watchTimer->isActive() && data->get_info(k)->filename==watchFile) {
				QMessageBox::critical(this,"File is under watch", "This file is under watch. Stop watching before closing it");
				return;
		}
		fileDelete(k);
		for (size_t i=m+1; i<N; i++)
			if (k<li.at(i))
				li[i]-=1;
	}
	updateFilesList();
        //plot3D->cold_restart(data);
        plot3D->Update();
        //plot2D->cold_restart(data);
        plot2D->update();
}

void MainForm::fileReload()
{
	if (!data->size()) return;
	QList<int> li=data->selectedCurves; //needs a copy because selected curves can be changed
	const size_t N=li.size();
	data->makeBackup();
	const int Kold=data->getActiveCurve(); //store active curve. It will be changed below and restored at the end
	for (int m=0; m<N; m++) { //for each selected curve
		int k=li.at(m);
		if (!data->get_info(k)->is_on_disk) {
			QMessageBox::warning(this, "Reload error", "File wasn't saved on disc", "Abort");
			continue;
		}
		QString fname=data->get_info(k)->filename;
		QString filter=data->get_info(k)->file_filter->name();
		data->setActiveCurve(k);
		try {
			fileOpen(fname, filter, 0);
			updateFilesList();
		}
		catch(MatrixException exc) {
			QMessageBox::critical(this,"Reading error", exc.what());
			updateFilesList();
		}
	}
	data->setActiveCurve(Kold);  //restore previously activated data
	plot3D->cold_restart(data);
	plot2D->cold_restart(data);
	foreach (size_t i, li) //restore selection as it was before
		filesListWidget->item(i)->setSelected(true);
	filesListWidget->setCurrentRow(Kold);
}	

void MainForm::fileWatch(bool notrunning)
{
	if (notrunning) {
		bool ok;
        int time_s=QInputDialog::getInt(this,"Enter time", "Check file every ... seconds",30,0,999999,1,&ok);
		if (!ok) {
			fileWatchAction->setChecked(false);
			return;
		}	
		watchTimer->start(time_s*1000);
		statusBar()->showMessage(QString("File will be checked every %1 s").arg(time_s));
		fileWatchAction->setText(tr("Stop watching"));
		watchFile=data->get_info(data->getActiveCurve())->filename;
		QFileInfo fi(watchFile);
		watchLastChange=fi.lastModified();
		QPalette pal=activePlot()->palette();  //should be simpler but I don't know how to do that (yet)
		pal.setColor(QPalette::Background, activePlot()->colourScheme.watchColour);
//		pal.setColor(QPalette::Background, Qt::yellow);
		activePlot()->setPalette(pal);
	}
	else {
		watchTimer->stop();
		fileWatchAction->setText(tr("Watch"));
		QPalette pal=activePlot()->palette();  //should be simpler but I don't know how to do that (yet)
		pal.setColor(QPalette::Background, activePlot()->colourScheme.backgroundColour);
//		pal.setColor(QPalette::Background, Qt::white);
		activePlot()->setPalette(pal);
	}
}

void MainForm::timerEvent()
{
	int k=-1;
	for (size_t i=0; i<data->size(); i++)
		if (data->get_info(i)->filename==watchFile)
			k=i;
	if (k<0) {
		QMessageBox::critical(this, "Watch file error", QString("File %1 suddenly disappeared").arg(watchFile));
		watchTimer->stop();
		fileWatchAction->setText(tr("Watch"));
		return;
		}
	QFileInfo fi(watchFile);
	if (fi.lastModified()!=watchLastChange) {
		watchLastChange=fi.lastModified();
		QString filter=data->get_info(k)->file_filter->name();
		fileDelete(k);
		fileOpen(watchFile, filter);
		plot3D->cold_restart(data);
		plot2D->cold_restart(data);
		statusBar()->showMessage(QString("Check at %1: File %2 has been modified at %2 and reloaded").arg( QDateTime::currentDateTime().toString("hh:mm:ss")).arg(watchFile).arg(watchLastChange.toString("hh:mm:ss")));
	}
	else {
		statusBar()->showMessage(QString("Check at %1: File %2 has not been modified").arg( QDateTime::currentDateTime().toString("hh:mm:ss")).arg(watchFile));
	}
}


void MainForm::fileExit()
{
	close();
}

void MainForm::filePrint()
{
    if (!data->size()) return;
    Plot2DWidget * plot=activePlot();
//    QPrinter * printer = new QPrinter(QPrinter::ScreenResolution);
//    printer->setOrientation(QPrinter::Landscape);
    QPrintDialog printDialog(printer, this);
    printDialog.setEnabledOptions(QAbstractPrintDialog::PrintToFile);
    if (printDialog.exec() == QDialog::Accepted) {
	    createPrintout(printer,plot);
    }
    
//    delete printer;
    statusBar()->showMessage("Dataset has been printed out",2000);
}

void MainForm::filePrintPreview()
{
    if (!data->size()) return;
//    QPrinter * printer = new QPrinter(QPrinter::ScreenResolution);
//    printer->setOrientation(QPrinter::Landscape);
    QPrintPreviewDialog printPreviewDialog(printer, this);
    connect(&printPreviewDialog,SIGNAL(paintRequested(QPrinter*)),this,SLOT(createPrintPreview(QPrinter*)));
    printPreviewDialog.exec();
//    delete printer;
    statusBar()->showMessage("Dataset has sent for preview",2000);
}

void MainForm::createPrintPreview(QPrinter* printer)
{
//	qDebug()<<"Preview requested";
	Plot2DWidget * plot=activePlot();
	createPrintout(printer,plot);
}


void MainForm::editCopy()
{
	QPixmap pixmap(QPixmap::grabWidget(activePlot()));
	QClipboard* clipboard =  QApplication::clipboard();
	clipboard->setPixmap(pixmap, QClipboard::Clipboard);
	statusBar()->showMessage("Screenshot has been put to the system clipboard",2000);
}

void MainForm::takeInset()
{
	Plot2DWidget* plot=activePlot();
	QFont oldfont=QApplication::font();
	QFont newfont(oldfont);
	size_t oldticks=data->global_options.ticks;
	data->global_options.ticks=3;
	newfont.setPointSizeF(oldfont.pointSizeF()*3);
	QApplication::setFont(newfont);
	plot->update();
	QPixmap pixmap(QPixmap::grabWidget(plot));
	QApplication::setFont(oldfont);
	data->global_options.ticks=oldticks;
	const size_t k=data->getActiveCurve();
	QList<graph_object> graphics=data->get_graphics(k);
	graph_object gr;
	gr.type=GRAPHIMAGE;
	gr.maindata=pixmap;
	gr.scale=0.2;
	plot->hot_restart();
	QPoint scpt(int(plot->width()*0.8)-5,5);
	double x,y;
	plot->fromscreen_toreal(scpt,x,y);
	gr.pos=QPointF(x,y);
	graphics.push_back(gr);
	data->set_graphics(k,graphics);
	plot->update();
	statusBar()->showMessage("Inset has been taken",2000);
}

/* //TODO experimental way of including vector graphics
void MainForm::takeInset()
{
	Plot2DWidget* plot=activePlot();
	QPicture pic;
	QPainter* paint=new QPainter(&pic);
	plot->draw_picture(paint);
	delete paint;
	QByteArray bdata(pic.data(),pic.size());
	const size_t k=data->getActiveCurve();
	QList<graph_object> graphics=data->get_graphics(k);
	graph_object gr;
	gr.type=GRAPHVPICTURE;
	gr.maindata=bdata;
	gr.scale=0.2;
	plot->hot_restart();
	QPoint scpt(int(plot->width()*0.8)-5,5);
	double x,y;
	plot->fromscreen_toreal(scpt,x,y);
	gr.pos=QPointF(x,y);
	graphics.push_back(gr);
	data->set_graphics(k,graphics);
	plot->update();
	statusBar()->showMessage("Inset has been taken",2000);
}
*/

void MainForm::pasteImage()
{
	QPixmap im=QPixmap::fromImage(QApplication::clipboard()->image());
	if (im.isNull())
		return;
	graph_object gr;
	gr.type=GRAPHIMAGE;
	gr.scale=0.25;
	gr.maindata=im;
	gr.pos=QPointF(activePlot()->xwinmin,activePlot()->ywinmax);
	QList<graph_object> graphics= data->get_graphics(data->getActiveCurve());
	graphics.push_back(gr);
	data->set_graphics(data->getActiveCurve(),graphics);
	activePlot()->update();
}

void MainForm::fileExportPdf()
{
    if (!data->size()) return;
    Plot2DWidget * plot=activePlot();

    QString s = QFileDialog::getSaveFileName(
                    this,
                    "Choose a filename to save under",
                    working_directory,
                    "PDF (*.pdf)");
     if (!s.endsWith(".pdf"))
	s+=QString(".pdf");

    QPrinter * printer = new QPrinter(QPrinter::ScreenResolution);
    printer->setPageSize(QPrinter::A4);
    printer->setOutputFormat(QPrinter::PdfFormat);
    printer->setOrientation(QPrinter::Landscape);
    printer->setOutputFileName(s);
    
    createPrintout(printer, plot);
    delete printer; 
    statusBar()->showMessage(QString("PDF has been saved in %1").arg(s),2000);
}


void MainForm::fileExportVector()
{
    QFileDialog file_dialog(
    			this,
			"Choose a file",
			 working_directory
                        );
    file_dialog.setFileMode(QFileDialog::AnyFile);
    QStringList filters;
    filters<<"Scalable Vector Graphics(*.svg)"<<"Encapsulated Postscript (*.eps)";
#ifdef USE_EMF_OUTPUT
    filters<<"Enhanced metafile (*.emf)";
    EMFDevice* emf=NULL;
#endif
    file_dialog.setNameFilters(filters);
    file_dialog.setLabelText(QFileDialog::Accept,"Save");

    QStringList s;
    if (file_dialog.exec())
    	s = file_dialog.selectedFiles();

   if (s.isEmpty()) return;
   QString filter=file_dialog.selectedNameFilter();
   QPainter paint;
   activePlot()->for_print= !(data->global_options.print1Dmode==PRINT1D_SKIP);
   PSDevice* ps=NULL;
   SVGDevice* svg=NULL;
   if (filter=="Scalable Vector Graphics(*.svg)") {
	if (!s.at(0).endsWith(".svg"))
		s[0]+=".svg";
	QDir dir(QDir::currentPath());
	svg= new SVGDevice(QSize(activePlot()->size()),dir.absoluteFilePath(s.at(0)));
	paint.begin(svg);
	}
   else if (filter=="Encapsulated Postscript (*.eps)") {
	if (!s.at(0).endsWith(".eps"))
		s[0]+=".eps";
	ps= new PSDevice(QSize(activePlot()->size()));
	paint.begin(ps);
	}
#ifdef USE_EMF_OUTPUT
   else {
	if (!s.at(0).endsWith(".emf"))
		s[0]+=".emf";
	emf=new EMFDevice(QSize(activePlot()->size()),s.at(0));
	paint.begin(emf);
   }
#endif
   paint.setClipping(true);
   paint.setClipRect(0,0,activePlot()->width(),activePlot()->height());
   activePlot()->draw_picture(&paint);
   paint.end();
   activePlot()->for_print=false;
if (filter!="Enhanced metafile (*.emf)") {

   QFile file(s.at(0));
        if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
            return;
   QTextStream out(&file);
   if (filter=="Scalable Vector Graphics(*.svg)")
        out << svg->result();
   else if (filter=="Encapsulated Postscript (*.eps)")
	out << ps->result();
   file.close();
   statusBar()->showMessage(QString("File %1 has been saved").arg(s.at(0)),2000);
}
   if (ps!=NULL)
	delete ps;
   if (svg!=NULL)
	delete svg;
#ifdef USE_EMF_OUTPUT
   if (emf!=NULL)
	delete emf;
#endif
}


/*
void MainForm::fileExportVector()
{
    QFileDialog file_dialog(
    			this,
			"Choose a file",
			 working_directory
                        );
    file_dialog.setFileMode(QFileDialog::AnyFile);
    QStringList filters;
    filters<<"Scalable Vector Graphics(*.svg)"<<"Encapsulated Postscript (*.eps)"; 
    file_dialog.setFilters(filters);
    file_dialog.setLabelText(QFileDialog::Accept,"Save");

    QStringList s;
    if (file_dialog.exec())
    	s = file_dialog.selectedFiles();

   if (s.isEmpty()) return;
   QString filter=file_dialog.selectedFilter();
   QPainter paint;
   activePlot()->for_print=true;
   PSDevice* ps=NULL;
//   SVGDevice* svg=NULL;
   QSvgGenerator* svg=NULL;
   if (filter=="Scalable Vector Graphics(*.svg)") {
	if (!s.at(0).endsWith(".svg"))
		s[0]+=".svg";
	QDir dir(QDir::currentPath());
//	svg= new SVGDevice(QSize(activePlot()->size()),dir.absoluteFilePath(s.at(0)));
	svg=new QSvgGenerator();
	svg->setFileName(s.at(0));
//	svg->setOutputDevice(&file);
	svg->setSize(QSize(activePlot()->size()));
	paint.begin(svg);
	}
   else {
	if (!s.at(0).endsWith(".eps"))
		s[0]+=".eps";
	ps= new PSDevice(QSize(activePlot()->size()));
	paint.begin(ps);
	}
   paint.setClipping(true);
   paint.setClipRect(0,0,activePlot()->width(),activePlot()->height());
   activePlot()->draw_picture(&paint);
   paint.end();
   activePlot()->for_print=false;
   if (filter=="Encapsulated Postscript (*.eps)") {
   	QFile file(s.at(0));
        if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
            return;
  	QTextStream out(&file);
	out << ps->result();
	file.close();
   }
   statusBar()->showMessage(QString("File %1 has been saved").arg(s.at(0)),2000);
   if (ps!=NULL)
	delete ps;
   if (svg!=NULL)
	delete svg;
}
*/

void MainForm::createPrintout(QPrinter* printer, Plot2DWidget* plot)
{
	ColourScheme oldScheme=plot->colourScheme;
	plot->setScheme(whiteScheme());
	QPainter * paint=new QPainter();
	QSize oldsize=plot->size();
	QFont oldfont=plot->font();
	QSize newsize=printer->pageRect().size();
	QFont newfont(oldfont);
	newfont.setPointSize(oldfont.pointSize()*newsize.height()/oldsize.height());
	plot->setFont(newfont);//scale font size

//bug in QT 4.2.1? paper size behaves crazy. I cannot set a proper paper size
//	newsize.rwidth()=newsize.rwidth()-13;
//	newsize.rheight()=newsize.rheight()-13;

	if (data->global_options.printPars)
		newsize.rwidth()=int(0.8*newsize.rwidth()); //take 80 % of the width
	plot->resize(newsize);
	paint->begin(printer);
	
	paint->setClipping(true);
	paint->setClipRect(0,0,plot->width(),plot->height());
        plot->for_print=!(data->global_options.print1Dmode==PRINT1D_SKIP);
	plot->draw_picture(paint);
	plot->for_print=false;
	paint->setClipping(false);
	bool is2D=(data->get_odata(data->getActiveCurve())->rows()>1);

	if (data->global_options.printPars) {
		QSvgRenderer pic(QString(":/images/printlogo.svg"), this);
		QImage bitmap;
		bool isSVG=true;//is logo SVG? If not than bitmap
	
		if (!data->global_options.customPrintLogo.isEmpty()) {
			if (!pic.load(data->global_options.customPrintLogo)) { //cannot load SVG, try bitmap
				if (bitmap.load(data->global_options.customPrintLogo))
					isSVG=false;
				else { //external load failed, load default SVG logo
					QMessageBox::warning(this,"Custom logo file problem","Custom print logo file cannot be loaded\nDefault logo will be used");
					pic.load(QString(":/images/printlogo.svg"));
				}
			}
		}

		paint->resetMatrix();
		paint->setClipRect(0,0,printer->pageRect().width(),printer->pageRect().height()); //bug in Qt 4.4.0 Without bitmap printing doesn't work!
		double pstart=printer->pageRect().width()*0.81;
		double pwidth=printer->pageRect().width()*0.19;
		double pheight=0.0;
		if (isSVG) { //render SVG
			QRect picrec=pic.viewBox();
			pheight=pwidth*picrec.height()/picrec.width();
			pic.render(paint, QRectF(pstart,0,pwidth,pheight));
		}
		else { //render bitmap
			QRect picrec=bitmap.rect();
			pheight=pwidth*picrec.height()/picrec.width();
                        paint->drawImage(QPoint(int(pstart),0),bitmap.scaledToWidth(int(pwidth),Qt::SmoothTransformation));
		}

		paint->setPen(Qt::black);

		QRectF rec(newsize.width()*1.01,pheight*1.05,printer->pageRect().width() - newsize.width()-13,newsize.height()-pheight-5);
		QTextOption topt;
		topt.setWrapMode(QTextOption::WrapAnywhere);
		QString sentry="All files:";
		paint->drawText(rec, sentry, topt);
		rec.setTop(rec.top()+paint->boundingRect(rec,Qt::AlignLeft,sentry).height()); //reduce retangle by 1 line 
		for (size_t i=0; i<data->size(); i++){
			if ((data->get_odata(i)->rows()>1)==is2D) {
				paint->setPen(activePlot()->get_color(i));
				sentry=data->get_short_filename(i);
				paint->drawText(rec,sentry,topt);
				rec.setTop(rec.top()+paint->boundingRect(rec,Qt::AlignLeft,sentry).height());
			}
		}
		paint->setPen(Qt::black);


		QString extra_text="\nActive data:\n";
                if (!data->get_info(data->getActiveCurve())->file_filter)
                        data->global_options.useSystemPrintPars=false; //prevents crash if filefilter name doesn't exist e.g. for any 1D derivatives from 2D
		if (data->global_options.useSystemPrintPars) {
                        if (data->get_info(data->getActiveCurve())->file_filter->name()=="Bruker XWIN-NMR 1D spectra"){
				QString base=working_directory;
				base.remove("1r");
				QFile file(base+"parm.txt");
//				qDebug()<<"Looking for file"<<base+"parm.txt";
				if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
//					qDebug()<<"file not found";
					extra_text+=infoTextEdit->toPlainText();
				}
				else {
					QTextStream in(&file);
					while (!in.atEnd()) {
						extra_text+=in.readLine();
						extra_text+="\n";
     					}
				}
			}
			else if (data->get_info(data->getActiveCurve())->file_filter->name()=="Varian VNMR FID"){
				QString vparams=interpreteVnmrAp();
				if (!vparams.isEmpty())
					extra_text+=vparams;
				else
					extra_text+=infoTextEdit->toPlainText();
			}
			else
				extra_text+=infoTextEdit->toPlainText();
		}
		else
			extra_text+=infoTextEdit->toPlainText();
//		QFont backup =paint->font();
		size_t nlines=extra_text.split("\n").size()+2;
//		qDebug()<<"nlines in extra text"<<nlines;
		double textHeight=paint->fontMetrics().lineSpacing()*nlines;
//		qDebug()<<"it took"<< textHeight<<"pixels when"<<rec.height()<<"available";
	
		if (textHeight>rec.height()){
			double ratio=rec.height()/textHeight;
//			qDebug()<<"font ratio"<<ratio;
			QFont font=paint->font();
//			qDebug()<<"old font size"<<font.pointSizeF();
			font.setPointSizeF(font.pointSizeF()*ratio);
//			qDebug()<<"new font size"<<font.pointSizeF();
			paint->setFont(font);
		}
		paint->drawText(rec,extra_text,topt);
//		printer->newPage();
//		paint->setFont(backup);
	}
	paint->end();
	plot->resize(oldsize);
	plot->setFont(oldfont);
	plot->setScheme(oldScheme);
	delete paint;
}

QString MainForm::interpreteVnmrAp()
{
//	qDebug()<<"Starting 'ap' analysis";
	QString string;
	const size_t k=data->getActiveCurve();
	if (data->get_info(k)->parameterFiles.size()!=1) {
//		qDebug()<<"Too short: Not a VNMR data?";
		return string;
	}
	if (data->get_info(k)->parameterFiles.at(0)!="procpar"){
//		qDebug()<<"Not a procpar: Not a VNMR data?";
		return string;
	}
	QHash< QString, QString > params=data->get_info(k)->allParameters.at(0);
	QString fullap=params["ap"];
	if (fullap.isEmpty()){
		qDebug()<<"'ap' string is empty";
		return string;
	}
	QStringList apv=fullap.split(" ",QString::SkipEmptyParts);
	if (apv.size()<2){
		qDebug()<<"cannot interprete 'ap': less then 2 entries";
		return string;
	}
	if (apv.at(0).toDouble()>1) {
		qDebug()<<"Multistring 'ap' is not supported yet";
		return string;
	}
	apv.removeFirst();
	QString aps=apv.join(" "); //spaces could be inside ap string!
//	QString aps=apv.at(1);
	aps.remove("""");//remove " at the beginning and at the end of the string
//	qDebug()<<"ap"<<aps;
	QStringList apent=aps.split(";",QString::SkipEmptyParts); //list of all big entries in ap parameter
	foreach (QString bent, apent){
		QStringList sent=bent.split(",",QString::SkipEmptyParts);//split by commas
		QStringList firste=sent[0].split(":",QString::SkipEmptyParts);//analise the first entry with the title
//		qDebug()<<"Title "<<firste[1];
//		qDebug()<<"First entry length"<<firste.size();
		if (firste.size()>2){
			string+=firste[1]+"\n";
			sent[0]=firste[2];
		}
		if (firste.size()>3)
			sent[0]+=":"+firste[3];//replace first entry
//		qDebug()<<"Replaced first entry "<<sent[0];
		foreach (QString inden, sent) {
			QStringList par_prec=inden.split(":",QString::SkipEmptyParts);
			QString par=par_prec[0];
//			qDebug()<<par;
			if (par.contains("(")){ //has printing conditions
				QStringList temps=par.split("(",QString::SkipEmptyParts);
				par=temps.at(0);//analise conditions
                                //TODO: here should be the condition analysis
			}
			QString pstring=params[par]; //take the string;
			if (pstring.isEmpty())
				continue;
			QStringList lprv=pstring.split(" ",QString::SkipEmptyParts);
			QString parv;
			if (lprv.size()>1)
				parv=lprv.at(1);
			else
				parv="";
			string+=par+": "+parv+"\n";
		}
	}
	return string;
}


void MainForm::updateFilesList()
{
	if (!data->mutex.tryLock())
		return;
	filesListWidget->clear();
	size_t n=data->size();
	for (size_t i=0; i<n; i++) {
		QString s=data->get_short_filename(i);
		QListWidgetItem* item = new QListWidgetItem(s);
		item->setFlags(item->flags() | Qt::ItemIsUserCheckable);
		if (data->get_display(i)->isVisible)
			item->setCheckState(Qt::Checked);
		else
			item->setCheckState(Qt::Unchecked);
		if (data->get_odata(i)->rows()<2) {
			item->setIcon(QIcon(":/images/1d.png"));
			item->setTextColor(plot2D->get_color(i));
		}
		else {
			item->setTextColor(plot3D->get_color(i));
			item->setIcon(QIcon(":/images/2d.png"));
		}
		item->setToolTip(infoString(i));
		filesListWidget->addItem(item);
	}
	n=data->getActiveCurve();
	if (data->size())
		filesListWidget->setCurrentRow(n);
	update_info(n);
	if (mdiArea->activeSubWindow())
		filesDockWidget->setWindowTitle("Files@"+mdiArea->activeSubWindow()->windowTitle());
	data->mutex.unlock();
}

void MainForm::on_filesListWidget_itemChanged()
{
	size_t n=data->size();
	if (n!=filesListWidget->count()) { 
		return; //item was changed because total length of ithem was changed - do nothing
	}
	if (!n) return;
	for (size_t i=0; i<n; i++)
	{	Display_ disp=*(data->get_display(i));
		if (filesListWidget->item(i)->checkState()==Qt::Checked) 
			disp.isVisible=true;
		else
			disp.isVisible=false;
		data->set_display(i,disp);
		plot2D->update();
		plot3D->update();
	}
}

void MainForm::on_filesListWidget_customContextMenuRequested()
{
	QMenu menu;
	menu.addAction(fileCloseAction);
	menu.addAction(fileReloadAction);
	menu.addAction(fileWatchAction);
	menu.addMenu(fileMoveToMenu);
	menu.exec(QCursor::pos());
}

void MainForm::resetAll2d()
{
    size_t n=data->size();
	for (size_t i=0; i<n; i++){
	Display_ display=*(data->get_display(i));
	if (data->get_odata(i)->rows()<=1) {
		display.vscale=1.0;
		display.xscale=1.0;
		display.vshift=0.0;
		display.xshift=0.0;
	}
	data->set_display(i, display);
	}
	plot2D->cold_restart(data);
}

void MainForm::resetAll3d()
{
    size_t n=data->size();
    for (size_t i=0; i<n; i++){
    Display_ display=*(data->get_display(i));
    if (data->get_odata(i)->rows()>1) {
		display.vscale=1.0;
		display.xscale=1.0;
		display.vshift=0.0;
		display.xshift=0.0;
    }
    data->set_display(i, display);
    }
    plot3D->cold_restart(data);
}

void MainForm::viewReal()
{
	if (!data->size()) return;
	foreach(int k, data->selectedCurves) {
		data->show_real(k);
		update_info(k);
		activePlot()->Update();
	}
	statusBar()->showMessage("Real mode",2000);
}

void MainForm::viewImag()
{
	if (!data->size()) return;
	foreach(int k, data->selectedCurves) {
		data->show_imag(k);
		update_info(k);
		activePlot()->Update();
	}
		statusBar()->showMessage("Imaginary mode",2000);
}

void MainForm::viewReferencing()
{
//check that data is 'homogeneuos'
const size_t ni=data->get_odata(data->getActiveCurve())->rows();
int dim=(ni<2)? 1:2;
foreach (int k, data->selectedCurves) {
	const size_t nit=data->get_odata(k)->rows();
	int dimt=(nit<2)? 1:2;
	if (dimt!=dim) {
		QMessageBox::critical(this, "Referencing fault", "All selected dataset should have the same number of dimensions (either 1D or 2D)", "Abort");
		return;
	}
}

double shift, shift1;
bool ok;
//actual referencing
if (dim==1) {
	if (!plot2D->is_leftbar) {
		QMessageBox::critical(this, "Referencing fault", "Please, set main marker first", "Abort");
		return;
	}
	double val=QInputDialog::getDouble(this, tr("Referencing"), tr("Value at marker's position"), plot2D->leftbarpos.x(), -1e10, 1e10, 3, &ok);
	if (!ok)
		return;
	shift=val-plot2D->leftbarpos.x();
	foreach (int k, data->selectedCurves) {
		gsimFD info=*(data->get_info(k));
		if (data->global_options.xfrequnits==UNITS_PPM && info.type==FD_TYPE_SPE)
			info.ref+=shift*info.sfrq; //convert to Hz
		if (data->global_options.xfrequnits==UNITS_KHZ && info.type==FD_TYPE_SPE)
			info.ref+=shift*1000; //convert to Hz
		else
			info.ref+=shift;
		data->set_info(k,info);
	}
	data->updateAxisVectors();
	resetAll2d();
}
else {
	if (!plot3D->is_leftbar) {
	QMessageBox::critical(this, "Referencing fault", "Please, set main marker first", "Abort");
		return;
	}
	double val=QInputDialog::getDouble(this, tr("Referencing on X axis"),
                                         tr("Value at marker's position"), plot3D->leftbarpos.x(), -1e10, 1e10, 3, &ok);
	if (!ok)
		return;
	shift=val-plot3D->leftbarpos.x();
	val=QInputDialog::getDouble(0, tr("Referencing on Y axis"),
                                        tr("Value at marker's position"), plot3D->leftbarpos.y(), -1e10, 1e10, 1, &ok);
	if (!ok)
		return;
	shift1=val-plot3D->leftbarpos.y();
	foreach (int k, data->selectedCurves) {
		gsimFD info=*(data->get_info(k));
		if (data->global_options.xfrequnits==UNITS_PPM && info.type==FD_TYPE_SPE)
			info.ref+=shift*info.sfrq; //convert to Hz
		else if (data->global_options.xfrequnits==UNITS_KHZ && info.type==FD_TYPE_SPE)
			info.ref+=shift*1000;
		else
			info.ref+=shift;
		if (data->global_options.yfrequnits==UNITS_PPM && info.type1==FD_TYPE_SPE)
			info.ref1+=shift1*info.sfrq1; //convert to Hz
		else if (data->global_options.yfrequnits==UNITS_KHZ && info.type1==FD_TYPE_SPE)
			info.ref1+=shift1*1000;
		else
			info.ref1+=shift1;
		data->set_info(k,info);
	}
	data->updateAxisVectors();
	resetAll3d();
}

statusBar()->showMessage("New referencing has been applied",2000);
}


void MainForm::viewSetRange()
{
	Plot2DWidget* plot=activePlot();
	expandDialog* expn=new expandDialog(plot);
	expn->xstartSpinBox->setValue(plot->xwinmin);
	expn->xendSpinBox->setValue(plot->xwinmax);
	expn->ystartSpinBox->setValue(plot->ywinmin);
	expn->yendSpinBox->setValue(plot->ywinmax);
	if (!expn->exec())
		return;
	double startx=expn->xstartSpinBox->value();
	double endx=expn->xendSpinBox->value();
	double starty=expn->ystartSpinBox->value();
	double endy=expn->yendSpinBox->value();
	if (startx>endx)
		std::swap(startx,endx);
	if (starty>endy)
		std::swap(starty,endy);
	plot->set_window_range(startx,endx,starty,endy);
	plot->update();
	delete expn;
}


double MainForm::convertionFactor(int oldu, int newu, double sfrq)
{
	double scaler=1.0;
	if (oldu==newu)
		return scaler;
	if (oldu==UNITS_HZ) {
		if (newu==UNITS_KHZ)
			scaler=1.0e-3;
		else if (newu==UNITS_PPM) {
			if (!sfrq)
				return 1.0;
			scaler=1.0/sfrq;
		}
	}
	else if (oldu==UNITS_KHZ) {
		if (newu==UNITS_HZ)
			scaler=1.0e3;
		else if (newu==UNITS_PPM) {
			if (!sfrq)
				return 1.0;
			scaler=1000.0/sfrq;
		}
	}
	else if (oldu==UNITS_PPM) {
		if (newu==UNITS_HZ)
			scaler=sfrq;
		else if (newu==UNITS_KHZ)
			scaler=1.0e-3*sfrq;
	}
	else if (oldu==UNITS_US) {
		if (newu==UNITS_MS)
			scaler=1.0e-3;
		else if (newu==UNITS_S)
			scaler=1.0e-6;
	}
	else if (oldu==UNITS_MS) {
		if (newu==UNITS_MS)
			scaler=1.0e3;
		else if (newu==UNITS_S)
			scaler=1.0e-3;
	}
	else if (oldu==UNITS_S) {
		if (newu==UNITS_US)
			scaler=1.0e6;
		else if (newu==UNITS_MS)
			scaler=1.0e3;
	}
	return scaler;
}

/*
void MainForm::changeScales(int oldx, int newx, int oldy, int newy) //changing integrals and peak peaking labels
{
	size_t N=data->size();
	for (size_t i=0; i<N; i++) {
		const gsimFD* info=data->get_info(i);
		double scaler=convertionFactor(oldx,newx,info->sfrq);
		double scaler2=convertionFactor(oldy,newy,info->sfrq1);

	//for all graphical information
		QList<graph_object> graphics=data->get_graphics(i);
		for (size_t j=0; j<graphics.size(); j++) {
			if ((graphics.at(j).type==GRAPHTEXT) || (graphics.at(j).type==GRAPHIMAGE)) {
				QPointF pos=graphics.at(j).pos.toPointF();
				QPointF newpos=QPointF(pos.x()*scaler, pos.y()*scaler2);
				graphics[j].pos=newpos;
				}
			else if (graphics.at(j).type==GRAPHLINE) {
				QLineF pos=graphics.at(j).pos.toLineF();
				QLineF newpos=QLineF(pos.x1()*scaler, pos.y1()*scaler2, pos.x2()*scaler, pos.y2()*scaler2);
				graphics[j].pos=newpos;
			}
		}
		data->set_graphics(i,graphics);
	//for peak list
		QList<Array_> arrays=data->get_arrays(i);
		if (arrays.size()) {
			int ans=-1;
			for (size_t y=0; y<arrays.size(); y++)
				if (arrays.at(y).name==QString("PeaksPos")) {
					ans=y;
					}
			if (ans>=0)
				for (size_t l=0; l<arrays.at(ans).data.size(); l++)
					arrays[ans].data(l)*=scaler;
			data->set_arrays(i,arrays);
		}
	//for integrals
		Integrals_ integrals=data->get_integrals(i);
		for (size_t j=0; j<integrals.xintegrals.size(); j++)
			for (size_t l=0; l<integrals.xintegrals.at(j).size();l++) {
				integrals.xintegrals[j](l)*=scaler;
				if (data->get_odata(i)->rows()>1) //correct y-positions for 2D integrals
					integrals.yintegrals[j](l)*=scaler2;
			}
		data->set_integrals(i,integrals);
	//for shifts
		Display_ disp=*(data->get_display(i));
		disp.xshift*=scaler;
		disp.vshift*=scaler2;
		data->set_display(i,disp);
	}
	double scaler=convertionFactor(oldx,newx,data->get_info(data->getActiveCurve())->sfrq);
	double scaler2=convertionFactor(oldy,newy,data->get_info(data->getActiveCurve())->sfrq1);
//for markers
	Plot2DWidget* plot=activePlot();
	QPointF leftM=plot->leftbarpos;
	plot->leftbarpos.setX(leftM.x()*scaler);
	plot->leftbarpos.setY(leftM.y()*scaler2);
	QPointF rightM=plot->rightbarpos;
	plot->rightbarpos.setX(rightM.x()*scaler);
	plot->rightbarpos.setY(rightM.y()*scaler2);
//for window range
	plot->xwinmax*=scaler;
	plot->xwinmin*=scaler;
	plot->ywinmax*=scaler2;
	plot->ywinmin*=scaler2;
	plot->xppt*=scaler;
	plot->yppt*=scaler2;
	plot->xrange*=scaler;
	plot->yrange*=scaler2;
//pre-calculated contours and raster stuff
	for (size_t i=0; i<plot3D->countours.size(); i++) {
		QLineF nline(plot3D->countours(i).x1()*scaler,plot3D->countours(i).y1()*scaler2,plot3D->countours(i).x2()*scaler,plot3D->countours(i).y2()*scaler2);
		plot3D->countours(i)=nline;
	}
	for (size_t i=0; i< plot3D->rectangles.size(); i++) {
		plot3D->rectangles[i].setX(plot3D->rectangles.at(i).x()*scaler);
		plot3D->rectangles[i].setY(plot3D->rectangles.at(i).y()*scaler2);
	}
	for (size_t i=0; i< plot3D->recWidth.size(); i++) {
		plot3D->recWidth[i].setX(plot3D->recWidth.at(i).x()*scaler);
		plot3D->recWidth[i].setY(plot3D->recWidth.at(i).y()*scaler2);
	}
}
*/

void MainForm::changeScales(int oldx, int newx, int oldy, int newy) //changing integrals and peak peaking labels
{
	size_t N=data->size();
	for (size_t i=0; i<N; i++) {
		const gsimFD* info=data->get_info(i);
		size_t np=data->get_odata(i)->cols();
		size_t ni=data->get_odata(i)->rows();
	
	//for all graphical information
		QList<graph_object> graphics=data->get_graphics(i);
		for (size_t j=0; j<graphics.size(); j++) {
			if ((graphics.at(j).type==GRAPHTEXT) || (graphics.at(j).type==GRAPHIMAGE)) {
				QPointF pos=graphics.at(j).pos.toPointF();
				QPointF newpos=QPointF(convertUnits(pos.x(),oldx,newx,info,np), convertUnits(pos.y(),oldy,newy,info,ni,false));
				graphics[j].pos=newpos;
				}
			else if (graphics.at(j).type==GRAPHLINE) {
				QLineF pos=graphics.at(j).pos.toLineF();
				QLineF newpos=QLineF(convertUnits(pos.x1(),oldx,newx,info,np), convertUnits(pos.y1(),oldy,newy,info,ni,false), convertUnits(pos.x2(),oldx,newx,info,np), convertUnits(pos.y2(),oldy,newy,info,ni,false));
				graphics[j].pos=newpos;
			}
		}
		data->set_graphics(i,graphics);
	//for peak list
		QList<Array_> arrays=data->get_arrays(i);
		if (arrays.size()) {
			int ans=-1;
			for (size_t y=0; y<arrays.size(); y++)
				if (arrays.at(y).name==QString("PeaksPos")) {
					ans=y;
					}
			if (ans>=0)
				for (size_t l=0; l<arrays.at(ans).data.size(); l++)
					arrays[ans].data(l)=convertUnits(arrays[ans].data(l),oldx,newx,info,np);
			data->set_arrays(i,arrays);
		}
	//for integrals
		Integrals_ integrals=data->get_integrals(i);
		for (size_t j=0; j<integrals.xintegrals.size(); j++)
			for (size_t l=0; l<integrals.xintegrals.at(j).size();l++) {
				integrals.xintegrals[j](l)=convertUnits(integrals.xintegrals[j](l),oldx,newx,info,np);
				if (data->get_odata(i)->rows()>1) //correct y-positions for 2D integrals
					integrals.yintegrals[j](l)=convertUnits(integrals.yintegrals[j](l),oldy,newy,info,ni,false);
			}
		data->set_integrals(i,integrals);
	//for shifts
		Display_ disp=*(data->get_display(i));
			disp.xshift=convertUnits(disp.xshift,oldx,newx,info,np,true,true);
			disp.vshift=convertUnits(disp.vshift,oldy,newy,info,ni,false,true);
		data->set_display(i,disp);
	}
	
	const gsimFD* info=data->get_info(data->getActiveCurve());
	size_t np=data->get_odata(data->getActiveCurve())->cols();
	size_t ni=data->get_odata(data->getActiveCurve())->rows();

//for markers
	Plot2DWidget* plot=activePlot();
	QPointF leftM=plot->leftbarpos;
	plot->leftbarpos.setX(convertUnits(leftM.x(),oldx,newx,info,np));
	plot->leftbarpos.setY(convertUnits(leftM.y(),oldy,newy,info,ni,false));
	QPointF rightM=plot->rightbarpos;
	plot->rightbarpos.setX(convertUnits(rightM.x(),oldx,newx,info,np));
	plot->rightbarpos.setY(convertUnits(rightM.y(),oldy,newy,info,ni,false));
//for window range
	plot->xwinmax=convertUnits(plot->xwinmax,oldx,newx,info,np);
	plot->xwinmin=convertUnits(plot->xwinmin,oldx,newx,info,np);
	if (ni>1) { //convert y-range for 2D data
		plot->ywinmax=convertUnits(plot->ywinmax,oldy,newy,info,ni,false);
		plot->ywinmin=convertUnits(plot->ywinmin,oldy,newy,info,ni,false);
	}
//	qDebug()<<"Plot boundary"<<plot->xwinmin<<plot->xwinmax;
	plot->set_window_range(plot->xwinmin,plot->xwinmax,plot->ywinmin,plot->ywinmax);

//pre-calculated contours and raster stuff
	for (size_t i=0; i<plot3D->countours.size(); i++) {
		QLineF nline(convertUnits(plot3D->countours(i).x1(),oldx,newx,info,np),convertUnits(plot3D->countours(i).y1(),oldy,newy,info,ni,false),convertUnits(plot3D->countours(i).x2(),oldx,newx,info,np),convertUnits(plot3D->countours(i).y2(),oldy,newy,info,ni,false));
		plot3D->countours(i)=nline;
	}
	for (size_t i=0; i< plot3D->rectangles.size(); i++) {
		plot3D->rectangles[i].setX(convertUnits(plot3D->rectangles.at(i).x(),oldx,newx,info,np));
		plot3D->rectangles[i].setY(convertUnits(plot3D->rectangles.at(i).y(),oldy,newy,info,ni,false));
	}
	for (size_t i=0; i< plot3D->recWidth.size(); i++) {
		double width=fabs(convertUnits(plot3D->recWidth.at(i).x(),oldx,newx,info,np,true,true));
		plot3D->recWidth[i].setX(width);
		plot3D->recWidth[i].setY(convertUnits(plot3D->recWidth.at(i).y(),oldy,newy,info,ni,false,true));
//		qDebug()<<width;
	}
}


void MainForm::toPpm() 
{
	int oldx=(data->get_info(data->getActiveCurve())->type==FD_TYPE_FID)? data->global_options.xtimeunits : data->global_options.xfrequnits;
	int oldy=(data->get_info(data->getActiveCurve())->type1==FD_TYPE_FID)? data->global_options.xtimeunits : data->global_options.xfrequnits;
	int newx=(data->get_info(data->getActiveCurve())->type==FD_TYPE_FID)? oldx : UNITS_PPM;
	int newy=(data->get_info(data->getActiveCurve())->type1==FD_TYPE_FID)? oldy : UNITS_PPM;
	data->global_options.xfrequnits=newx;
	data->global_options.yfrequnits=newy;
	data->updateAxisVectors();
	changeScales(oldx,newx,oldy,newy);
	activePlot()->update();
}

void MainForm::toHz() 
{
	int oldx=(data->get_info(data->getActiveCurve())->type==FD_TYPE_FID)? data->global_options.xtimeunits : data->global_options.xfrequnits;
	int oldy=(data->get_info(data->getActiveCurve())->type1==FD_TYPE_FID)? data->global_options.xtimeunits : data->global_options.xfrequnits;
	int newx=(data->get_info(data->getActiveCurve())->type==FD_TYPE_FID)? oldx : UNITS_HZ;
	int newy=(data->get_info(data->getActiveCurve())->type1==FD_TYPE_FID)? oldy : UNITS_HZ;
	data->global_options.xfrequnits=newx;
	data->global_options.yfrequnits=newy;
	data->updateAxisVectors();
	changeScales(oldx,newx,oldy,newy);
	activePlot()->update();
}

void  MainForm::cycChangeXAxis()
{
	Plot2DWidget* plot=activePlot();
	int oldx,newx;
	if (plot->xaxis_label=="us"){
		oldx=UNITS_US;
		newx=UNITS_MS;
	}
	else if (plot->xaxis_label=="ms"){
		oldx=UNITS_MS;
		newx=UNITS_S;
	}
	else if (plot->xaxis_label=="s"){
		oldx=UNITS_S;
		newx=UNITS_TPTS;
	}
        else if (plot->xaxis_label=="pts"){
		oldx=UNITS_TPTS;
		newx=UNITS_US;
	}
	else if (plot->xaxis_label=="Hz"){
		oldx=UNITS_HZ;
		newx=UNITS_KHZ;
	}
	else if (plot->xaxis_label=="kHz"){
		oldx=UNITS_KHZ;
                newx=UNITS_PPM;
                for (size_t k=0; k<data->size(); k++)
                        if (!data->get_info(k)->sfrq)
                                newx=UNITS_HZ;
	}
	else if (plot->xaxis_label=="ppm"){
		oldx=UNITS_PPM;
		newx=UNITS_HZ;
	}

        int oldy=(data->get_info(data->getActiveCurve())->type1==FD_TYPE_FID)? data->global_options.ytimeunits : data->global_options.yfrequnits;
	changeScales(oldx,newx,oldy,oldy);
	if (data->get_info(data->getActiveCurve())->type==FD_TYPE_FID)
		data->global_options.xtimeunits=newx;
	else
		data->global_options.xfrequnits=newx;
	data->updateAxisVectors();
//        plot->init_settings();
	plot->update();
}

void  MainForm::cycChangeYAxis()
{
	Plot2DWidget* plot=activePlot();
	int oldy,newy;
	if (plot->yaxis_label=="us"){
		oldy=UNITS_US;
		newy=UNITS_MS;
	}
	else if (plot->yaxis_label=="ms"){
		oldy=UNITS_MS;
		newy=UNITS_S;
	}
	else if (plot->yaxis_label=="s"){
		oldy=UNITS_S;
		newy=UNITS_TPTS;
	}
	else if (plot->yaxis_label=="pts (time)"){
		oldy=UNITS_TPTS;
		newy=UNITS_US;
	}
	else if (plot->yaxis_label=="Hz"){
		oldy=UNITS_HZ;
		newy=UNITS_KHZ;
	}
	else if (plot->yaxis_label=="kHz"){
		oldy=UNITS_KHZ;
		newy=UNITS_PPM;
		for (size_t k=0; k<data->size(); k++)
			if (!data->get_info(k)->sfrq1)
				newy=UNITS_HZ;
	}
	else if (plot->yaxis_label=="ppm"){
		oldy=UNITS_PPM;
                newy=UNITS_HZ;
	}
	int oldx=(data->get_info(data->getActiveCurve())->type==FD_TYPE_FID)? data->global_options.xtimeunits : data->global_options.xfrequnits;
	changeScales(oldx,oldx,oldy,newy);
	if (data->get_info(data->getActiveCurve())->type1==FD_TYPE_FID)
		data->global_options.ytimeunits=newy;
	else
		data->global_options.yfrequnits=newy;
	data->updateAxisVectors();
	plot->update();
}

void MainForm::fixScalings()
{
	if (!data->size()) return;
	data->makeBackup();
for (size_t k=0; k<data->size(); k++) {
	Display_ display=*(data->get_display(k));
	gsimFD info=*(data->get_info(k));
	size_t np=data->get_odata(k)->cols();
	size_t ni=data->get_odata(k)->rows();
	if (!ni) ni=1;
	double xsc=display.xscale;
	double xsh=display.xshift;
	double ysc=display.vscale;
	double ysh=display.vshift;
	complex temp(0.0,0.0);

//xaxis fixing
	if (data->global_options.xfrequnits==UNITS_PPM)
		xsh*=info.sfrq;
	else if (data->global_options.xfrequnits==UNITS_KHZ)
		xsh*=1000;
	if (xsc<0) {  //just to avoid negative spectral width
		invert(*data->get_odata(k), true);
		xsc=fabs(xsc);
	}
	info.ref=xsh+info.ref*xsc;
	if (info.type==FD_TYPE_SPE) 
		info.sw*=xsc;
	else 	info.sw/=xsc;
     //Scaling + shift together doesn't work !
//yaxis fixing
	if (ni>1) {
		if (data->global_options.yfrequnits==UNITS_PPM)
			ysh*=info.sfrq1;
		else if (data->global_options.yfrequnits==UNITS_KHZ)
			ysh*=1000;
		if (ysc<0) {
			invert(*data->get_odata(k), false);
			ysc=fabs(ysc);
		}
		if (info.type1==FD_TYPE_SPE) 
			info.sw1=info.sw1*ysc;
		else 	info.sw1=info.sw1/ysc;
		info.ref1=ysh+info.ref1*ysc;
	}
	else {
		for (size_t i=0; i<ni; i++)
		for (size_t j=0; j<np; j++){
			temp=data->get_odata(k,i,j)*ysc+ysh;
			data->set_odata(k,i,j,temp);
		}
	}
data->set_info(k,info);
display.xscale=1.0;
display.xshift=0.0;
display.vscale=1.0;
display.vshift=0.0;
data->set_display(k,display);
data->updateAxisVectors();
}
	plot3D->cold_restart(data);
	plot2D->cold_restart(data);
statusBar()->showMessage("All scaling/shifts have been fixed",2000);
}

void MainForm::helpAbout()
{
    aboutForm  about;
    about.exec();
}

void MainForm::helpAboutQt()
{
    QMessageBox::aboutQt ( this );
}

void MainForm::helpHomeWebpage()
{
         QDesktopServices::openUrl(QUrl("http://gsim.sf.net"));
}

void MainForm::helpForum()
{
	 QDesktopServices::openUrl(QUrl("http://sourceforge.net/forum/forum.php?forum_id=604010"));
}

void MainForm::openDocumentation(QString fname)
{
	QString s=QCoreApplication::applicationDirPath()+"/"+fname;
	if (!QFile::exists(s)) {
		QMessageBox::warning(this, "File error", QString("File '%1' has not been found in GSim installation directory, GSim reinstallation can fix this problem").arg(fname));
		return;
	}
	QDesktopServices::openUrl(QUrl::fromLocalFile(s));
}

void MainForm::delIntegralAsked()
{
	if (activePlot()->is_leftbar) {
		promptDialog dial(this);
		dial.setWindowTitle("Delete integral");
		QRadioButton* sel= new QRadioButton(QString("Only selected"),&dial);
		sel->setChecked(true);
		QRadioButton* all= new QRadioButton(QString("All integrals"),&dial);
		QButtonGroup* group=new QButtonGroup(&dial);
		group->addButton(sel);
		group->addButton(all);
		group->setId(sel,0);
		group->setId(all,1);
		dial.putWidget(sel);
		dial.putWidget(all);
		int res=dial.exec();
		if (!res)
			return;
		if (group->checkedId())
			delIntegrals();
		else
			delSelectedIntegral();
	}
	else {
		delIntegrals();
	}
}

void MainForm::delSelectedIntegral()
{
	const size_t k=data->getActiveCurve();
	bool is2d=(data->get_odata(k)->rows()>1);
//	qDebug()<<"Is2D"<<is2d;
	int which = (is2d)? findIntegral2D(k,plot3D->leftbarpos):findIntegral(k,plot2D->leftbarpos.x());
//	qDebug()<<"Which integral:"<<which;
	if (which<0) {
		QMessageBox::critical(this, "Integral is not found", QString("Cannot find an integral at the current main marker position"));
		return;
	}
	data->makeBackup();
	Integrals_ allint=data->get_integrals(k);
	if (which>=allint.xintegrals.size() || which>=allint.yintegrals.size())
		qDebug()<<"Something wrong with the (x,y) integral sizes";
	allint.xintegrals.removeAt(which);
	allint.yintegrals.removeAt(which);
	data->set_integrals(k,allint);
	QList<Array_> arrays=data->get_arrays(k);
	int q=-1;
		for (size_t j=0; j<arrays.size(); j++)
			if (arrays.at(j).name==QString("IntegList"))
				q=j;
		if (q>=0) {
			if (which>=arrays[q].data.size()) {
				qDebug()<<"Something wrong with the IntegList size";
				return;
			}
			for (size_t jj=which+1; jj<arrays[q].data.size(); jj++)
				arrays[q].data(jj-1)=arrays[q].data(jj);
			arrays[q].data.resize(arrays[q].data.size()-1);
		}
	data->set_arrays(k,arrays);
}

void MainForm::delIntegrals()
{
	data->makeBackup();
	size_t len=data->size();
	for (size_t i=0; i<len; i++) {
		data->delete_integrals(i);
		QList<Array_> arrays=data->get_arrays(i);
		int q=-1;
		for (size_t j=0; j<arrays.size(); j++)
			if (arrays.at(j).name==QString("IntegList"))
				q=j;
		if (q>=0)
			arrays.removeAt(q);
		data->set_arrays(i,arrays);
	}
	activePlot()->update();
	integralsInfoText->clear();
}

void MainForm::nextSlice()
{
	if (!data->size())
		return;
	parent_ history;
	QList<int> list=data->selectedCurves;
	for (size_t i=0; i<list.size(); i++) {
		const size_t k=list.at(i);
		if (data->get_odata(k)->rows()<2) {//only 1D spectra can have 2D parents
				history=data->getParent(k);
				changeSlice(k, history.sliceIndex+1, history.isRow);
			}
	}
	for (int i=0;i<filesListWidget->count();i++)
		filesListWidget->item(i)->setSelected(false);
	foreach (int i, list)
		filesListWidget->item(i)->setSelected(true);
}

void MainForm::previousSlice()
{
	if (!data->size())
		return;
	parent_ history;
	QList<int> list=data->selectedCurves;
	for (size_t i=0; i<list.size(); i++) {
		const size_t k=list.at(i);
		if (data->get_odata(k)->rows()<2) {//only 1D spectra can have 2D parents
				history=data->getParent(k);
				changeSlice(k, history.sliceIndex-1, history.isRow);
			}
	}
	for (int i=0;i<filesListWidget->count();i++)
		filesListWidget->item(i)->setSelected(false);
	foreach (int i, list)
		filesListWidget->item(i)->setSelected(true);
}


void MainForm::changeSlice(size_t k, int newindex, bool isRow)
{
	parent_ history=data->getParent(k);
	if (!history.hasParent) return;
	size_t np=data->get_odata(history.parentIndex)->cols();
	size_t ni=data->get_odata(history.parentIndex)->rows();
	if (ni<1) ni=1;
	QString fname;
	if (isRow) {
	if (newindex>int(ni)-1 || newindex<0) 
			return;
			for (size_t i=np-1; i--;)
				data->set_odata(k,0,i,data->get_odata(history.parentIndex,newindex,i));
			fname=QString("Row%1@").arg(newindex+1)+data->get_short_filename(history.parentIndex);
	}
	else {
		if (newindex>int(data->get_odata(history.parentIndex)->cols())-1 || newindex<0) return;
		for (size_t i=ni-1; i--;)
				data->set_odata(k,0,i,data->get_odata(history.parentIndex,i,newindex));
		fname=QString("Col%1@").arg(newindex+1)+data->get_short_filename(history.parentIndex);
	}
	history.sliceIndex=newindex;
	data->setParent(k, history);
	gsimFD info=*(data->get_info(k));
	info.filename=fname;
	info.is_on_disk=false;
	data->set_info(k,info);
	updateFilesList();
	statusBar()->showMessage(QString("Slice %1 is taken").arg(newindex),2000);
}

void MainForm::editUndo()
{
	data->undo();
	plot2D->cold_restart(data);
	plot3D->cold_restart(data);
	updateFilesList();
	update_info(data->getActiveCurve());
}

void MainForm::editSpectralPars()
{
	if (!data->size()) return;
	spectralParsDialog* pard = new spectralParsDialog(this, 0);
	pard->exec();
	delete pard;
}

void MainForm::editData()
{
	if (!data->size()) return;
	if (datatable!=NULL) 
		delete datatable;
	DataTable* dtable = new DataTable(this, 0);
	dtable->show();
	datatable=dtable;
}

void MainForm::analysisDeconv()
{
	if (!data->size()) return;
	if(deconvform!=NULL)
		delete deconvform;
	data->makeBackup();
	DeconvForm* dd = new DeconvForm(this, 0);
	dd->show();
	dd->raise();
	deconvform=dd;
}

void MainForm::analysisArray()
{
	if (!data->size()) return;
	if (arraymanager!=NULL)
		delete arraymanager;
	ArrayManager* am = new ArrayManager(this, 0);
	am->show();
	am->raise();
	arraymanager=am;
}

void MainForm::analysisAdvFit()
{
	fit=new fitDialog(this);
	fit->adjustSize();
	fit->show();
	fit->raise();
	fit->activateWindow();
	connect(fit,SIGNAL(finished()),this,SLOT(fitFinished(int)));
}

void MainForm::fitFinished(int)
{
	delete fit;
}

void MainForm::editOptions()
{
	optionsDialog* opt= new optionsDialog(this, 0, data);
	opt->exec();
	delete opt;
	data->updateAxisVectors();
	plot2D->cold_restart(data);
	plot3D->cold_restart(data);
}

void MainForm::on_dirTreeView_activated(const QModelIndex& index)
{
	QFileInfo fileinfo=dirModel->fileInfo(index);
	if (!fileinfo.isFile())
		return;
	QString fname=fileinfo.absoluteFilePath();
	fileOpen(fname);
}
