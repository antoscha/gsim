#include "proceditor.h"

ProcEditor::ProcEditor(MainForm *parent, Qt::WindowFlags fl)
	:QDialog(parent, fl) {

    setupUi(this);
    p=parent;
    setWindowTitle( "Spectral data" );
    setWindowIcon( QPixmap( ":/images/gsim.png" ) );

    int avLength=p->availableProcList.size();
    for (int i=0; i<avLength; i++)
	new QListWidgetItem(p->availableProcList.at(i)->getName(), availableListWidget);
    if (avLength>0)
	availableListWidget->setCurrentRow(0);
    int seLength=p->selectedProcList.size();
    for (int i=0; i<seLength; i++)
	new QListWidgetItem(p->selectedProcList.at(i)->getName(), selectedListWidget);
    if (seLength>0)
	selectedListWidget->setCurrentRow(0);
    p->storedProcList=p->selectedProcList;
    int toolsLength=p->data->global_options.procToolBarList.size();
    for (int i=0; i<toolsLength; i++)
	new QListWidgetItem(p->data->global_options.procToolBarList.at(i), toolsListWidget);
}

ProcEditor::~ProcEditor()
{
}

void ProcEditor::on_upButton_clicked()
{
	int pos=selectedListWidget->currentRow();
	if (pos<0)
		return;
	if (pos>0) {
		QListWidgetItem* item=selectedListWidget->takeItem(pos);
		selectedListWidget->insertItem(pos-1, item);
		selectedListWidget->setCurrentRow(pos-1);
	}
}

void ProcEditor::on_downButton_clicked()
{
	int pos=selectedListWidget->currentRow();
	if (pos<0)
		return;
	if (pos<selectedListWidget->count()-1) {
		QListWidgetItem* item=selectedListWidget->takeItem(pos);
		selectedListWidget->insertItem(pos+1, item);
		selectedListWidget->setCurrentRow(pos+1);
	}
}

void ProcEditor::on_addButton_clicked()
{
	QListWidgetItem* item=new QListWidgetItem(*availableListWidget->currentItem());
	selectedListWidget->addItem(item);
}

void ProcEditor::on_removeButton_clicked()
{
	delete selectedListWidget->takeItem(selectedListWidget->currentRow());
}

void ProcEditor::on_addToolButton_clicked()
{
	QListWidgetItem* item=new QListWidgetItem(*availableListWidget->currentItem());
	toolsListWidget->addItem(item);
}

void ProcEditor::on_delToolButton_clicked()
{
	delete toolsListWidget->takeItem(toolsListWidget->currentRow());
}

void ProcEditor::on_okButton_clicked()
{
	size_t n=selectedListWidget->count();
	p->storedProcList=p->selectedProcList;
	p->selectedProcList.clear();
	for (size_t i=0; i<n; i++) {
		BaseProcessingFunc* func=p->findFunc(selectedListWidget->item(i)->text());
		if (func!=NULL)
			p->selectedProcList.push_back(func);
	}
	size_t ntools=toolsListWidget->count();
	p->data->global_options.procToolBarList.clear();
	for (size_t i=0; i<ntools; i++) {
		BaseProcessingFunc* func=p->findFunc(toolsListWidget->item(i)->text());
		if (func!=NULL)
			p->data->global_options.procToolBarList.push_back(func->getName());
	}
}

void ProcEditor::on_cancelButton_clicked()
{

}
