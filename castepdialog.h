#ifndef CASTEPDIALOG_H
#define CASTEPDIALOG_H

#include <QDialog>
#include "ui_castepdialog.h"
#include <QSettings>

class castepDialog : public QDialog, public Ui::castepDialog
{
    Q_OBJECT

public:
    castepDialog( QWidget* parent = 0, Qt::WindowFlags fl = 0 );
    ~castepDialog();
    void readSettings();
private:
    void writeSettings();
};

#endif // CASTEPDIALOG_H
