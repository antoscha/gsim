#include "filefilters.h"
#include "castepdialog.h"
#include <sys/types.h>
#include <iostream>
#include <fstream>
#include <string>
#include <sys/stat.h>
#include <sys/types.h>
#include <QDir>
#include <QTextStream>
#include <QDebug>
#include <QInputDialog>
#include <QMessageBox>
#include <QBuffer>

#define LINE_MAX_LEN 256

bool BaseFileFilter::isreadable(const char* fname)
{
  FILE* fd=NULL;
  fd=fopen(fname,"rt");
  if (fd!=NULL) {
    fclose(fd);
    return true;
  }
  else
    return false;
}

int BaseFileFilter::which_array(QString &name, QList<Array_>& arrays)
{
int result=-1;
size_t n=arrays.size();
for (size_t i=0; i<n; i++)
	if (name==arrays.at(i).name) result=i;
return result;
}

void BaseFileFilter::read(QList<data_entry>& data, QString fname)
{
	data.clear();
	data_entry newdata;
	data.push_back(newdata);
    int f=read(data[0].spectrum, data[0].info, data[0].arrays, fname.toLatin1());
//	data[0].info.setSpectrum(&data[0].spectrum);
	if (f) {
//		throw Failed("File cannot be loaded");
		data.clear(); //exception cannot be caught by Qt
	}
}

int BaseFileFilter::read(cmatrix & spec, gsimFD & info , QList<Array_> & , const char* fname)
{
	return read(spec,info,fname);
}

void BaseFileFilter::write(QList<data_entry>& data, QString& fname)
{
    write(data[0].spectrum, data[0].info, fname.toLatin1(), data[0].arrays);
}

void BaseFileFilter::write(const cmatrix& c, gsimFD i, const char* f, QList<Array_> )
{
	write(c,i,f);
}

void BaseFileFilter::extract(const data_entry*, QStringList& all)
{
	all.clear();
}

bool SpinsightFileFilter::matching(const char* fname)
{/* Looking for files "data" and "acq" */
     char testname[256];
     strcpy(testname,fname);
     if (strlen(testname)<4) return false;
     char* p=testname+strlen(testname)-4;
     if (strcmp(p,"data")) {
       return false;
     }
     if (!isreadable(testname))
       return false;
     strcpy(p,"acq");
     return isreadable(testname);
}

QString SpinsightFileFilter::short_filename(QString s) 
{
	if (s.contains(QChar('/'))||s.contains(QChar('\\'))) { //Unix and Windows style separators
		s.remove(s.size()-5,5);
		s.remove(0, s.lastIndexOf(QRegExp("[/\\\\]"))+1);
	}
	return s;
}

int SpinsightFileFilter::read_spinsight_parfile(QString fname, QHash<QString, QString> &pars) //don't forget to use parameterFiles separately
{
	QFile file(fname);
        if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
            return 1; //file doesn't exist
        QTextStream in(&file);
        while (!in.atEnd()) {
            QString line = in.readLine();
//	    cout<<line.toLatin1().data()<<endl;
            if (line.contains("=")) {
		QStringList buf=line.split("=", QString::SkipEmptyParts);
		if (buf.size()<2)
			pars[buf.at(0)]=QString("");
		else {
			if (buf.at(1).endsWith("s"))
				buf[1].chop(1);
			if (buf.at(1).endsWith("u") || buf.at(1).endsWith("m"))
				buf[1].chop(1);
			pars[buf.at(0)]=buf.at(1);
		}
	    }
        }
	file.close();
	return 0;
}

void SpinsightFileFilter::read(QList<data_entry>& d, QString s) {
	BaseFileFilter::read(d,s);
	for (size_t i=0; i<d.size(); i++)
		if (d.at(i).info.type==FD_TYPE_SPE) 
			invert(d[i].spectrum, true);
}

int SpinsightFileFilter::read(cmatrix &a, gsimFD &par, QList<Array_> &arrays, const char* fname)
/*function for reading of SPINSIGHT 1D FIDs and spectra. FID stores in "data" file in binary format.
4 bytes per each (real or imag) point in big-endian format (as integer). Spectra - the same but as float. First half of file stores the real part, second half -imaginary
adopted partly from MatNMR of J. vanBeek. Thanks!*/

{
  FILE* pFile;
  unsigned char* datat;
  char parfname[256];
//  par.ni=1; //default value
  par.sw1=1.0;
  int dataformat=FD_TYPE_FID;
  double freq[4];
  for (size_t i=0; i<4; i++)
	freq[i]=0.0;
  int ch1=0, ch2=0;
  double rmp1=0.5, rmp2=0.5;
  double rmv1=0.0, rmv2=0.0;
  int rmvunits1=2, rmvunits2=2;

  //Reading acq file
  strcpy(parfname,fname);
  strcpy(parfname+strlen(parfname)-4,"acq");

//load parameters
 
  par.parameterFiles<<"acq";
  QHash<QString, QString> lPar; //loaded parameters
  int fault=read_spinsight_parfile(parfname, lPar);
  if (fault) {
	throw Failed("read_spinsight: acq file doesn't exist");
  }
  par.allParameters.append(lPar);

//analyse parameters
if (lPar.contains("ch1"))
    ch1=lPar.value("ch1").toInt();
if (lPar.contains("ch2"))
    ch2=lPar.value("ch2").toInt();
if (lPar.contains("sf1"))
    freq[0]=lPar.value("sf1").toDouble();
if (lPar.contains("sf2"))
    freq[1]=lPar.value("sf2").toDouble();
if (lPar.contains("sf3"))
    freq[2]=lPar.value("sf3").toDouble();
if (lPar.contains("sf4"))
    freq[3]=lPar.value("sf4").toDouble();
if (lPar.contains("ppfn"))
    par.pulprog=lPar.value("ppfn");
if (lPar.contains("sw"))
    par.sw=lPar.value("sw").toDouble()*1e3;
if (lPar.contains("sw2"))
    par.sw1=lPar.value("sw2").toDouble()*1e3;
//if (lPar.contains("al2"))
//    par.ni=lPar.value("al2").toInt();
//if (lPar.contains("al"))
//    par.np=lPar.value("al").toInt();
if (lPar.contains("speed"))
    par.spinrate=lPar.value("speed").toDouble()*1e3;
if (lPar.contains("temp"))
    par.temperature=lPar.value("temp").toDouble();


//find arrays:
    QHashIterator<QString, QString> iter(lPar);
    while (iter.hasNext()) {
        iter.next();
      if (iter.key().contains('[') && iter.key().contains(']'))
	{
//	   cout<<"Found "<<iter.key().toLatin1().data()<<" with value "<<iter.value().toDouble()<<endl;
	   QString s=iter.key();
	   QStringList ls=s.split("[", QString::SkipEmptyParts);
	   int k=which_array(ls[0], arrays); //ls[0] - array name
	   if (k<0) {  //array doesn't exist
			Array_ a(ls[0]);
			arrays.append(a);
			k=arrays.size()-1;
		}
		int index=ls[1].remove("]").toInt();
//		cout<<"Index:"<<index<<endl;
		while(arrays[k].data.size()<=index)
			arrays[k].data.push_back(0.0);
		arrays[k].data(index)=iter.value().toDouble();
	}
  	}

//Determine frequency in both dimensions
if (ch1>4 || ch2>4)
	throw Failed("read_spinsight: wrong channel in acq files");
par.sfrq=freq[ch1-1]; //direct frequency is a frequencty of channel 1
par.sfrq1=freq[ch2-1]; //direct frequency is a frequencty of channel 1
if (!par.sfrq1)
	par.sfrq1=par.sfrq; //probably homonuclear experiment

//The same thing for 'proc' file

  strcpy(parfname+strlen(parfname)-3,"proc");

//load parameters

  par.parameterFiles<<"proc";
  lPar.clear(); //loaded parameters
//  cout<<parfname<<"\n";
  fault=read_spinsight_parfile(parfname, lPar);
  if (fault)
	throw Failed("read_spinsight: proc file doesn't exist");
  par.allParameters.append(lPar);


//analyse parameters
if (lPar.contains("com"))
    par.title=lPar["com"].remove('\n');
if (lPar.contains("datatype")) {
	double value=lPar.value("datatype").toDouble();
	if (!(value==1.0 || value==0.0))
		throw Failed("read_spinsight: datatype in 'proc' file is not 0 or 1");
      if (value==1.0)  
	dataformat=FD_TYPE_SPE;
      else 
	dataformat=FD_TYPE_FID;
}

if (lPar.contains("domain1")) {
	double value=lPar.value("domain1").toDouble();
	if (!(value==1.0 || value==0.0))
		throw Failed("read_spinsight: domain1 in 'proc' file is not 0 or 1");
      if (value==1.0) par.type=FD_TYPE_SPE;
      else par.type=FD_TYPE_FID;
    }

if (lPar.contains("domain2")) {
	double value=lPar.value("domain2").toDouble();
	if (!(value==1.0 || value==0.0)) 
			throw Failed("read_spinsight: domain2 in 'proc' file is not 0 or 1");
      if (value==1.0) par.type1=FD_TYPE_SPE;
      else par.type1=FD_TYPE_FID;
    }
size_t ni=1,np;
if (lPar.contains("current_size1"))
      np=lPar.value("current_size1").toInt();
if (lPar.contains("current_size2"))
      ni=lPar.value("current_size2").toInt();
if (!ni)
	ni=1;
if (lPar.contains("rmp1"))
      rmp1=lPar.value("rmp1").toDouble();
if (lPar.contains("rmp2"))
      rmp2=lPar.value("rmp2").toDouble();
if (lPar.contains("rmv1"))
      rmv1=lPar.value("rmv1").toDouble();
if (lPar.contains("rmv2"))
      rmv2=lPar.value("rmv2").toDouble();
if (lPar.contains("rmvunits1"))
      rmvunits1=lPar.value("rmvunits1").toInt();
if (lPar.contains("rmvunits2"))
      rmvunits2=lPar.value("rmvunits2").toInt();

switch (rmvunits1) { //direct dimension
	case 1://ppm
                rmv1*=par.sfrq; //convert to Hz
		par.ref=rmv1+(par.sfrq-rmp1)*1e6;
		break;
	case 2://kHz
		par.ref=rmv1*1e3+(par.sfrq-rmp1)*1e6;
		break;
	case 5://Hz
		par.ref=rmv1+(par.sfrq-rmp1)*1e6;
		break;
}

switch (rmvunits2) { //indirect dimension
	case 1:
		rmv2*=par.sfrq1; //convert to kHz
		par.ref1=rmv2+(par.sfrq1-rmp2)*1e6;
		break;
	case 2:
		par.ref1=rmv2*1e3+(par.sfrq1-rmp2)*1e6;
		break;
	case 5:
		par.ref=rmv2+(par.sfrq1-rmp2)*1e6;
		break;
}

  strcpy(parfname+strlen(parfname)-4,"proc_setup");
  par.parameterFiles<<"proc_setup";
  lPar.clear(); //loaded parameters
//  cout<<parfname<<"\n";
  fault=read_spinsight_parfile(parfname, lPar);
  if (fault)
	throw Failed("read_spinsight: proc_setup file doesn't exist");
  par.allParameters.append(lPar);

//open data file
  pFile=fopen(fname, "rb");
  if (pFile==0) 
      throw Failed("read_spinsight: File 'data' is not found");
// obtain file size and reset to start 
  fseek(pFile, 0, SEEK_END);
  const int32_t lSize = ftell(pFile);
  rewind(pFile);
  if (lSize & 7) {
    fclose(pFile);
	throw Failed("read_spinsight: file length is not multiple of 8. Corrupt?");
  }

   if ((np*ni)!=lSize/8) {
//	fclose(pFile);
//	throw Failed("read_spinsight: file length is not match parameters. Corrupt?");
	int ret=QMessageBox::warning(0, "Warning", "File length doesn't match the parameters. Can be corrupted. Continue?",QMessageBox::Yes | QMessageBox::No);
	switch(ret){
	case QMessageBox::Yes:
		np=lSize/8;
		ni=1;
		break;
	case QMessageBox::No:
		fclose(pFile);
//		throw Failed("read_spinsight: file length is not match parameters. Aborted by user.");
		return 1;
		break;
	}
	}
// allocate memory to contain the whole file. 
  datat = (unsigned char*) malloc(lSize);
  if (datat==NULL) {
	fclose(pFile);
	throw Failed("read_spinsight: Cannot allocate memory for data from 'data' file");
	}
// copy the file into the buffer.
  fread(datat, 1, lSize, pFile);
  fclose(pFile);
// Change byte order if mashine is little-endian
  if (!ambigendian()) {
    unsigned char* ptr=datat;
    size_t i=0;
    for (i=lSize/4;i--;ptr+=4) {
      swap_bytes(ptr,ptr+3);
      swap_bytes(ptr+1,ptr+2);
    }
  }

  const int len=lSize/8;
  List<complex> templist;
  a.clear();
  if (dataformat==FD_TYPE_FID)  {
    int32_t* asint=(int32_t*)datat;
    complex tmp(0.0,0.0);
    for (int i=0; i<len; i++) {
      //real(tmp,asint[i]);
      //imag(tmp,asint[i+len]);
      tmp=complex(asint[i],asint[i+len]);
      templist.push_back(tmp);
    }
  }
  else {
    if (sizeof(float)!=4)
	throw Failed("read_spinsight: 'float' data type on this machine is incompatible with GSim: only raw FID could be loaded");
    float* asfloat=(float*)datat;
    complex tmp(0.0,0.0);
    for (int i=0; i<len; i++) {
      //real(tmp,asfloat[i]);
      //imag(tmp,asfloat[i+len]);
      tmp=complex(asfloat[i],asfloat[i+len]);
      templist.push_back(tmp);
    }
   }
  a.create(ni, np);
  for (size_t i=ni; i--;)
  for (size_t j=np; j--;)
	a(i,j)=templist(j+np*i);
  free(datat);
  qDebug()<<"Read Spinsight passed OK";
  return 0;
}

double SpinsightFileFilter::absmax(const BaseList<complex>& a) //use const if input unmodified
  // More flexible to pass row rather than matrix
{
  double t;
  double max=0.0;
  for (size_t i=a.size();i--;) {
    t=fabs(real(a(i)));
    if (t>max)
      max=t;
    t=fabs(imag(a(i)));
    if (t>max)
      max=t;
  }
  return max;
}

template <class T> void SpinsightFileFilter::write_out(const BaseList<complex>& data, int sc, ofstream& f)
{
  const size_t size(data.size());
  List<T> out(size*2); //make sure buffer has correct size

  for (size_t i=size;i--;) {
    out(i)= T(sc*real(data(i)));
    out(i+size)= T(sc*imag(data(i)));
  }

  if (!ambigendian()) {
    unsigned char* p=reinterpret_cast<unsigned char*>(out.vector());
    for (size_t i=size*2;i--;p+=4) {
      ::std::swap(p[0],p[3]);
      ::std::swap(p[1],p[2]);
    }
  }
  const char* ptr=reinterpret_cast<char*>(out.vector());
  f.write(ptr,sizeof(T)*size*2);
//   for (k=0; k<size*4; k++)
//     f<<ptr[k];
}

void SpinsightFileFilter::write_data(const cmatrix& data, const gsimFD& parameters, const char* dir)
{
  QDir Dir(dir);
  if (!Dir.exists()) {
	bool ok=Dir.mkpath(dir);
	if (!ok) qWarning("Can't create directory");
  }

//if (mkdir(dir, 511))
//    throw Failed("write_data: make directory"); //throw exception if create directory failed

  char datafile[LINE_MAX_LEN];
  snprintf(datafile,sizeof(datafile),"%s/data",dir); //snprintf is safer - strange things will happen if input is too long for LINE_MAX_LEN, but it's less likely to cause a disaster
  //cout<<datafile<<endl;
  ofstream filestr;
  filestr.open(datafile,  ofstream::binary | ofstream::out | ofstream::trunc);
  if (!filestr) 
    throw Failed("write_data: open file"); //only check once - if one file works, the others will
  //size_t size=2*data.size();
  //cout << size << "=" << parameters.ni << "*" << parameters.np <<endl;

  if (parameters.type==FD_TYPE_FID) //FID stored as integers
    {
      double ampl=absmax(data.row());
      int sc=1;
      if (ampl) { //check for zero data!
	double rough_sc=1000000/ampl;
	while (rough_sc>10){
	  rough_sc/=10;
	  sc*=10;}
      }
      write_out<int32_t>(data.row(), sc, filestr);
    }
  else
    write_out<float>(data.row(), 1, filestr);
  filestr.close();
}

void SpinsightFileFilter::write_parameters(gsimFD par, const char * dir, QList<Array_> arrays, size_t ni, size_t np)
{
  char tmpname[LINE_MAX_LEN];

  snprintf(tmpname,sizeof(tmpname),"%s/simpson.acq",dir);
  ofstream filestr;
  filestr.open(tmpname, ofstream::out | ofstream::trunc);
  if (!filestr)
    throw Failed("write_parameters: open file");
  filestr<<"ch1;ppg ch1;2;-;1;4;0;1;long\n";
  filestr<<"sf1;ch1 spect freq;100.6;MHz;1.0;800.5;7;1;float\n";
  filestr<<"dw;dwell;50;u;0.13;100000;2;1;float\n";
  filestr<<"sw;spectrum width;20;kHz;0.01;7693;3;1;float\n";
  filestr<<"al;acq length;512;-;2;1048576;0;1;long\n";
  if (ni>1) {
    filestr<<"ch2;ppg ch2;2;-;1;4;0;1;long\n";
    filestr<<"sf2;ch2 spect freq;100.6;MHz;1.0;800.5;7;1;float\n";
    filestr<<"dw2;dwell 2;50;u;0.13;100000;2;1;float\n";
    filestr<<"sw2;spectrum width 2;20;kHz;0.01;7693;3;1;float\n";
    filestr<<"al2;acq length 2;512;-;2;1048576;0;1;long\n";
  }
// Write Arrays if exists;
   for (size_t i=0; i<arrays.size(); i++) {
    filestr<<arrays.at(i).name.toLatin1().data()<<"; array "<<arrays.at(i).name.toLatin1().data()<<";1;m;0.000000000000001;9999999999999.9;3;1;float\n";
	}

  filestr.close();

  snprintf(tmpname,sizeof(tmpname),"%s/acq",dir);
  filestr.open(tmpname, ofstream::out | ofstream::trunc);
//  if (!par.pulprog.isEmpty())
//  	filestr<<"ppfn="<<par.pulprog.toLatin1().data()<<"\n";
//  else  //This code can make a mess if open a file in Spinsight
	filestr<<"ppfn=gsim\n";
  filestr<<"ch1=1\n";
  if (par.sfrq<0)
  	filestr<<"sf1=125.0\n";
  else
  	filestr<<"sf1="<<par.sfrq/1e6<<"\n";
  filestr<<"dw="<<1/par.sw<<"\n";
  filestr<<"sw="<<par.sw/1000<<"\n";
  filestr<<"al="<<np<<"\n";

  if (ni>1){
    filestr<<"ch2=2\n";
    if (par.sfrq1<0)
    	filestr<<"sf2=500.0\n";
   else
        filestr<<"sf2="<<par.sfrq1<<"\n";
    filestr<<"dw2="<<1/par.sw1<<"\n";
    filestr<<"sw2="<<par.sw1/1000<<"\n";
    filestr<<"al2="<<ni<<"\n";
  }

// Write Arrays if exists;
   for (size_t i=0; i<arrays.size(); i++) {

   filestr<<"array_num_values_"<<arrays.at(i).name.toLatin1().data()<<"="<<arrays.at(i).data.size()<<"\n";
   filestr<<"array_dim_"<<arrays.at(i).name.toLatin1().data()<<"=2\n";
   for (size_t j=0; j<arrays.at(i).data.size(); j++)
    filestr<<arrays.at(i).name.toLatin1().data()<<"["<<j<<"]="<<arrays.at(i).data(j)<<"m\n";
   }

  filestr.close();

  snprintf(tmpname,sizeof(tmpname),"%s/proc",dir);
  filestr.open(tmpname, ofstream::out | ofstream::trunc);

  filestr<<"com="<<par.title.toLatin1().data()<<"\n\n";
  filestr<<"datatype=" << ((par.type==FD_TYPE_FID)? 0 : 1) << '\n'; //integer 32-bit/float 32-bit
  filestr<<"domain1="<<((par.type==FD_TYPE_FID)? 0:1)<<'\n';//time/freq
  filestr<<"current_size1="<<np<<"\n";
  filestr<<"rmp1="<<par.sfrq<<"\n";//spectrum centre
  filestr<<"rmv1="<<par.ref/1000<<"\n";//ref at 0
  filestr<<"rmvunits1=2\n";//kHz
  if (ni>1){
    filestr<<"current_size2="<<ni<<"\n";
    filestr<<"domain2="<<((par.type1==FD_TYPE_FID)? 0:1)<<'\n';
    filestr<<"rmp2="<<par.sfrq1<<"\n";
    filestr<<"rmv2="<<par.ref1<<"\n";
    filestr<<"rmvunits2=2\n";
    }
  filestr.close();


  snprintf(tmpname,sizeof(tmpname),"%s/proc_setup",dir);
  filestr.open(tmpname, ofstream::out | ofstream::trunc);
  filestr<<"ft1=1\n";
  if (ni>1){
    filestr<<"psort1=1\n";
    filestr<<"ft2=1\n";
    }
  filestr.close();
}

void SpinsightFileFilter::write(const cmatrix& data, gsimFD parameters, const char* dir, QList<Array_> arrays)
{
//  if (parameters.ni==0)
//    parameters.ni=1;
  write_data(data, parameters, dir);
  write_parameters(parameters, dir, arrays,data.rows(),data.cols());
}

void SpinsightFileFilter::write(QList<data_entry>& data, QString& fname)
{
    write(data[0].spectrum, data[0].info, fname.toLatin1(), data[0].arrays);
	fname+="/data";
}

void SpinsightFileFilter::extract(const data_entry* dat, QStringList& all)
{
	all.clear();
	QHash<QString, QString> par;
		int q;
	q=-1; //will contain the index of proc parameter files
	for (size_t i=dat->info.parameterFiles.size(); i--;)
		if (dat->info.parameterFiles.at(i)=="proc_setup")
			q=i;
	if (q<0) {
		QMessageBox::critical(0, "Failed", "Parameters from 'proc_setup' file is not found");
		return;
	}
	par=dat->info.allParameters.at(q);
	if (par.contains("setsize_size1")) //Rearrange
		if (zToInt(par.value("setsize_size1"))>0)
			all<<"Rearrange"<<"*"<<par.value("setsize_size1");
	if (par.contains("bc1"))  //DC Offset
		all<<"DC Offset"<<"*"<<QString("10.0");
	if (par.contains("gm_gb1")) //LB (dir)
		if (par.value("gm_gb1").toDouble()>0.0)
			all<<"LB (dir)"<<"*"<<QString("gauss;")+par.value("gm_gb1");
	if (par.contains("em_lb1")) 
		if (par.value("em_lb1").toDouble()>0.0)
			all<<"LB (dir)"<<"*"<<QString("lorentz;")+par.value("em_lb1");
	if (zToInt(par.value("dl1"))) //Resize (dir)
		all<<"Resize (dir)"<<"*"<<par.value("dl1");
	if (par.contains("ft1"))//FT/FFT (dir)
		all<<"FT/FFT (dir)"<<"*"<<"<none>";
	if (par.contains("ph01")) //Phase (dir)
		if (par.value("ph01").toDouble()>0.0)
                        all<<"Phase (dir)"<<"*"<<par.value("ph01")+";"+par.value("ph11");
	if (dat->spectrum.rows()>1) {
	if (par.contains("gm_gb2"))  //LB (indir)
		if (par.value("gm_gb2").toDouble()>0.0)
			all<<"LB (indir)"<<"*"<<QString("gauss;")+par.value("gm_gb2");
	if (par.contains("em_lb2"))
		if (par.value("em_lb2").toDouble()>0.0)
			all<<"LB (indir)"<<"*"<<QString("lorentz;")+par.value("em_lb2");
	if (zToInt(par.value("dl2"))) //Resize (indir)
		all<<"Resize (indir)"<<"*"<<par.value("dl2");
        if (par.contains("ft2")){ //FT/FFT (indir)
		if (par.contains("psort1"))
                        all<<"FT/FFT (indir)"<<"*"<<"STATES";
		else if (par.contains("rcft") || par.contains("rrft")) 
			all<<"FT/FFT (indir)"<<"*"<<"TPPI";
		else
			all<<"FT/FFT (indir)"<<"*"<<"complex";
        }
	if (par.contains("ph02"))
		if (par.value("ph02").toDouble()>0.0)
			all<<"Phase (indir)"<<"*"<<par.value("ph02")+";"+par.value("ph12")+";0.5";
	} //end for 2D specific functions
	if (par.contains("mag1")) 
		all<<"Magnitude"<<"*"<<"<none>";
	return;
}

bool XwinnmrFileFilter::matching(const char* fname)
{/* Looking for files "fid" or "ser" and "acqu" */
     char testname[256];
     strcpy(testname,fname);
     if (strlen(testname)<3) return false;
     char* p=testname+strlen(testname)-3;
     if (strcmp(p,"fid") && strcmp(p,"ser")) {
       return false;
     }
     if (!isreadable(testname))
       return false;
     strcpy(p,"acqu");
     return isreadable(testname);
}

QString XwinnmrFileFilter::short_filename(QString s)
{
//	s.remove(0, s.lastIndexOf(QRegExp("[/\\\\]"))+1);
	if (s.contains(QChar('/')) || s.contains(QChar('\\'))) {
		s.remove(s.size()-4,4);
		int ind=s.lastIndexOf(QRegExp("[/\\\\]"));
		s[ind]=QChar('_');
		s.remove(0, s.lastIndexOf(QRegExp("[/\\\\]"))+1);
	}
	return s;
}

int XwinnmrFileFilter::read(cmatrix &a, gsimFD &par, QList<Array_> &arrays, const char* fname)
{
	read_xwinnmr_arrays(arrays, fname);
	int result= read_xwinnmr(a,par,fname);
	return result;
}

void XwinnmrFileFilter::read_xwinnmr_array(Array_ &array, QFile* file)
{
        while (!file->atEnd()) {
            QString line = QString(file->readLine());
            line.remove(QChar(' '));
            line.remove(QChar('\n'));
            double multiplier=1.0;
            int ind = line.indexOf(QRegExp("[smu]"));
            while ((ind < line.size()) && (ind>0)){
                QString single_value = line.left(ind+1);
                if (single_value.at(single_value.size()-1)==QChar('s')) {
                    multiplier=1.0e3;
                    single_value.truncate(single_value.size()-1);
                }
                else if (single_value.at(single_value.size()-1)==QChar('m')) {
                    multiplier=1.0;
                    single_value.truncate(single_value.size()-1);
                }
                else if (single_value.at(single_value.size()-1)==QChar('u')) {
                    multiplier=1.0e-3;
                    single_value.truncate(single_value.size()-1);
                }
                bool convOK=true;
                array.data.push_back(single_value.toDouble(&convOK)*multiplier);
                if (!convOK) {
                 file->close();
                 throw Failed("read_bruker: array file is corrupted");
                }
                line = line.right(line.size()-ind-1);
                ind = line.indexOf(QRegExp("[smu]"));
            }

         }
}

void XwinnmrFileFilter::read_xwinnmr_arrays(QList<Array_> &arrays, const char* fname)
{
	QStringList names;
	names<<"valist"<<"vclist"<<"vdlist"<<"vplist"<<"vtlist"<<"fqlist"<<"masrlist";
	for (size_t i=names.size(); i--;) {
		QString arfname=fname;
		arfname.chop(3);
		arfname+=names.at(i);
//		qDebug()<<"Looking for"<<arfname;
		QFile va(arfname);
        	if (va.open(QIODevice::ReadOnly | QIODevice::Text)) {
            Array_ ar(names.at(i).toLatin1().data());
			read_xwinnmr_array(ar, &va);
			va.close();
			arrays.push_back(ar);
		}
	}
}

int XwinnmrFileFilter::read_xwinnmr_parfile(QString fname, QHash<QString, QString> &pars) //don't forget to use parameterFiles separately
{
	QFile file(fname);
        if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
            return 1; //file doesn't exist
        QTextStream in(&file);
        while (!in.atEnd()) {
            QString line = in.readLine();
            if (line.contains("=")) {
		QStringList buf=line.split("=", QString::SkipEmptyParts);
		buf[0].remove("##$");
		if (buf.size()<2)
			pars[buf.at(0)]=QString("");
		else
			if (buf.at(1).contains("(") && buf.at(1).contains(")") && buf.at(1).contains("..")) //arrayed data as P or D
				pars[buf.at(0)]=in.readLine();
			else
				pars[buf.at(0)]=buf.at(1);
	    }
        }
	file.close();
	return 0;
}


int XwinnmrFileFilter::read_xwinnmr(cmatrix &a, gsimFD &par, const char* fname)
/*function for reading of Bruker XWIN-NMR FIDs. FID stores in "fid" or "ser" file in binary format.
4 bytes per each (real or imag) point in big-endian format (as integer). Real and imaginary part is interlacing.
adopted partly from MatNMR of J. vanBeek. Thanks!*/
{
  FILE* pFile;
//  FILE* acqFile;
size_t np, ni=1;

  unsigned char* datat;
  char parfname[256];
//  par.ni=1; //default value
  par.sw1=1.0;
  par.type=FD_TYPE_FID;

//Reading acq file
  strcpy(parfname,fname);
  strcpy(parfname+strlen(parfname)-3,"acqu");
//  cout<<parfname<<endl;
//load parameters
 
  par.parameterFiles<<"acqu";
  QHash<QString, QString> lPar; //loaded parameters
  int fault=read_xwinnmr_parfile(parfname, lPar);
  if (fault) {
	throw Failed("read_bruker: acqu file doesn't exist");
  }
  par.allParameters.append(lPar);

//analysing parameters
if (lPar.contains("SW_h"))
      par.sw=lPar.value("SW_h").toDouble();
if (lPar.contains("SFO1"))
      par.sfrq=lPar.value("SFO1").toDouble();
if (lPar.contains("PULPROG")) {
      par.pulprog=lPar.value("PULPROG");
      par.pulprog.remove('\n');
      par.pulprog.remove('>');
      par.pulprog.remove('<');
}
if (lPar.contains("MASR"))
      par.spinrate=lPar.value("MASR").toDouble();
if (lPar.contains("TE"))
      par.temperature=lPar.value("TE").toDouble();

//The same thing for 'acqu2' file
//Reading acqu2 file
  strcpy(parfname+strlen(parfname)-4,"acqu2");

  par.parameterFiles<<"acqu2";
  lPar.clear(); //loaded parameters
  fault=read_xwinnmr_parfile(parfname, lPar);

if (!fault) {
  par.allParameters.append(lPar);


//analysing parameters
if (lPar.contains("SW_h"))
      par.sw1=lPar.value("SW_h").toDouble();
if (lPar.contains("SFO1"))
      par.sfrq1=lPar.value("SFO1").toDouble();
if (lPar.contains("TD"))
      ni=lPar.value("TD").toInt();
}
else
	par.parameterFiles.removeLast();	

//The same thing for 'acqus' file which needed for digital filtered data
//Reading acqus file
  strcpy(parfname+strlen(parfname)-5,"acqus");

  par.parameterFiles<<"acqus";
  lPar.clear(); //loaded parameters
  fault=read_xwinnmr_parfile(parfname, lPar);
//  cout<<parfname<<endl;
if (!fault)
  par.allParameters.append(lPar);
else
  par.parameterFiles.removeLast();

int bytorda=1;
if (lPar.contains("BYTORDA"))
      bytorda=lPar.value("BYTORDA").toInt();

//print_parameters(par);
//trying to read a title from specified file
  strcpy(parfname,fname);
//  QString pfname;

  QFileInfo finfo(parfname); //create File info
  QDir curdir=finfo.absoluteDir(); //take a directory name
   if (curdir.cd("pdata"))  //if possible to move to pdata directory
  {
  QStringList savedproc=curdir.entryList(QDir::Dirs|QDir::NoDotAndDotDot, QDir::Name);
  if (savedproc.size()) {
  QString procdir;
  if (savedproc.size()>1)
  	procdir=QInputDialog::getItem(0, "Select Bruker processed data", "Several processed data are found, please select one", savedproc);
  else
	procdir=savedproc.at(0);
//  strcpy(parfname+strlen(parfname)-5,"pdata/"); //looking for 'title' file for proc no.1
    QString pfname=curdir.absolutePath()+"/"+procdir+"/title";
//  strcat(parfname,procdir.toLatin1().data());
//  strcat(parfname,"/title");
//  cout<<pfname.toLatin1().data()<<endl;

  QFile textFile(pfname);
        if (textFile.open(QIODevice::ReadOnly | QIODevice::Text)) {
		QTextStream in2(&textFile);
		par.title=QString("proc/1: ")+in2.readLine();
		par.title.remove(QChar('\n'));
	}
  textFile.close();
  pfname=curdir.absolutePath()+"/"+procdir+"/proc";
//    cout<<"File:"<<pfname.toLatin1().data()<<endl;
  par.parameterFiles<<"proc";
  lPar.clear(); //loaded parameters
    fault=read_xwinnmr_parfile(pfname.toLatin1().data(), lPar);
//  cout<<parfname<<endl;
if (!fault) {
  par.allParameters.append(lPar);
  cout<<"done\n";}
else
  par.parameterFiles.removeLast();
//if (lPar.contains("OFFSET"))
//      par.ref=lPar.value("OFFSET").toDouble()*par.sfrq-par.sw/2.0;

//  if (lPar.contains("SF"))
//	par.ref=(lPar.value("SF").toDouble()-par.sfrq)*1e6;

  pfname=curdir.absolutePath()+"/"+procdir+"/proc2";
  par.parameterFiles<<"proc2";
  //cout<<"File 2:"<<pfname.toLatin1().data()<<endl;
  lPar.clear(); 
    fault=read_xwinnmr_parfile(pfname.toLatin1().data(), lPar);
//  cout<<parfname<<endl;
if (!fault) {
  par.allParameters.append(lPar);
  //cout<<"done again\n";
	}
else
  par.parameterFiles.removeLast();
//if (lPar.contains("OFFSET"))
 //     par.ref1=lPar.value("OFFSET").toDouble()*par.sfrq1-par.sw1/2.0;
  if (lPar.contains("SF"))
	par.ref1=(par.sfrq1-lPar.value("SF").toDouble())*1e6; 
// loading procs file 
  pfname=curdir.absolutePath()+"/"+procdir+"/procs";
  par.parameterFiles<<"procs";
  //cout<<"File 3:"<<pfname.toLatin1().data()<<endl;
  lPar.clear(); 
    fault=read_xwinnmr_parfile(pfname.toLatin1().data(), lPar);
//  cout<<parfname<<endl;
if (!fault) {
  par.allParameters.append(lPar);
  //cout<<"done again\n";
	}
else
  par.parameterFiles.removeLast();
  if (lPar.contains("SF"))
	par.ref=(par.sfrq-lPar.value("SF").toDouble())*1e6;

//loading procs2
  pfname=curdir.absolutePath()+"/"+procdir+"/procs2";
  par.parameterFiles<<"procs2";
  //cout<<"File 4:"<<pfname.toLatin1().data()<<endl;
  lPar.clear(); 
    fault=read_xwinnmr_parfile(pfname.toLatin1().data(), lPar);
//  cout<<parfname<<endl;
if (!fault) {
  par.allParameters.append(lPar);
  //cout<<"done again\n";
	}
else
  par.parameterFiles.removeLast();
  if (lPar.contains("SF"))
	par.ref1=(par.sfrq1-lPar.value("SF").toDouble())*1e6;

  }

  }
//open data file
  pFile=fopen(fname, "rb");
  if (pFile==0)
	throw Failed("read_bruker: 'ser' or 'fid' file is not found");
  /* obtain file size and reset to start */
  fseek(pFile, 0, SEEK_END);
  const long lSize = ftell(pFile);
//  cout<<"Size:"<<lSize<<endl;
//  cout<<"NI:"<<par.ni<<endl;
  rewind(pFile);
  if (lSize & 7) {
    fclose(pFile);
    throw Failed("read_bruker: file length is not multiple of 8. Corrupt?");
  }
  np=lSize/8/ni;
  //cout<<"NP:"<<np<<endl;
   if ((np*ni)!=lSize/8) {
	fclose(pFile);
	throw Failed("read_bruker: file length is not match parameters. Corrupt?");
	}
  datat = (unsigned char*) malloc(lSize);
  if (datat==NULL) 
	throw Failed("read_bruker: Cannot allocate memory for data from 'fid'/'ser' file");

// copy the file into the buffer. 
  fread(datat, 1, lSize, pFile);
  fclose(pFile);
  if (!ambigendian()==bytorda) {
    unsigned char* ptr=datat;
    size_t i=0;
    for (i=lSize/4;i--;ptr+=4) {
      swap_bytes(ptr,ptr+3);
      swap_bytes(ptr+1,ptr+2);
    }
  }

  const int len=lSize/4; //for bytes per each number
  List<complex> templist;
  a.clear();
    int32_t* asint=(int32_t*)datat;
    complex tmp(0.0,0.0);
    for (int i=0; i<len; i=i+2) {
      //real(tmp,asint[i]);
      //imag(tmp,asint[i+1]);
      tmp=complex(asint[i],asint[i+1]);
      templist.push_back(tmp);
    }
  a.create(ni, np);
  for (size_t i=ni; i--;)
  for (size_t j=np; j--;)
	a(i,j)=templist(j+np*i);
  free(datat);
  return 0;
}

void XwinnmrFileFilter::extract(const data_entry* dat, QStringList& all)
{
	all.clear();
	QHash<QString, QString> par;
		int q;
	q=-1; //will contain the index of proc parameter files
	for (size_t i=dat->info.parameterFiles.size(); i--;)
		if (dat->info.parameterFiles.at(i)=="proc")
			q=i;
	if (q<0) {
		QMessageBox::critical(0, "Failed", "Parameters from 'proc' file is not found");
		return;
	}
	par=dat->info.allParameters.at(q);

	if (par.contains("SI"))  //DC Offset
		all<<"Resize (dir)"<<"*"<<par.value("SI");
	if (par.contains("BC_mod"))  //DC Offset
		if (par.value("BC_mod").toInt()!=0)
			all<<"DC Offset"<<"*"<<QString("10.0");
	if (par.contains("WDW"))  //LB
		if (par.value("WDW").toInt()==1)
			if (par.contains("LB"))
				all<<"LB (dir)"<<"*"<<"bruker_lorentz;"+par.value("LB");
	all<<"auto Bruker"<<"*"<<QString("<none>");
//	if (par.contains("FT_mod"))  //LB
//		if (par.value("FT_mod").toInt())
			all<<"FT/FFT (dir)"<<"*"<<"<none>";
	if (par.contains("PH_mod"))  {//phasing
		if (par.value("PH_mod").toInt()==1)
                        all<<"Phase (dir)"<<"*"<<QString("%1;").arg(180+par.value("PHC0").toDouble())+QString("%1").arg(-1*par.value("PHC1").toDouble());
		if (par.value("PH_mod").toInt()==2)
			all<<"Magnitude"<<"*"<<"<none>";
	}
	//indirect dimension
	par.clear();
	q=-1; //will contain the index of proc2 parameter files
	for (size_t i=dat->info.parameterFiles.size(); i--;)
		if (dat->info.parameterFiles.at(i)=="proc2")
			q=i;
	if (q<0) 
		return;  // 1D dataset only
	
	par=dat->info.allParameters.at(q);
	if (par.contains("SI"))  //DC Offset
		all<<"Resize (indir)"<<"*"<<QString("%1").arg(par.value("SI").toInt()*2);

	if (par.contains("WDW"))  //LB
		if (par.value("WDW").toInt()==1)
			if (par.contains("LB"))
				all<<"LB (indir)"<<"*"<<"lorentz;"+par.value("LB");

	if (par.contains("FT_mod"))  {//FT
		if (par.value("MC2").toInt()==0)
			all<<"FT/FFT (indir)"<<"*"<<"complex";
		if (par.value("MC2").toInt()==2)
			all<<"FT/FFT (indir)"<<"*"<<"TPPI";
		if (par.value("MC2").toInt()==3)
			all<<"FT/FFT (indir)"<<"*"<<"STATES";
		if (par.value("MC2").toInt()==4)
			all<<"FT/FFT (indir)"<<"*"<<"STATES_TPPI";	
	}
	if (par.contains("PH_mod")) { //phasing
		if (par.value("PH_mod").toInt()==1)
                        all<<"Phase (indir)"<<"*"<<QString("%1;").arg(180+par.value("PHC0").toDouble())+QString("%1").arg(-1*par.value("PHC1").toDouble());
		if (par.value("PH_mod").toInt()==2)
			all<<"Magnitude"<<"*"<<"<none>";
	}
}


bool XwinnmrprocFileFilter::matching(const char* fname)
{/* Looking for files "fid" or "ser" and "acqu" */
	QString fn=QString(fname);
	if (!QFile::exists(fn))
		return false;
	if (!fn.endsWith("1r"))
		return false;

	fn.chop(2);
	fn+="1i";
	if (!QFile::exists(fn))
		return false;
	fn.chop(2);
	fn+="proc";
	if (!QFile::exists(fn))
		return false;
	return true;
}

QString XwinnmrprocFileFilter::short_filename(QString s)
{
		if (s.contains(QChar('/')) || s.contains(QChar('\\'))) {
		if (s.endsWith("1r"))
			s.chop(3); //remove '/1r'
		else if (s.endsWith("1rr"))
			s.chop(4);
		s.remove("pdata/");
		int ind=s.lastIndexOf(QRegExp("[/\\\\]"));
		s[ind]=QChar('_');
		ind=s.lastIndexOf(QRegExp("[/\\\\]"));
		s[ind]=QChar('_');
		s.remove(0, s.lastIndexOf(QRegExp("[/\\\\]"))+1);
		}
	return s;
}

int XwinnmrprocFileFilter::read(cmatrix &a, gsimFD &par, QList<Array_> & , const char* fname)
//function for reading of Bruker XWIN-NMR processing data. Real and imaginary parts stored in 1r and 1i files in binary format. 8 bytes per each (real or imag) point in big-endian format (as integer 64). Could have a big-end li-and problem
{
  FILE* pFile;
  FILE* pFile2;
//  FILE* acqFile;
size_t np, ni;

  unsigned char* datat;
  unsigned char* datat2;
  char parfname[256];
  ni=1; //default values
  par.sw1=1.0;
  par.type=FD_TYPE_SPE;
  par.file_filter=this;

//Reading proc file
  strcpy(parfname,fname);
  strcpy(parfname+strlen(parfname)-2,"proc");
//load parameters
  par.parameterFiles<<"proc";
  QHash<QString, QString> lPar; //loaded parameters
  int fault=read_xwinnmr_parfile(parfname, lPar);
  if (fault) {
	throw Failed("read_bruker: proc file doesn't exist");
  }
  par.allParameters.append(lPar);
//analysing parameters
//if (lPar.contains("SF"))
//      par.sfrq=lPar.value("SF").toDouble();
//if (lPar.contains("SW_p"))
//     par.sw=lPar.value("SW_p").toDouble();
//if (lPar.contains("OFFSET"))
//      par.ref=lPar.value("OFFSET").toDouble()*par.sfrq-par.sw/2.0;


  strcpy(parfname+strlen(parfname)-4,"procs");
//load parameters
  par.parameterFiles<<"procs";
  lPar.clear();
  double ymax_p;
  fault=read_xwinnmr_parfile(parfname, lPar);
  if (!fault) {
  par.allParameters.append(lPar);
//analysing parameters
if (lPar.contains("SF"))
      par.sfrq=lPar.value("SF").toDouble();
if (lPar.contains("SW_p"))
      par.sw=lPar.value("SW_p").toDouble();
if (lPar.contains("OFFSET"))
      par.ref=lPar.value("OFFSET").toDouble()*par.sfrq-par.sw/2.0;
if (lPar.contains("YMAX_p")) //read scaling coefficient
      ymax_p=lPar.value("YMAX_p").toDouble();
}



//trying to read first title
  strcpy(parfname,fname); //read title from 'text' file
  strcpy(parfname+strlen(parfname)-4,"title"); //looking for 'title' file for proc no.1
  QFile textFile(parfname);
        if (textFile.open(QIODevice::ReadOnly | QIODevice::Text)) {
		QTextStream in2(&textFile);
		par.title=in2.readLine();
		par.title.remove(QChar('\n'));
	}
  textFile.close();


//open 1r file
  pFile=fopen(fname, "rb");
  if (pFile==0)
	throw Failed("read_bruker: '1r' file is not found");
  /* obtain file size and reset to start */

  char fname2[256];
  strcpy(fname2, fname);
  strcpy(fname2+strlen(fname)-2,"1i");
  pFile2=fopen(fname2, "rb");
  if (pFile2==0)
	throw Failed("read_bruker: '1i' file is not found");


  fseek(pFile, 0, SEEK_END);
  long lSize = ftell(pFile);
//  cout<<"Size:"<<lSize<<endl;
  rewind(pFile);
  if (lSize & 7) {
    fclose(pFile);
    throw Failed("read_bruker: file length is not multiple of 8. Corrupt?");
  }
   np=lSize/8;
//   cout<<"NP:"<<par.np<<endl;

  datat = (unsigned char*) malloc(lSize);
  datat2 = (unsigned char*) malloc(lSize);
  if ((datat==NULL) || (datat2==NULL)) 
	throw Failed("read_bruker: Cannot allocate memory for data from '1r'/'1i' file");

// copy the file into the buffer. 
  fread(datat, 1, lSize, pFile);
  fread(datat2, 1, lSize, pFile2);
  fclose(pFile);
  fclose(pFile2);
  if (!ambigendian()) {
    unsigned char* ptr=datat;
    unsigned char* ptr2=datat2;
    size_t i=0;
    for (i=lSize/8;i--;ptr+=8) {
      swap_bytes(ptr,ptr+3);
      swap_bytes(ptr+1,ptr+2);
      swap_bytes(ptr2,ptr2+3);
      swap_bytes(ptr2+1,ptr2+2);
    }
  }

  const int len=lSize/sizeof(int64_t);
  List<complex> templist;
  a.clear();
    int64_t* asint=(int64_t*)datat;
    int64_t* asint2=(int64_t*)datat2;
    complex tmp(0.0,0.0);
    for (int i=0; i<len; i++) {
      //real(tmp,asint[i]);
      //imag(tmp,asint2[i]);
      tmp=complex(asint[i],asint2[i]);
      templist.push_back(tmp);
    }
  a.create(ni, np);
  for (size_t i=ni; i--;)
  for (size_t j=np; j--;)
	a(i,j)=templist(j+np*i);
  free(datat);
  return 0;
}

bool VnmrFileFilter::matching(const char* fname)
{/* Looking for files "fid" or "data" and "procpar" */
	QString name=fname;
	if ((!name.endsWith("fid")) && (!name.endsWith("data")) && (!name.endsWith("phasefile")))
		return false;
	if (name.endsWith("fid"))
		namelength=3;
	else if  (name.endsWith("data"))
		namelength=4;
    else if  (name.endsWith("phasefile")) //phasefile
		namelength=9;
//	qDebug()<<"Name length:"<<namelength;
	name.chop(namelength);
	name+="procpar";
//	qDebug()<<"trying in the same dir"<<name;
	if (!QFile::exists(name)){
		name.chop(7);
		name+="../procpar";
//		qDebug()<<"trying in one directory up"<<name;
		if (!QFile::exists(name))
			return false;
	}
	return true;
 /*   
     char testname[256];
     strcpy(testname,fname);
     if (strlen(testname)<3) return false;
     char* p=testname+strlen(testname)-3;
     if (strcmp(p,"fid")) {
       p--;
       if (strcmp(p,"data"))
       	return false;
     }
     if (!isreadable(testname))
       return false;
     strcpy(p,"procpar");
     if (!isreadable(testname)){
     	strcpy(p,"../procpar");//try one level up
        if (!isreadable(testname))
		return false;
     }
     return true;*/
}

QString VnmrFileFilter::short_filename(QString s) 
{
	QStringList en=s.split(QRegExp("[/\\\\]"),QString::SkipEmptyParts);
        if (en.size()>1) {
	if (s.endsWith("fid"))
		s=(en.at(en.size()-2)=="acqfil")? en.at(en.size()-3) : en.at(en.size()-2);
	else {
		int upd=(en.size()>3)? 3 : 2;//for data end phasefile
		s=en.at(en.size()-upd);
            }
        }
	return s;
}

int VnmrFileFilter::read_vnmr_parfile(QString fname, QHash<QString, QString> &pars) //don't forget to use parameterFiles separately
{
	QFile file(fname);
        if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
            return 1; //file doesn't exist
        QTextStream in(&file);
        while (!in.atEnd()) {
            QString line = in.readLine();
	    if (line.at(0).isDigit() || line.at(0)==QChar('"'))
		continue;

	    QStringList values=line.split(" ", QString::SkipEmptyParts);
	    line=in.readLine();
	    line.remove('\n');
	    line.remove('"');
	    pars[values.at(0)]=line;
	    //in.readLine();
        }
	file.close();
	return 0;
}


int VnmrFileFilter::read(cmatrix &a, gsimFD &par, QList<Array_> &arrays, const char* fname)
/*function for reading of Varian VNMR FIDs. FID stores in "fid" file in binary format.
Adopted from MatNMR of J. vanBeek. Thanks!*/
{
	FILE* pFile;
	size_t np,ni;

	unsigned char* datat;
	char parfname[256];
	ni=1; //default value
	par.sw1=1.0;
	par.type=(namelength==3)? FD_TYPE_FID : FD_TYPE_SPE;
	QString refpos, refpos1;
	double reffrq=0, reffrq1=0;
	QStringList values;
	QStringList ArrayNames;

//Reading procpar or curpar file 
  
	strcpy(parfname,fname);
    if (par.type==FD_TYPE_SPE) //data is the processed spectrum - one need to go up from the 'datdir' directory
        strcpy(parfname+strlen(parfname)-7-namelength,"procpar"); //looking for 'procpar' file
    else
        strcpy(parfname+strlen(parfname)-namelength,"procpar"); //looking for 'procpar' file
	if (!isreadable(parfname)) {
        strcpy(parfname+strlen(parfname)-7,"../curpar");
		par.parameterFiles<<"curpar"; //from the 'experimental' directory read 'curpar' instead of 'procpar'
	}
	else {
		par.parameterFiles<<"procpar";
	}
  
	QHash<QString, QString> lPar; //loaded parameters
	int fault=read_vnmr_parfile(parfname, lPar);
	if (fault)
		throw Failed("read_vnmr: 'procpar' file doesn't exist");
	par.allParameters.append(lPar);

//analysing parameters	
	if (lPar.contains("SW_h"))
		par.sw=lPar.value("SW_h").toDouble();

//read arrays
	if (lPar.contains("array")) {
		values=lPar.value("array").split(" ");
		for (int i=1; i<values.size(); i++)
			ArrayNames<<values.at(i);
	}

	for (int q=0; q<ArrayNames.size(); q++) 
	{
		if (lPar.contains(ArrayNames.at(q))) {
//			cout<<"Array "<<ArrayNames.at(q).toLatin1().data()<<endl;
			QString line=lPar.value(ArrayNames.at(q));
			line.remove('\n');
			values=line.split(" ");
			Array_ ar(ArrayNames.at(q));
			for (int i=1; i<=values.at(0).toInt(); i++)
				ar.data.push_back(values.at(i).toDouble());
			arrays.push_back(ar);
		}
	}

//interprete spectral parameters
	
	if (lPar.contains("sfrq")) {
		values=lPar.value("sfrq").split(" ");
		par.sfrq=values.at(1).toDouble();
	}

	if (lPar.contains("dfrq")) {
		values=lPar.value("dfrq").split(" ");
		par.sfrq1=values.at(1).toDouble();
	}

	if (!par.sfrq)
		par.sfrq=par.sfrq1;  //Strange but working

 	if (lPar.contains("sw")) {
		values=lPar.value("sw").split(" ");
		par.sw=values.at(1).toDouble();
	}

	if (lPar.contains("sw1")) {
		values=lPar.value("sw1").split(" ");
		par.sw1=values.at(1).toDouble();
	}

	if (lPar.contains("refsource1")) { //source for the referencing in indirect dimension (dfrq - second freq, sfrq - first)
		values=lPar.value("refsource1").split(" ");
		if (values.at(1)!="""dfrq""")
		par.sfrq1=par.sfrq;
	}

	if (lPar.contains("seqfil")) {
		values=lPar.value("seqfil").split(" ");
		par.pulprog=values.at(1);
		par.pulprog.remove('"');
	}

//reffrq based referencing

	if (lPar.contains("reffrq")) {
		values=lPar.value("reffrq").split(" ");
		reffrq=values.at(1).toDouble();
	}

	if (lPar.contains("reffrq1")) {
		values=lPar.value("reffrq1").split(" ");
		reffrq1=values.at(1).toDouble();
	}



/* rfp and rfl based referencing (rather example)
double rfp, rfl, rfp1, rfl1;

if (lPar.contains("rfp") && lPar.contains("rfl")) {
	values=lPar.value("rfp").split(" ");
	rfp=values.at(1).toDouble();
	values=lPar.value("rfl").split(" ");
	rfl=values.at(1).toDouble();
}
*/

	if (lPar.contains("refpos")) {
		values=lPar.value("refpos").split(" ");
		refpos=values.at(1);
	}

	if (lPar.contains("refpos1")) {
		values=lPar.value("refpos1").split(" ");
		refpos1=values.at(1);
	}

	if ((!refpos.isEmpty()) && (refpos!="n")){
		par.ref=-(reffrq-par.sfrq)*1e6;
//		par.ref=rfp-rfl+par.sw/2; //for rfp,rfl based referencing
	}

	if ((!refpos1.isEmpty()) && (refpos1!="n"))
		par.ref1=-(reffrq1-par.sfrq1)*1e6;	
	
	if (lPar.contains("srate")) {
		values=lPar.value("srate").split(" ");
		par.spinrate=values.at(1).toDouble();
	}

	if (lPar.contains("temp")) {
		values=lPar.value("temp").split(" ");
		par.temperature=values.at(1).toDouble();
	}

	strcpy(parfname,fname); //read title from 'text' file
	strcpy(parfname+strlen(parfname)-3,"text"); //looking for 'text' file
	QFile textFile(parfname);
	if (textFile.open(QIODevice::ReadOnly | QIODevice::Text)) {
		QTextStream in2(&textFile);
		par.title=in2.readLine();
		par.title.remove(QChar('\n'));
	}
	textFile.close(); 
/*open data file*/
	pFile=fopen(fname, "rb");
	if (pFile==0) {
		throw Failed("read_vnmr: File 'fid' is not found");
//		QMessageBox::critical(0, "read_vnmr","File 'fid' is not found");
//		return 1;
  	}
/* obtain file size and reset to start */
	fseek(pFile, 0, SEEK_END);
	const long lSize = ftell(pFile);
	rewind(pFile);

/* allocate memory to contain the whole file. */
	datat = (unsigned char*) malloc(lSize);
    if (datat==NULL) {
		throw Failed("read_vnmr: Cannot allocate memory for data from 'fid' file");
	}
/* copy the file into the buffer. */
	fread(datat, 1, lSize, pFile);
	fclose(pFile);
/* Change byte order if machine is little-endian*/
	if (!ambigendian()) {
		unsigned char* ptr=datat;
		size_t i=0;
		for (i=lSize/4;i--;ptr+=4) {
			swap_bytes(ptr,ptr+3);
			swap_bytes(ptr+1,ptr+2);
		}
	}

//read in the Varian header
	int32_t* asint=(int32_t*)datat;
	int16_t* asint16 = (int16_t*)datat;
	int32_t  nblocks = asint[0];
	int32_t  ntraces = asint[1];
	np = asint[2];
//	int32_t  ebytes = asint[3];
//	int32_t  tbytes = asint[4];
//	int32_t  bbytes = asint[5];

	int16_t  vers_id;
	int16_t  status;

	if (!ambigendian()) { //not sure but seems it working!
		vers_id = asint16[13];
		status = asint16[12];
	}
	else {
		vers_id = asint16[12];
		status = asint16[13];
	}
	int32_t  nbheaders = asint[7];
		
	asint=asint+8;
	if (namelength==9) {
		nblocks=1;} //for phasefile only part of the data can be present
	else {
		ni=nblocks*ntraces;
	}
//	qDebug()<<"nblocks:"<<nblocks;
//	qDebug()<<"ntraces:"<<ntraces;
//	qDebug()<<"nbheaders"<<nbheaders;
//	qDebug()<<"np"<<np;
	List<double> onlydata;

/*    if (((status&0x4)!=0x4) && ((status&0x8) != 0x8))
      for (size_t i=0;i<nblocks; i++){
       for (size_t j=0; j<nbheaders; j++) {
        //fread(id, nbheaders*14, 'int16');	//read in the block headers (nbheaders*28 bytes)
	asint=asint+nbheaders*7;
	asint16=(int16_t *)asint;
       }
       for (size_t q=0; q<ntraces*np; q++)	//read in the actual data (ntraces*np)
		onlydata.push_back(*(asint16+q));
	}
*/

//    else if (((status&0x4)==0x4) && ((status&0x8) != 0x8))

//	qDebug()<<"Status:"<<status;

//	bool s_data=((status&0x1) == 0x1);
//	bool s_spec=((status&0x2) == 0x2);
//	bool s_32=((status&0x4) == 0x4);
//	bool s_float=((status&0x8) == 0x8);
//	bool s_complex=((status&0x10) == 0x10);
//	bool s_hypercomplex=((status&0x20) == 0x20);
//	bool s_acqpar=((status&0x80) == 0x80);
//	bool s_secnd=((status&0x100) == 0x100);
//	bool s_transf=((status&0x200) == 0x200);
//	bool s_np=((status&0x800) == 0x800);
//	bool s_nf=((status&0x1000) == 0x1000);
//	bool s_ni=((status&0x2000) == 0x2000);
//	bool s_ni2=((status&0x4000) == 0x4000);


//	qDebug()<<"data"<<s_data;
//	qDebug()<<"spectrum"<<s_spec;
//	qDebug()<<"32 bit"<<s_32;
//	qDebug()<<"float"<<s_float;
//	qDebug()<<"complex"<<s_complex;
//	qDebug()<<"hypercomplex"<<s_hypercomplex;
//	qDebug()<<"ACQ parameters"<<s_acqpar;
//	qDebug()<<"Second FT"<<s_secnd;
//	qDebug()<<"Transposed"<<s_transf;
//	qDebug()<<"NP active"<<s_np;
//	qDebug()<<"NF active"<<s_nf;
//	qDebug()<<"NI active"<<s_ni;
//	qDebug()<<"NI2 active"<<s_ni2;



	if ((status&0x8) != 0x8) {
//		qDebug()<<"status&0x8 true";
		for (size_t i=0;i<nblocks; i++){
			for (size_t j=0; j<nbheaders; j++) {
//			fread(id, nbheaders*14, 'int16');	//read in the block headers (nbheaders*28 bytes)
			asint=asint+nbheaders*7;
//			asint16=(int16_t *)asint;
			}
			for (size_t q=0; q<ntraces*np; q++)	//read in the actual data (ntraces*np)
				onlydata.push_back(*(asint+q));
			asint+=ntraces*np;
		}
	}
	else { 
//		qDebug()<<"status&0x8 false";
		float* asfloat=(float *)asint;
		for (size_t i=0;i<nblocks; i++){
			for (size_t j=0; j<nbheaders; j++) {
//				fread(id, nbheaders*14, 'int16');	//read in the block headers (nbheaders*28 bytes)
				asfloat=asfloat+nbheaders*7; //float supposed to have 4 bytes length
			}
			for (size_t q=0; q<ntraces*np; q++)	//read in the actual data (ntraces*np)
				onlydata.push_back(*(asfloat+q));
			asfloat+=ntraces*np;
		}
	}
	List<complex> templist;
	if (namelength==9) {//phasefile contains real data only
  		for (size_t i=0; i<onlydata.size(); i++)
			templist.push_back(complex(onlydata(i),0.0));
	}
	else {
		for (size_t i=0; i<onlydata.size()/2; i++)
		templist.push_back(complex(onlydata(2*i),onlydata(2*i+1)));
		np/=2;
	}
	a.create(ni, np);
//	for (size_t i=ni; i--;)
//	for (size_t j=np; j--;)
	for (size_t i=0; i<ni; i++)
		for (size_t j=0; j<np; j++)
			a(i,j)=templist(j+np*i);
	free(datat);
	return 0;
}

void VnmrFileFilter::write(QList<data_entry>& alldata, QString& fname)
{
	write_data(alldata[0],fname);
}

struct datafilehead {
	long nblocks;
	long ntraces;
	long np;
	long ebytes;
	long tbytes;
	long bbytes;
	short vers_id;
	short status;
	short nbheaders;
	datafilehead() {nblocks=0;ntraces=0;np=0;ebytes=0;tbytes=0;bbytes=0;vers_id=0;status=0;nbheaders=0;};
};

struct datablockhead {
	short scale;
	short status;
	short index;
	short mode;
	long ctcount;
	float lpval;
	float rpval;
	float lvd;
	float tlt;
	datablockhead() {scale=1; status=0; index=0; mode=0; ctcount=0; lpval=0; rpval=0; lvd=0; tlt=0;};
};

void VnmrFileFilter::write_data(data_entry& dat,QString& fname)
{
	datafilehead fh;
	fh.nblocks=1;
	fh.ntraces=dat.spectrum.rows();
	fh.np=dat.spectrum.cols();
	fh.ebytes=sizeof(float);
	fh.tbytes=fh.np*fh.ebytes;
	fh.bbytes=fh.ntraces*fh.tbytes+fh.nbheaders*sizeof(struct datablockhead);
	fh.vers_id=0;//??
	fh.nbheaders=1;
	fh.status=0x1|0x8 ;//contains data, data is float
        if (dat.info.type==FD_TYPE_SPE)
		fh.status+=0x2;
	datablockhead dh;
	dh.scale=1;
	dh.status=fh.status;
	QByteArray binary = QByteArray::fromRawData((char*) &fh, sizeof(struct datafilehead));
	binary.append(QByteArray::fromRawData((char*) &dh, sizeof(struct datablockhead)));
//	qDebug()<<"Headers are there";
	for (size_t i=0; i<fh.ntraces; i++)
	for (size_t j=0; j<fh.np; j++) {
		float re=float(real(dat.spectrum(i,j)));
		float im=float(imag(dat.spectrum(i,j)));
		binary.append(QByteArray::fromRawData((char*)&re,sizeof(float)));
		binary.append(QByteArray::fromRawData((char*)&im,sizeof(float)));
	}
//	qDebug()<<"data are there";
	QDir dir;
	dir.mkdir(fname);
	QString datafname=fname+"/fid";
//	qDebug()<<"Filename"<<datafname;
	QFile file(datafname);
	file.open(QIODevice::WriteOnly);
	file.write(binary);
	file.close();
}

void VnmrFileFilter::write_parfile(data_entry& dat,QString& fname)
{
	QString pars;
	if (dat.info.sw)
		pars+="";
}

QString SimpsonFileFilter::short_filename(QString s) 
{
	return s.remove(0, s.lastIndexOf(QRegExp("[/\\\\]"))+1);
}

bool SimpsonFileFilter::matching(const char* fname)
{       
	bool result=false;
	QFile file(fname);
        if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
            return false;
        QTextStream in(&file);
	QString line = QString(in.readLine());
	if (!line.contains(QString("SIMP")))
		result=false;
	else
		result=true;
	file.close();
	return result;
}

void SimpsonFileFilter::read(QList<data_entry>& data, QString fname)
{
	BaseFileFilter::read(data,fname);
	read_source(data[0],fname);
}

void SimpsonFileFilter::read_source(data_entry& dat, QString fname)
{
	QFile file(fname);
        if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
            return;
        QTextStream in(&file);
	bool endReached=false;
	QString source;
        while (!in.atEnd()) {
            QString line = in.readLine();
	    if (!endReached) {
		if (line.contains("END"))
			endReached=true;
	    }
	    else {
		source+="\n";
		source+=line;
	    }
        }
	file.close();
	if (source.isEmpty())
		return;
	QHash<QString, QString> lpar; //loaded parameters
	lpar[QString("source")]=source;
	dat.info.allParameters.append(lpar);
	dat.info.parameterFiles.append(short_filename(fname));
}

int SimpsonFileFilter::read(cmatrix & c, gsimFD & i, const char* f) 
{
int fault=0;
try {
	simpsonFD inf;
	inf.sfrq=0.0;
	inf.sfrq1=0.0;
	inf.ref=0.0;
	inf.ref1=0.0;
	read_simpson(c,inf,f);
//	for (int p=0; p<inf.ni; p++)
//	  for (int q=0; p<inf.np; q++)
//	    qDebug()<<real(c(p,q))<<imag(c(p,q));
	i.type=inf.type;
	i.sw=inf.sw;
	i.sw1=inf.sw1;
	i.sfrq=inf.sfrq;
        //Invert sign for negative frequncies (as happens for pNMRsim output for negative gamma nuclei)
        if (i.sfrq<0.0)
            i.sfrq=-i.sfrq;
	i.sfrq1=inf.sfrq1;
        if (i.sfrq1<0.0)
            i.sfrq1=-i.sfrq1;
	i.ref=inf.ref;
	i.ref1=inf.ref1;
//	qDebug()<<"Spec freq:"<<i.sfrq;
}
catch (MatrixException) {
	fault=1;
}
if (!fault)
	if (i.type==FD_TYPE_SPE) 
		invert(c, true);
//cout<<"Data ready\n";
return fault;
}

void SimpsonFileFilter::write(const cmatrix & c, gsimFD i, const char* f)
{
cmatrix n(c);
if (i.type==FD_TYPE_SPE) 
	invert(n, true);
write_simpson(f, n, i.sw, i.sw1, i.type==FD_TYPE_SPE);
}

bool SpinevolutionFileFilter::matching(const char* fname)
{       
	bool result=true;
	QFile file(fname);
        if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
            return false;
        QTextStream in(&file);
	QString line = QString(in.readLine());
	QStringList values=line.split(" ",QString::SkipEmptyParts);
	if (values.size()<2)
		result=false; //each string contains at least 1 number
	bool ok;
	for (size_t i=0; i<values.size(); i++) {
		values.at(i).toDouble(&ok); //all entries should be numbers
		if (!ok) result=false;
	}
	file.close();
	return result;
}

QString SpinevolutionFileFilter::short_filename(QString s) 
{
	return s.remove(0, s.lastIndexOf(QRegExp("[/\\\\]"))+1);
}

int SpinevolutionFileFilter::read(cmatrix &a, gsimFD &par, const char* fname)
{
	QFile file(fname);
        if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
            return 1;
	QList<QStringList> pts, pts_im;
	QTextStream in(&file);
	QString buffer;
        while (!in.atEnd()) {
            	buffer=in.readLine();
		pts.append(buffer.split(" ",QString::SkipEmptyParts));
	} //data is loaded

	file.close();
	
	QString name=fname;
	if (name.endsWith("_re.dat")){
		name.chop(7);
		name.append("_im.dat");
		QFile file_im(name);
		if (file_im.open(QIODevice::ReadOnly | QIODevice::Text)) {//Im file is open
			QTextStream in_im(&file_im);
	        	while (!in_im.atEnd()) {
        	    		buffer=in_im.readLine();
				pts_im.append(buffer.split(" ",QString::SkipEmptyParts));
			} //data is loaded

			file_im.close();
		}
	}
	if (!pts.size()) {
		QMessageBox::critical(0, "Error", "File is empty");
		return 1;
	}
	size_t ni=1,np;
	ni=pts.at(0).size()-1;
	if (!ni) {
		QMessageBox::critical(0, "Error", "File contains less then 2 columns");
		return 1;
	}

	np=pts.size();
	if (np<2) {
		QMessageBox::critical(0, "Error", "File contains less then 2 points");
		return 1;
	}

	a.create(ni,np,complex(0.0,0.0));
	bool hasIm=false;
	if (pts.size()==pts_im.size())
		if (pts_im.at(0).size() == pts.at(0).size())
			hasIm=true;
	for (size_t i=0; i<ni; i++)
	for (size_t j=0; j<np; j++) {
		if (pts.at(j).size()!=pts.at(0).size()) {
			QMessageBox::critical(0, "Error", "File has strings with different length");
			return 1;
		}
		bool ok;
		real(a(i,j),pts.at(j).at(i+1).toDouble(&ok));
		if (!ok) {
			QMessageBox::critical(0, "Error", "Cannot convert string to number");
			return 1;
		}
		if (hasIm){
			if (pts_im.at(j).size()!=pts.at(0).size()) {
				QMessageBox::critical(0, "Error", "Imaginary file has strings with different length");
				return 1;
			}
			imag(a(i,j),pts_im.at(j).at(i+1).toDouble(&ok));
			if (!ok) {
				QMessageBox::critical(0, "Error", "Cannot convert string in imaginary file to number");
				return 1;
			}
		}
//		cout<<i<<":"<<j<<endl;
		}
	par.type=FD_TYPE_SPE;
	par.type1=FD_TYPE_SPE;
	par.sw=fabs(pts.at(0).at(0).toDouble()-pts.at(pts.size()-1).at(0).toDouble());
	par.sw1=par.sw;
//	cout<<"sw="<<par.sw<<"\n";
	return 0;
}


bool CastepFileFilter::matching(const char* fname)
{
	bool result=true;
	QFile file(fname);
        if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
            return false;
        QTextStream in(&file);
	QString line1 = QString(in.readLine());
	QString line2 =  QString(in.readLine());
	if ((!line1.contains("==="))||(!line2.contains("Atom:")))
		result=false;
	file.close();
	return result;
}

QString CastepFileFilter::short_filename(QString s) 
{
	return s.remove(0, s.lastIndexOf(QRegExp("[/\\\\]"))+1);
}

void CastepFileFilter::averageShifts()
{
    if (castepData.idenAtoms.isEmpty())
        return;
    for (size_t k=0; k<castepData.idenAtoms.size(); k++)// a bit complicated check that ";" format treated properly
        for (size_t q; q<castepData.idenAtoms.at(k).size(); q++)
            if (castepData.extralabels.size())
                if(!castepData.idenAtoms.at(k).at(q).contains(":")){
                    throw Failed("read_castep: .magres file has X:n atom format: all atoms in averaging string should be in the same X:n format ");
                    return;
                }
    for (size_t k=0; k<castepData.idenAtoms.size(); k++) { //for each group, k- the group number
        size_t N=castepData.idenAtoms.at(k).size(); //number of atoms to be averaged
        QList<int> positions;
        for (size_t i=0; i<castepData.allNames.size();i++){
            QString currentAtom;
            if (castepData.extralabels.size()){
                currentAtom=QString(castepData.allNames.at(i)+":%1").arg(castepData.extralabels.at(i));
            }
            else{
                currentAtom=QString(castepData.allNames.at(i)+"%1").arg(castepData.indexes.at(i));
            }
            for (size_t q=0; q<N; q++){
                    if (castepData.idenAtoms.at(k).at(q)==currentAtom)
                        positions.append(i);
            }
        }
        if (positions.size()!=N){
            throw Failed("read_castep: some atoms from the averaging string are not found in the file");
            return;
        }
        double aviso=0.0; //average chemical shift
        for (size_t p=0; p<N;p++){
            aviso+=castepData.iso(positions.at(p));
        }
        aviso/=N;
        for (size_t p=0; p<N; p++){
           castepData.iso(positions.at(p))=aviso;
        }
    }
}

void CastepFileFilter::read(QList<data_entry>& list, QString fname)
{
	QFile file(fname);
        if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
            return;
	QTextStream in(&file);
	QString atom;
        QString extralabel;
	int index;
	double iso;
	castepData.availableAtomTypes.clear();
	castepData.allNames.clear();
	castepData.indexes.clear();
	castepData.iso.clear();
        castepData.extralabels.clear();
        castepData.multiplicity.clear();
        Array_ peaks("PeaksPos");
	bool ok;
	while (!in.atEnd()) {
		QString line=QString(in.readLine());
		if (line.contains("Isotropic")) { //new atom entry found
			QStringList s=line.split(" ", QString::SkipEmptyParts);
			if (s.size()<4)
				continue;
			atom=s.at(0);
//analyze if atom name contains ":"
                        if (atom.contains(":")){
                            int position = atom.indexOf(":");
                            extralabel=atom;
                            extralabel.remove(0,position+1);
                            atom.truncate(position);
                        }
			index=s.at(1).toInt(&ok);
			if (!ok)
				continue;
			iso=s.at(3).toDouble(&ok);
			if (!ok)
				continue;
			if (!castepData.availableAtomTypes.contains(atom))
				castepData.availableAtomTypes.append(atom);
                        int previous_index=-1;
     //check for an entry with same atom  name and extra label
                        if (castepData.extralabels.size()){ //if format contains extra labels
                              for (size_t i=0; i<castepData.allNames.size(); i++){
                                if ((atom==castepData.allNames.at(i))&&(extralabel==castepData.extralabels.at(i)))
                                    previous_index=i;
                            }
                        }
                        if (previous_index>=0){
                            castepData.multiplicity[previous_index]++;
                        }
                        else {
                            castepData.allNames.append(atom);
                            castepData.indexes.append(index);
                            castepData.iso.push_back(iso);
                            castepData.multiplicity.append(1);
                            if (!extralabel.isEmpty())
                                castepData.extralabels.append(extralabel);
                        }
		}
	}



//file reading finished, analise data
	data_entry dat;

	castepDialog* dialog = new castepDialog(NULL); //
	dialog->nucleiComboBox->insertItems(0,castepData.availableAtomTypes);
        dialog->readSettings();
	if (dialog->exec()==QDialog::Rejected) {
		throw Failed("read_castep: interupted by user");
		return;
	}
	QString type=dialog->nucleiComboBox->currentText();
	dat.info.sfrq=dialog->freqSpinBox->value();
	size_t np=dialog->npSpinBox->value();
	double correction=dialog->shiftSpinBox->value();
	double width=dialog->lwSpinBox->value();
        // read and prepare idenAtoms string:
        castepData.idenAtoms.clear();
        QString origIdenAtoms=dialog->idenAtomsLineEdit->text();
        QStringList semiIdenAtoms=origIdenAtoms.split(";", QString::SkipEmptyParts);
        for (size_t i=0; i<semiIdenAtoms.size(); i++) {
            QString temps=semiIdenAtoms.at(i);
            QStringList entryIdenAtoms=temps.split(",", QString::SkipEmptyParts);
            castepData.idenAtoms.append(entryIdenAtoms);
        }
	delete dialog;

        averageShifts();

	for (size_t i=0; i<castepData.iso.size(); i++)
		castepData.iso(i)=correction-castepData.iso(i);

	dat.spectrum.create(1,np,complex(0.0,0.0));
	dat.info.type=FD_TYPE_SPE;

	List<double> iso_sel; //selected shifts
	for (size_t i=0; i<castepData.iso.size(); i++)
                if (type==castepData.allNames.at(i)) {
			iso_sel.push_back(castepData.iso(i));
                        int test = -1;
                        for (size_t j=0; j<peaks.data.size(); j++)
                            if ((peaks.data[j] == castepData.iso(i)))
                                test=i;
                        if (test<0){
                            double peak_pos=convertUnits(castepData.iso(i),UNITS_PPM,data->global_options.xfrequnits,&dat.info, np, true, false);
                            peaks.data.push_back(peak_pos);
                        }
                }


	dat.info.sw=(max(iso_sel)-min(iso_sel))*dat.info.sfrq*1.1+10.0*width; //110 % of the cs-range + 10 linewidths specified
	dat.info.ref=(max(iso_sel)+min(iso_sel))*dat.info.sfrq*0.5;
        qSort(peaks.data.begin(),peaks.data.end(),qGreater<double>());
        dat.arrays.append(peaks);
        dat.display.hasPeaks=true;

        double res=dat.info.sw/np;
	for (size_t i=0; i<np; i++)
		dat.axis.xvector.push_back(dat.info.ref+dat.info.sw/2.0-i*res);

//	qDebug()<<"x-vector created";
	for (size_t i=0; i<castepData.allNames.size(); i++) {
		if (castepData.allNames.at(i)!=type)
			continue;


		size_t ind;

                double iso_hz=convertUnits(castepData.iso(i), UNITS_PPM, UNITS_HZ, &dat.info, np, true, false);
                int xi_=lround ((dat.axis.xvector(0)-iso_hz)/dat.info.sw*(np-1));
//		qDebug()<<"Xvector from "<<dat.axis.xvector(0)<<" to "<<dat.axis.xvector(np-1);
//		qDebug()<<"Xi"<<xi_;


                if (xi_<0) xi_=0;
		ind=size_t(xi_);
		if (xi_>=np) ind=np-1;
                List<double> line(np,0.0);
		for (size_t j=0; j<np; j++) {
			double delx=dat.axis.xvector(j)-dat.axis.xvector(ind);
                        double lor=1.0+(delx/(0.5*width))*(delx/(0.5*width));
                        dat.spectrum(0,j)+=castepData.multiplicity.at(i)*complex(1.0/lor,0.0);
		}

//                qDebug()<<"Line with shift"<<iso_hz<< "(Hz) has index "<<ind;
//                dat.spectrum(0,ind)+=complex(1.0,0.0);
		graph_object g;
		g.type=GRAPHTEXT;
                g.pos=QPointF(convertUnits(castepData.iso(i),UNITS_PPM,data->global_options.xfrequnits,&dat.info, np, true, false),-0.05);

                if (castepData.extralabels.size()>0)//Extra labels with ":" format present
                    g.maindata=QString(castepData.allNames.at(i)+":%1").arg(castepData.extralabels.at(i));
                else
                    g.maindata=QString(castepData.allNames.at(i)+"%1").arg(castepData.indexes.at(i));
		dat.graphics.append(g);
//		qDebug()<<"Line has been set";
	}
	list.clear();
	list.append(dat);
}


QString MatlabFileFilter::short_filename(QString s) 
{
	return s.remove(0, s.lastIndexOf(QRegExp("[/\\\\]"))+1);
}

bool MatlabFileFilter::matching(const char* fname) //very dumb, should be more specific
{       
	bool result=true;
	if (! QString(fname).contains(".mat"))
		return false;
	QFile file(fname);
        if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
            return false;
	file.close();
	return result;
}

template <class T> void MatlabFileFilter::read_data(T& cntrl, QList<data_entry>& data)
{
	data_entry entry;
	entry.info.type=FD_TYPE_SPE;
	entry.info.type1=FD_TYPE_SPE;//spectra by default as needed for 
	List<double> list;
	List<cmatrix> clist;
	List<char> string;
	cmatrix matrix;
	matlab_controller::header_info info;
	while (cntrl.peek(info)) {
//		cout<<"Analysing: "<<info.name<<endl;
		switch (info.type) {
			case matlab_controller::ARRAY:
                                //cout<<"This is array"<<endl;
				if ((QString(info.name)=="sw") && (!info.iscomplex))
					read_numbers(cntrl,&entry.info.sw,&entry.info.sw1);
				else if ((QString(info.name)=="dt") && (!info.iscomplex)) { //can occurs in pNMRsim produced files
					entry.info.type=FD_TYPE_FID;
					entry.info.type1=FD_TYPE_FID;
					double dt1, dt2;
					read_numbers(cntrl,&dt1,&dt2);
					entry.info.sw=(dt1)?1.0/dt1:10.0;
					entry.info.sw1=(dt2)?1.0/dt2:10.0;
					}
				else if ((QString(info.name)=="type") && (!info.iscomplex))
					read_numbers(cntrl,&entry.info.type,&entry.info.type1);
				else if ((QString(info.name)=="sfrq") && (!info.iscomplex)) 
					read_numbers(cntrl,&entry.info.sfrq,&entry.info.sfrq1);
				else if ((QString(info.name)=="ref") && (!info.iscomplex))
					read_numbers(cntrl,&entry.info.ref,&entry.info.ref1);
				else if ((QString(info.name)=="ph0") && (!info.iscomplex))
					read_numbers(cntrl,&entry.info.phd0,&entry.info.phi0);
				else if ((QString(info.name)=="ph1") && (!info.iscomplex))
					read_numbers(cntrl,&entry.info.phd1,&entry.info.phi1);
//				else if ((QString(info.name)=="xr") && (!info.iscomplex))
//					read_numbers(cntrl,&entry.info.xrd,&entry.info.xri);
				else if ((QString(info.name)=="spinrate") && (!info.iscomplex))
					read_numbers(cntrl,&entry.info.spinrate);
				else if ((QString(info.name)=="temperature") && (!info.iscomplex))
					read_numbers(cntrl,&entry.info.temperature);
				else if ((QString(info.name)=="scale") && (!info.iscomplex))
					read_numbers(cntrl,&entry.display.xscale,&entry.display.vscale);
				else if ((QString(info.name)=="shift") && (!info.iscomplex))
					read_numbers(cntrl,&entry.display.xshift,&entry.display.vshift);
				else if ((QString(info.name)=="is_real") && (!info.iscomplex))
					read_numbers(cntrl,&entry.display.isRealOn);
				else if ((QString(info.name)=="is_visible") && (!info.iscomplex))
					read_numbers(cntrl,&entry.display.isVisible);
				else if ((QString(info.name)=="has_peaks") && (!info.iscomplex))
					read_numbers(cntrl,&entry.display.hasPeaks);
				else if ((QString(info.name)=="hyperdata") && (info.iscomplex))
					cntrl.read(entry.hyperspectrum);
				else if ((QString(info.name)=="data") && (info.iscomplex)){
                                        //cout<<"Trying to read data"<<endl;
					cntrl.read(entry.spectrum);
					hasData=true;
//					entry.info.np=entry.spectrum.cols();
//					entry.info.ni=entry.spectrum.rows();
                                        //cout<<"NP:"<<entry.spectrum.cols()<<endl;
                                        //cout<<"NI:"<<entry.spectrum.rows()<<endl;
				}
				else {
					cntrl.read(list);
                                        //cout<<"Value "<<info.name<<" will be ignored\n";
					}
				break;
			case matlab_controller::CHAR:
                                //cout<<"This is char"<<endl;
				if (QString(info.name)=="title") {
					cntrl.read(string);
					entry.info.title=string.vector();
				}
				else if (QString(info.name)=="pulprog") {
					cntrl.read(string);
					entry.info.pulprog=string.vector();
				}
				else if (QString(info.name)=="processing") {
					cntrl.read(string);
					entry.info.processing=string.vector();
//					qDebug()<<"processing:"<<entry.info.processing;
				}
				else
					cntrl.next();
				break;
			case matlab_controller::STRUCT:
                                //cout<<"This is structure"<<endl;
				if (QString(info.name).startsWith("graphics_object")) {
					matlab_controller::composite gr(cntrl);
					read_graphics(gr,entry);
				}
				else if (QString(info.name)=="arrays") {
					matlab_controller::composite arr(cntrl);
					read_arrays(arr,entry);
				}
				else if (QString(info.name)=="params") {
					matlab_controller::composite par(cntrl);
					read_params(par,entry);
				}
				else if (QString(info.name)=="integrals") {
					matlab_controller::composite integ(cntrl);
					read_integrals(integ,entry);
				}
				else
					cntrl.next();
				break;
			default:
                                //cout<<"Unknown type\n";
				cntrl.next();
				break;
		}
	}
	if (hasData)
		data.push_back(entry);
	else
		throw Failed("read_matlab: no data present in Matlab file");
}

void MatlabFileFilter::read_arrays(matlab_controller::composite& ctr, data_entry& inst)
{
	matlab_controller::header_info info;
	while (ctr.peek(info)) {
		if ((info.type==matlab_controller::ARRAY)&&(!info.iscomplex)){
			List<double> data;
			ctr.read(data);
			Array_ arr(info.name);
			arr.data=data;
			inst.arrays.push_back(arr);
		}
		else {
                        //cout<<"read_arrays: Entry "<<info.name<<" will be igonred"<<endl;
			ctr.next();
		}
	}
}

void MatlabFileFilter::read_integrals(matlab_controller::composite& ctr, data_entry& inst)
{
	matlab_controller::header_info info;
	while (ctr.peek(info)) {
		if ((info.type==matlab_controller::ARRAY)&&(!info.iscomplex)){
			if (QString(info.name)=="integral_scale") {
				read_numbers(ctr, &inst.integrals.integral_scale);
			}
			else if (QString(info.name)=="integral_screen_scale") {
				read_numbers(ctr, &inst.integrals.integral_vscale);
			}
			else if (QString(info.name)=="integral_screen_shift") {
				read_numbers(ctr, &inst.integrals.integral_vshift);
			}
                        else if (QString(info.name)=="integral_tilt") {
                                read_numbers(ctr, &inst.integrals.integral_tlt);
                        }
                        else if (QString(info.name)=="integral_level") {
                                read_numbers(ctr, &inst.integrals.integral_lvl);
                        }
			else {
				rmatrix mat;
				ctr.read(mat);
				if (mat.rows()!=2) {
                                        cout<<"read_integrals: wrong size "<<mat.rows()<<endl;
					continue;
				}
				inst.integrals.xintegrals.push_back(mat.row(0));
				inst.integrals.yintegrals.push_back(mat.row(1));
			}
		}
		else {
			cout<<"read_integrals: Entry "<<info.name<<" will be igonred"<<endl;
			ctr.next();
		}
	}
}

void MatlabFileFilter::read_params(matlab_controller::composite& ctr, data_entry& inst)
{
	matlab_controller::header_info info;
	QStringList pfiles=inst.info.parameterFiles;
	QList< QHash <QString, QString> > allParameters=inst.info.allParameters;
	QHash<QString, QString> paramsFromFile;
	while (ctr.peek(info)) {
		if (info.type==matlab_controller::STRUCT){ //for each file
			paramsFromFile.clear();
			matlab_controller::header_info infoi;
			matlab_controller::composite pctr(ctr);
			while (pctr.peek(infoi)) {
				if (infoi.type==matlab_controller::CHAR) {
					List<char> ch;
					pctr.read(ch);
					paramsFromFile.insert(infoi.name, ch.vector());
				}
				else {
					cout<<"read_params: wrong entry"<<endl;
					pctr.next();
				}
			}
			if (!paramsFromFile.empty()) {
				pfiles.push_back(info.name);
				allParameters.push_back(paramsFromFile);
			}
		}
		else {
			cout<<"read_params: wrong entry"<<endl;
			ctr.next();
		}
	}
	inst.info.parameterFiles=pfiles;
	inst.info.allParameters=allParameters;
}

void MatlabFileFilter::read_graphics(matlab_controller::composite& ctr, data_entry& inst)
{
	matlab_controller::header_info info;
	graph_object graph;
	int completeness=0;
	List<double> positions;
	List<double> maindata;
	List<char> text;
	List<double> colourlist;
	List<char> fontname;
	while (ctr.peek(info)) {
		if ((QString(info.name)=="type") && (!info.iscomplex)) {
			int t;
			read_numbers(ctr,&t);
			graph.type=GraphType(t);
			completeness++;}
		else if ((QString(info.name)=="scale") && (!info.iscomplex)) {
			read_numbers(ctr,&graph.scale);
			completeness++;}
		else if ((QString(info.name)=="pos") && (!info.iscomplex)) {
			ctr.read(positions);
			completeness++;}
		else if ((QString(info.name)=="colour") && (!info.iscomplex)){
			ctr.read(colourlist);
		}
		else if ((QString(info.name)=="font") && (info.type==matlab_controller::CHAR)){
			ctr.read(fontname);
		}
		else if ((QString(info.name)=="maindata")) {
			if (info.type==matlab_controller::CHAR) {
				ctr.read(text);
				completeness++;}
			else if (info.type==matlab_controller::ARRAY) {
				ctr.read(maindata);
				completeness++;
			}
			else {
				cout<<"read_graphics: entry "<<info.name<<" has wrong format for maindata"<<endl;
				completeness++;
				ctr.next();
			}
		}
		else {
			cout<<"read_graphics: entry "<<info.name<<" is ignored"<<endl;
			ctr.next();
		}
	}
	if (completeness!=4) {
		cout<<"read_graphics: entry  is incomplete"<<endl;
		return;
	}
	if (positions.size()==4)
		graph.pos=QLineF(positions(0),positions(1),positions(2),positions(3));
	else if (positions.size()==2)
		graph.pos=QPointF(positions(0),positions(1));
	else {
		cout<<"Wrong positions specified";
		return;
	}
	if (graph.type==GRAPHTEXT)
		graph.maindata=text.vector();
	else if (graph.type==GRAPHIMAGE) {
		QByteArray png;
                //cout<<"MainData size "<<maindata.size()<<endl;
		for (size_t i=0; i<maindata.size(); i++) {
//			cout<<"Double "<<maindata<<endl;
			png.push_back(char(maindata(i)));
			}
		QPixmap pix;
                //cout<<png.data()<<endl;
		pix.loadFromData(png);
                //cout<<"Pixmap loaded\n";
		graph.maindata=pix;
	}
	if (colourlist.size()==4)
		graph.colour=QColor(int(colourlist(0)),int(colourlist(1)),int(colourlist(2)),int(colourlist(3)));
	if (fontname.size())
		graph.font=fontname.vector();
	inst.graphics.push_back(graph);
}

void MatlabFileFilter::read(QList<data_entry>& data, QString fname)
{
	fname.remove(".mat");
	hasData=false;
    matlab_controller main(fname.toLatin1().data());
	matlab_controller::header_info info;
	while (main.peek(info)) {
		if (info.type==matlab_controller::STRUCT) {
			matlab_controller::composite dscntrl(main);
			read_data(dscntrl, data);
		}
		else
			read_data(main, data);
	}
}

//int MatlabFileFilter::read(cmatrix & c, gsimFD & i, const char* f) 
//{
//	qDebug()<<"Read MATLAB routine";
//	return 0;
//}

void MatlabFileFilter::write(QList<data_entry>& sdata, QString& fname)
{
	fname.remove(".mat");
//	qDebug()<<"Write MATLAB routine";
    matlab_controller mainmc(fname.toLatin1().data(),5);
	for (size_t k=0; k<sdata.size(); k++) { //for each dataset
                const data_entry* inst=&(sdata.at(k));
//		char * nm=inst->info.file_filter->short_filename(inst->info.filename).replace(".","_").toLatin1().data();
                char * nm=(char*) "spectrum1";
//		qDebug()<<"data will be stored as"<<nm;
//determine how many elements the structure can contain
		int elements=17;
		if (inst->arrays.size())
			elements++;
		if (inst->integrals.xintegrals.size())
			elements++;
		if (inst->info.allParameters.size())
			elements++;
		if (inst->graphics.size())
			elements+=inst->graphics.size();
		if (inst->hyperspectrum.size())
			elements++;
		matlab_controller::composite dmc(mainmc,nm,matlab_controller::STRUCT,elements);
		if (inst->spectrum.rows()>1) {
			dmc.write(inst->spectrum,"data");
			if (inst->hyperspectrum.size())
				dmc.write(inst->hyperspectrum,"hyperdata");
			dmc.write(pair(inst->info.sw,inst->info.sw1),"sw");
			dmc.write(pair(inst->info.type,inst->info.type1),"type");
			dmc.write(pair(inst->info.sfrq,inst->info.sfrq1),"sfrq");
			dmc.write(pair(inst->info.ref,inst->info.ref1),"ref");
			dmc.write(pair(inst->info.phd0,inst->info.phi0),"ph0");
			dmc.write(pair(inst->info.phd1,inst->info.phi1),"ph1");
//			dmc.write(pair(inst->info.xrd,inst->info.xri),"xr");
			dmc.write(pair(inst->display.xscale,inst->display.vscale),"scale");
			dmc.write(pair(inst->display.xshift,inst->display.vshift),"shift");
			}
		else {
			dmc.write(inst->spectrum,"data");
			dmc.write(one(inst->info.sw),"sw");
			dmc.write(one(inst->info.type),"type");
			dmc.write(one(inst->info.sfrq),"sfrq");
			dmc.write(one(inst->info.ref),"ref");
			dmc.write(one(inst->info.phd0),"ph0");
			dmc.write(one(inst->info.phd1),"ph1");
//			dmc.write(one(inst->info.xrd),"xr");
			dmc.write(one(inst->display.xscale),"scale");
			dmc.write(one(inst->display.xshift),"shift");
		}
		//common data for 1D and 2D
            dmc.write(inst->info.title.toLatin1().data(),"title");
            dmc.write(inst->info.pulprog.toLatin1().data(),"pulprog");
            dmc.write(inst->info.processing.toLatin1().data(),"processing");
			dmc.write(one(inst->info.spinrate),"spinrate");
			dmc.write(one(inst->info.temperature),"temperature");
			dmc.write(one(inst->display.isRealOn),"is_real");
			dmc.write(one(inst->display.isVisible),"is_visible");
			dmc.write(one(inst->display.hasPeaks),"has_peaks");
			if (inst->arrays.size()) {
//				qDebug()<<"Arrays size"<<inst->arrays.size();
				matlab_controller::composite arrmc(dmc,"arrays", matlab_controller::STRUCT, inst->arrays.size());
				for (size_t i=0; i<inst->arrays.size(); i++)
                    arrmc.write(inst->arrays[i].data, inst->arrays[i].name.toLatin1().data());
			}
			if (inst->integrals.xintegrals.size()) {
//				qDebug()<<"Number of integrals"<<inst->integrals.xintegrals.size();
                                matlab_controller::composite intmc(dmc,"integrals", matlab_controller::STRUCT, inst->integrals.xintegrals.size()+5);
				intmc.write(one(inst->integrals.integral_scale),"integral_scale");
				intmc.write(one(inst->integrals.integral_vscale),"integral_screen_scale");
				intmc.write(one(inst->integrals.integral_vshift),"integral_screen_shift");
                                intmc.write(one(inst->integrals.integral_tlt),"integral_tilt");
                                intmc.write(one(inst->integrals.integral_lvl),"integral_level");
				for (size_t i=0; i<inst->integrals.xintegrals.size(); i++) {
					rmatrix mat(2,inst->integrals.xintegrals.at(i).size());
					for (size_t j=0; j<inst->integrals.xintegrals.at(i).size();j++) {
						mat(0,j)=inst->integrals.xintegrals.at(i)(j);
						mat(1,j)=inst->integrals.yintegrals.at(i)(j);
					}
                    intmc.write(mat, QString("integral%1").arg(i).toLatin1().data());
				}
			}
			if (inst->info.allParameters.size()) {
				matlab_controller::composite parmc(dmc,"params", matlab_controller::STRUCT, inst->info.parameterFiles.size());
				for (size_t i=0; i<inst->info.parameterFiles.size(); i++) {
                    matlab_controller::composite parfilemc(parmc,inst->info.parameterFiles.at(i).toLatin1().data(), matlab_controller::STRUCT, inst->info.allParameters.at(i).size());
					QHashIterator<QString, QString> it(inst->info.allParameters.at(i));
 					while (it.hasNext()) {
						it.next();
                        parfilemc.write(it.value().toLatin1().data(), it.key().toLatin1().data());
					}
				}
			}
			if (inst->graphics.size()) {
//				matlab_controller::composite agmc(dmc,"graphics", matlab_controller::STRUCT, inst->graphics.size());
				for (size_t i=0; i<inst->graphics.size(); i++) {
                    matlab_controller::composite gmc(dmc,QString("graphics_object%1").arg(i).toLatin1().data(), matlab_controller::STRUCT, 6);
					gmc.write(one(inst->graphics.at(i).type),"type");
					gmc.write(one(inst->graphics.at(i).scale),"scale");
					List<double> colourlist;
					colourlist.push_back(inst->graphics.at(i).colour.red());
					colourlist.push_back(inst->graphics.at(i).colour.green());
					colourlist.push_back(inst->graphics.at(i).colour.blue());
					colourlist.push_back(inst->graphics.at(i).colour.alpha());
					gmc.write(colourlist,"colour");
                    gmc.write(inst->graphics.at(i).font.toLatin1().data(),"font");
					List<double> pos;
					if ((inst->graphics.at(i).type==GRAPHTEXT) || (inst->graphics.at(i).type==GRAPHIMAGE)) {
						pos.push_back(inst->graphics.at(i).pos.toPointF().x());
						pos.push_back(inst->graphics.at(i).pos.toPointF().y());
					}
					else if (inst->graphics.at(i).type==GRAPHLINE) {
						pos.push_back(inst->graphics.at(i).pos.toLineF().x1());
						pos.push_back(inst->graphics.at(i).pos.toLineF().y1());
						pos.push_back(inst->graphics.at(i).pos.toLineF().x2());
						pos.push_back(inst->graphics.at(i).pos.toLineF().y2());
					}
					gmc.write(pos,"pos");
					if (inst->graphics.at(i).type==GRAPHLINE) {
						List<double> maindata(1,0.0);
						gmc.write(maindata,"maindata");
					}
					else if (inst->graphics.at(i).type==GRAPHIMAGE) {
						List<double> maindata;
						QPixmap pixmap=inst->graphics.at(i).maindata.value<QPixmap>();
                                                //cout<<"pixmap loaded"<<endl;
						QByteArray bytes;
						QBuffer buffer(&bytes);
						buffer.open(QIODevice::WriteOnly);
						pixmap.save(&buffer, "PNG");
						buffer.close();
//						qDebug()<<"picture size"<<bytes.size();
						for (size_t i=0; i<bytes.size();i++)
							cout<<double(bytes.at(i))<<" ";
						cout<<endl;
//						cout<<"Saved PNG:"<<bytes.data()<<endl;
						for (size_t j=0; j<bytes.size(); j++)
							maindata.push_back(bytes.at(j));
                                                //cout<<"Saved data"<<maindata<<endl;
						gmc.write(maindata,"maindata");
					}
					else if (inst->graphics.at(i).type==GRAPHTEXT) 
                        gmc.write(inst->graphics.at(i).maindata.toString().toLatin1().data(),"maindata");
				}
			}
	}
}
