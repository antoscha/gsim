#include "widgets.h"
#include <QFileDialog>
#include <QDir>
#include <QKeyEvent>
#include <QDebug>

promptDialog::promptDialog(QWidget* parent) : QDialog(parent)
{
	ml=new QVBoxLayout;
	wl=new QVBoxLayout;
	QDialogButtonBox* buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
	connect(buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
	connect(buttonBox, SIGNAL(rejected()), this, SLOT(reject()));
	setLayout(ml);
	ml->addLayout(wl);
	ml->addWidget(buttonBox);
}

void promptDialog::putLayout(QLayout* layout)
{
	wl->addLayout(layout);
}

void promptDialog::putWidget(QWidget* widget)
{
	wl->addWidget(widget);
}

/*
zTableWidget::zTableWidget(QWidget* parent) : QTableWidget(parent)
{
	copyshortcut=new QShortcut(QKeySequence(tr("Ctrl+C", "Copy")),
                         parent);
	pasteshortcut=new QShortcut(QKeySequence(tr("Ctrl+V", "Paste")),
                         parent);
	connect(copyshortcut, SIGNAL(activated()), this, SLOT(fromTableToClipboard()));
	connect(pasteshortcut, SIGNAL(activated()), this, SLOT(fromClipboardToTable()));
}

zTableWidget::~zTableWidget() 
{
	delete copyshortcut;
	delete pasteshortcut;
}

void zTableWidget::fromTableToClipboard()
{
QString selection;
foreach (QTableWidgetSelectionRange range, selectedRanges()) {
int lCol=range.leftColumn();
int rCol=range.rightColumn();
int tRow=range.topRow();
int bRow=range.bottomRow();
for (int i=tRow; i<=bRow; i++){
for (int j=lCol; j<=rCol; j++) {
	selection+=item(i,j)->text();
	if (j!=rCol) selection+="	";//add TAB if it is not a last data
}
selection+="\n";
}
}
QClipboard* clipboard= QApplication::clipboard();
clipboard->setText(selection);
}

void zTableWidget::fromClipboardToTable()
{

}
*/

QHBoxLayout* hBlock(QWidget* parent,QString name,QWidget* widget)
{
	QHBoxLayout *layout = new QHBoxLayout;
	QLabel * label= new QLabel(name,parent);
	layout->addWidget(label);
	layout->addWidget(widget);
	return layout;
}


void fromTableToClipboard(QTableWidget* table)
{
QString selection;
foreach (QTableWidgetSelectionRange range, table->selectedRanges()) {
int lCol=range.leftColumn();
int rCol=range.rightColumn();
int tRow=range.topRow();
int bRow=range.bottomRow();
for (int i=tRow; i<=bRow; i++){
for (int j=lCol; j<=rCol; j++) {
	selection+=table->item(i,j)->text();
	if (j!=rCol) selection+="	";//add TAB if it is not a last data
}
selection+="\n";
}
}
QClipboard* clipboard= QApplication::clipboard();
clipboard->setText(selection);
}

void  fromClipboardToTable(QTableWidget* table)
{
	QClipboard* clipboard= QApplication::clipboard();
	QString text=clipboard->text();
	QStringList lines=text.split("\n",QString::SkipEmptyParts);
	if (!lines.size())
		return;
	bool useSelection;
	int startx, starty;
	if (!table->selectedRanges().size()) {
		useSelection=false;
	}
        else if (table->selectedRanges().size()==1){
		if (table->selectedRanges().at(0).rowCount()==1 && table->selectedRanges().at(0).columnCount()==1)
                    useSelection=false;
        }
	else {
		useSelection =true;
	}
	if (useSelection) {
		int rr=0, cc=0; //specify initial position in buffer
		for (size_t k=0; k<table->selectedRanges().size(); k++) { //for all selected ranges
			for (size_t i=0; i<table->selectedRanges().at(k).rowCount(); i++) { //for all rows
				QStringList contents=lines.at(rr).split("	",QString::SkipEmptyParts);
				if (!contents.size())
					continue; // prevent crash when empty data is pasted
				for (size_t j=0; j<table->selectedRanges().at(k).columnCount(); j++) {
					starty=table->selectedRanges().at(k).topRow();
					startx=table->selectedRanges().at(k).leftColumn();
					table->item(i+starty,j+startx)->setText(contents.at(cc) );
					cc++;
					if (cc>=contents.size())
						cc=0;
				}
			rr++;
			if (rr>=lines.size())
				rr=0;
			}
		}
	}
	else {
		startx=table->currentColumn();
		starty=table->currentRow();
		int endy=(lines.size()+starty<table->rowCount())? lines.size()+starty : table->rowCount();
		for (int y=starty, j=0; y<endy; y++, j++) {
		QString line=lines.at(j);
		QStringList contents=line.split("	",QString::SkipEmptyParts);
		int endx=(contents.size()+startx<table->columnCount())? contents.size()+startx : table->columnCount();
		for (int x=startx, i=0; x<endx; x++, i++) {
			QTableWidgetItem* item = table->item(y,x);
			item->setText( contents.at(i) );
			table->setItem(y,x,item);
			table->setItemSelected(item,true);
		}
		}
	}
}

LineWithBrowser::LineWithBrowser(QWidget *parent)
    : QLineEdit(parent)
{
    browserButton = new QToolButton(this);
    QPixmap pixmap(":/images/dir.png");
    browserButton->setIcon(QIcon(pixmap));
    browserButton->setIconSize(pixmap.size());
    browserButton->setCursor(Qt::ArrowCursor);
    browserButton->setToolTip(tr("Browse"));
    browserButton->setStyleSheet("QToolButton { border: none; padding: 0px; }");
    connect(browserButton, SIGNAL(clicked()), this, SLOT(setFilename()));

    int frameWidth = style()->pixelMetric(QStyle::PM_DefaultFrameWidth);
    setStyleSheet(QString("QLineEdit { padding-right: %1px; } ").arg(browserButton->sizeHint().width() + frameWidth + 1));
    QSize msz = minimumSizeHint();
    setMinimumSize(qMax(msz.width(), browserButton->sizeHint().height() + frameWidth * 2 + 2),
                   qMax(msz.height(), browserButton->sizeHint().height() + frameWidth * 2 + 2));
}

void LineWithBrowser::resizeEvent(QResizeEvent *)
{
    QSize sz = browserButton->sizeHint();
    int frameWidth = style()->pixelMetric(QStyle::PM_DefaultFrameWidth);
    browserButton->move(rect().right() - frameWidth - sz.width(),
                      (rect().bottom() + 1 - sz.height())/2);
}

void LineWithBrowser::setFilename()
{
	setText(QFileDialog::getOpenFileName(this, tr("Choose file"), QDir::homePath(), tr("Files (*)")));
}



commandLineWidget::commandLineWidget(QWidget *parent)
    : QLineEdit(parent)
{
	commandCompleter = new QCompleter(this);
	commandCompleter->setModel(new QStringListModel(commandCompleter));
	setCompleter(commandCompleter);
	commandCompleter->setCompletionMode(QCompleter::InlineCompletion);
	connect(this,SIGNAL( returnPressed()),this,SLOT(resetHistory()));

    clearButton = new QToolButton(this);
    QPixmap pixmap(QPixmap(":/images/delete.png").scaled(16,16));
    clearButton->setIcon(QIcon(QPixmap(":/images/delete.png").scaled(16,16)));
    clearButton->setIconSize(pixmap.size());
    clearButton->setCursor(Qt::ArrowCursor);
    clearButton->setStyleSheet("QToolButton { border: none; padding: 0px; }");
    clearButton->hide();
    connect(clearButton, SIGNAL(clicked()), this, SLOT(clear()));
    connect(this, SIGNAL(textChanged(const QString&)), this, SLOT(updateCloseButton(const QString&)));
    int frameWidth = style()->pixelMetric(QStyle::PM_DefaultFrameWidth);
    setStyleSheet(QString("QLineEdit { padding-right: %1px; } ").arg(clearButton->sizeHint().width() + frameWidth + 1));
    QSize msz = minimumSizeHint();
    setMinimumSize(qMax(msz.width(), clearButton->sizeHint().height() + frameWidth * 2 + 2),
                   qMax(msz.height(), clearButton->sizeHint().height() + frameWidth * 2 + 2));
}

void commandLineWidget::keyPressEvent ( QKeyEvent * k)
{
	if (k->key()==Qt::Key_Up) {
		if (!historyList.size())
			return; //do nothing if history is empty
		if (historyPosition<0) //set to the last point
			historyPosition=historyList.size()-1;
		if (historyPosition>=0 && historyPosition<historyList.size())
			setText(historyList.at(historyPosition));
		else
			qDebug()<<"historyPosition is outside historyList";
		if (historyPosition>0)
			historyPosition--;
	}
	else if (k->key()==Qt::Key_Down) 
	{
		if (historyPosition>=0 && historyPosition<historyList.size()-1) {
			historyPosition++;
			setText(historyList.at(historyPosition));
		}
		else if (historyPosition==historyList.size()-1)
			setText("");
	}
	else
		QLineEdit::keyPressEvent (k);
}

void commandLineWidget::addToHistory(QString s)
{
	if (historyList.size()>100)
		historyList.removeAt(0);
	historyList<<s;
}

void commandLineWidget::resetHistory()
{
	historyPosition=-1;
}

void commandLineWidget::resizeEvent(QResizeEvent *)
{
    QSize sz = clearButton->sizeHint();
    int frameWidth = style()->pixelMetric(QStyle::PM_DefaultFrameWidth);
    clearButton->move(rect().right() - frameWidth - sz.width(),
                      (rect().bottom() + 1 - sz.height())/2);
}

void commandLineWidget::updateCloseButton(const QString& text)
{
    clearButton->setVisible(!text.isEmpty());
}
