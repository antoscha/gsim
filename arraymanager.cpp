
#include "arraymanager.h"
#include "base.h"

#include <QVariant>
#include <QClipboard>
#include <QHeaderView>
#include <QDebug>

#include <math.h>
//#include "nrutil.h"

#define EPS 1.0e-16
#define FPMIN 1.0e-30
#define MAXIT 10000
#define XMIN 2.0
#define PI 3.141592653589793
#define NUSE1 7
#define NUSE2 8
static int imaxarg1,imaxarg2;
#define IMAX(a,b) (imaxarg1=(a),imaxarg2=(b),(imaxarg1) > (imaxarg2) ?\
        (imaxarg1) : (imaxarg2))
#define SIGN(a,b) ((b) >= 0.0 ? fabs(a) : -fabs(a))
void nrerror(char error_text[]) {cout<<error_text<<endl;};

double chebev(double a, double b, double c[], int m, double x)
/*Chebyshev evaluation: All arguments are input. c[0..m-1] is an array of Chebyshev coe-
cients, the rst m elements of c output from chebft (which must have been called with the
same a and b). The Chebyshev polynomial
Pm-1
k=0 ckTk(y) − c0=2 is evaluated at a point
y = [x−(b+a)=2]=[(b− a)=2], and the result is returned as the function value.*/
{
void nrerror(char error_text[]);
double d=0.0,dd=0.0,sv,y,y2;
int j;
if ((x-a)*(x-b) > 0.0) nrerror((char*) "x not in range in routine chebev");
y2=2.0*(y=(2.0*x-a-b)/(b-a)); //Change of variable.
for (j=m-1;j>=1;j--) { //Clenshaw's recurrence.
	sv=d;
	d=y2*d-dd+c[j];
	dd=sv;
}
return y*d-dd+0.5*c[0]; //Last step is dierent.
}

void bessjy(double x, double xnu, double *rj, double *ry, double *rjp, double *ryp)
/*Returns the Bessel functions rj = J , ry = Y and their derivatives rjp = J0 , ryp = Y0, for
positive x and for xnu =   0. The relative accuracy is within one or two signicant digits
of EPS, except near a zero of one of the functions, where EPS controls its absolute accuracy.
FPMIN is a number close to the machine's smallest floating-point number. All internal arithmetic
is in double precision. To convert the entire routine to double precision, change the float
declarations above to double and decrease EPS to 10−16. Also convert the function beschb.*/
{
void beschb(double x, double *gam1, double *gam2, double *gampl,
double *gammi);
int i,isign,l,nl;
double a,b,br,bi,c,cr,ci,d,del,del1,den,di,dlr,dli,dr,e,f,fact,fact2,
	fact3,ff,gam,gam1,gam2,gammi,gampl,h,p,pimu,pimu2,q,r,rjl,
	rjl1,rjmu,rjp1,rjpl,rjtemp,ry1,rymu,rymup,rytemp,sum,sum1,
	temp,w,x2,xi,xi2,xmu,xmu2;
if (x <= 0.0 || xnu < 0.0) nrerror((char*) "bad arguments in bessjy");
nl=(x < XMIN ? (int)(xnu+0.5) : IMAX(0,(int)(xnu-x+1.5)));
/*nl is the number of downward recurrences of the J's and upward recurrences of Y 's. xmu
lies between −1=2 and 1/2 for x < XMIN, while it is chosen so that x is greater than the
turning point for x  XMIN.*/
xmu=xnu-nl;
xmu2=xmu*xmu;
xi=1.0/x;
xi2=2.0*xi;
w=xi2/PI; //The Wronskian.
isign=1; 
/*
Evaluate CF1 by modied Lentz's method (x5.2).
isign keeps track of sign changes in the denominator.
*/
h=xnu*xi;
if (h < FPMIN) h=FPMIN;
b=xi2*xnu;
d=0.0;
c=h;
for (i=1;i<=MAXIT;i++) {
	b += xi2;
	d=b-d;
	if (fabs(d) < FPMIN) d=FPMIN;
	c=b-1.0/c;
	if (fabs(c) < FPMIN) c=FPMIN;
	d=1.0/d;
	del=c*d;
	h=del*h;
	if (d < 0.0) isign = -isign;
	if (fabs(del-1.0) < EPS) break;
}
if (i > MAXIT) nrerror((char*) "x too large in bessjy; try asymptotic expansion");
rjl=isign*FPMIN; //Initialize J and J0 for downward recurrence.
rjpl=h*rjl;
rjl1=rjl; //Store values for later rescaling.
rjp1=rjpl;
fact=xnu*xi;
for (l=nl;l>=1;l--) {
	rjtemp=fact*rjl+rjpl;
	fact -= xi;
	rjpl=fact*rjtemp-rjl;
	rjl=rjtemp;
}
if (rjl == 0.0) rjl=EPS;
f=rjpl/rjl; //Now have unnormalized J and J0 .
if (x < XMIN) { //Use series.
	x2=0.5*x;
	pimu=PI*xmu;
	fact = (fabs(pimu) < EPS ? 1.0 : pimu/sin(pimu));
	d = -log(x2);
	e=xmu*d;
	fact2 = (fabs(e) < EPS ? 1.0 : sinh(e)/e);
	beschb(xmu,&gam1,&gam2,&gampl,&gammi);// Chebyshev evaluation of 􀀀1 and 􀀀2.
	ff=2.0/PI*fact*(gam1*cosh(e)+gam2*fact2*d); //f0.
	e=exp(e);
	p=e/(gampl*PI); //p0.
	q=1.0/(e*PI*gammi);// q0.
	pimu2=0.5*pimu;
	fact3 = (fabs(pimu2) < EPS ? 1.0 : sin(pimu2)/pimu2);
	r=PI*pimu2*fact3*fact3;
	c=1.0;
	d = -x2*x2;
	sum=ff+r*q;
	sum1=p;
	for (i=1;i<=MAXIT;i++) {
		ff=(i*ff+p+q)/(i*i-xmu2);
		c *= (d/i);
		p /= (i-xmu);
		q /= (i+xmu);
		del=c*(ff+r*q);
		sum += del;
		del1=c*p-i*del;
		sum1 += del1;
		if (fabs(del) < (1.0+fabs(sum))*EPS) break;
	}
        if (i > MAXIT) nrerror((char*) "bessy series failed to converge");
	rymu = -sum;
	ry1 = -sum1*xi2;
	rymup=xmu*xi*rymu-ry1;
	rjmu=w/(rymup-f*rymu); //Equation (6.7.13).
} else { //Evaluate CF2 by modied Lentz's method (x5.2).
	a=0.25-xmu2;
	p = -0.5*xi;
	q=1.0;
	br=2.0*x;
	bi=2.0;
	fact=a*xi/(p*p+q*q);
	cr=br+q*fact;
	ci=bi+p*fact;
	den=br*br+bi*bi;
	dr=br/den;
	di = -bi/den;
	dlr=cr*dr-ci*di;
	dli=cr*di+ci*dr;
	temp=p*dlr-q*dli;
	q=p*dli+q*dlr;
	p=temp;
	for (i=2;i<=MAXIT;i++) {
		a += 2*(i-1);
		bi += 2.0;
		dr=a*dr+br;
		di=a*di+bi;
		if (fabs(dr)+fabs(di) < FPMIN) dr=FPMIN;
		fact=a/(cr*cr+ci*ci);
		cr=br+cr*fact;
		ci=bi-ci*fact;
		if (fabs(cr)+fabs(ci) < FPMIN) cr=FPMIN;
		den=dr*dr+di*di;
		dr /= den;
		di /= -den;
		dlr=cr*dr-ci*di;
		dli=cr*di+ci*dr;
		temp=p*dlr-q*dli;
		q=p*dli+q*dlr;
		p=temp;
		if (fabs(dlr-1.0)+fabs(dli) < EPS) break;
	}
        if (i > MAXIT) nrerror((char*) "cf2 failed in bessjy");
	gam=(p-f)/q;// Equations (6.7.6) { (6.7.10).
	rjmu=sqrt(w/((p-f)*gam+q));
	rjmu=SIGN(rjmu,rjl);
	rymu=rjmu*gam;
	rymup=rymu*(p+q/gam);
	ry1=xmu*xi*rymu-rymup;
}
fact=rjmu/rjl;
*rj=rjl1*fact; //Scale original J and J0 .
*rjp=rjp1*fact;
for (i=1;i<=nl;i++) {// Upward recurrence of Y .
	rytemp=(xmu+i)*xi2*ry1-rymu;
	rymu=ry1;
	ry1=rytemp;
}
*ry=rymu;
*ryp=xnu*xi*rymu-ry1;
}

void beschb(double x, double *gam1, double *gam2, double *gampl, double *gammi)
/*Evaluates Gamma1 and Gamma2 by Chebyshev expansion for |x|<=1/2. Also returns 1=Gamma(1 + x) and
1=Gamma(1 − x). If converting to double precision, set NUSE1 = 7, NUSE2 = 8.*/
{
double chebev(double a, double b, double c[], int m, double x);
double xx;
static double c1[] = {
	-1.142022680371168e0,6.5165112670737e-3,
	3.087090173086e-4,-3.4706269649e-6,6.9437664e-9,
	3.67795e-11,-1.356e-13};
static double c2[] = {
	1.843740587300905e0,-7.68528408447867e-2,
	1.2719271366546e-3,-4.9717367042e-6,-3.31261198e-8,
	2.423096e-10,-1.702e-13,-1.49e-15};
xx=8.0*x*x-1.0; 
/*
Multiply x by 2 to make range be −1 to 1,
and then apply transformation for evaluating
even Chebyshev series.
*/
*gam1=chebev(-1.0,1.0,c1,NUSE1,xx);
*gam2=chebev(-1.0,1.0,c2,NUSE2,xx);
*gampl= *gam2-x*(*gam1);
*gammi= *gam2+x*(*gam1);
}


double redor(double t, double d)
{
	if (!(t*d))
		return 1.0;
	double jv=0.0, yv=0.0, djv=0.0, dyv=0.0;
	bessjy(sqrt(2.0)*t*d, 0.25, &jv, &yv, &djv, &dyv);
	double invjv=jv*cos(0.25*M_PI) - yv*sin(0.25*M_PI);
	double red=(M_PI/2/sqrt(2.0))*jv*invjv;
	return red;
}

double my_abs(double x) {
return fabs(x);}

double optimiser::operator()(const vector<double>& paras) const
{
	double chi2=0.0;
	List<double> dest;
	for (int n=0; n<paras.size(); n++)
		(*ppars)[n]=paras[n];
//	cout<<"Xvec:"<<theXvec<<endl;
//	cout<<"Yvec:"<<theMeasurements<<endl;
	for (size_t i=0; i<theXvec.size(); i++) {
		*xval=theXvec[i];
		dest.push_back(parser->Eval());
		chi2+=(theMeasurements(i)-dest(i))*(theMeasurements(i)-dest(i))/(theError*theError);
	}
//	cout<<"comp val="<<dest<<endl;
//	cout<<"Chi2="<<chi2<<endl;
	return chi2;
}

/*function for libcmatrix optimiser
void lcmat_optimiser::operator() (BaseList<double>& dest, const BaseList<double>& paras) const
{
	const size_t n=xvals.size();
	for (int i=0; i<paras.size(); i++)
		(*ppars)[n]=paras(n);
	for (size_t i=0; i<xvals.size(); i++) {
		*xval=xvals(i);
		dest(i)=parser->Eval();
	}
}
*/

ArrayManager::ArrayManager(MainForm *parent, Qt::WindowFlags fl)
	:QMainWindow(parent, fl) {

    setupUi(this);
    setIconSize(QSize(24,24));

    p=parent;
    data=p->data;
    max_length=0;
    if (!data->size()) return;

    setWindowTitle( "Array Manager" );
    setWindowIcon( QPixmap( ":/images/gsim.png" ) );

    fileNewArrayAction = new QAction (this);
    fileNewArrayAction->setText( tr( "New array" ) );

    fileFromFile = new QAction (this); 
    fileFromFile->setText( tr( "Create from file..." ) );
    fileFromFile->setIcon(QIcon::fromTheme("document-open"));

    fileSpecialInterlacing = new QAction (this); 
    fileSpecialInterlacing->setText( tr( "Interlacing" ) );

    fileSpecialREDOR = new QAction (this); 
    fileSpecialREDOR->setText( tr( "REDOR" ) );

    fileSpecialFromSpectrum = new QAction (this); 
    fileSpecialFromSpectrum->setText( tr( "From spectrum" ) );

    fileInvertArray=new QAction (this);
    fileInvertArray->setText( tr( "Inverted" ) );


    fileExitAction = new QAction( this);
    fileExitAction->setText( tr( "E&xit" ) );
    fileExitAction->setShortcut(QKeySequence(tr("Ctrl+Q")));

    fileExportMatlabAction = new QAction( this);
    fileExportMatlabAction->setText(tr("E&xport to MatLAB"));

    editCopyAction = new QAction( this);
    editCopyAction->setText( tr( "&Copy" ) );
    editCopyAction->setShortcut(QKeySequence(tr("Ctrl+C")));
    editCopyAction->setIcon( QIcon::fromTheme("edit-copy") );

    plotCopyAction = new QAction( this);
    plotCopyAction->setText( tr( "Copy plot" ) );

    plotExportVectorAction=new QAction( this);
    plotExportVectorAction->setText( tr( "E&xport plot as vector graphics" ) );
    plotExportVectorAction->setIcon( QIcon( ":/images/vectorgfx.png" ) );

    editPasteAction = new QAction( this);
    editPasteAction->setText( tr( "&Paste" ) );
    editPasteAction->setShortcut(QKeySequence(tr("Ctrl+V")));
    editPasteAction->setIcon( QIcon::fromTheme("edit-paste" ) );

//    editCopyVarsAction = new QAction( this);
//    editCopyVarsAction->setText( tr( "&Copy" ) );
//    editCopyVarsAction->setIcon( QIcon::fromTheme("edit-copy") );

//    editPasteVarsAction = new QAction( this);
//    editPasteVarsAction->setText( tr( "&Paste" ) );
//    editPasteVarsAction->setIcon( QIcon::fromTheme("edit-paste") );


    toolBar = new QToolBar(this);
    addToolBar(toolBar);
    toolBar->addAction(fileFromFile);
    toolBar->addSeparator();
    toolBar->addAction(editCopyAction);
    toolBar->addAction(editPasteAction);


//    menubar = new QMenuBar(this);

    fileMenu = menuBar()->addMenu(tr("&File"));
    fileMenu->addAction(fileNewArrayAction);
    fileMenu->addSeparator();
    fileMenu ->addAction(fileFromFile);
	fileSpecialMenu = fileMenu->addMenu(tr("&Create special"));
  	fileSpecialMenu->addAction(fileSpecialInterlacing);
	fileSpecialMenu->addAction(fileSpecialREDOR);
	fileSpecialMenu->addAction(fileSpecialFromSpectrum);
	fileSpecialMenu->addAction(fileInvertArray);

    fileMenu->addSeparator();
    fileMenu->addAction(fileExportMatlabAction);
    fileMenu->addSeparator();
    fileMenu->addAction(fileExitAction);

    editMenu = menuBar()->addMenu(tr("&Edit"));
    editMenu->addAction(editCopyAction);
    editMenu->addAction(editPasteAction);
    editMenu->addSeparator();
    editMenu->addAction(plotCopyAction);
    editMenu->addAction(plotExportVectorAction);
   
    plotBox->setEnabled(false);


    connect( fileNewArrayAction, SIGNAL( triggered() ), this, SLOT( fileNewArray() ) );
    connect( fileExitAction, SIGNAL( triggered() ), this, SLOT( on_okButton_clicked() ) );
    connect( fileInvertArray, SIGNAL( triggered() ), this, SLOT( fileInvert() ) );
    connect( fileSpecialInterlacing, SIGNAL( triggered() ), this, SLOT( createInterl() ) );
    connect( fileSpecialFromSpectrum, SIGNAL( triggered() ), this, SLOT( createFromSpectrum() ) );
    connect( fileSpecialREDOR, SIGNAL( triggered() ), this, SLOT( createREDOR() ) );
    connect( fileFromFile, SIGNAL( triggered() ), this, SLOT( fromFile() ) );
    connect( fileExportMatlabAction, SIGNAL( triggered() ), this, SLOT( fileExportMatlab() ) );
    connect( editCopyAction, SIGNAL( triggered() ), this, SLOT( editCopy() ) );
    connect( editPasteAction, SIGNAL( triggered() ), this, SLOT( editPaste() ) );
    connect( plotCopyAction, SIGNAL( triggered() ), this, SLOT( plotCopy() ) );
    connect( plotExportVectorAction, SIGNAL( triggered() ), this, SLOT( plotExportVector() ) );
    connect( arrayplot, SIGNAL(customContextMenuRequested(const QPoint&)),this, SLOT(plotContextMenu(const QPoint&)));

	arrayTable->resizeRowsToContents();
        arrayTable->horizontalHeader()->hide();
for (size_t i=0; i<data->size(); i++) {
	QString s=data->get_short_filename(i);
	data1comboBox->addItem(s);
	data2comboBox->addItem(s);
	}
    readSettings();

//define parser missing function:
    parser.DefineFun("redor", redor, false);
    parser.DefineFun("abs", my_abs, false);

    size_t k=data->getActiveCurve();
    data1comboBox->setCurrentIndex(k);
    data2comboBox->setCurrentIndex(k);
    on_data1comboBox_activated(k);
    on_data2comboBox_activated(k);
}



/*
 *  Destroys the object and frees any allocated resources
 */
ArrayManager::~ArrayManager()
{
    // no need to delete child widgets, Qt does it all for us
}

void ArrayManager::closeEvent ( QCloseEvent * )
{
	writeSettings();
	arrayTable->clear();
}

void ArrayManager::on_okButton_clicked()
{
	bool ok = read_col(data1comboBox, array1comboBox, 0);
	if (!ok) 
		return;
	ok= read_col(data2comboBox, array2comboBox, 1);
	if (!ok) 
		return;
	close();
}

void ArrayManager::on_cancelButton_clicked()
{
	close();
}

void ArrayManager::editCopy()
{
QTableWidget* table=arrayTable;
if (varsTable->hasFocus())
	table=varsTable;
fromTableToClipboard(table);
}

void ArrayManager::editPaste()
{
QTableWidget* table=arrayTable;
if (varsTable->hasFocus())
	table=varsTable;
fromClipboardToTable(table);
}

void ArrayManager::plotCopy()
{
	QPixmap pixmap(QPixmap::grabWidget(arrayplot));
	QClipboard* clipboard =  QApplication::clipboard();
	clipboard->setPixmap(pixmap, QClipboard::Clipboard);
}

void ArrayManager::plotExportVector()
{
    QFileDialog file_dialog(
    			this,
			"Choose a file",
			 p->working_directory
                        );
    file_dialog.setFileMode(QFileDialog::AnyFile);
    QStringList filters;
    filters<<"Scalable Vector Graphics(*.svg)"<<"Encapsulated Postscript (*.eps)";
#ifdef USE_EMF_OUTPUT
    filters<<"Enhanced metafile (*.emf)";
    EMFDevice* emf=NULL;
#endif
    file_dialog.setNameFilters(filters);
    file_dialog.setLabelText(QFileDialog::Accept,"Save");

    QStringList s;
    if (file_dialog.exec())
    	s = file_dialog.selectedFiles();

   if (s.isEmpty()) return;
   QString filter=file_dialog.selectedNameFilter();
   QPainter paint;
   PSDevice* ps=NULL;
   SVGDevice* svg=NULL;
   if (filter=="Scalable Vector Graphics(*.svg)") {
	if (!s.at(0).endsWith(".svg"))
		s[0]+=".svg";
	QDir dir(QDir::currentPath());
	svg= new SVGDevice(QSize(arrayplot->size()),dir.absoluteFilePath(s.at(0)));
	paint.begin(svg);
	}
   else if (filter=="Encapsulated Postscript (*.eps)") {
	if (!s.at(0).endsWith(".eps"))
		s[0]+=".eps";
	ps= new PSDevice(QSize(arrayplot->size()));
	paint.begin(ps);
	}
#ifdef USE_EMF_OUTPUT
   else {
	if (!s.at(0).endsWith(".emf"))
		s[0]+=".emf";
	emf=new EMFDevice(QSize(arrayplot->size()),s.at(0));
	paint.begin(emf);
   }
#endif
   paint.setClipping(true);
   paint.setClipRect(0,0,arrayplot->width(),arrayplot->height());
   arrayplot->draw_picture(&paint);
   paint.end();

if (filter!="Enhanced metafile (*.emf)") {

   QFile file(s.at(0));
        if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
            return;
   QTextStream out(&file);
   if (filter=="Scalable Vector Graphics(*.svg)")
        out << svg->result();
   else if (filter=="Encapsulated Postscript (*.eps)")
	out << ps->result();
   file.close();
//   statusBar()->showMessage(QString("File %1 has been saved").arg(s.at(0)),2000);
}
   if (ps!=NULL)
	delete ps;
   if (svg!=NULL)
	delete svg;
#ifdef USE_EMF_OUTPUT
   if (emf!=NULL)
	delete emf;
#endif
}

void ArrayManager::plotContextMenu(const QPoint& p)
{
//	qDebug()<<"menu requested";
	QMenu menu;
	menu.addAction(plotCopyAction);
	menu.addAction(plotExportVectorAction);
	menu.exec(arrayplot->mapToGlobal(p));
}
/*
void ArrayManager::editCopyVars()
{
fromTableToClipboard(varsTable);
}

void ArrayManager::editPasteVars()
{
//	cout<<"Paste doesn't work yet\n";
fromClipboardToTable(varsTable);
}
*/

void ArrayManager::on_data1comboBox_activated(int k)
{
	fill_comboBox(k, array1comboBox);
	on_array1comboBox_activated();
}


void ArrayManager::on_data2comboBox_activated(int k)
{
	fill_comboBox(k,array2comboBox);
	on_array2comboBox_activated();
}

void ArrayManager::fill_comboBox(size_t k, QComboBox* ComboBox)
{
	if (!data->get_arrays(k).size()) {
		ComboBox->setEnabled(false);
		return;
	}
	ComboBox->setEnabled(true);
	ComboBox->clear();
	QList<Array_> arrays=data->get_arrays(k);
	size_t n=arrays.size();
	for (size_t i=0; i<n; i++)
		ComboBox->addItem(arrays.at(i).name);
}


void ArrayManager::updateTable()
{
	arrayTable->clear();
	fill_col(data1comboBox, array1comboBox, 0);
	fill_col(data2comboBox, array2comboBox, 1);
}

void ArrayManager::on_array1comboBox_activated()
{
	updateTable();
}

void ArrayManager::on_array2comboBox_activated()
{
	updateTable();
}

void ArrayManager::fill_col(QComboBox* dataBox, QComboBox* arrayBox, size_t column)
{
//	cout<<"ArrayManager::fill_row\n";
//	arrayTable->setHorizontalHeaderLabels(headers);
	size_t k=dataBox->currentIndex();
	if (!data->get_arrays(k).size()) {
		arrayBox->setEnabled(false);
		return;
	}
	QList<Array_> arrays=data->get_arrays(k);
	size_t N=arrays.size();
//	cout<<"kuku\n";
	size_t m=arrayBox->currentIndex();
	if (m>=N) return;
	Array_ arr=arrays.at(m);
	size_t len=arr.data.size();
	if (max_length<len) max_length=len;
	arrayTable->setRowCount(max_length);
	for (size_t i=0; i<len; i++) {
		QTableWidgetItem *newItem = new QTableWidgetItem(tr("%1").arg(arr.data(i)));
		arrayTable->setItem(i,column,newItem);
	}
}

bool ArrayManager::read_col(QComboBox* dataBox, QComboBox* arrayBox, size_t column)
{
	size_t k=dataBox->currentIndex();
	QList<Array_> arrays=data->get_arrays(k);
	if (!arrays.size())
		return true; //empty data is a correct data
//	if (arrays.size()>arrayTable->rowCount())
//		qCritical("read_col:Length of the array is wrong!");
	size_t m=arrayBox->currentIndex();
	if (m>=arrays.size())
		qCritical("read_col:m is too big!");
	bool ok;
	for (size_t i=0; i<arrays.at(m).data.size(); i++) {
		double value=arrayTable->item(i,column)->text().toDouble(&ok);
		if (!ok) {
			QMessageBox::warning(this, "Reading error", "One of the cells contains non-numerical value");
			return false;
		}
		arrays[m].data(i)=value;
	}
	data->set_arrays(k, arrays);
	return true;
}

void ArrayManager::fileNewArray()
{
	promptDialog dial(this);
	QStringList sets;
	for (size_t i=0; i<data->size(); i++)
		sets<<data->get_short_filename(i);
	QComboBox* db=new QComboBox(&dial);
	db->addItems(sets);
	dial.putLayout(hBlock(db,"Dataset",db));
	QLineEdit* nameline=new QLineEdit(&dial);
	nameline->setText("NewArray");
	dial.putLayout(hBlock(nameline,"Array name",nameline));
	QSpinBox* sb=new QSpinBox(&dial);
	sb->setMinimum(1);
	sb->setMaximum(99999999);
	sb->setValue(16);
	dial.putLayout(hBlock(nameline,"Size",sb));
	int res=dial.exec();
	if (res) {
		const int k=db->currentIndex();
		QList<Array_> arr=data->get_arrays(k);
		Array_ na(nameline->text());
		na.data.create(sb->value(),0.0);
		arr.push_back(na);
		data->set_arrays(k,arr);
		on_data1comboBox_activated(data1comboBox->currentIndex());
		on_data2comboBox_activated(data2comboBox->currentIndex());
	}
}


void ArrayManager::fileInvert()
{
	const size_t k=data1comboBox->currentIndex();
	QList<Array_> arrays=data->get_arrays(k);
	const size_t m=array1comboBox->currentIndex();
	List<double> newdat(arrays[m].data.size());
	for (size_t i=0,j=arrays[m].data.size()-1; i<arrays[m].data.size();i++,j--)
		newdat(i)=arrays[m].data(j);
	arrays[m].data=newdat;
	data->set_arrays(k,arrays);
	updateTable();
}

void ArrayManager::on_showButton_clicked()
{
	arrayplot->xdata.clear();
	arrayplot->ydata.clear();
	QFont f;
	f.fromString(data->global_options.axisFont);
	arrayplot->setFont(f);
	for (size_t i=0; i<arrayTable->rowCount();i++) {
		bool ok ,ok2;
		double x=0,y=0;
		if (arrayTable->item(i,0)!=NULL)
			x=arrayTable->item(i,0)->text().toDouble(&ok);
		else ok=false;
		if (arrayTable->item(i,1)!=NULL)
			y=arrayTable->item(i,1)->text().toDouble(&ok2);
		else ok2=false;
		if (ok && ok2) {
			arrayplot->xdata.push_back(x);
			arrayplot->ydata.push_back(y);
		}
	}
	arrayplot->fit.clear();
	plotBox->setEnabled(true);
	arrayplot->update();
}

void ArrayManager::on_setExpressionButton_clicked()
{
	varNames.clear();
	var.clear();
	try {
		parser.SetExpr(expressionLineEdit->text().toStdString());
	// Get the map with the variables
		mu::varmap_type variables = parser.GetUsedVar();
//		cout << "Number: " << (int)variables.size() << "\n";

	// Get the number of variables 
		mu::varmap_type::const_iterator item = variables.begin();

	// Query the variables
		for (; item!=variables.end(); ++item)
		{
			varNames<<QString::fromStdString(item->first);
		}
		if (!varNames.contains("x")) {
			QMessageBox::warning(this, "x is not found", "You expression should contain x as a variable");
			parser.SetExpr("");
			return;	
		}
		int i=varNames.indexOf("x");
		varNames.removeAt(i);
		var.resize(varNames.size());
		for (int i=0; i<varNames.size(); i++) {
			var[i]=1.0;
			parser.DefineVar(varNames.at(i).toStdString(), &var[i]);
		}
		parser.DefineVar("x",&xvar);
		createVarsTable();
		evalButton->setEnabled(true);
		fitButton->setEnabled(true);
	}
	catch (mu::Parser::exception_type &e)
	{
		QMessageBox::critical(this,"Bad expression",QString::fromStdString(e.GetMsg()));
		return;
	}
}

void ArrayManager::on_expressionLineEdit_textChanged(QString)
{
	evalButton->setEnabled(false);
	fitButton->setEnabled(false);
}

void ArrayManager::createVarsTable()
{
	varsTable->clear();
	QStringList header;
	header<<"Variable"<<"Value"<<"Lower sigma"<<"Upper sigma";
	varsTable->setRowCount(varNames.size());
	varsTable->setColumnCount(4);
	varsTable->hideColumn(2);
	varsTable->hideColumn(3);
	varsTable->setHorizontalHeaderLabels(header);
	for (int i=0; i<varNames.size(); i++) {
		varsTable->setItem(i,0,new QTableWidgetItem(varNames.at(i)));
		varsTable->item(i,0)->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);
		varsTable->setItem(i,1,new QTableWidgetItem(QString("%1").arg(var[i])));
//		varsTable->setItem(i,2,new QTableWidgetItem("0.01"));
	}
}

void ArrayManager::on_evalButton_clicked()
{
	arrayplot->fit.clear();
	readVarsFromTable();
	try{
		for (size_t i=0; i<arrayplot->xdata.size(); i++) {
			xvar=arrayplot->xdata(i);
			arrayplot->fit.push_back(parser.Eval());
		}
	}
	catch (mu::Parser::exception_type &e)
	{
		QMessageBox::critical(this,"Evaluation problem",QString::fromStdString(e.GetMsg()));
		return;
	}
	arrayplot->update();
}

double conf95[]={1.0,3.84,5.99,7.82,9.49,11.07,12.59,14.07,15.51,16.92,18.31,19.68};
#include "Minuit2/MnPrint.h"

void ArrayManager::on_fitButton_clicked()
{
	QString log;
	readVarsFromTable();
	log+=QString("Number of points in original data: %1<br>").arg(arrayplot->xdata.size());
	err.clear();
	for (size_t i=0; i<var.size(); i++) {
		err.push_back(0.05*var[i]);
	}

//preset noise level to 2% of overall data
	double noise=0.02*(max(arrayplot->ydata)-min(arrayplot->ydata));
	log+=QString("Range of <i>Y</i>-values: %1<br>").arg(max(arrayplot->ydata)-min(arrayplot->ydata));
	log+=QString("By default data is assumed to have 5% noise which is: %1<br>").arg(noise);
	optimiser gen(arrayplot->ydata,arrayplot->xdata,noise,&parser,&xvar,&var);

	MnMigrad migrad(gen, var, err);
	MnSimplex simplex(gen, var, err);

	try {

		FunctionMinimum min=migrad();
//		cout<<"Migrad minimum found\n";
		FunctionMinimum min_simplex=simplex();
//		cout<<"MnGRAD results:\n";
//		cout<<min;
//		cout<<"MnSimplex results:\n";
//		cout<<min_simplex;
#ifdef USE_OLD_MINUIT
		if (!min.isValid()) {
			QMessageBox::warning(this, "Optimisation failed", "Optimisation unsuccesful\nTry other initial parameters/function");
			log+="Convergence has NOT been reached<br>";
			return;
			}
		var=min_simplex.userParameters().params();
		for (size_t i=0; i<var.size(); i++) {
			varsTable->item(i,1)->setText(QString("%1").arg(var[i]));
		}
#else
		if (!min.IsValid()) {
			QMessageBox::warning(this, "Optimisation failed", "Optimisation unsuccesful\nTry other initial parameters/function");
			log+="Convergence has NOT been reached<br>";
			return;
			}
		var=min_simplex.UserParameters().Params();
		for (size_t i=0; i<var.size(); i++) {
			varsTable->item(i,1)->setText(QString("%1").arg(var[i]));
		}
#endif
/*		if (min.isValid()) {
			gen.setUp(1.0);//simple 1 sigma
			qDebug()<<"Final chi^2:"<<min.fval();
			double stddev=sqrt(min.fval()*noise*noise)/arrayplot->xdata.size();//standard deviation recomputed from the chi^2 taken from response function
			qDebug()<<"noise"<<noise<<"stddev"<<stddev;
//			gen.setErr(stddev);
			MnMinos minos(gen,min);
			varsTable->showColumn(2);
			varsTable->showColumn(3);
			for (int i=0; i<varsTable->rowCount(); i++){
				std::pair<double,double> e=minos(i);
				varsTable->setItem(i,2,new QTableWidgetItem(QString("%1").arg(e.first)));
				varsTable->item(i,2)->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);
				varsTable->setItem(i,3,new QTableWidgetItem(QString("%1").arg(e.second)));
				varsTable->item(i,3)->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);
			}
//		   }
		}*/
//		if (min.IsValid()&&min.HasValidCovariance()) {
#ifdef USE_OLD_MINUIT
		if (min_simplex.isValid()){
#else
		if (min_simplex.IsValid()){
#endif

#ifdef USE_OLD_MINUIT
			log+=QString("<br><b>Simplex:</b> convergence HAS been reached in %1 steps<br>").arg(min_simplex.nfcn());
			double stddev=sqrt(min_simplex.fval()*noise*noise)/arrayplot->xdata.size();//standard deviation recomputed from the chi^2 taken from response function
#else
			log+=QString("<br><b>Simplex:</b> convergence HAS been reached in %1 steps<br>").arg(min_simplex.NFcn());
			double stddev=sqrt(min_simplex.Fval()*noise*noise)/arrayplot->xdata.size();//standard deviation recomputed from the chi^2 taken from response function
#endif
			log+="Parameters:<br>";
			for (size_t i=0; i<var.size(); i++)
				log+=QString("%1<br>").arg(var[i]);
#ifdef USE_OLD_MINUIT
			log+=QString("Final <i>chi</i><sup>2</sup>: %1<br>").arg(min_simplex.fval());
#else
			log+=QString("Final <i>chi</i><sup>2</sup>: %1<br>").arg(min_simplex.Fval());
#endif
			log+=QString("Standard deviation: %1<br>").arg(stddev);
			varsTable->showColumn(2);
			varsTable->showColumn(3);
#ifdef USE_OLD_MINUIT
			log+=QString("<br><b>Gradient method:</b> convergence HAS been reached in %1 steps<br>").arg(min.nfcn());
#else
			log+=QString("<br><b>Gradient method:</b> convergence HAS been reached in %1 steps<br>").arg(min.NFcn());
#endif
			log+="Parameters:<br>";
#ifdef USE_OLD_MINUIT
			for (size_t i=0; i<min.userParameters().params().size(); i++)
				log+=QString("%1<br>").arg(min.userParameters().params()[i]);
			log+=QString("Final <i>chi</i><sup>2</sup>: %1<br>").arg(min.fval());
#else
			for (size_t i=0; i<min.UserParameters().Params().size(); i++)
				log+=QString("%1<br>").arg(min.UserParameters().Params()[i]);
			log+=QString("Final <i>chi</i><sup>2</sup>: %1<br>").arg(min.Fval());
#endif
			log+="<br><i>Covariance matrix:<br></i>";
			for (int i=0; i<varsTable->rowCount(); i++){
				for (int j=0; j<varsTable->rowCount(); j++){
#ifdef USE_OLD_MINUIT
					log+=QString("%1	").arg(2.0*(min.error().invHessian())(i,j));
#else
					log+=QString("%1	").arg(2.0*(min.Error().InvHessian())(i,j));
#endif
				}
				log+="<br>";
			}
			log+="<br>";
			for (int i=0; i<varsTable->rowCount(); i++){
#ifdef USE_OLD_MINUIT
				double err=sqrt(2.0*(min.error().invHessian())(i,i))/noise*stddev;
#else
				double err=sqrt(2.0*(min.Error().InvHessian())(i,i))/noise*stddev;
#endif
				log+=QString("Error (computed from inv Hessian) for parameter %1 is %2<br>").arg(i).arg(err);
//Work around for the Minos bug under Windows
#ifdef Q_WS_WIN
				varsTable->setItem(i,2,new QTableWidgetItem(QString("%1").arg(err)));
				varsTable->item(i,2)->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);
				varsTable->setItem(i,3,new QTableWidgetItem(QString("%1").arg(err)));
				varsTable->item(i,3)->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);
#endif 
			}
			//gen.setErr(stddev);
#ifndef Q_WS_WIN
			log+="<br><b>MINOS</b> analysis recalculated to standard deviation (using simplex results)<br>";
			MnMinos minos(gen,min);
			for (int i=0; i<varsTable->rowCount(); i++){
				std::pair<double,double> e=minos(i);
				log+=QString("Error for parameter %1. Upper: %2, Lower: %3<br>").arg(i).arg(e.first/noise*stddev).arg(e.second/noise*stddev);
				varsTable->setItem(i,2,new QTableWidgetItem(QString("%1").arg(e.first/noise*stddev)));
				varsTable->item(i,2)->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);
				varsTable->setItem(i,3,new QTableWidgetItem(QString("%1").arg(e.second/noise*stddev)));
				varsTable->item(i,3)->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);
			}
#endif
		}
		else { //fitting has failed
			varsTable->hideColumn(2);
			varsTable->hideColumn(3);
		}
		on_evalButton_clicked();
	}
	catch (mu::Parser::exception_type &e)
	{
		QMessageBox::critical(this,"Evaluation problem",QString::fromStdString(e.GetMsg()));
		return;
	}
/*
//libcmatrix optimisation
	lcmat_optimiser loptim;
	loptim.parser=&parser; //set pointer to parser
	loptim.xvals=arrayplot->xdata;//set list of x-values
	loptim.pparas=&var;//set pointer to parameters, which needed to parser
	loptim.xval=&xvar;//set pointer to x-vlaue, needed for parser
	rmatrix covar;//covariance matrix
	List<size_t> which;
	List<double> errs; //estimate error of each parameter
	List<double> parlist;
	List<double> dest(arrayplot->ydata.size(),0.0);
	for (int i=o; i<var.size(); i++) {
		which.push_back(i);//which list specifies variable parameters (all in our case)
		double er=var[i]*0.01;
		if (!er)
			er=0.001;
		errs.push_back(er);
		parlist.push_back(var[i]);
	}
	try {
		c2=fitdata(covar, var, loptim, arrayplot->ydata, which, errs, noise, 2)
	}
	catch (MatrixExeption e) {
		cout<<e.what();
	}
*/
	promptDialog dial(this);
	QTextEdit* textbox=new QTextEdit(&dial);
	textbox->clear();
	textbox->setHtml(log);
	dial.putWidget(textbox);
	dial.exec();
}

void ArrayManager::readVarsFromTable()
{
	err.clear();
	err.resize(var.size());
	for (int i=0; i<varsTable->rowCount(); i++) {
		var[i]=varsTable->item(i,1)->text().toDouble();
//		err[i]=varsTable->item(i,2)->text().toDouble();
	}
}

void ArrayManager::createInterl()
{
	if (!data->size()) return;
	size_t k=data1comboBox->currentIndex();
	size_t ni= data->get_odata(k)->rows();
	Array_ interl(QString("Interlacing_list"));
	bool is_even=true;
	for (size_t i=0; i<ni; i++)
	{
		if (is_even) {
			interl.data.push_back(ni-1-i/2);
			is_even=false;
		}
		else {
			interl.data.push_back((i-1)/2);
			is_even=true;
		}
	}
//	cout<<interl.data<<endl;
	QList<Array_> arrays=data->get_arrays(k);
	arrays.append(interl);
	data->set_arrays(k, arrays);
	fill_comboBox(data1comboBox->currentIndex(), array1comboBox);
	fill_comboBox(data2comboBox->currentIndex(), array2comboBox);
	updateTable();
}

void ArrayManager::createREDOR()
{
	if (!data->size()) return;
	const size_t k=data1comboBox->currentIndex();
	size_t ni= data->get_odata(k)->rows();
	Array_ redor(QString("REDOR_list"));
	size_t half=ni/2;
	for (size_t i=0; i<half; i++) {
		redor.data.push_back(i+1);
		redor.data.push_back(i+half+1);
	}
	QList<Array_> arrays=data->get_arrays(k);
	arrays.append(redor);
	data->set_arrays(k, arrays);
	fill_comboBox(data1comboBox->currentIndex(), array1comboBox);
	fill_comboBox(data2comboBox->currentIndex(), array2comboBox);
	updateTable();
}

void ArrayManager::createFromSpectrum()
{
	if (!data->size()) return;
	const size_t k=data1comboBox->currentIndex();
	if (data->get_odata(k)->rows()>1) {
		QMessageBox::critical(this,"Unsupported function","This function is working for 1D data only.\nPlease select 1D dataset in left pooldown menu");
		return;
	}
	Array_ reald(QString("RealSpectrum"));
	Array_ imagd(QString("ImagSpectrum"));
	Array_ xarr(QString("XVector"));
	const size_t np=data->get_odata(k)->cols();
	for (size_t i=0; i<np; i++) {
		reald.data.push_back(real(data->get_odata(k,0,i)));
		imagd.data.push_back(imag(data->get_odata(k,0,i)));
		xarr.data.push_back(data->get_x(k,i));
	}
	QList<Array_> arrays=data->get_arrays(k);
	arrays.append(reald);
	arrays.append(imagd);
	arrays.append(xarr);
	data->set_arrays(k, arrays);
	fill_comboBox(data1comboBox->currentIndex(), array1comboBox);
	fill_comboBox(data2comboBox->currentIndex(), array2comboBox);
	updateTable();
}

void ArrayManager::fromFile()
{
	if (!data->size()) return;
	size_t k=data1comboBox->currentIndex();
	QString fname = QFileDialog::getOpenFileName(this,"Choose a file",p->working_directory,"All files (*)");
	if (fname.isEmpty())
		return;
	bool ok;
	QString s=fname;
	QString name=QInputDialog::getText(this,"Name","Input a name for the array",QLineEdit::Normal, s.remove(0, s.lastIndexOf(QRegExp("[/\\\\]"))+1), &ok);
	if (!ok) {
		QMessageBox::critical(this, "Error", "The name is invalid");
		return;
	}
	Array_ newarr(name);
	QFile file(fname);
        if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
            return;

        QTextStream in(&file);
        while (!in.atEnd()) {
        	QString line = in.readLine();
		line.remove(QChar('\n'));
		double multiplier=1.0;
		if (line.at(line.size()-1)==QChar('s')) {
			multiplier=1.0e3;
			line.truncate(line.size()-1);
		}
		if (line.at(line.size()-1)==QChar('m')) {
			multiplier=1.0;
			line.truncate(line.size()-1);
		}
		if (line.at(line.size()-1)==QChar('u')) {
			multiplier=1.0e-3;
			line.truncate(line.size()-1);
		}
        	double val=line.toDouble(&ok);
		if (!ok) {
			QMessageBox::critical(this, "Error", QString("Value %1 is invalid").arg(line));
			return;
		}
		val*=multiplier;
		newarr.data.push_back(val);
        }
	QList<Array_> arrays=data->get_arrays(k);
	arrays.append(newarr);
	data->set_arrays(k, arrays);
	fill_comboBox(data1comboBox->currentIndex(), array1comboBox);
	fill_comboBox(data2comboBox->currentIndex(), array2comboBox);
	updateTable();
}


void ArrayManager::fileExportMatlab()
{
    QString title = QFileDialog::getSaveFileName(
                    this,
                    "Choose a filename to save under",
                    QDir::homePath(),
                    "Matlab files (*)");
     if (title.isEmpty()) return;
    matlab_controller mcont(title.toLatin1().data(),5, 2); //save to 'fname' using Matlab 5 format
	size_t n=data->size();
	for (size_t k=0; k<n; k++) {
		QList<Array_> arrays=data->get_arrays(k);
		size_t len=arrays.size();
		for (size_t i=0; i<len; i++) {
			QString title=data->get_short_filename(k)+"_"+arrays.at(i).name;
            mcont.write(arrays[i].data, title.toLatin1().data());
			//write here
		}
	}
}

void ArrayManager::on_arrayTable_customContextMenuRequested()
{
	QMenu menu;
	menu.addAction(editCopyAction);
	menu.addAction(editPasteAction);
	menu.exec(QCursor::pos());
}

void ArrayManager::on_varsTable_customContextMenuRequested()
{
	on_arrayTable_customContextMenuRequested();
//	cout<<"This works\n";
//	QMenu menu;
//	menu.addAction(editCopyVarsAction);
//	menu.addAction(editPasteVarsAction);
//	menu.exec(QCursor::pos());
}

void ArrayManager::readSettings()
{
        QSettings settings("GSim", "GSim");
        settings.beginGroup("ArrayManager");
        resize(settings.value("size", QSize(550, 400)).toSize());
        move(settings.value("pos", QPoint(100, 100)).toPoint());
	expressionLineEdit->setText(settings.value("lastExpression",QString("")).toString());
        settings.endGroup();
}

void ArrayManager::writeSettings()
{
	QSettings settings("GSim", "GSim");
        settings.beginGroup("ArrayManager");
        settings.setValue("size", size());
        settings.setValue("pos", pos());
	settings.setValue("lastExpression",expressionLineEdit->text());
        settings.endGroup();
}
