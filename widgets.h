#ifndef widgets_h_
#define widgets_h_

#include <QTableWidget>
#include <QClipboard>
#include <QApplication>
#include <QShortcut>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QDialog>
#include <QLabel>
#include <QDialogButtonBox>
#include <QLineEdit>
#include <QToolButton>
#include <QStringListModel>
#include <QCompleter>

void fromTableToClipboard(QTableWidget*);
void fromClipboardToTable(QTableWidget*);


QHBoxLayout* hBlock(QWidget*,QString,QWidget*);

class promptDialog : public QDialog
{
Q_OBJECT
public:
     promptDialog(QWidget* parent);
     ~promptDialog() {};
     void putWidget(QWidget*);
     void putLayout(QLayout*);
private:
     QVBoxLayout* ml;
     QVBoxLayout* wl;
};

/*
class zTableWidget: public QTableWidget
{
Q_OBJECT
public:
    zTableWidget( QWidget *parent=0);
    ~zTableWidget();
public slots:
    void fromTableToClipboard();
    void fromClipboardToTable();
private:
    QShortcut* copyshortcut;
    QShortcut* pasteshortcut;
};
*/

class LineWithBrowser : public QLineEdit
{
    Q_OBJECT

public:
    LineWithBrowser(QWidget *parent = 0);

protected:
    void resizeEvent(QResizeEvent *);

private slots:
    void setFilename();

private:
    QToolButton *browserButton;
};

class commandLineWidget : public QLineEdit
{
Q_OBJECT
public:
      commandLineWidget(QWidget *parent = 0);
      inline QStringListModel* model() {return qobject_cast<QStringListModel*> (commandCompleter->model());};
      void addToHistory(QString);
protected:
      QCompleter* commandCompleter;
      virtual void keyPressEvent ( QKeyEvent *);
      QStringList historyList;
      int historyPosition;
      void resizeEvent(QResizeEvent *);
      QToolButton *clearButton;
private slots:
      void resetHistory();
      void updateCloseButton(const QString &text);
};

#endif
