#ifndef FITDIALOG_H
#define FITDIALOG_H

#include <QVariant>
#include <QDialog>
#include <QLabel>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QGridLayout>
#include <QSettings>
#include <QProcess>
#include <QDir>
#include "ui_fitdialog.h"

class MainForm;

struct simulSetup {
	QString workingDir;
	QString expSpec;
	QString program;
	QString addArguments;
	QString resultingSpectrum;
	QString editorCommand;
	simulSetup() {program="pNMRsim";editorCommand="kate";};
};

class fitDialog : public QDialog, public Ui::fitDialog
{
    Q_OBJECT

public:
    fitDialog( MainForm* parent = 0, Qt::WindowFlags fl = 0 );
    ~fitDialog();

public slots:
  void on_infileBox_currentIndexChanged(int);
  void on_simButton_clicked();
  void on_fitButton_clicked();
  void on_setupButton_clicked();
  void on_cancelSetupButton_clicked();
  void on_saveSetupButton_clicked();
  void on_editButton_clicked();
  void on_browseDirButton_clicked();
  void on_saveLogButton_clicked();
  void on_saveParamsButton_clicked();
  void on_openParamsButton_clicked();
  void on_newSimButton_clicked();
  void on_finishButton_clicked();
  void updateDir();
  void updateText();
  void finish(int, QProcess::ExitStatus);
  void readSettings();
  void writeSettings();
private:
  void parse_line(QString);
  bool fitIsActive;
  QProcess* proc;
  double reference;
  MainForm* mainform;
  simulSetup setup;
  QString wd() {return (setup.workingDir.isEmpty())? QDir::homePath()+"/.gsim" : setup.workingDir;};
  QString currentParam(const QString& );
  QHash<QString,QStringList> recentlyUsed;
};


#endif // FITDIALOG_H
