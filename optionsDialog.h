#ifndef OPTIONSDIALOG_H
#define OPTIONSDIALOG_H

#include <QVariant>
#include <QDialog>
#include <QLabel>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QGridLayout>
#include <QSettings>
#include <QFontDialog>
#include "ui_optionsDialog.h"
#include "base.h"
#include "mainform.h"

class optionsDialog : public QDialog, public Ui::optionsDialog
{
    Q_OBJECT

public:
    optionsDialog( MainForm* parent = 0, Qt::WindowFlags fl = 0 , DataStore* d = 0);
    ~optionsDialog();


public slots:
  void on_okButton_clicked();
  void on_cancelButton_clicked();
  void on_styleComboBox_currentIndexChanged(const QString &);
  void on_fontButton_clicked();
  void on_schemeComboBox_currentIndexChanged(const QString &);
private:
  void readSettings();
  void writeSettings();
  DataStore* data;
  MainForm* p;
};

#endif // ABOUTFORM_H
