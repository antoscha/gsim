;NSIS Modern User Interface
;Script automatically created by Mihov NSIS Helper 3.3 and adjusted later
;http://freeware.mihov.com
;-----------------------------------------------------
!include "MUI2.nsh"
Name "GSim"
OutFile "gsim_setup.exe"
InstallDir "$PROGRAMFILES\GSim"

;Get install folder from registry for updates
InstallDirRegKey HKCU "Software\GSim" ""

var StartMenuFolder

!define MUI_ICON "install.ico"
!define MUI_UNICON "install.ico" 
!define MUI_ABORTWARNING
!insertmacro MUI_PAGE_WELCOME
!insertmacro MUI_PAGE_LICENSE "LICENSE.GPL"
!insertmacro MUI_PAGE_DIRECTORY

  ;Start Menu Folder Page Configuration
  !define MUI_STARTMENUPAGE_REGISTRY_ROOT "HKCU" 
  !define MUI_STARTMENUPAGE_REGISTRY_KEY "Software\GSim" 
  !define MUI_STARTMENUPAGE_REGISTRY_VALUENAME "GSim"
  
  !insertmacro MUI_PAGE_STARTMENU Application $StartMenuFolder


!insertmacro MUI_PAGE_INSTFILES
!insertmacro MUI_PAGE_FINISH
!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES
!insertmacro MUI_LANGUAGE "English"
ShowUninstDetails show

Section "Program Files"
  SetOutPath "$INSTDIR"
  File "gsim.exe"
  File "LICENSE.GPL"
  File "mingwm10.dll"
  File "QtCore4.dll"
  File "QtGui4.dll"
  File "QtXml4.dll"
  File "QtSvg4.dll"
  File "qgif4.dll"
  File "qjpeg4.dll"
  File "qmng4.dll"
  File "qtiff4.dll"
  File "qsvg4.dll"
  File "qico4.dll"
  File "README_GSIM.pdf"
  File "quickstart.pdf"
 
  ;Store install folder
  WriteRegStr HKCU "Software\GSim" "" $INSTDIR
 
 ;Create uninstaller
 WriteUninstaller "$INSTDIR\Uninst.exe"
 
  WriteRegStr HKEY_LOCAL_MACHINE "Software\Microsoft\Windows\CurrentVersion\Uninstall\GSim" "DisplayName" "GSim (remove only)"
  WriteRegStr HKEY_LOCAL_MACHINE "Software\Microsoft\Windows\CurrentVersion\Uninstall\GSim" "UninstallString" '"$INSTDIR\uninst.exe"'
  WriteUnInstaller "uninst.exe"
SectionEnd
 
Section "Start Menu Shortcuts"
;  CreateDirectory "$SMPROGRAMS\GSim"
;  CreateShortCut "$SMPROGRAMS\GSim\Uninstall GSim.lnk" "$INSTDIR\uninst.exe" "" "$INSTDIR\uninst.exe" 0
;  CreateShortCut "$SMPROGRAMS\GSim\GSim.lnk" "$INSTDIR\gsim.exe" "" "$INSTDIR\gsim.exe" 0
;  CreateShortCut "$SMPROGRAMS\GSim\GSim help.lnk" "$INSTDIR\README_GSIM.pdf" "" "$INSTDIR\README_GSIM.pdf" 0
;  CreateShortCut "$SMPROGRAMS\GSim\Quick start.lnk" "$INSTDIR\quickstart.pdf" "" "$INSTDIR\quickstart.pdf" 0

!insertmacro MUI_STARTMENU_WRITE_BEGIN Application
    
 ;Create shortcuts
  CreateDirectory "$SMPROGRAMS\GSim"
  CreateShortCut "$SMPROGRAMS\GSim\Uninstall GSim.lnk" "$INSTDIR\uninst.exe" "" "$INSTDIR\uninst.exe" 0
  CreateShortCut "$SMPROGRAMS\GSim\GSim.lnk" "$INSTDIR\gsim.exe" "" "$INSTDIR\gsim.exe" 0
  CreateShortCut "$SMPROGRAMS\GSim\GSim help.lnk" "$INSTDIR\README_GSIM.pdf" "" "$INSTDIR\README_GSIM.pdf" 0
  CreateShortCut "$SMPROGRAMS\GSim\Quick start.lnk" "$INSTDIR\quickstart.pdf" "" "$INSTDIR\quickstart.pdf" 0 
!insertmacro MUI_STARTMENU_WRITE_END
SectionEnd
 
Section "Uninstall"
  RMDir /r "$INSTDIR"
  ; remove shortcuts, if any.
  RMDir /r "$SMPROGRAMS\GSim"
 
  DeleteRegKey HKEY_LOCAL_MACHINE "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\GSim"
  DeleteRegKey /ifempty HKCU "Software\GSim"

  MessageBox MB_YESNO "Uninstall saved configuratons?" IDYES true IDNO false
true:
  DeleteRegKey HKCU "Software\GSim\GSim"
  RMDir /r $PROFILE\.gsim
  Goto next
false:
next:

SectionEnd
