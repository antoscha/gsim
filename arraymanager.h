/****************************************************************************
** Form interface generated from reading ui file 'aboutform.ui'
**
** Created: Wed May 25 11:16:53 2005
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.1.2   edited Dec 19 11:45 $)
**
****************************************************************************/

#ifndef ARRAYMANAGER_H
#define ARRAYMANAGER_H

#include <QVariant>
#include <QMainWindow>
#include "ui_arraymanager.h"
#include "mainform.h"
#include "widgets.h"
#include <QMenu>
#include <QToolBar>
#include <QMenuBar>
#include <QMessageBox>
#include <QFileDialog>
#include <matlabio.h>
#include <QInputDialog>
#include <QTextStream>
#include <muParser.h>

#ifdef USE_OLD_MINUIT
	#include "Minuit/FCNBase.h"
	#include "Minuit/MnMinos.h"
	#include "Minuit/MnMigrad.h"
	#include "Minuit/MnSimplex.h"
#else
	#include "Minuit2/FCNBase.h"
	#include "Minuit2/MnMinos.h"
	#include "Minuit2/MnMigrad.h"
	#include "Minuit2/MnSimplex.h"
	using namespace ROOT::Minuit2;
#endif

#include "graphics_out.h"
//#include <iostream.h>



class optimiser: public FCNBase {
public:
	optimiser(const List<double>& meas, const List<double>& xvec, const double& mvar, mu::Parser* p, double* xx, vector<double> * pr )
			{theXvec=xvec; theMeasurements=meas; parser=p; xval=xx; theError=mvar; ppars=pr; upv=1.0;};
	~optimiser() {};
#ifdef USE_OLD_MINUIT
	virtual double up() const {return upv;};
#else
	virtual double Up() const {return upv;};
#endif
	void setUp(double v) {upv=v;};
	void setErr(double e) {theError=e;};
	virtual double operator()(const vector<double>&) const;
private:
	double upv;
	List<double> theMeasurements;
	List<double> theXvec;
	double theError;
	mu::Parser* parser;
	double* xval;
	vector<double> * ppars;
};


/* libcmatrix optimiser
#include "optim.h"

class lcmat_optimiser: public BaseMinFunction
{
public:
	lcmat_optimiser() {};
	void operator() (BaseList<double>& , const BaseList<double>& ) const;
	mu::Parser* parser;
	BaseList<double> xvals;
	double* xval;
	vector<double>* ppars;
};
*/

class ArrayManager : public QMainWindow, public Ui::ArrayManager
{
    Q_OBJECT

public:
    ArrayManager( MainForm* parent = 0, Qt::WindowFlags fl = Qt::Window);
    ~ArrayManager();

//    QMenuBar *menubar;
    QToolBar *toolBar;
    QMenu *fileMenu;
	QMenu *fileSpecialMenu;
    QMenu *editMenu;
    QAction* fileNewArrayAction;
    QAction* fileExitAction;
    QAction* fileSpecialInterlacing;
    QAction* fileSpecialREDOR;
    QAction* fileSpecialFromSpectrum;
    QAction* fileFromFile;
    QAction* fileInvertArray;
    QAction* fileExportMatlabAction;
    QAction* editCopyAction;
    QAction* editPasteAction;
    QAction* plotCopyAction;
    QAction* plotExportVectorAction;
//    QAction* editCopyVarsAction;
//    QAction* editPasteVarsAction;


protected:
   MainForm * p;
   DataStore * data;
   mu::Parser parser;
   QStringList varNames;
   vector<double> var;
   vector<double> err;
   double xvar;

   void createVarsTable();

   size_t max_length;
   QStringList headers;
   void fill_comboBox(size_t , QComboBox*);
   void fill_col(QComboBox*, QComboBox*, size_t);
   bool read_col(QComboBox*, QComboBox*, size_t);
   void updateTable();
   void readSettings();
   void writeSettings();
   void closeEvent ( QCloseEvent * );

public slots:
  void on_okButton_clicked();
  void on_cancelButton_clicked();
  void on_data1comboBox_activated(int);
  void on_data2comboBox_activated(int);
  void on_array1comboBox_activated();
  void on_array2comboBox_activated();
  void on_showButton_clicked();
  void on_setExpressionButton_clicked();
  void on_evalButton_clicked();
  void on_fitButton_clicked();
  void on_expressionLineEdit_textChanged(QString);
  void readVarsFromTable();
  void fileNewArray();
  void createInterl();
  void createREDOR();
  void createFromSpectrum();
  void fromFile();
  void fileInvert();

  void fileExportMatlab();

  void editCopy();
  void editPaste();
  void plotCopy();
  void plotExportVector();
  void plotContextMenu(const QPoint&);
  void on_arrayTable_customContextMenuRequested();
  void on_varsTable_customContextMenuRequested();
};

#endif // ARRAYMANAGER_H
