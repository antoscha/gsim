#ifndef spinsightio_h_
#define spinsightio_h_

//Simple convertor from SPINSIGHT FID to SIMPSON spectrum (non-phased)
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <unistd.h>
#include <ctype.h>
#include <math.h>
#include "base.h"
//#include "List.h"
//#include "cmatrix_utils.h"
//#include "base.h"
#include <QList>

using namespace std;
using namespace libcmatrix;

int isreadable(const char*);

int is_spinsight(const char*);

inline int ambigendian()
{
  static const int x=1;
  return ( (*(char *)&x) != 1);
};

inline void swap_bytes(unsigned char* a, unsigned char* b)
{
  unsigned char c=*a;
  *a=*b;
  *b=c;
};

int read_spinsight(cmatrix &, gsimFD &, const char*);

#endif
