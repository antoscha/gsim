#ifndef base_h_
#define base_h_
#include <cstdio>
#include <QString>
#include "List.h" //from libcmatrix
#include "cmatrix.h"
#include <QList>
#include <QFile>
#include <QHash>
#include <QMutex>

#include <QPoint>
#include "simpsonio.h"
#include "widgets.h"

using namespace libcmatrix;
using namespace std;

enum FileFormat {FFORMAT_AUTO, FFORMAT_SIMPSON, FFORMAT_SPINSIGHT, FFORMAT_XWINNMR, FFORMAT_VNMR, FFORMAT_SPINEVOLUTION, FFORMAT_XWINNMRPROC};
//gccenum AxisUnits {UNIT_HZ, UNIT_PPM, UNIT_US,  UNIT_VIRT};

enum {UNITS_HZ, UNITS_KHZ, UNITS_PPM, UNITS_US, UNITS_MS, UNITS_S, UNITS_FPTS, UNITS_TPTS, UNITS_TVIRT, UNITS_FVIRT, UNITS_NONE};

enum {UNITSTYPE_FREQ, UNITSTYPE_TIME, UNITSTYPE_IND};

enum { 
	OpenUpdateWorkingDirectory=0x01,
	OpenSetActive=0x02,
	OpenFullRestart=0x04,
	OpenAppend=0x08
};

enum {
	SaveUpdateFileName=0x01,
	SaveUpdateWorkingDirectory=0x02
};


class BaseFileFilter;

void invert(cmatrix&, bool);
void invert(List<double>&);
void halfshift(List<complex>& );
void halfshift(cmatrix& );

int zToInt(QString, bool* ok=0);

template <class T> int indexFromName(T& list, QString s)
{
	int ind=-1;
	for (size_t i=0; i<list.size(); i++)
		if (list.at(i)->name()==s)
			ind=i;
	return ind;
}

enum {PRINT1D_ALL, PRINT1D_SKIP, PRINT1D_SPLIT};

class global_options_ {
public:
	size_t ticks;
	bool hasXaxis;
	bool hasYaxis;
	bool hasExtraTicks;
	size_t extraTicks;
	QString axisFont;
	bool antialiasing;
	bool smoothTransformations;
	bool animatedMarkers;
	QString customPrintLogo;
	bool printPars;
	int printLineWidth;
        int print1Dmode; //PRINT1D_ALL-print single line; PRINT1D_SKIP-reduce resolution; PRINT1D_SPLIT-split
	QString style;
	int xtimeunits, xfrequnits, ytimeunits, yfrequnits;
	bool useHypercomplex;
	bool useBold1D;
	bool mixFidSpec;
	bool useSkyline;
	bool useSystemPrintPars;
	bool yLeftLabels; //print labels on the left from y-axis
	QStringList procToolBarList;
	size_t maxAttachedProc; //maximum of remebered files with attached processing
	global_options_() {readSettings();maxAttachedProc=100;useSystemPrintPars=true;};
	~global_options_() {writeSettings();};
private:
	void readSettings();
	void writeSettings();
};

struct parent_ { //contain link to parent
bool hasParent;
bool isRow;
bool isTempDeconv; //temporary line, introduced  in deconvolution and used in phasing
size_t parentIndex;
size_t sliceIndex;
parent_() {hasParent=false; isRow=true; isTempDeconv=false; parentIndex=999999; sliceIndex=999999;};
};

struct Array_ {
QString name;
List<double> data;
Array_(QString);
};

struct Display_  {
public:
    double vscale;
    double vshift;
    double xscale;
    double xshift;
    bool isRealOn;
    bool isVisible;
    bool hasPeaks;
    bool changed() const {return (vscale!=1.0)||(vshift)||(xscale!=1.0)||(xshift); };
    Display_() {vscale=1.0; vshift=0.0; xscale=1.0; xshift=0.0;isRealOn=true; isVisible=true; hasPeaks=false;};
};

struct Axis_ {
    List<double> xvector, yvector;
};

struct Integrals_ {
    QList < List<double> > yintegrals;
    QList < List<double> > xintegrals;
    double integral_scale;
    double integral_vshift;
    double integral_vscale;
    double integral_lvl;
    double integral_tlt;
    Integrals_() {integral_scale=1; integral_vscale=0.1; integral_vshift=0.0; integral_lvl=0.0; integral_tlt=1.0;}
};

class gsimFD
{
public:
    double sw, sw1;
    double sfrq, sfrq1;
    int type, type1;
    double ref, ref1;
    bool is_on_disk;
    double phd0, phd1, phi0, phi1; //applied phases in direct and indirect dimensions (in degrees)
    double spinrate; //spin rate in Hz
    double temperature; //temperature in C
    QString processing; //applied processing

    BaseFileFilter* file_filter;
    QString filename;
    QString title;
    QString pulprog; //name of the pulse program

    QStringList parameterFiles;
    QList< QHash <QString, QString> > allParameters;
    gsimFD() {sfrq=0.0; sfrq1=0.0; ref=0.0; ref1=0.0; phd0=0.0; phd1=0.0; phi0=0.0; phi1=0.0; type1=FD_TYPE_FID; is_on_disk=false; file_filter=NULL;sw=10.0;sw1=10.0;spinrate=0.0;temperature=0.0;processing="";};
};

enum GraphType {GRAPHTEXT, GRAPHLINE, GRAPHIMAGE, GRAPHVPICTURE}; ///type of extra graphics: text, line, bitmap image or vector picture

struct graph_object{
	GraphType type;//one of the for types above
	QColor colour; //colour of the line and text
	QString font; //contains font for text entries
	QVariant pos; //QpointF (real coordinates) for text and bitmap, QLineF for lines
	QVariant maindata; //can contain QPixmap (for GRAPHIMAGE)  or QString (for GRAPHTEXT)
	bool selected;
	double scale; ///used by images only. Applicable for horizontal and vertical scales
	double angle; ///used by text as a rotation angle
	graph_object() {selected=false; scale=1.0; angle=0.0; colour=Qt::transparent; };
	};


class data_entry
{
public:
    QList<Array_> arrays;
    Display_ display;
    Axis_ axis;
    Integrals_ integrals;
    parent_ parent;
    cmatrix spectrum;
    cmatrix hyperspectrum;
    gsimFD info;
    QList<graph_object> graphics;
    data_entry(){spectrum.clear(); hyperspectrum.clear();};
};


class DataStore: public QObject
{
Q_OBJECT
private:
	QList<data_entry> data;
	QList<data_entry> backup;
	size_t active_curve;
public:
	DataStore(QObject * parent=0);
	~DataStore();
	mutable QMutex mutex;
	global_options_ global_options;
	void makeBackup();
	void undo();
	inline void add_entry(data_entry entry) {data.append(entry);};
	inline void set_entry(size_t k, data_entry entry) {data[k]=entry;};
	const inline data_entry* get_entry(size_t k) { return &data[k]; };
	inline void setActiveCurve(size_t k) { active_curve=k;};
	QList<int> selectedCurves; //contains a list of the selected curves. One of them is active.
	const inline size_t getActiveCurve() {if (active_curve>=data.size() && active_curve) active_curve=data.size()-1; return active_curve;}; //return the 'Active' (only single) curve
	void anyFileOpen(QString&, BaseFileFilter*,int flags=OpenUpdateWorkingDirectory | OpenSetActive | OpenFullRestart |OpenAppend);
	int anyFileSave(size_t, QString&, BaseFileFilter*,int flags=SaveUpdateFileName | SaveUpdateWorkingDirectory); //index, name, format, flags
	inline size_t size(){return data.size();};
	inline size_t size(int i) {return data.at(i).spectrum.size();};
	QString	       get_short_filename(size_t); //usually filename without directory pathes


//stupid integral section
	void add_integral(size_t, List<double>&, List<double>&);
	inline Integrals_ get_integrals(size_t k) {return data[k].integrals;};
	inline void set_integrals(size_t k, Integrals_ integ) {data[k].integrals=integ;};
	inline void add_integral_scale(size_t k, double scale) {data[k].integrals.integral_scale=scale;};
	inline size_t integrals_size(size_t k) {return data.at(k).integrals.xintegrals.size();};
	inline size_t integrals_size(size_t k, size_t i) {return data.at(k).integrals.xintegrals.at(i).size();};
	inline double get_integral_x(size_t k, size_t i, size_t q) {return data.at(k).integrals.xintegrals.at(i)(q);};
	inline double get_integral_value(size_t k, size_t i, size_t q) {return data.at(k).integrals.yintegrals.at(i)(q);};
	inline double get_integral_scale(size_t k) {return data.at(k).integrals.integral_scale;};


	inline double get_x(size_t k, size_t i) {return data.at(k).axis.xvector(i);};
	inline double get_y(size_t k, size_t i) {return data.at(k).axis.yvector(i);};
	inline List<double> get_x(size_t k) {return data.at(k).axis.xvector;};
	inline List<double> get_y(size_t k) {return data.at(k).axis.yvector;};
	inline void set_x(size_t k, List<double> newv) {data[k].axis.xvector=newv;};
	inline void set_y(size_t k, List<double> newv) {data[k].axis.yvector=newv;};

	inline double get_value(size_t k, size_t i, size_t j) {return (data.at(k).display.isRealOn)?  real(data.at(k).spectrum(i,j)) : imag(data.at(k).spectrum(i,j)); };

	inline complex get_odata(size_t k, size_t i, size_t j) {return data.at(k).spectrum(i,j);};
	inline cmatrix* get_odata(size_t k) {return &(data[k].spectrum);};

	inline void set_odata(size_t k, size_t i, size_t j, complex val) {data[k].spectrum(i,j)=val;};
	inline void set_odata(size_t k, cmatrix newmatrix) {data[k].spectrum=newmatrix;};

	inline complex get_hdata(size_t k, size_t i, size_t j) {return data.at(k).hyperspectrum(i,j);};
	inline cmatrix* get_hdata(size_t k) {return &(data[k].hyperspectrum);};
	inline void set_hdata(size_t k, size_t i, size_t j, complex val) {data[k].hyperspectrum(i,j)=val;};
	inline void set_hdata(size_t k, cmatrix newmatrix) {data[k].hyperspectrum=newmatrix;};
	inline bool hasHyperData(const size_t k) {return (data[k].spectrum.rows()==data[k].hyperspectrum.rows())&&(data[k].spectrum.cols()==data[k].hyperspectrum.cols());};


	void takeRow(size_t,size_t);
	void takeCol(size_t, size_t);
	void takeHorProjection(size_t);
	void takeVerProjection(size_t);
	void takeHorPosSky(size_t);
	void takeVerPosSky(size_t);
	void takeHorNegSky(size_t);
	void takeVerNegSky(size_t);
	void takeHorTrain(size_t);
	void takeStack(size_t);
	void delete_curve(int);
	inline void delete_integrals(size_t k) {data[k].integrals.xintegrals.clear(); data[k].integrals.yintegrals.clear();};
	void resize(size_t, size_t, size_t); //dataindex, new np, new ni
	void create_xvector(size_t, double, double, size_t);
	void create_yvector(size_t, double, double, size_t);
	parent_ getParent(size_t k){return data.at(k).parent;};
	void setParent(size_t k, parent_ P) {data[k].parent=P;};
	inline const gsimFD* get_info(size_t k) {return &data.at(k).info;};
	inline void set_info(size_t k, gsimFD in) {data[k].info=in;};
	
	inline QList<graph_object> get_graphics(size_t k) {return data[k].graphics;};
	inline graph_object* get_graphobject(size_t k, size_t i) {return &data[k].graphics[i];};
	inline void set_graphics(size_t k, QList<graph_object> g) {data[k].graphics=g;};

	inline const Display_* get_display(size_t k) {return &data.at(k).display;};
	inline void set_display(size_t k, Display_ in) {data[k].display=in;};

	inline QList<Array_> get_arrays(size_t k) {return data.at(k).arrays;};
	inline void set_arrays(size_t k, QList<Array_> list) {data[k].arrays=list;};
//	void update_axis_vectors(size_t k);
	void updateAxisVectors();
	inline void show_real(size_t k) {data[k].display.isRealOn=true;};
	inline void show_imag(size_t k) {data[k].display.isRealOn=false;};
	complex average_pt(size_t k, int j, int dj, int i=0, int di=0);
        int currentXUnits(size_t);
        int currentYUnits(size_t);

#ifdef USE_SCRIPTS
//interface for scripting
public slots:
	int currentSpectrum() {return getActiveCurve();};
	int spectraCount() {return size();};
	QString shortFilename(int k) {if (k<0 || k>size()) return "ERROR"; else return get_short_filename(k);};
	const double x(int k, int i) {return get_x(k,i);};
	const double y(int k, int i) {return get_y(k,i);};
	const double value(int k, int i, int j) {return value(k,i,j);};
	const double rvalue(int k, int i, int j) {return get_odata(k,i,j).re;};
	const double ivalue(int k, int i, int j) {return get_odata(k,i,j).im;};
	void setRvalue(int k, int i, int j, double val) {set_odata(k,i,j,complex(val,get_odata(k,i,j).im));};
	void setIvalue(int k, int i, int j, double val) {set_odata(k,i,j,complex(get_odata(k,i,j).re,val));};
#endif

signals:
	void undoChanged(bool);
	void dataEmptified(bool);
};

bool isreadable(const char*);


inline void swap_bytes(unsigned char* a, unsigned char* b)
{
  unsigned char c=*a;
  *a=*b;
  *b=c;
};


class ColourScheme
{
public:
	QString name;
	QColor backgroundColour;
	QColor textColour;
	QColor markerColour;
	QColor axisColour;
	QColor watchColour;
	QList<QColor> plot2DColour;
	QList<QColor> plot3DContourColour;
	QList<QColor> plot3DRasterColour;
	QColor rectBorderColour;
	QColor rectFillColour;
};

class whiteScheme : public ColourScheme
{
public:
	whiteScheme();
};

class blackScheme : public ColourScheme
{
public:
	blackScheme();
};

class yellowScheme : public ColourScheme
{
public:
	yellowScheme();
};

//int round (double);

double convertUnits(double, int, int, const gsimFD*, int, bool directd=true, bool deltaonly=false); //interconvert units, 1st - old value, 2nd - old units, 3rd-new units, 4th - info associeted with the dataset, 5th - true for direct dimension, false for indirect; 6th - true if convert only scale (without shift of the beginning of coordinates)
List<double> convertUnits(List<double>&, int,int, const gsimFD*, int, bool directd=true, bool deltaonly=false); //interconvert a list of values
int unitsType(int);//determine the type of UNITS 


#endif
