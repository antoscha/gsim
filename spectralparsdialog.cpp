
#include "spectralparsdialog.h"
#include "base.h"

#include <QDebug>

spectralParsDialog::spectralParsDialog(MainForm *parent, Qt::WindowFlags fl)
	:QDialog(parent, fl) {
    setupUi(this);
    p=parent;
    setWindowTitle( "Spectral parameters" );
    setWindowIcon( QPixmap( ":/images/gsim.png" ) );
    size_t k=p->data->getActiveCurve();
    const gsimFD* info=p->data->get_info(k);
    titleLineEdit->setText(info->title);
    titleLineEdit->setObjectName("title");
    npLabel->setText(QString("%1 pts").arg(p->data->get_odata(k)->cols()));
    niLabel->setText(QString("%1 pts").arg(p->data->get_odata(k)->rows()));
    refSpinBox->setValue(info->ref);
    refSpinBox->setObjectName("ref");
    ref1SpinBox->setValue(info->ref1);
    ref1SpinBox->setObjectName("ref1");
    swSpinBox->setValue(info->sw);
    swSpinBox->setObjectName("sw");
    sw1SpinBox->setValue(info->sw1);
    sw1SpinBox->setObjectName("sw1");
    sfrqSpinBox->setValue(info->sfrq);
    sfrqSpinBox->setObjectName("sfrq");
    sfrq1SpinBox->setValue(info->sfrq1);
    sfrq1SpinBox->setObjectName("sfrq1");
    spinrateSpinBox->setValue(info->spinrate);
    spinrateSpinBox->setObjectName("spinrate");
    temperatureSpinBox->setValue(info->temperature);
    temperatureSpinBox->setObjectName("temperature");
    switch (info->type) {
	case FD_TYPE_FID:
		typeComboBox->setCurrentIndex(0);
		break;
	case FD_TYPE_SPE:
		typeComboBox->setCurrentIndex(1);
		break;
    }
    typeComboBox->setObjectName("type");
    switch (info->type1) {
	case FD_TYPE_FID:
		type1ComboBox->setCurrentIndex(0);
		break;
	case FD_TYPE_SPE:
		type1ComboBox->setCurrentIndex(1);
		break;
    }
    type1ComboBox->setObjectName("type1");
//fill parameters
    on_searchLineEdit_textChanged("");
//set grey all uncommon parameters
    if (p->data->selectedCurves.size()>1)
    foreach (int l, p->data->selectedCurves) {
	 const gsimFD* thisinfo=p->data->get_info(l);
	if (thisinfo->title!=info->title)
		setGrey(titleLineEdit);
	if (thisinfo->ref!=info->ref)
		setGrey(refSpinBox);
	if (thisinfo->ref1!=info->ref1)
		setGrey(ref1SpinBox);
	if (thisinfo->sw!=info->sw)
		setGrey(swSpinBox);
	if (thisinfo->sw1!=info->sw1)
		setGrey(sw1SpinBox);
	if (thisinfo->sfrq!=info->sfrq)
		setGrey(sfrqSpinBox);
	if (thisinfo->sfrq1!=info->sfrq1)
		setGrey(sfrq1SpinBox);
	if (thisinfo->spinrate!=info->spinrate)
		setGrey(spinrateSpinBox);
	if (thisinfo->temperature!=info->temperature)
		setGrey(temperatureSpinBox);
	if (thisinfo->type!=info->type)
		setGrey(typeComboBox);
	if (thisinfo->type1!=info->type1)
		setGrey(type1ComboBox);
    }
}

void spectralParsDialog::setGrey(QWidget* wid)
{
	QString sheet=QString("*[objectName=\"%1\"] {color : grey}").arg(wid->objectName());
	wid->setStyleSheet(sheet);
	QDoubleSpinBox* dbox=qobject_cast<QDoubleSpinBox*>(wid);
	if (dbox)
		connect(dbox,SIGNAL(valueChanged(double)),this,SLOT(resetColours()));
	else {
		QLineEdit* ledit=qobject_cast<QLineEdit*>(wid);
		if (ledit)
			connect(ledit,SIGNAL(textChanged(QString)),this,SLOT(resetColours()));
		else {
			QComboBox* cbox=qobject_cast<QComboBox*>(wid);
			if (cbox)
				connect(cbox,SIGNAL(currentIndexChanged(int)),this,SLOT(resetColours()));
		}
	}
}

void spectralParsDialog::resetColours()
{
	disconnect(sender());
	QWidget* wid=qobject_cast<QWidget*>(sender());
	wid->setStyleSheet(qApp->styleSheet());
}

/*
 *  Destroys the object and frees any allocated resources
 */
spectralParsDialog::~spectralParsDialog()
{
    // no need to delete child widgets, Qt does it all for us
}


void spectralParsDialog::on_cancelButton_clicked()
{
	close();
}

bool notGrey(QWidget* wid)
{
	return (wid->styleSheet()==qApp->styleSheet());
}

void spectralParsDialog::on_okButton_clicked()
{
//qDebug()<<"size"<<!p->data->size();
if (!p->data->size()) {
//	qDebug()<<"Kuku";
	close();
	return;
}
foreach(int k, p->data->selectedCurves) {
	gsimFD info=*(p->data->get_info(k));
	if (notGrey(titleLineEdit))
		info.title=titleLineEdit->text();
	if (notGrey(refSpinBox))
		info.ref=refSpinBox->value();
	if (notGrey(ref1SpinBox))
		info.ref1=ref1SpinBox->value();
	if (notGrey(swSpinBox))
		info.sw=swSpinBox->value();
	if (notGrey(sw1SpinBox))
		info.sw1=sw1SpinBox->value();
	if (notGrey(sfrqSpinBox))
		info.sfrq=sfrqSpinBox->value();
	if (notGrey(sfrq1SpinBox))
		info.sfrq1=sfrq1SpinBox->value();
	if (notGrey(spinrateSpinBox))
		info.spinrate=spinrateSpinBox->value();
	if (notGrey(temperatureSpinBox))
		info.temperature=temperatureSpinBox->value();
    if (notGrey(typeComboBox))	
    switch (typeComboBox->currentIndex()) {
	case 0:
		info.type=FD_TYPE_FID;
		break;
	case 1:
		info.type=FD_TYPE_SPE;
		break;
    }
    if (notGrey(type1ComboBox))
    switch (type1ComboBox->currentIndex()) {
	case 0:
		info.type1=FD_TYPE_FID;
		break;
	case 1:
		info.type1=FD_TYPE_SPE;
		break;
    }
	p->data->set_info(k, info);
}
p->data->updateAxisVectors();
p->update_info(p->data->getActiveCurve());
p->activePlot()->cold_restart(p->data);
close();
}

/*
void spectralParsDialog::on_allButton_clicked()
{
	searchParameter("*");
}
*/

/*
void spectralParsDialog::on_searchButton_clicked()
{
	searchParameter(searchLineEdit->text());
}
*/

void spectralParsDialog::on_searchLineEdit_textChanged(const QString& text)
{
	QString stext=text;
	stext.append("*");
	searchParameter(stext);
}

void spectralParsDialog::searchParameter(QString string)
{
   if (!p->data->size())
	return;
   resultTextEdit->clear();
   foreach (int k, p->data->selectedCurves) {
    if (p->data->selectedCurves.size()>1)
	resultTextEdit->append(QString("<b>Dataset: ")+p->data->get_short_filename(k)+"</b>");
    gsimFD info=*(p->data->get_info(k));
    QRegExp parameter(string, Qt::CaseInsensitive, QRegExp::Wildcard);
    bool ok=false;
    for (size_t i=0; i<info.parameterFiles.size(); i++) {
	QStringList outres;
	QHash<QString, QString> lPar= info.allParameters.at(i);
	QHashIterator<QString, QString> it(lPar);
	while (it.hasNext()) {
		it.next();
     		if (parameter.exactMatch(it.key())) {
			outres<<it.key()+" : "+it.value();
			ok=true;
		}
	}
     if (!outres.isEmpty()) {
	resultTextEdit->append(QString("<b>File:")+info.parameterFiles.at(i)+"</b>");
	qSort(outres.begin(),outres.end());
	foreach (QString s, outres)
		resultTextEdit->append(s);
     }
     }
     if (!ok)
	resultTextEdit->append(QString("<b>Parameter is not found!</b>"));
   }
}
