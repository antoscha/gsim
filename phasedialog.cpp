#include <QMessageBox>
#include "phasedialog.h"


#include <QVariant>


PhaseDialog::PhaseDialog(QWidget* parent, Qt::WindowFlags fl)
	:QDialog(parent, fl) {
    setupUi(this);
    setWindowTitle( "Change phase" );
    setWindowIcon( QPixmap( ":/images/gsim.png" ) );

    // signals and slots connections
    connect( rpSlider, SIGNAL(valueChanged(int)), this, SLOT( rpSliderMoved()));
    connect( lpSlider, SIGNAL(valueChanged(int)), this, SLOT( lpSliderMoved()));
    connect(  plusRangeButton, SIGNAL(clicked()), this, SLOT( plusButtonPressed()));
    connect(  minusRangeButton, SIGNAL(clicked()), this, SLOT( minusButtonPressed()));
    readSettings();
}


PhaseDialog::~PhaseDialog()
{
    writeSettings();
    // no need to delete child widgets, Qt does it all for us
}

void PhaseDialog::readSettings()
{
        QSettings settings("GSim", "GSim");
        settings.beginGroup("PhaseDialog");
        resize(settings.value("size", QSize(400, 200)).toSize());
        move(settings.value("pos", QPoint(200, 200)).toPoint());
        settings.endGroup();
}

void PhaseDialog::writeSettings()
{
	QSettings settings("GSim", "GSim");
        settings.beginGroup("PhaseDialog");
        settings.setValue("size", size());
        settings.setValue("pos", pos());
        settings.endGroup();
}

void PhaseDialog::plusButtonPressed()
{
	int max=lpSlider->maximum();
	lpSlider->setMaximum(max+3600000);	
	lpSlider->setMinimum(max);
	lpSlider->setValue(max);
}

  void PhaseDialog::minusButtonPressed()
{
	int min=lpSlider->minimum();
	lpSlider->setMaximum(min);	
	lpSlider->setMinimum(min-3600000);
	lpSlider->setValue(min);
}

void PhaseDialog::rpSliderMoved()
{
	rpLabel->setText(QString("zero order: %1").arg(rpSlider->value()/10000.0,0,'f',1));
}

void PhaseDialog::lpSliderMoved()
{
	lpLabel->setText(QString("first order: %1").arg(lpSlider->value()/10000.0,0,'f',1));
}
