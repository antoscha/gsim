#ifndef PLOT2DWODGET_H
#define PLOT2DWODGET_H

#ifdef QT_OPENGL_LIB
#include <QGLWidget>
#else
#include <QWidget>
#endif

#include <QPainter>
#include <QPicture>
#include <QMouseEvent>
#include "string.h"
#include "base.h"
#include <algorithm>
#include <QUrl>
#include <QLabel>
#include <QMovie>

#define STICS 5 
#define AXOFFSET 4
#define TICSLEN 5

//#ifdef Q_WS_WIN
//#define USE_CUSTOM_POLYLINE
//#endif

//#ifdef Q_WS_MAC
//#define USE_CUSTOM_POLYLINE
//#endif

using namespace std;
using namespace libcmatrix;

enum {MOVEALL, VERTSCALING, HORSCALING, GRAPHSELECTION}; ///mode for the mouse action
enum {SPECTRAASLINES, SPECTRAASSTICKS, SPECTRAASLINESANDPOINTS}; ///representation mode

double first_decimal(double); ///returns the rounded value of the argument

#ifdef QT_OPENGL_LIB
class Plot2DWidget : public QGLWidget
#else
class Plot2DWidget : public QWidget
#endif
{
Q_OBJECT
public:
    Plot2DWidget( QWidget *parent=0);
    ~Plot2DWidget();

   virtual void hot_restart(); ///init all settings, reset global range, no recalculations
   virtual void cold_restart(DataStore *); ///hot restart + contours recalculation
   virtual void Update(); ///no window resseting
   virtual void fromreal_toindex (size_t, QPointF, size_t &, bool with_shifts=true);///convert real coordinates in QPointF to index (3rd argument) according to setting for curve (1st argument). If with_shifts is true then it takes shifts/scalings into account
   virtual void init_settings();///sets correct units for the axis
   bool is_xaxis_inverted, is_yaxis_inverted;///store direction of the axis
   QString xaxis_label, yaxis_label;///store axis labels (as us, ppm, Hz)
   QRect xlabelrect, ylabelrect;
   int hoffset;
   bool for_print; ///'true' if picture is drawing for printer else false. Controls 'bold' drawing for active curve and colours (only black for printing)

   virtual void	draw_picture(QPainter *);///draw entire contents
   ColourScheme colourScheme;
   void setScheme(ColourScheme s) {QPalette pal=palette();  //should be simpler but I don't know how to do that (yet)
				   pal.setColor(QPalette::Window, s.backgroundColour);
				   pal.setColor(QPalette::WindowText, s.textColour);
				   setPalette(pal); colourScheme=s;};
   virtual QColor get_color(size_t);///return colour for the curve size_t
   bool is_leftbar;///is main marker set? 
   bool is_rightbar;///is secondary marker set?
   int  visualMode; ///determines how spectra will be represented, see enum
   bool whitewash;
//   bool is_stick_mode;///stick mode
   bool is_xgrid, is_ygrid;///grids?
   int  move_state;///Determines how the mouse moves affects the plot (zoom, scale, shift)
   void		set_window_range(double, double, double, double);///sets windows range as xmin,xmax, ymin, ymax
   double	xwinmax, xwinmin, ywinmax, ywinmin, xppt, yppt, xrange, yrange;///4 parameters: window position, then 2: step in real coordinates per screen point, then ranges
    QPointF leftbarpos; ///position of the main marker in real coordinates
    QPointF rightbarpos;///position of the secondary marker in real coordinates


  DataStore* data;/// Contain pointer to data 
  mutable QMutex plotmutex; ///mutex that prevents data being changed during plotting opreations
  bool selectedGraphContainsText();
  bool selectedGraphContainsImageOnly();
signals:
	void openFileAsked(QString); ///dropEvent emits this signal
	void onXaxisLabelClicked();
	void onYaxisLabelClicked();
	void leftbarSet();
	void rightbarSet();
public slots:
   inline void set_leftbar(double x, double y) {leftbarpos.setX(x); leftbarpos.setY(y); is_leftbar=true;};
   inline void set_rightbar(double x, double y) {rightbarpos.setX(x); rightbarpos.setY(y); is_rightbar=true;};
   void 	unselectAll(); 
   void		deleteSelected();
   void		changeColour();
   void 	changeFont();
   void		changeAngle();
   void     fromreal_toscreen (const size_t, const size_t, QPolygon&, const size_t); ///for part of the spectrum
   void		fromreal_toscreen (const size_t, const size_t, QPolygonF&, const size_t);
   void		fromscreen_toreal (QPoint, double&, double & );
   void		fromscreen_toreal (QPoint, double&, double &, int );
protected:
   void 	dragEnterEvent(QDragEnterEvent *);
   void 	dropEvent(QDropEvent *);
   void 	image_dropped(QDropEvent *, QImage&);
   void		paintEvent( QPaintEvent * );
   void		resizeEvent (QResizeEvent *);
   void		mousePressEvent( QMouseEvent * e);
   void		mouseMoveEvent (QMouseEvent *e);
   void		mouseReleaseEvent (QMouseEvent *e);
   void 	mouseDoubleClickEvent (QMouseEvent *e);
   virtual void wheelEvent(QWheelEvent *);
   virtual void keyPressEvent ( QKeyEvent * e); 
   void		draw_axis(QPainter *);///draws x-axis
#ifdef USE_CUSTOM_POLYLINE
   void 	drawPolyline(QPainter*, QPolygonF); ///custom Polyline that looks faster then native function on Windows
#endif
   void         drawPolylineSplit(QPainter*, QPolygonF);
   void		draw_logo(QPainter *);
   void		draw_spectra(QPainter *);
   virtual void	draw_bars(QPainter *);
   void		draw_bar_lines(QPainter*, QPointF);///draw just lines for the bar, used as a part of draw_bars
   void		draw_integrals(QPainter *);
   void 	draw_peaks(QPainter *);
   void		draw_extra_graph(QPainter *);
   
   	QAction* deleteGraphObjectAction;
	QAction* changeColourAction;
	QAction* changeFontAction;
	QAction* changeAngleAction;
 
   graph_object* 	find_graph_object(double, double, int, int* index=0);
   void         moveSelected(double, double);
   QRect	selectionRect;
   int 		grabbed_point;
   int		grabbed_object_index;
   int 		grabbed_object_dataset;
   void         selectByRectangle();
   bool		no_mousemove; ///set to false by mousePressEvent and reset to true if mouseMoveEvent occurs
   bool		first_click; ///set TRUE by first mousePressEvent. Second mousePressEvent or mouseReleaseEvent which called 
   QPoint	start_point; ///holds the position where mouse has been clicked
   double	xglobmax, xglobmin, yglobmax, yglobmin;///global max and min values
   virtual void	set_global_range(); ///determines global max and min for all datasets

   virtual void	fromreal_toscreen (double, double, QPointF &);///from given x and y returns the postion on the screen
   template <class T> void fromreal_toscreen (double, double, T &, size_t); ///from given x and y in QPoint or QPointF returns the position on the screen for specific curve

   QPoint	point_old; ///keeps a mouse position when mousePressEvent occurs and used to determine the amount of the move in mouseMoveEvent.
   Qt::MouseButton	action;
   bool mousePressed; ///switched on in mousePressEvent and off in mouseMoveEvent. Used for drawing acceleration when plot is moving
   virtual void	draw_yaxis(QPainter *);///draws y-axis
   virtual bool willMarkBePrinted(QPoint);  ///checks should or not axis mark (number) be printed at the given point
   int labeloffset; ///needed for Plot3DWidget with projections

   QLabel* markerLabel1;
   QMovie* markerMovie1;
   QLabel* markerLabel2;
   QMovie* markerMovie2;
};

#endif
