#ifndef PROCESSING_H
#define PROCESSING_H

#include <QPushButton>
#include <QTableWidget>
#include <QMainWindow>
#include <QtAlgorithms>
#include "base.h"
#include "filefilters.h"
#include "plot2dwidget.h"
#include "plot3dwidget.h"
#include "cmatrix_utils.h"
//#include "mainform.h"
#include <QTableWidget>
#include <QApplication>
#include <QProgressDialog>
#include "phasedialog.h"
//#include "optim.h"
#include <vector>

#ifdef USE_OLD_MINUIT
	#include "Minuit/FCNBase.h"
	#include "Minuit/FunctionMinimum.h"
	#include "Minuit/MnPrint.h"
	//#include "Minuit/MnMigrad.h"
	#include "Minuit/MnUserParameterState.h"
	#include "Minuit/VariableMetricMinimizer.h"
	#include "Minuit/SimplexMinimizer.h"
	#include "Minuit/CombinedMinimizer.h"
#else
	#include "Minuit2/FCNBase.h"
	#include "Minuit2/FunctionMinimum.h"
	#include "Minuit2/MnPrint.h"
	//#include "Minuit/MnMigrad.h"
	#include "Minuit2/MnUserParameterState.h"
	#include "Minuit2/VariableMetricMinimizer.h"
	#include "Minuit2/SimplexMinimizer.h"
	#include "Minuit2/CombinedMinimizer.h"
	using namespace ROOT::Minuit2;
#endif
#include "ui_modelessdialog.h"
#include <QHBoxLayout>
#include <QDoubleSpinBox>
#include <QSpinBox>


class ModelessDialog : public QDialog, public Ui::modelessDialog
{
Q_OBJECT
public:
    ModelessDialog( QWidget* parent = 0, Qt::WindowFlags fl = 0 ) :QDialog(parent, fl)
	{
		setupUi(this);
		setWindowTitle( "Query" );
		setWindowIcon( QPixmap( ":/images/gsim.png" ) );	
	};
	~ModelessDialog(){};
};

enum FT_MODE {FT_COMPLEX, FT_STATES, FT_TPPI, FT_STATESTPPI, FT_ECHOANTIECHO};
class MainForm;

class BaseProcessingFunc : public QObject
{
Q_OBJECT
public:
	BaseProcessingFunc(DataStore*, QString, QString, QString); //should take the pointer to data + name + pointers to plot2D and plot3D;
	~BaseProcessingFunc();
	void placeToTable(QTableWidget*,const int); //need a pointer to table and a row index in the table;
	DataStore * data;
	void setPlots(Plot2DWidget * , Plot3DWidget *, MainForm* );
	Plot2DWidget* plot2D;
	Plot3DWidget* plot3D;
	MainForm* mainwindow;
	virtual void apply(int);
	virtual void plotsUpdate();
	QAction* action() {return menuAction;};
	virtual QIcon icon();
	QString getName() {return name;};
	QString getCommand() {return command;}; //return the command which calls the function
	QStringList option;
	void optionError(QString);
	void setOptions(QString);
	bool toTable; ///controls whether the 'setOption' send data to table or to optionString
	double getDouble(size_t, bool&);
        double getDouble(size_t, bool&, int, size_t, bool direct=true, bool deltaonly=false); ///analisys the srtring and returns result according to Units
	int getInt(size_t, bool&);
	QString optionString;
	void applyInt(int); ///internal function needs exact row number
        QString previousOptionString();
public slots:
	void applyU();
	virtual void prepDoInteractive(); ///update row position; needed if several similar objects exist
	virtual void doInteractive();
	void menuAsksOptions();
	void applyThroughMenu(); 
signals:
	void changeInfo();
	void changeFilesList();
	void stringIsReady();///omited when doInteractive completely finished, corrects problem with phasing
private:
	QString name;
	QString command;
	QTableWidget* table;
	QAction* menuAction;
	int row;
	void readOptions();
	void makeBackUp();
};

class SetSizeDirProc: public BaseProcessingFunc
{
public:
	SetSizeDirProc(DataStore* d) : BaseProcessingFunc(d, tr("Resize (dir)"), tr("128"), tr("size")) { };
	void apply(int);
	void doInteractive();
//	void plotsUpdate();
};

class SetSizeIndirProc: public BaseProcessingFunc
{
public:
	SetSizeIndirProc(DataStore* d) : BaseProcessingFunc(d, tr("Resize (indir)"), tr("128"), tr("isize")) { };
	void apply(int);
	void doInteractive();
	void plotsUpdate();
};

class DirShiftProc: public BaseProcessingFunc
{
public:
	DirShiftProc(DataStore* d, QString n=tr("Shift (dir)"), QString o=tr("1"), QString c=tr("shift")) : BaseProcessingFunc(d,n,o,c) { };
	virtual void apply(int);
	virtual void doShift(int,int);
	void doInteractive();
	void plotsUpdate();
};

class IndirShiftProc: public DirShiftProc
{
public:
	IndirShiftProc(DataStore* d):DirShiftProc(d, tr("Shift (indir)"), tr("1"), tr("ishift")) {};
	void apply(int);
};

class autoBruker: public DirShiftProc
{
public:
	autoBruker(DataStore* d, QString n=tr("auto Bruker"), QString o=tr("none"),QString c=tr("abruk") ):DirShiftProc(d, n, o, c) {};
	double findShift(int);
	virtual void apply(int);
	void doInteractive();
};

class DirLBProc: public autoBruker
{
Q_OBJECT
public:
	DirLBProc(DataStore* d, QString n=tr("LB (dir)"), QString o=tr("lorentz;0"), QString c=tr("lb")) : autoBruker(d,n,o,c) { };
	virtual void apply(int);
	void doInteractive();
	QHBoxLayout* shiftLayout;
	QHBoxLayout* lbLayout;
public slots:
	void typeChanged(QString);
//	void plotsUpdate();
};

class IndirLBProc:public DirLBProc
{
public:
	IndirLBProc(DataStore* d) : DirLBProc(d, tr("LB (indir)"), tr("lorentz;0"), tr("ilb")) {};
	void apply(int);
};

class DirFtProc: public BaseProcessingFunc
{
public:
	DirFtProc(DataStore* d, QString n=tr("FT/FFT (dir)"), QString o=tr("<none>"), QString c=tr("ft")) : BaseProcessingFunc(d, n, o, c) { };
	virtual void apply(int);
	void FourierTrans(int, FT_MODE, int);
	bool is_power2(size_t );
	size_t next_power2(const size_t);
	void StatesResort(size_t);
	void EchoAntiechoResort(size_t);
	void ZeroImag(size_t);
//	void doInteractive();
//	void plotsUpdate();
};

class magnSpectrumProc: public BaseProcessingFunc
{
public:
	magnSpectrumProc(DataStore* d, QString n=tr("Magnitude"), QString o=tr("<none>"), QString c=tr("magn")) : BaseProcessingFunc(d, n, o,c) { };
	virtual void apply(int);
//	void doInteractive();
	void plotsUpdate();
};

class addNoiseProc: public BaseProcessingFunc
{
public:
	addNoiseProc(DataStore* d, QString n=tr("addNoise"), QString o=tr("1.0"), QString c=tr("noise")) : BaseProcessingFunc(d, n, o, c) { };
	virtual void apply(int);
	void doInteractive();
	void plotsUpdate();
};

class IndirFtProc: public DirFtProc
{
public:
	IndirFtProc(DataStore* d) : DirFtProc(d, tr("FT/FFT (indir)"), tr("complex"), tr("ift")) { };
        void apply(int);
	void doInteractive();
//	void plotsUpdate();
};

class DirPhaseProc: public BaseProcessingFunc
{
Q_OBJECT

public:
	DirPhaseProc(DataStore* d, QString n=tr("Phase (dir)"), QString o=tr("0;0"), QString c=tr("ph")) : BaseProcessingFunc(d, n, o, c) {count=0; mode2D=false;};
	void apply(int);
	virtual void doInteractive();
	PhaseDialog* pd;
	List<cmatrix> backup;
	List<cmatrix> hyperbackup; //stores hypercoplex data
//	virtual void applyPhase(double, double, double, int);
//	virtual void applyPhase(double, double, double, size_t);
	virtual void applyPhase(double, double,size_t); //arguments: ph0, ph1, dataset index
	void undoPrevPhase();
	void plotsUpdate();
	bool mode2D;
	void prepare2D();
	void finish2D();
	void doDialog(bool forAll=false);
	ModelessDialog* takedialog;
	int count;
	QList<size_t> switchedOff;
	size_t KK;
	virtual bool isDir() {return true;};
public slots:
	virtual void displayPhase();
	void writePhase();
	void cancelClicked();
	void moreSlicesRequested();
	void doneSlicesRequested();
};

class IndirPhaseProc: public DirPhaseProc
{
Q_OBJECT
public:
	IndirPhaseProc(DataStore* d, QString n=tr("Phase (indir)"), QString o=tr("0;0;0"), QString c=tr("iph")) : DirPhaseProc(d, n, o, c) {};
//	void applyPhase(double, double, double);
//	void applyPhase(double, double, double, size_t);
	void applyPhase(double, double);
	void applyPhase(double, double, size_t);
	bool isDir() {return false;};
//public slots:
//	void displayPhase();	
};

class phaseOptimiser: public FCNBase {
public:
	phaseOptimiser(cmatrix* d) {data=d;};
	~phaseOptimiser() {};
#ifdef USE_OLD_MINUIT
	virtual double up() const {return 1.0;};
#else
	virtual double Up() const {return 1.0;};
#endif
	virtual double operator()(const vector<double>&) const;
private:
	cmatrix* data;
};

class autoPhaseProc: public BaseProcessingFunc
{
Q_OBJECT
public:
	autoPhaseProc(DataStore* d, QString n=tr("auto Phase (dir)"), QString o=tr("<N/A>"), QString c=tr("aph")) : BaseProcessingFunc(d, n, o, c) {};
	void apply(int);
	void plotsUpdate() {
		plot2D->update();
		plot3D->cold_restart(data);};
};


class t1DepPhaseProc:public BaseProcessingFunc
{
public:
	t1DepPhaseProc(DataStore* d, QString n=tr("t1-dep PHC"), QString o=tr("0;0;0;0;1;0.5"), QString c=tr("t1dph")) : BaseProcessingFunc(d, n, o, c) {};
	void apply(int);
	void doInteractive();
};

class TshearingProc:public BaseProcessingFunc
{
public:
    TshearingProc(DataStore* d, QString n=tr("TShearing"), QString o=tr("0.0;STATES"), QString c=tr("tshear")) : BaseProcessingFunc(d, n, o, c) {};
    void apply(int);
    void doInteractive();
};

class t1DepGaussLB:public BaseProcessingFunc
{
public:
	t1DepGaussLB(DataStore* d, QString n=tr("t1-dep GLB"), QString o=tr("0;0;0;1"), QString c=tr("t1dglb")) : BaseProcessingFunc(d, n, o, c) {};
	void apply(int);
	void doInteractive();
};

class RearrangeProc: public BaseProcessingFunc
{
public:
	RearrangeProc(DataStore* d) : BaseProcessingFunc(d, tr("Rearrange"), tr("128"), tr("rearr")) { };
	void apply(int);
	void doInteractive();
//	void plotsUpdate();
};

class DCOffsetProc: public BaseProcessingFunc
{
public:
	DCOffsetProc(DataStore* d) : BaseProcessingFunc(d, tr("DC Offset"), tr("10"), tr("dc")) { };
	void apply(int);
	void doInteractive();
	void plotsUpdate();
};

class ShearingProc:public BaseProcessingFunc
{
Q_OBJECT
public:
	ShearingProc(DataStore* d, QString n="Shearing", QString o="0.0;1.0;1;cut", QString c="shear") : BaseProcessingFunc(d, n, o, c) {hasAxis=false;};
	virtual void apply(int);
	virtual void doInteractive();
	void shear(const size_t, double,const double,const int,bool);
	bool hasAxis;
//	void plotsUpdate();
public slots:
	void drawShAxis();
protected:
	QDoubleSpinBox* size1;
	QDoubleSpinBox* size2;
	QSpinBox* step;
	void delShAxis();
};


//class ThreeQMASProc: public ShearingProc
//{
//public:
//	ThreeQMASProc(DataStore* d, QString n="3Q MAS", QString o="3/2;STATES"): ShearingProc(d,n,o) {};
//	void apply();
//	void doInteractive();
//};


struct lSortRow{
	List<complex> row;
	double x;
	};

	bool lSortRowLessThen(const lSortRow&, const lSortRow&);

class SortRowsProc:public BaseProcessingFunc
{
public:
	SortRowsProc(DataStore* d) : BaseProcessingFunc(d, tr("Sort rows"), tr("<N/A>"), tr("sort")) { };
	void apply(int);
	void doInteractive();
	void plotsUpdate();
};

class DirInvertProc:public BaseProcessingFunc
{
public:
	DirInvertProc(DataStore* d) : BaseProcessingFunc(d, tr("Invert (dir)"), tr("<none>"), tr("inv")) { };
	void apply(int);
	void plotsUpdate();
};

class IndirInvertProc:public BaseProcessingFunc
{
public:
	IndirInvertProc(DataStore* d) : BaseProcessingFunc(d, tr("Invert (indir)"), tr("<none>"), tr("iinv")) { };
	void apply(int);
	void plotsUpdate();
};

class appendSpectrum:public BaseProcessingFunc
{
public:
	appendSpectrum(DataStore* d) : BaseProcessingFunc(d, tr("Append data"), tr("<N/A>"),tr("append")) { };
	void apply(int);
	void doInteractive();
	void plotsUpdate();
};

class addSpectra:public BaseProcessingFunc
{
public:
	addSpectra(DataStore* d) : BaseProcessingFunc(d, tr("Add spectra"), tr("<N/A>"), tr("add")) { };
	void apply(int);
	cmatrix cubic_interpolate(size_t&, double, double, cmatrix, double, double, bool&);
	void doInteractive();
	void plotsUpdate();
};

enum {BL_POLY, BL_FR, BL_BERN};

class baselineOptimiser: public FCNBase {
public:
	baselineOptimiser(const List<double>& meas,
		  const List<int>& xvec,
		  const double mvar) : theMeasurements(meas),
			theXvec(xvec)
			{theError=mvar;};
	~baselineOptimiser() {};
#ifdef USE_OLD_MINUIT
	virtual double up() const {return 1.0;};
#else
	virtual double Up() const {return 1.0;};
#endif
	void calculate(const vector<double>&, List<double>&) const;
	virtual double operator()(const vector<double>&) const;
	int type; //can be BL_POLY, BL_FR
private:
	List<double> theMeasurements;
	List<int> theXvec;
	double theError;
};

/*
class baselineFunction : public BaseFitFunction
{
public:
	baselineFunction() {};
	void set_xvector(List<double>);
	void set_function(int); //type of function
	void operator()(BaseList<double> dest, const BaseList<double>& paras) const;
private:
	List<double> xvector;
	int type;
};
*/
class baselineCorrection : public BaseProcessingFunc
{
Q_OBJECT
public:
	baselineCorrection(DataStore* d) : BaseProcessingFunc(d, tr("Auto BL correction"), tr("poly;4;0;0"), tr("blc")) { };
	void apply(int);
	void doInteractive();
	void plotsUpdate();
private:
	List<double> ydata;
	List<int> xdata;
	List<int> xtrunc;
	List<double> ytrunc;
	void excludeRegion(size_t, size_t);
	ModelessDialog* peakdialog;
	int count;
	List<int> findPeaks(int, const int col=0);
	double calcNoise(int, const int col=0);
	int firstGraphicObject;
public slots:
	void morePeaksRequested();
	void donePeaksRequested();
};

class  smoothProc: public BaseProcessingFunc
{
public:
	smoothProc(DataStore* d) : BaseProcessingFunc(d, tr("Smoothing"), tr("5;0"), tr("smooth")) { };
	void apply(int);
	void doInteractive();
	void plotsUpdate();
};

class  transposeProc: public BaseProcessingFunc
{
public:
	transposeProc(DataStore* d) : BaseProcessingFunc(d, tr("Transpose"), tr("<none>"), tr("tr")) { };
	void apply(int);
	void plotsUpdate();
};

class cropProc:public BaseProcessingFunc
{
public:
	cropProc(DataStore* d) : BaseProcessingFunc(d, tr("Crop"), tr("0;100;0;100"), tr("crop")) { };
	void apply(int);
	void doInteractive();
	void plotsUpdate();
};

/*
class removePeaks:public BaseProcessingFunc
{
public:
	removePeaks(DataStore* d) : BaseProcessingFunc(d, tr("Remove Peaks"), tr("<N/A>")) { };
	void apply(int);
	double calcNoise(int k);
};
*/

class loadFileProc:public BaseProcessingFunc
{
public:
	loadFileProc(DataStore* d) : BaseProcessingFunc(d, tr("Load File"), tr("<none>"), tr("load")) { };
	void apply(int);
	void doInteractive();
	void plotsUpdate();
};

class saveFileProc:public BaseProcessingFunc
{
public:
	saveFileProc(DataStore* d) : BaseProcessingFunc(d, tr("Save File"), tr("<none>"), tr("save")) { };
	void apply(int);
	void doInteractive();
};

class ExternalCommandProc:public BaseProcessingFunc
{
public:
	ExternalCommandProc(DataStore* d) : BaseProcessingFunc(d, tr("External Command"), tr("<none>"), tr("cmd")) { };
	void apply(int);
	void doInteractive();
};

#ifdef QT_SCRIPT_LIB
#include <QtScript>

class scriptProc:public BaseProcessingFunc
{
public:
	scriptProc(DataStore*);
	~scriptProc();
	void apply(int);
	void doInteractive();
private:
	QScriptEngine* engine;
};
#endif //QT_SCRIPT_LIB

class drawDiag:public BaseProcessingFunc
{
public:
	drawDiag(DataStore* d) : BaseProcessingFunc(d, tr("Draw Diagonal"), tr("1"), tr("drdiag")) { };
	void apply(int);
	void doInteractive();
	void plotsUpdate() {plot3D->update();};
};

class balanceIndir:public BaseProcessingFunc
{
public:
	balanceIndir(DataStore* d) : BaseProcessingFunc(d, tr("XY balance (indir)"), tr("1.0"), tr("ibln")) { };
	void apply(int);
	void doInteractive();
	void plotsUpdate() {plot3D->update();};
};

class binningDir: public BaseProcessingFunc
{
public:
	binningDir(DataStore* d) : BaseProcessingFunc(d, tr("Binning (dir)"),tr("0.14,full"),tr("bin")) {};
	void apply(int);
	void doInteractive();
};

class redorProc: public BaseProcessingFunc
{
public:
	redorProc(DataStore* d) : BaseProcessingFunc(d, tr("redor"),tr("0.14,full"),tr("redor")) {};
	void apply(int);
	void plotsUpdate();
//	void doInteractive();
};

class scale: public BaseProcessingFunc
{
public:
        scale(DataStore* d) : BaseProcessingFunc(d, tr("Scale"),tr("1.0"),tr("sc")) {};
        void apply(int);
        void doInteractive();
};


class sum_sb: public BaseProcessingFunc
{
public:
        sum_sb(DataStore* d) : BaseProcessingFunc(d, tr("Sum Sidebands (dir)"),tr("5000.0"),tr("as")) {};
        void apply(int);
        void doInteractive();
};

#endif // PROCESSING_H
