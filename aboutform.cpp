/****************************************************************************
** Form implementation generated from reading ui file 'aboutform.ui'
**
** Created: Wed May 25 11:16:53 2005
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.1.2   edited Dec 19 11:45 $)
**
** WARNING! All changes made in this file will be lost!
****************************************************************************/

#include "aboutform.h"

#include <QVariant>
#include <QPushButton>
#include <QLabel>
#include <QLayout>
#include <QImage>
#include <QPixmap>
#include <QVBoxLayout>
#include <QHBoxLayout>

#include <QPictureIO>

/*
 *  Constructs a aboutForm as a child of 'parent', with the
 *  name 'name' and widget flags set to 'f'.
 *
 *  The dialog will by default be modeless, unless you set 'modal' to
 *  TRUE to construct a modal dialog.
 */
aboutForm::aboutForm(QWidget *parent, Qt::WindowFlags fl)
	:QDialog(parent, fl) {
    setWindowTitle( "About program" );
    setWindowIcon( QPixmap( ":/images/gsim.png" ) );
    aboutFormLayout = new QVBoxLayout( this);

    layout8 = new QVBoxLayout();

    layout7 = new QHBoxLayout();

    logoLabel = new QLabel( this);
    logoLabel->setPixmap( QPixmap(":/images/aboutlogo.png") );
    layout7->addWidget( logoLabel );

    textLabel = new QLabel( this);
    layout7->addWidget( textLabel );
    layout8->addLayout( layout7 );

    layout4 = new QHBoxLayout();
    QSpacerItem* spacer = new QSpacerItem( 41, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout4->addItem( spacer );

    okButton = new QPushButton( this);
    layout4->addWidget( okButton );
    QSpacerItem* spacer_2 = new QSpacerItem( 51, 20, QSizePolicy::Expanding, QSizePolicy::Minimum );
    layout4->addItem( spacer_2 );
    layout8->addLayout( layout4 );
    aboutFormLayout->addLayout( layout8 );
    languageChange();
    resize( QSize(251, 153).expandedTo(minimumSizeHint()) );
   
    // signals and slots connections
    connect( okButton, SIGNAL( clicked() ), this, SLOT( close() ) );
}

/*
 *  Destroys the object and frees any allocated resources
 */
aboutForm::~aboutForm()
{
    // no need to delete child widgets, Qt does it all for us
}

/*
 *  Sets the strings of the subwidgets using the current
 *  language.
 */
void aboutForm::languageChange()
{
  
    logoLabel->setText( QString::null );
    textLabel->setText( QString( "<p align=\\\"center\\\"><b><i>GSim:</i></b> NMR viewing/processing tool<br><br>(c) Vadim Zorin<br>2005-2008 Solid-State NMR Group, Durham University, UK<br>2008-2013 V. Zorin<br><br>Version: %1<br>Compilation date: %2</p>" ).arg(VERSION).arg(QString(__DATE__)) );
    okButton->setText( tr( "OK" ) );
}

