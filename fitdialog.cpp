#include "fitdialog.h"
#include <QDir>
#include <QTextStream>
#include <QCheckBox>
#include <QProcess>
#include <QDebug>
#include <QMessageBox>
#include <QFileDialog>
#include "mainform.h"

fitDialog::fitDialog(MainForm* parent, Qt::WindowFlags fl)
	:QDialog(parent, fl) {
    mainform=parent;
    setupUi(this);
    setWindowTitle( "Fit using pNMRsim" );
    setWindowIcon( QPixmap( ":/images/gsim.png" ) );
    stack->setCurrentIndex(0);
    reference=0.0;
    readSettings();
    updateDir();
    if (!mainform->data->size()) {
	fitButton->setEnabled(false);
    }
    fitIsActive=false;
}

void fitDialog::updateDir()
{
    infileBox->clear();
    QDir inputDir(wd());
    QStringList fileList=inputDir.entryList();
    foreach (QString file, fileList) {
		if (file.endsWith(".in"))
			infileBox->addItem(file);
	}
}

void fitDialog::readSettings()
{
        QSettings settings("GSim", "GSim");
        settings.beginGroup("SimulationDialog");
        resize(settings.value("size", QSize(400, 200)).toSize());
        move(settings.value("pos", QPoint(200, 200)).toPoint());
	setup.workingDir=settings.value("WorkingDirectory", "").toString();
	setup.expSpec=settings.value("ExperimentalSpectrum", "").toString();
	setup.program=settings.value("Program", "pNMRsim").toString();
	setup.addArguments=settings.value("AdditionalArguments", "").toString();
	setup.resultingSpectrum=settings.value("ResultingSpectrum", "").toString();
	setup.editorCommand=settings.value("Editor", "kwrite").toString();
        settings.endGroup();
}

void fitDialog::writeSettings()
{
	QSettings settings("GSim", "GSim");
        settings.beginGroup("SimulationDialog");
        settings.setValue("size", size());
        settings.setValue("pos", pos());
	settings.setValue("WorkingDirectory",setup.workingDir);
	settings.setValue("ExperimentalSpectrum",setup.expSpec);
	settings.setValue("Program",setup.program);
	settings.setValue("AdditionalArguments",setup.addArguments);
	settings.setValue("ResultingSpectrum",setup.resultingSpectrum);
	settings.setValue("Editor",setup.editorCommand);
        settings.endGroup();
}

fitDialog::~fitDialog()
{
    // no need to delete child widgets, Qt does it all for us
}

void fitDialog::on_infileBox_currentIndexChanged(int k)
{
	reference=0.0;
	if (k<0)
		return;
	titleLabel->clear();
	paramsTableWidget->clear();
	paramsTableWidget->setRowCount(0);
	paramsTableWidget->setColumnCount(5);
	paramsTableWidget->setHorizontalHeaderLabels(QStringList()<<"Parameter"<<"Value"<<"Vary?"<<"Error"<<"Comment");
	QFile file(wd()+QString("/")+infileBox->itemText(k));
	if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
	return;
	QTextStream in(&file);
	while (!in.atEnd()) {
	QString line = in.readLine();
	parse_line(line);
	}
	file.close();
	paramsTableWidget->resizeColumnsToContents();
}

QString fitDialog::currentParam(const QString& name)
{
	QString result=QString("Not found");
	if (!mainform->data->size())
		return result;
	const size_t k=mainform->data->getActiveCurve();
	const gsimFD* info=mainform->data->get_info(k);
	if (name=="SW")
		result=QString("%1").arg(info->sw);
	else if (name=="SWI")
		result=QString("%1").arg(info->sw1);
	else if (name=="FREQ")
		result=QString("%1").arg(info->sfrq);
	else if (name=="FREQI")
		result=QString("%1").arg(info->sfrq1);
	else if (name=="REF") {
		result=QString("%1").arg(info->ref);
		reference=info->ref;
		}
	else if (name=="REFI")
		result=QString("%1").arg(info->ref1);
	else if (name=="SPINRATE")
		result=QString("%1").arg(info->spinrate);
	else if (name=="TEMPERATURE")
		result=QString("%1").arg(info->temperature);
	else if (name=="NP")
		result=QString("%1").arg(mainform->data->get_odata(k)->cols());
	else if (name=="NI")
		result=QString("%1").arg(mainform->data->get_odata(k)->rows());
	else if (name=="CENTREBANDINDEX")
		if (mainform->data->get_arrays(mainform->data->getActiveCurve()).size())
		if (mainform->data->get_arrays(mainform->data->getActiveCurve()).at(0).name=="Centreband")
			result=QString("%1").arg(mainform->data->get_arrays(mainform->data->getActiveCurve()).at(0).data(0));
	return result;
}

void fitDialog::parse_line(QString string)
{
//	qDebug()<<"Parsing:"<<string;
	if (!string.startsWith("##"))
		return;
	if (string.startsWith("##par:")){
		string.remove("##par:");
		const size_t k=paramsTableWidget->rowCount();
		paramsTableWidget->setRowCount(k+1);
		QStringList dat=string.split(";", QString::SkipEmptyParts);
		if (dat.size()<3) {
			QMessageBox::critical(this, "Parsing problem", QString("Declaration\n %1\n should have at least 3 entries\nString will be ignored").arg(string));
			return;
		}
		QTableWidgetItem *item = new QTableWidgetItem(dat[0]); //name of the parameter
		item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
		paramsTableWidget->setItem(k, 0, item);
		QString value=(dat.at(1)=="-")? currentParam(dat.at(0)) : dat.at(1);
	//overwrite if recently have been used
		if (recentlyUsed.contains(dat.at(0)))
			value=recentlyUsed.value(dat.at(0)).at(0);
		item = new QTableWidgetItem(value);//default value
		paramsTableWidget->setItem(k, 1, item);

		item = new QTableWidgetItem();
		item->setFlags(Qt::ItemIsEnabled);
		if (dat.at(2)=="fixed") {
			item->setBackground(QBrush(Qt::gray));
			QTableWidgetItem* erritem = new QTableWidgetItem("");//empty error
			paramsTableWidget->setItem(k, 3, erritem);
		}
		else if (dat.at(2)=="variable") {
			item->setFlags(item->flags() | Qt::ItemIsUserCheckable);
			item->setCheckState(Qt::Checked);
			QTableWidgetItem* erritem = new QTableWidgetItem("0.0");//error
		//overwrite if recently have been used
			if (recentlyUsed.contains(dat.at(0)))
				erritem->setText(recentlyUsed.value(dat.at(0)).at(1));
			paramsTableWidget->setItem(k, 3, erritem);
		}
		paramsTableWidget->setItem(k,2,item);

		item = new QTableWidgetItem(dat.at(3));//comments
		item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
		paramsTableWidget->setItem(k, 4, item);
	}
	else if (string.startsWith("##title:")){
		string.remove("##title:");
		titleLabel->setText(titleLabel->text()+"<br>"+string);
	}
}

void fitDialog::on_simButton_clicked()
{
	recentlyUsed.clear();
	QString program=setup.program;
	QStringList arguments;
	if (!setup.addArguments.isEmpty())
		arguments<<setup.addArguments.split(" ", QString::SkipEmptyParts);
	arguments<<wd()+QString("/")+infileBox->currentText();
	QStringList env = QProcess::systemEnvironment();
	for (size_t i=0; i<paramsTableWidget->rowCount(); i++) {
//store values in the 'recentlyUsed'
		QStringList rec_entry;
		rec_entry<<paramsTableWidget->item(i,1)->text()<<paramsTableWidget->item(i,3)->text();
		recentlyUsed[paramsTableWidget->item(i,0)->text()]=rec_entry;
//start analising
		QStringList vallist;
		QStringList errlist;
		QString vals=paramsTableWidget->item(i,1)->text();
		QString errs=paramsTableWidget->item(i,3)->text();
//Create a list with parameters
		if (vals.contains(','))
			vallist=vals.split(',',QString::SkipEmptyParts);
		else if (vals.contains(';'))
			vallist=vals.split(';',QString::SkipEmptyParts);
		else vallist.push_back(vals);
//the same for errors
		if (errs.contains(','))
			errlist=errs.split(',',QString::SkipEmptyParts);
		else if (errs.contains(';'))
			errlist=errs.split(';',QString::SkipEmptyParts);
		else errlist.push_back(errs);
//check are the lengthes matching or not
		if (vallist.size()>1) {
			if (errlist.size()>1){
				if (vallist.size()!=errlist.size()){
					QMessageBox::critical(this,"Arrays mismatch", QString("Parameter %1 contains %2 values but %3 errors.\n The number of errors should be the same as the number of parameters or equal to 1").arg(paramsTableWidget->item(i,0)->text()).arg(vallist.size()).arg(errlist.size()));
					return;
				}
				
			}
		}
//expand error list if contains just 1 error
		if (errlist.size()==1)
			for (size_t j=1; j<vallist.size(); j++)
				errlist.push_back(errlist.at(0));

//assemble the system variable value
		QString sysv=paramsTableWidget->item(i,0)->text()+"=";

		for (size_t j=0;j<vallist.size(); j++) {
			double val=vallist.at(j).toDouble();
			double err=errlist.at(j).toDouble();
			qDebug()<<paramsTableWidget->item(i,0)->text()<<val<<err;
			if ( (paramsTableWidget->item(i,2)->checkState()==Qt::Checked) && (!val) && (!err)) {
				QMessageBox::warning(this,"Error required", QString("Parameter %1 is set to 0. To perform fit, the error estimate should be set.\n Set error and try again").arg(paramsTableWidget->item(i,0)->text()));
				return;
			}
			sysv+=vallist.at(j); //set parameter value
			if (paramsTableWidget->item(i,2)->checkState()==Qt::Checked) {
					sysv+="V";//add V if variable
					if (err)
						sysv+=errlist.at(j);//add error if variable and error is non-zero
			}
			if (j!=vallist.size()-1)
				sysv+=",";//add comma if not the last value
		}
//		qDebug()<<"Set variable"<<sysv;
		env<<sysv;
		//create 'values only' variables
		sysv=paramsTableWidget->item(i,0)->text()+"VAL=";
		for (size_t j=0;j<vallist.size(); j++) {
			sysv+=vallist.at(j); //set parameter value
			if (j!=vallist.size()-1)
				sysv+=",";//add comma if not the last value
		}
			env<<sysv;
	}
	proc = new QProcess(this);
	stack->setCurrentIndex(1);
	proc->setProcessChannelMode(QProcess::MergedChannels);
	connect(proc,SIGNAL(readyRead()),this,SLOT(updateText()));
	connect(proc,SIGNAL(finished( int, QProcess::ExitStatus)),this,SLOT(finish(int, QProcess::ExitStatus)));
//	qDebug()<<"started with argument"<<arguments;
	if ((fitIsActive)&&(mainform->data->size())) {
		fitIsActive=false; //reset the value
		QString fname;
		if (!setup.expSpec.isEmpty())
			fname=setup.expSpec;
		else
			fname=wd()+QString("/experimental.spe");
		env<<QString("FNAME=")+fname;
		if (mainform->data->get_display(mainform->data->getActiveCurve())->changed())
			QMessageBox::warning(this,"Scaling is applied","The active dataset has dynamic shift/scaling\nwihich won't be taken into account in fitting");
		mainform->fileSave(fname,"SIMPSON spectra or FID", 0); //Don't update file name or working directory
//		qDebug()<<"Set variable FNAME";
	}
	finishButton->setText("Stop");
	qDebug()<<"Envirounment"<<env;
	proc->setEnvironment(env);
	proc->start(program,arguments);
//	QDialog::accept();
}

void fitDialog::on_fitButton_clicked()
{
	fitIsActive=true;
	on_simButton_clicked();
}

void fitDialog::on_setupButton_clicked()
{
	stack->setCurrentIndex(2);
	workingDirLineEdit->setText(setup.workingDir);
	expSpecLineEdit->setText(setup.expSpec);
	startLineEdit->setText(setup.program);
	argumentsLineEdit->setText(setup.addArguments);
	resultLineEdit->setText(setup.resultingSpectrum);
	editorLineEdit->setText(setup.editorCommand);
}

void fitDialog::on_saveSetupButton_clicked()
{
	setup.workingDir=workingDirLineEdit->text();
	setup.expSpec=expSpecLineEdit->text();
	setup.program=startLineEdit->text();
	setup.addArguments=argumentsLineEdit->text();
	setup.resultingSpectrum=resultLineEdit->text();
	setup.editorCommand=editorLineEdit->text();
	stack->setCurrentIndex(0);
	writeSettings();
	updateDir();
}

void fitDialog::on_editButton_clicked()
{
	if (setup.editorCommand.isEmpty()) {
		QMessageBox::critical(this,"Error", "Editor is not specified");
		return;
	}
	QProcess::execute(setup.editorCommand+" "+wd()+"/"+infileBox->currentText());
	on_infileBox_currentIndexChanged(infileBox->currentIndex());
}

void fitDialog::on_cancelSetupButton_clicked()
{
	stack->setCurrentIndex(0);
}

void fitDialog::on_browseDirButton_clicked()
{
	 QString dir = QFileDialog::getExistingDirectory(this, tr("Open Directory"),
                                                 mainform->working_directory,
                                                 QFileDialog::ShowDirsOnly
                                                 | QFileDialog::DontResolveSymlinks);
	if (!dir.isEmpty())
		workingDirLineEdit->setText(dir);
}

void fitDialog::on_saveLogButton_clicked()
{
	 QString s =QFileDialog::getSaveFileName(this, tr("Save File"),
                            mainform->working_directory,
                            tr("Text (*.txt *.log)"));
	if (s.isEmpty())
		return;
	QFile file(s);
	if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
		return;
	QTextStream out(&file);
	out << outputTextEdit->document()->toPlainText();
	file.close();
}

void fitDialog::on_saveParamsButton_clicked()
{
	QString s =QFileDialog::getSaveFileName(this, tr("Save File"),
                            mainform->working_directory,
                            tr("Simulation parameters (*.sim)"));
	if (s.isEmpty())
		return;
	QFile file(s);
	if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
		return;
	QTextStream out(&file);
	out<<infileBox->currentText()<<"\n";
	out<<QFileInfo(wd()+"/"+infileBox->currentText()).lastModified().toString()<<"\n";
	for (size_t i=0; i<paramsTableWidget->rowCount(); i++) {
		out<<paramsTableWidget->item(i,0)->text()<<"|"<<paramsTableWidget->item(i,1)->text()<<"\n";
	}
	file.close();
}

void fitDialog::on_openParamsButton_clicked()
{
	QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"),
                                                 mainform->working_directory,
                                                 tr("Simulation parameters (*.sim)"));
	if (fileName.isEmpty())
		return;
	QFile file(fileName);
	if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        	return;
	QTextStream in(&file);
	QString title=in.readLine();
	QString modified=in.readLine();
	if (title!=infileBox->currentText()) {
		QMessageBox::warning(this,"File mismatch","These parameters have been saved for different .in file");
	}
	else if (modified!=QFileInfo(wd()+"/"+infileBox->currentText()).lastModified().toString()) {
		QMessageBox::warning(this,"File mismatch","The .in file has been modified since these parameters were saved");
	}
	while (!in.atEnd()) {
		QString line = in.readLine();
		QStringList par=line.split("|");
		if (par.size()!=2) {
			QMessageBox::critical(this,"Corrupted line","One of the line is corrupted and will be ignored");
			continue;
		}
		for (size_t i=0; i<paramsTableWidget->rowCount(); i++) {
			if (paramsTableWidget->item(i,0)->text()==par.at(0))
				paramsTableWidget->item(i,1)->setText(par.at(1));
		}
	}
}

void fitDialog::on_newSimButton_clicked()
{
	outputTextEdit->clear();
	stack->setCurrentIndex(0);
}

void fitDialog::updateText()
{
//	qDebug()<<"Signal received"<<proc->readLine();
	outputTextEdit->append(proc->readAll());
}

void fitDialog::finish(int, QProcess::ExitStatus stat)
{
	finishButton->setText("Finish");
	if (stat==QProcess::NormalExit) {
		QString fname;
		if (!setup.resultingSpectrum.isEmpty())
			fname=setup.resultingSpectrum;
		else {
			QDir dir(wd());
			QStringList entries =dir.entryList();
			QString checkName=infileBox->currentText().remove(".in")+".fid";
			if (entries.contains(checkName))
				fname=wd()+"/"+checkName;
			checkName=infileBox->currentText().remove(".in")+".spe";
			if (entries.contains(checkName))
				fname=wd()+"/"+checkName;
		}
//		qDebug()<<"trying to open file"<<fname;
		mainform->fileOpen(fname,"SIMPSON spectra or FID",OpenFullRestart | OpenAppend);
		if (reference) {
			gsimFD info=*(mainform->data->get_info(mainform->data->size()-1));
			info.ref=reference;
			mainform->data->set_info(mainform->data->size()-1,info);
			mainform->data->updateAxisVectors();
			mainform->plot2D->cold_restart(mainform->data);
		}
		QMessageBox::information(this, "Success","Simulation finished");
	}
	else
		QMessageBox::information(this, "Process stopped",QString("Simulation has been interrupted: %1").arg(proc->errorString()));
}

void fitDialog::on_finishButton_clicked()
{
//	qDebug()<<"Finish clicked";
	if (proc->state()==QProcess::Running) {
		proc->terminate();
		finishButton->setText("Finish");
	}
	else {
//		qDebug()<<"Asking to close";
		close();
	}
}
