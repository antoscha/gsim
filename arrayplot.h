#ifndef ARRAYWIDGET_H
#define ARRAYWIDGET_H

#ifdef QT_OPENGL_LIB
#include <QGLWidget>
#else
#include <QWidget>
#endif

#include <QPainter>
#include <QPicture>
#include <QMouseEvent>
#include "string.h"
#include "base.h"
#include <algorithm>
#include <QUrl>
#include <QLabel>
#include <QMovie>

#define STICS 5 
#define AXOFFSET 4
#define TICSLEN 5

//#ifdef Q_WS_WIN
//#define USE_CUSTOM_POLYLINE
//#endif

//#ifdef Q_WS_MAC
//#define USE_CUSTOM_POLYLINE
//#endif

using namespace std;
using namespace libcmatrix;

#ifdef QT_OPENGL_LIB
class ArrayWidget : public QGLWidget
#else
class ArrayWidget : public QWidget
#endif
{
Q_OBJECT
public:
    ArrayWidget( QWidget *parent=0);
    ~ArrayWidget();

   List<double> xdata;
   List<double> ydata;
   List<double> fit;

   bool is_xgrid, is_ygrid;///grids?
   void		set_window_range(double, double, double, double);///sets windows range as xmin,xmax, ymin, ymax
   double	xwinmax, xwinmin, ywinmax, ywinmin, xppt, yppt, xrange, yrange;///4 parameters: window position, then 2: step in real coordinates per screen point, then ranges

public slots:

   void		fromscreen_toreal (QPoint, double&, double & );
   void 	resizeEvent (QResizeEvent *);
   void 	paintEvent (QPaintEvent *);
   void 	draw_picture(QPainter*);
protected:
    void	draw_axis(QPainter *);///draws x-axis
   void		draw_points(QPainter *);
   void		draw_fit(QPainter*);

   double	xglobmax, xglobmin, yglobmax, yglobmin;///global max and min values
   virtual void	set_global_range(); ///determines global max and min for all datasets

   virtual void	fromreal_toscreen (double, double, QPointF &);///from given x and y returns the postion on the screen
 
   virtual void	draw_yaxis(QPainter *);///draws y-axis
};

#endif
