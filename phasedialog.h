#ifndef PHASEDIALOG_H
#define PHASEDIALOG_H

#include <QVariant>
#include <QDialog>
#include <QLabel>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QGridLayout>
#include <QSettings>
#include "ui_phasingform.h"


class PhaseDialog : public QDialog, public Ui::PhasingForm
{
    Q_OBJECT

public:
    PhaseDialog( QWidget* parent = 0, Qt::WindowFlags fl = 0 );
    ~PhaseDialog();
    int lpOldValue;

public slots:
  void plusButtonPressed();
  void minusButtonPressed();
  void rpSliderMoved();
  void lpSliderMoved();
private:
  void readSettings();
  void writeSettings();
};

#endif // ABOUTFORM_H
