/****************************************************************************
** Form interface generated from reading ui file 'aboutform.ui'
**
** Created: Wed May 25 11:16:53 2005
**      by: The User Interface Compiler ($Id: qt/main.cpp   3.1.2   edited Dec 19 11:45 $)
**
****************************************************************************/

#ifndef ABOUTFORM_H
#define ABOUTFORM_H

#include <QVariant>
#include <QDialog>
#include <QLabel>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QGridLayout>
#include <version.h>

class QVBoxLayout;
class QHBoxLayout;
class QGridLayout;
class QLabel;
class QPushButton;

class aboutForm : public QDialog
{
    Q_OBJECT

public:
    aboutForm( QWidget* parent = 0, Qt::WindowFlags fl = 0 );
    ~aboutForm();

    QLabel* logoLabel;
    QLabel* textLabel;
    QPushButton* okButton;

protected:
    QVBoxLayout* aboutFormLayout;
    QVBoxLayout* layout8;
    QHBoxLayout* layout7;
    QHBoxLayout* layout4;

protected slots:
    virtual void languageChange();

};

#endif // ABOUTFORM_H
