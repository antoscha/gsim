#ifndef EXPANDDIALOG_H
#define EXPANDDIALOG_H

#include <QDialog>
#include "ui_expanddialog.h"
#include <QSettings>
#include "plot2dwidget.h"

class Plot2DWidget;

class expandDialog : public QDialog, public Ui::expandDialog
{
    Q_OBJECT

public:
    expandDialog( Plot2DWidget* parent = 0, Qt::WindowFlags fl = 0 );
    ~expandDialog();
public slots:
  void on_markersButton_clicked();

private:
  void readSettings();
  void writeSettings();
  Plot2DWidget* plot;
};

#endif // EXPANDDIALOG_H
