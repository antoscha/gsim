
#include "mainform.h"

#include <QVariant>
#include <QAction>
#include <QMenuBar>
#include <QMenu>
#include <QToolBar>
#include <QDebug>

/*
 *  Constructs a WindowForm as a child of 'parent', with the
 *  name 'name' and widget flags set to 'f' and initialize Ui interface.
 *
 */
MainForm::MainForm( QWidget* parent, Qt::WindowFlags fl )
    : QMainWindow( parent, fl )
{
   setupUi(this);
   (void)statusBar();
    //statusBar = new QStatusBar(this);
//    if ( !name )
    setWindowTitle( "GSim" );
    setIconSize(QSize(24,24));
    setWindowIcon( QPixmap( ":/images/gsim.png" ) );

    // actions
    fileNewWindowAction = new QAction( this);
    fileNewWindowAction->setText( tr( "&New window" ) );
    fileNewWindowAction->setIcon( QIcon::fromTheme("window-new") );
    fileNewWindowAction->setShortcut(QKeySequence::New);

    fileOpenAction = new QAction( this);//, "fileOpenAction" );
    fileOpenAction->setIcon( QIcon::fromTheme("document-open") );
    fileOpenAction->setText( tr( "&Open..." ) );
    fileOpenAction->setShortcut(QKeySequence::Open);

    fileSaveAction = new QAction( this);//, "fileOpenAction" );
    fileSaveAction->setIcon( QIcon::fromTheme("document-save") );
    fileSaveAction->setText( tr( "&Save..." ) );
    fileSaveAction->setShortcut(QKeySequence::Save);

//    fileSaveDirAction = new QAction( this);//, "fileOpenAction" );
//    fileSaveDirAction->setText( tr( "&Save as Dir..." ) );
//    fileSaveDirAction->setShortcut(QKeySequence(tr("Ctrl+D")));

    fileCloseAction = new QAction(this);
    fileCloseAction->setIcon(QIcon::fromTheme("edit-delete") );
    fileCloseAction->setText(tr("&Close"));
    fileCloseAction->setShortcut(QKeySequence(tr("Ctrl+Del")));

    
    filePrintAction = new QAction( this);//, "filePrintAction" );
    filePrintAction->setIcon( QIcon::fromTheme("document-print" ) );
    filePrintAction->setText(tr("&Print"));
    filePrintAction->setShortcut(QKeySequence::Print);

    filePrintPreviewAction = new QAction( this);//, "filePrintAction" );
    filePrintPreviewAction->setIcon( QIcon::fromTheme("document-print-preview") );
    filePrintPreviewAction->setText(tr("Print pre&view"));

    fileExportPdfAction = new QAction( this);
    fileExportPdfAction->setIcon( QIcon( ":/images/pdf.png" ) );
    fileExportPdfAction->setText( tr( "Export to P&DF..." ) );

    fileExportVectorAction = new QAction( this);
    fileExportVectorAction->setText( tr( "Export to vector &graphics..." ) );
    fileExportVectorAction->setIcon( QIcon( ":/images/vectorgfx.png" ) );
    fileExportVectorAction->setShortcut(QKeySequence(tr("Ctrl+Shift+G")));

    fileReloadAction = new QAction( this);
    fileReloadAction->setIcon( QIcon::fromTheme("view-refresh") );
    fileReloadAction->setText(tr("&Reload"));
    fileReloadAction->setShortcut(QKeySequence(tr("Ctrl+R")));

    fileWatchAction = new QAction( this);
    fileWatchAction->setCheckable(true);
    fileWatchAction->setIcon(QIcon( ":/images/eyes.png" ));
    fileWatchAction->setText(tr("&Watch"));
    fileWatchAction->setShortcut(QKeySequence(tr("Ctrl+T")));


    fileExitAction = new QAction( this);//, "fileExitAction" );
    fileExitAction->setText(tr ("E&xit"));
    fileExitAction->setShortcut(QKeySequence::Close);

    editCopyAction = new QAction( this);
    editCopyAction->setIcon(QIcon::fromTheme("edit-copy"));
    editCopyAction->setText(tr("&Copy screen"));
    editCopyAction->setShortcut(QKeySequence(tr("Ctrl+Shift+C")));

    takeInsetAction = new QAction( this);
    takeInsetAction->setText(tr("Take &inset"));

    pasteImageAction = new QAction( this);
    pasteImageAction->setText(tr("Paste image"));

    editUndoAction = new QAction( this);
    editUndoAction->setIcon(QIcon::fromTheme("edit-undo"));
    editUndoAction->setText(tr("&Undo"));
    editUndoAction->setShortcut(QKeySequence::Undo);
    editUndoAction->setEnabled(false);

    editSpectralParsAction = new QAction( this);
    editSpectralParsAction->setText(tr("Spectral pa&rameters"));
    editSpectralParsAction->setShortcut(QKeySequence(tr("P")));

    editDataAction = new QAction( this);
    editDataAction->setText(tr("&Data"));
    editDataAction->setShortcut(QKeySequence(tr("D")));

    editFixScalingsAction = new QAction( this);
    editFixScalingsAction->setText(tr("&Fix shifts/scalings"));
    editFixScalingsAction->setShortcut(QKeySequence(tr("F")));

    viewRealAction = new QAction( this);
    viewRealAction->setText(tr("&Real"));
    viewRealAction->setShortcut(QKeySequence(tr("R")));

    viewImagAction = new QAction( this);
    viewImagAction->setText(tr("&Imaginary"));
    viewImagAction->setShortcut(QKeySequence(tr("I")));

    viewPpmAction = new QAction( this);
    viewPpmAction->setText(tr("&ppm"));
    
    viewHzAction = new QAction( this);
    viewHzAction->setText(tr("&Hz"));

    viewReferencingAction = new QAction( this);
    viewReferencingAction->setText(tr("&Referencing"));
    viewReferencingAction->setShortcut(QKeySequence(tr("Ctrl+Alt+R")));

    viewSetRangeAction = new QAction( this);
    viewSetRangeAction->setText(tr("Set ran&ge"));
    viewSetRangeAction->setIcon(QIcon(":/images/extend.png"));
    viewSetRangeAction->setShortcut(QKeySequence(tr("Ctrl+G")));

    viewOptionsAction= new QAction(this);
    viewOptionsAction->setText(tr("&Options"));

    analysisDeconvAction = new QAction( this);
    analysisDeconvAction->setText(tr("&Deconvolution"));

    analysisArrayAction = new QAction(this);
    analysisArrayAction->setText(tr("&Array manager"));
    analysisArrayAction->setShortcut(QKeySequence(tr("A")));

    analysisAdvFitAction = new QAction(this);
    analysisAdvFitAction->setText(tr("&pNMRsim simulation"));

    helpAboutAction = new QAction( this);//, "helpAboutAction" );
    helpAboutAction->setText(tr ("&About"));

    helpAboutQtAction = new QAction( this);//, "helpAboutAction" );
    helpAboutQtAction->setText(tr ("About &Qt"));

    helpHomeWebpageAction = new QAction( this);
    helpHomeWebpageAction->setIcon(QIcon::fromTheme("applications-internet"));
    helpHomeWebpageAction -> setText(tr ("Homepage"));

    helpForumAction = new QAction( this);
    helpForumAction->setIcon(QIcon::fromTheme("applications-internet"));
    helpForumAction -> setText(tr ("GSim forum"));

    helpDocAction = new QAction( this);
    helpDocAction->setShortcut(QKeySequence(QKeySequence::HelpContents));
    helpDocAction -> setText(tr ("Documentation"));

    helpQuickstartAction = new QAction( this);
    helpQuickstartAction -> setText(tr ("Quick Start"));

//plot toolbar actions
   gridAction=new QAction( this);
   gridAction->setIcon(QIcon(":/images/grids.png"));
   gridAction->setText("Grid on/off");
   gridAction->setCheckable(true);

   hscaleAction=new QAction( this);
   hscaleAction->setIcon(QIcon(":/images/hscale.png"));
   hscaleAction->setText("Horizontal shift/scaling");
   hscaleAction->setCheckable(true);

   vscaleAction=new QAction( this);
   vscaleAction->setIcon(QIcon(":/images/vscale.png"));
   vscaleAction->setText("Vertical shift/scaling");
   vscaleAction->setCheckable(true);

   moveAction=new QAction( this);
   moveAction->setIcon(QIcon(":/images/move.png"));
   moveAction->setText("Move/shift spectral window");
   moveAction->setCheckable(true);
   moveAction->setChecked(true);

   selectAction=new QAction( this);
   selectAction->setIcon(QIcon(":/images/arrow.png"));
   selectAction->setText("Select/Edit graphic objects");
   selectAction->setCheckable(true);

   removeMarkersAction=new QAction( this);
   removeMarkersAction->setIcon(QIcon(":/images/removeMarkers.png"));
   removeMarkersAction->setText("Remove markers");
   removeMarkersAction->setShortcut(QKeySequence(tr("M")));

   resetPlotAction=new QAction( this);
   resetPlotAction->setIcon(QIcon(":/images/reload.png"));
   resetPlotAction->setText("Reset all");

   drawTextAction=new QAction( this);
   drawTextAction->setIcon(QIcon(":/images/text.png"));
   drawTextAction->setText("Draw text at the main marker position...");

   drawLineAction=new QAction( this);
   drawLineAction->setIcon(QIcon(":/images/pencil.png"));
   drawLineAction->setText("Draw line between markers positions");

   drawImageAction=new QAction( this);
   drawImageAction->setIcon(QIcon(":/images/image.png"));
   drawImageAction->setText("Draw bitmap image with the top left corner at the main marker position...");

   plotToolBar = new QToolBar(tr("Plot toolbar"),this);
   plotToolBar->setIconSize(QSize(16,16));
   plotToolBar->setObjectName(tr("Plot operations"));
   plotToolBar->addAction(gridAction);
   plotToolBar->addSeparator();
   plotToolBar->addAction(hscaleAction);
   plotToolBar->addAction(vscaleAction);
   plotToolBar->addAction(moveAction);
   plotToolBar->addAction(selectAction);
   plotToolBar->addSeparator();
   plotToolBar->addAction(removeMarkersAction);
   plotToolBar->addAction(resetPlotAction);
   plotToolBar->addSeparator();
   plotToolBar->addAction(drawTextAction);
   plotToolBar->addAction(drawLineAction);
   plotToolBar->addAction(drawImageAction);

   procToolBar = new QToolBar(tr("Processing toolbar"),this);
   procToolBar->setIconSize(QSize(16,16));
   procToolBar->setObjectName(tr("Processing operations"));
    // toolbars
    toolBar = new QToolBar(tr("Main toolbar"),this);
    toolBar->setIconSize(QSize(16,16));
    toolBar->setObjectName(tr("Toolbar"));
    toolBar->addAction(fileOpenAction);
    toolBar->addAction(fileSaveAction);
    toolBar->addAction(fileReloadAction);
    toolBar->addAction(fileWatchAction);
    toolBar->addAction(fileCloseAction);
    toolBar->addAction(filePrintAction);
    toolBar->addSeparator();
    toolBar->addAction(editCopyAction);
    toolBar->addAction(editUndoAction);
    toolBar->addSeparator();
    toolBar->addAction(viewSetRangeAction);

//



    addToolBar(toolBar);
    addToolBar(plotToolBar);
    addToolBar(procToolBar);
    
    // menubar
    menubar = new QMenuBar( this);

    fileMenu = menubar->addMenu(tr("&File"));
    fileMenu->addAction(fileNewWindowAction);
    fileMenu->addAction(fileOpenAction);
//    fileMenu->addAction(fileFtpAction);
    fileMenu->addAction(fileSaveAction);
//    fileMenu->addAction(fileSaveDirAction);
    fileMenu->addAction(fileReloadAction);
    fileMenu->addAction(fileWatchAction);
    	fileMoveToMenu = fileMenu->addMenu(tr("&Move to"));
    fileMenu->addAction(fileCloseAction);
    fileMenu->addAction(fileExportPdfAction);
    fileMenu->addAction(fileExportVectorAction);
    fileMenu->addAction(filePrintAction);
    fileMenu->addAction(filePrintPreviewAction);
    fileMenu->addSeparator();
    fileMenu->addAction(fileExitAction);

    editMenu = menubar->addMenu(tr("&Edit"));
    editMenu->addAction(editUndoAction);
    editMenu->addSeparator();
    editMenu->addAction(editCopyAction);
    editMenu->addAction(takeInsetAction);
    editMenu->addAction(pasteImageAction);
    editMenu->addAction(editSpectralParsAction);
    editMenu->addAction(editDataAction);
//    editMenu->addSeparator();
//	invertMenu = editMenu->addMenu(tr("In&vert"));
//  	invertMenu->addAction(editInvertHorAction);
//    	invertMenu->addAction(editInvertVertAction);
    editMenu->addSeparator();
    editMenu->addAction(editFixScalingsAction);

    viewMenu = menubar->addMenu(tr("&View"));
    viewMenu->addAction(viewRealAction);
    viewMenu->addAction(viewImagAction);
    viewMenu->addAction(viewReferencingAction);
//    viewMenu->addAction(viewExtendAction);
    viewMenu->addAction(viewSetRangeAction);
    viewMenu->addSeparator();
	rulerMenu = viewMenu->addMenu(tr("&Ruler"));
	rulerMenu->addAction(viewPpmAction);
	rulerMenu->addAction(viewHzAction);
    viewMenu->addSeparator();
    
    viewMenu->addAction(viewOptionsAction);
    viewWindowsAction=viewMenu->addMenu(createPopupMenu());
    viewWindowsAction->setText(tr("&Tools"));

    analysisMenu = menubar->addMenu(tr("&Analysis"));
    analysisMenu->addAction(analysisDeconvAction);
    analysisMenu->addAction(analysisArrayAction);
    analysisMenu->addAction(analysisAdvFitAction);

    procMenu = menubar->addMenu(tr("P&rocess"));

//    procMenu->addAction(processBatchAction);
//    procMenu->addSeparator();
//    optionsMenu->addAction(optionsEditAction);
    windowsMenu = menubar->addMenu(tr("&Windows"));

    helpMenu = menubar->addMenu(tr("&Help"));
    helpMenu->addAction(helpDocAction);
    helpMenu->addAction(helpQuickstartAction);
    helpMenu->addAction(helpHomeWebpageAction);
    helpMenu->addAction(helpForumAction);
    helpMenu->addSeparator();
    helpMenu->addAction(helpAboutAction);
    helpMenu->addAction(helpAboutQtAction);
    

    commandLineEdit=new commandLineWidget(this);
    commandLineEdit->setToolTip("Type your processing command here");
    statusBar()->addPermanentWidget(commandLineEdit);

    
    setMenuBar(menubar);

    // signals and slots connections
    connect( fileOpenAction, SIGNAL( triggered() ), this, SLOT( fileOpenDialog() ) );
    connect( fileNewWindowAction, SIGNAL( triggered() ), this, SLOT( fileNewWindow() ) );
//    connect( fileFtpAction, SIGNAL( triggered() ), this, SLOT( fileFtpDialog() ) );
    connect( fileSaveAction, SIGNAL( triggered() ), this, SLOT( fileSaveDialog() ) );
//    connect( fileSaveDirAction, SIGNAL( triggered() ), this, SLOT( fileSaveDir() ) );
    connect(fileCloseAction, SIGNAL(triggered() ), this, SLOT( fileClose() ) );
    connect( filePrintAction, SIGNAL( triggered() ), this, SLOT( filePrint() ) );
    connect( filePrintPreviewAction, SIGNAL( triggered() ), this, SLOT( filePrintPreview() ) );
    connect( fileExportPdfAction, SIGNAL( triggered() ), this, SLOT( fileExportPdf() ) );
    connect( fileExportVectorAction, SIGNAL( triggered() ), this, SLOT( fileExportVector() ) );

    connect( fileReloadAction, SIGNAL( triggered() ), this, SLOT( fileReload() ) );
    connect( fileWatchAction, SIGNAL( triggered(bool) ), this, SLOT(fileWatch(bool) ) );
    connect( fileExitAction, SIGNAL( triggered() ), this, SLOT( fileExit() ) );
    connect(editUndoAction, SIGNAL( triggered() ), this, SLOT (editUndo()));
    connect(editCopyAction, SIGNAL( triggered() ), this, SLOT (editCopy()));
    connect(takeInsetAction, SIGNAL( triggered() ), this, SLOT (takeInset()));
    connect(pasteImageAction, SIGNAL( triggered() ), this, SLOT (pasteImage()));
    connect (editSpectralParsAction, SIGNAL( triggered() ), this, SLOT (editSpectralPars()));
    connect (editDataAction, SIGNAL( triggered() ), this, SLOT (editData()));
    connect (editFixScalingsAction, SIGNAL( triggered() ), this, SLOT (fixScalings()));
    connect (viewSetRangeAction, SIGNAL( triggered() ), this, SLOT(viewSetRange()));
    connect (viewRealAction, SIGNAL( triggered() ), this, SLOT (viewReal()));
    connect (viewImagAction, SIGNAL( triggered() ), this, SLOT (viewImag()));
    connect (viewPpmAction, SIGNAL( triggered() ), this, SLOT (toPpm()));
    connect (viewHzAction, SIGNAL( triggered() ), this, SLOT (toHz()));
    connect (viewReferencingAction, SIGNAL( triggered() ), this, SLOT (viewReferencing()));
//    connect (viewExtendAction, SIGNAL( triggered() ), this, SLOT (viewExtend()));
    connect (viewOptionsAction, SIGNAL (triggered() ), this, SLOT (editOptions()));
    connect (analysisDeconvAction, SIGNAL (triggered() ), this, SLOT (analysisDeconv()));
    connect (analysisArrayAction, SIGNAL (triggered() ), this, SLOT (analysisArray()));
    connect (analysisAdvFitAction, SIGNAL (triggered() ), this, SLOT (analysisAdvFit()));
//    connect (processBatchAction, SIGNAL (triggered() ), this, SLOT (batchProcessing()));
    connect( filesListWidget, SIGNAL( currentRowChanged(int) ), this, SLOT( curveSelected(int) ) );
    connect( filesListWidget, SIGNAL( itemSelectionChanged ()), this, SLOT( filesSelectionChanged() ) );
    connect( integralSetPushButton, SIGNAL( clicked() ), this, SLOT (add_integral() ) );
    connect( levelsPushButton, SIGNAL( clicked() ), this, SLOT (update2d() ) );
    connect( slicePushButton, SIGNAL( clicked() ),this, SLOT (takeSlice() ) );
    connect( helpAboutAction, SIGNAL(triggered()), this, SLOT (helpAbout()));
    connect( helpAboutQtAction, SIGNAL(triggered()), this, SLOT(helpAboutQt()));
    connect( helpDocAction, SIGNAL(triggered()), this, SLOT (helpDoc()));
    connect( helpQuickstartAction, SIGNAL(triggered()), this, SLOT (helpQuickstart()));
    connect( helpHomeWebpageAction, SIGNAL(triggered()), this, SLOT(helpHomeWebpage()));
    connect( helpForumAction, SIGNAL(triggered()), this, SLOT(helpForum()));
    //connect( delIntegralsPushButton, SIGNAL(clicked()), this, SLOT(delIntegrals()));
    connect( delIntegralsPushButton, SIGNAL(clicked()), this, SLOT(delIntegralAsked()));
    connect( plusSliceButton, SIGNAL(clicked()), this, SLOT(nextSlice()));
    connect( minusSliceButton, SIGNAL(clicked()), this, SLOT(previousSlice()));
    connect( mult2DButton, SIGNAL(clicked()), this, SLOT(mult2dPressed()));
    connect( div2DButton, SIGNAL(clicked()), this, SLOT(div2dPressed()));

    connect( mdiArea, SIGNAL(subWindowActivated ( QMdiSubWindow *)), this, SLOT(windowActivated( QMdiSubWindow *)));

//plottoolbar action connections (doesn't work automatically)
   connect(gridAction, SIGNAL(toggled(bool)), this, SLOT(gridActiontoggled(bool)));
   connect(moveAction, SIGNAL(triggered(bool)), this, SLOT(moveActiontriggered(bool)));
   connect(hscaleAction, SIGNAL(triggered(bool)), this, SLOT(hscaleActiontriggered(bool)));
   connect(vscaleAction, SIGNAL(triggered(bool)), this, SLOT(vscaleActiontriggered(bool)));
   connect(selectAction, SIGNAL(triggered(bool)), this, SLOT(selectActiontriggered(bool)));
   connect(resetPlotAction, SIGNAL(triggered(bool)), this, SLOT(resetPlotActiontriggered(bool)));
   connect(removeMarkersAction, SIGNAL(triggered(bool)), this, SLOT(removeMarkersActiontriggered(bool)));
   connect(drawTextAction, SIGNAL(triggered(bool)), this, SLOT(drawTextActiontriggered(bool)));
   connect(drawLineAction, SIGNAL(triggered(bool)), this, SLOT(drawLineActiontriggered(bool)));
   connect(drawImageAction, SIGNAL(triggered(bool)), this, SLOT(drawImageActiontriggered(bool)));
   connect(commandLineEdit, SIGNAL(returnPressed()), this, SLOT(commandLineEdit_returnPressed()));

    init();

    //set dirView in dirTree view

    dirModel = new QDirModel;
    dirModel->setSorting(QDir::DirsFirst | QDir::Name);
//    dirModel->setReadOnly(false);

    dirTreeView->setModel(dirModel);
    dirTreeView->hideColumn(1);
    dirTreeView->hideColumn(2);
    dirTreeView->hideColumn(3);
    dirTreeView->header()->hide();
//    dirTreeView->setRootIndex(dirModel->index(QDir::rootPath()));

    dirTreeView->setDragEnabled(true);
    QModelIndex inx=dirModel->index(working_directory);
    dirTreeView->expand(inx);
    dirTreeView->setCurrentIndex(inx);

    watchTimer=new QTimer(this);
    connect(watchTimer, SIGNAL(timeout()), this, SLOT(timerEvent()));
//      watcher=NULL;

    printer = new QPrinter(QPrinter::ScreenResolution);
    printer->setOrientation(QPrinter::Landscape);
}

/*
 *  Destroys the object and frees any allocated resources
 */
MainForm::~MainForm()
{
    destroy();
    // no need to delete child widgets, Qt does it all for us
}

