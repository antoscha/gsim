#include "castepdialog.h"

castepDialog::castepDialog(QWidget* parent, Qt::WindowFlags fl)
	:QDialog(parent, fl) {
    setupUi(this);
    setWindowIcon( QPixmap( ":/images/gsim.png" ) );

    // signals and slots connections
//    readSettings();
}


castepDialog::~castepDialog()
{
    writeSettings();
    // no need to delete child widgets, Qt does it all for us
}

void castepDialog::readSettings()
{
        QSettings settings("GSim", "GSim");
        settings.beginGroup("CastepDialog");
        resize(settings.value("size", QSize(367, 220)).toSize());
        move(settings.value("pos", QPoint(200, 200)).toPoint());
        int index=nucleiComboBox->findText(settings.value("Type",QString("")).toString());
        if (index>0)
            nucleiComboBox->setCurrentIndex(index);
        freqSpinBox->setValue(settings.value("NMRfreq",125.0).toDouble());
	npSpinBox->setValue(settings.value("NP",8192).toInt());
	shiftSpinBox->setValue(settings.value("Shift",0.0).toDouble());
	lwSpinBox->setValue(settings.value("LW",10.0).toDouble());
        idenAtomsLineEdit->setText(settings.value("idenAtoms",QString("")).toString());
        settings.endGroup();
}

void castepDialog::writeSettings()
{
	QSettings settings("GSim", "GSim");
        settings.beginGroup("CastepDialog");
        settings.setValue("size", size());
        settings.setValue("pos", pos());
        settings.setValue("Type",nucleiComboBox->currentText());
	settings.setValue("NMRfreq",freqSpinBox->value());
	settings.setValue("NP",npSpinBox->value());
	settings.setValue("Shift",shiftSpinBox->value());
	settings.setValue("LW",lwSpinBox->value());
        settings.setValue("idenAtoms",idenAtomsLineEdit->text());
        settings.endGroup();
}
