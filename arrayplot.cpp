#include "plot2dwidget.h"
#include "version.h"
#include <QDebug>
#include <QInputDialog>
#include <QMessageBox>
#include <QTimer>
#include <QMenu>
#include <QColorDialog>
#include <QFontDialog>
#include "plot2dwidget.h"
#include "arrayplot.h"


#ifdef QT_OPENGL_LIB
ArrayWidget::ArrayWidget( QWidget *parent ): QGLWidget(parent)
#else
ArrayWidget::ArrayWidget( QWidget *parent ): QWidget(parent)
#endif
{
    xglobmax=-1e36;
    yglobmax=-1e36;
    xglobmin=1e36;
    yglobmin=1e36;
    xwinmin=1e36;
    ywinmin=1e36;
    xwinmax=-1e36;
    ywinmax=-1e36;
}

ArrayWidget::~ArrayWidget()
{
 //   delete picture;
}

void ArrayWidget::set_global_range()
{
    if (!xdata.size())
	return;
    if (!fit.size()) {
    	yglobmin=min(ydata);
    	yglobmax=max(ydata);
    }
    else {
	yglobmin=(min(ydata)<min(fit))?min(ydata):min(fit);
	yglobmax=(max(ydata)>max(fit))?max(ydata):max(fit);
    }
    xglobmin=min(xdata);
    xglobmax=max(xdata);
}

void ArrayWidget::set_window_range(double xmin, double xmax, double ymin, double ymax)
{
    xwinmin=xmin;
    xwinmax=xmax;
    ywinmin=ymin;
    ywinmax=ymax;
    if (ywinmin==ywinmax) {
	ywinmin-=5.0;
	ywinmax+=5.0;
	} // it saves from crash if all data is zeros
    if (xwinmin==xwinmax) {
	xwinmin-=5.0;
	xwinmax+=5.0;
	} // the same for the x-range
    xrange=xwinmax-xwinmin;
    yrange=ywinmax-ywinmin;
    xppt=xrange/(width()-1);
    yppt=yrange/(height()-1);
}

void ArrayWidget::fromreal_toscreen (double x, double y, QPointF & point)
{
    point.setX((x-xwinmin)/xppt);
    point.setY(height()-(y-ywinmin)/yppt);
}

void ArrayWidget::fromscreen_toreal (QPoint point, double& x, double& y)
{
    x= point.x()*xppt+xwinmin;
    y= (height()-point.y())*yppt+ywinmin;
//qDebug()<<"Fromscreen_toreal (no k)"<<x<<y;
}

void ArrayWidget::paintEvent( QPaintEvent * )
{
    QPainter* paint = new QPainter();
    if (!paint) cerr<<"paintEvent: Cannot create painter\n";
    paint->begin( this );          // paint in the widget
    draw_picture(paint);
    paint->end();                       // painting done
    delete paint;
}

void ArrayWidget::draw_picture(QPainter* paint)
{
    paint->setBrush(Qt::SolidPattern);
//    paint->setRenderHints(paint->renderHints() | QPainter::Antialiasing);
    if (xdata.size()) {
    	set_global_range();
    	set_window_range(xglobmin-(xglobmax-xglobmin)*0.15,xglobmax+(xglobmax-xglobmin)*0.05,yglobmin-(yglobmax-yglobmin)*0.15,yglobmax+(yglobmax-yglobmin)*0.05);
    	draw_points(paint);  // draw picture
	draw_fit(paint);
	draw_axis(paint);
	draw_yaxis(paint);
    }
}

void ArrayWidget::resizeEvent (QResizeEvent *)
{
    set_window_range(xwinmin, xwinmax, ywinmin, ywinmax);
    update();
}

void ArrayWidget::draw_axis(QPainter * paint)
{
int extraoffset=3;
QPen pen = paint->pen();
pen.setColor(Qt::black);
pen.setStyle(Qt::SolidLine);
paint->setPen(pen);
double yaxisline=height()-TICSLEN-fontMetrics().height()-extraoffset;

double startxat=0.0;

paint->drawLine(QLineF(startxat,yaxisline,width(),yaxisline));

double step=first_decimal(xrange/5);
double firstlabel= int(xwinmin/step)*step;
double lastlabel= (int(xwinmax/step)+1)*step;
QPointF p;
for (double i=firstlabel; i<=lastlabel; i=i+step)
{
    QPointF p1, p2;
    if (fabs(i/step)<1e-10) 
	i=0; //fix rounding error around zero
    fromreal_toscreen(i, 0, p);

	p1.setX(p.x());
	p1.setY(yaxisline);
	p2.setX(p.x());
	p2.setY(yaxisline+TICSLEN);
        if (p.x()>startxat && p.x()<width()) {
		paint->drawLine(p1,p2);
    	}

	size_t n=5;//5 extra tics
	pen.setStyle(Qt::SolidLine);
	paint->setPen(pen);
	double sstep=step/n;
	if (i==firstlabel) //creating minor ticks bakward 
		for (size_t j=0; j<n; j++) {
			fromreal_toscreen(i-sstep*j,0, p1);
			p1.setY(yaxisline);
			if (p1.x()>startxat && p1.x()<width())
				paint->drawLine(p1,p1+QPointF(0.0, 2.0));
			}	
		for (size_t j=0; j<n; j++) {
			fromreal_toscreen(i+sstep*j,0, p1);
			p1.setY(yaxisline);
			if (p1.x()>startxat && p1.x()<width())
				paint->drawLine(p1,p1+QPointF(0.0, 2.0));
		}

    QString lab= QString("%1").arg(i,0, 'g', -1);
    QPoint pp(int(p.x()-paint->fontMetrics().width(lab,-1)/2), height()-extraoffset);
    paint->drawText(pp,lab);
}
}



void ArrayWidget::draw_yaxis(QPainter * paint)
{
	QSize win=size();
	QPen pen=paint->pen();
	pen.setColor(Qt::black);
	paint->setPen(pen);
	int length=height();
	length-=TICSLEN+fontMetrics().height()+3;//should be synchronised with X-axis position (better way?)
	QLine line(AXOFFSET, 0, AXOFFSET, length);
	paint->drawLine(line);
	double step=first_decimal(yrange/5);
	double firstlabel= int(ywinmin/step)*step;
	double lastlabel= int(ywinmax/step)*step;
	QPointF p;
	for (double i=firstlabel; i<=lastlabel; i=i+step)
	{
		QPointF p1,p2;
    		if (fabs(i/step)<1e-10) 
			i=0; //fix rounding error around zero
		fromreal_toscreen(0,i, p);
		p1.setY(p.y());
		p1.setX(AXOFFSET);
		p2.setY(p.y());
		p2.setX(AXOFFSET+TICSLEN);
		if (p1.y()<length) //dont paint tick outside the axis
			paint->drawLine(p1,p2);
			size_t n=5;
			pen.setStyle(Qt::SolidLine);
			paint->setPen(pen);
			double sstep=step/n;
			if (i==firstlabel)
			for (size_t j=0; j<n; j++) {
				fromreal_toscreen(0,i-sstep*j, p1);
				p1.setX(AXOFFSET);
				if (p1.y()<length) //dont paint tick outside the axis
					paint->drawLine(p1,p1+QPointF(2.0, 0.0));
			}
			for (size_t j=0; j<n; j++) {
				fromreal_toscreen(0,i+sstep*j, p1);
				p1.setX(AXOFFSET);
				if (p1.y()<length) //dont paint tick outside the axis
					paint->drawLine(p1,p1+QPointF(2.0, 0.0));
			}
		QString lab= QString("%1").arg(i,0, 'g', -1);
		QPoint pp(AXOFFSET+TICSLEN+2,int(p.y()+paint->fontMetrics().height()/2.5));
		paint->drawText(pp,lab);
	}
}


void ArrayWidget::draw_points(QPainter * paint)
{
size_t npt=xdata.size();
if (!npt)
	return;
if (npt!=ydata.size()){
	cout<<"Xarray is different from y array\n";
	return;
}
paint->setPen(Qt::blue);
QPointF point;
for (size_t i=0; i<npt; i++)
{
	fromreal_toscreen(xdata(i),ydata(i),point);
//	qDebug()<<point;
	paint->drawLine(point-QPoint(-3,0),point-QPoint(3,0));
	paint->drawLine(point-QPoint(-3,-3),point-QPoint(3,3));
	paint->drawLine(point-QPoint(-3,3),point-QPoint(3,-3));
}
paint->setPen(Qt::black);
}

bool	myQPointFLessThan (const QPointF& p1,const QPointF& p2) {return p1.x()<p2.x();};

void ArrayWidget::draw_fit(QPainter * paint)
{
	size_t npt=fit.size();
	if (!npt)
		return;
	paint->setPen(Qt::red);
	QPointF point;
	QPolygonF poly;
	for (size_t i=0; i<npt; i++)
	{
		fromreal_toscreen(xdata(i),fit(i),point);
		poly<<point;
	}
	qSort(poly.begin(),poly.end(),myQPointFLessThan);
	paint->drawPolyline(poly);
	paint->setPen(Qt::black);
}


