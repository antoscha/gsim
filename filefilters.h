#ifndef FILEFILTERS_H
#define FILEFILTERS_H

#include "base.h"
#include "matlabio.h"
#include <QStringList>

class BaseFileFilter
{
public:
BaseFileFilter(DataStore* p=NULL) {data=p;};
virtual ~BaseFileFilter() {};
	virtual bool matching(const char*) {return false;}; //return is filename match this particular filter or not
	virtual void read(QList<data_entry>&, QString);
	virtual int read(cmatrix &, gsimFD &, QList<Array_> &, const char*);
	virtual int read(cmatrix &, gsimFD &, const char*) {return 1;};
	virtual void write(QList<data_entry>&, QString&);
	virtual void write(const cmatrix&, gsimFD, const char*, QList<Array_>);
	virtual void write(const cmatrix&, gsimFD, const char*) {cout<<"Write is not realised\n";};
	virtual QString short_filename(QString) {return QString("Shortname function is missing");};
	virtual QString name() {return QString("name is missing");}; //name of filters for dialogs
	virtual QString extensions() {return QString("*");}; //supported file extensions
	virtual bool canRead() {return false;};
	virtual bool canWrite() {return false;};
	virtual bool invertAfterFT() {return false;};
	virtual bool savesToDir() {return false;};
	virtual void extract(const data_entry*, QStringList&);
	DataStore* data;
protected:
	bool isreadable(const char*); //is file readable?
	int which_array(QString &, QList<Array_>&);
};

class SpinsightFileFilter : public BaseFileFilter 
{
public:
SpinsightFileFilter(DataStore* p=NULL) : BaseFileFilter(p) {};
virtual ~SpinsightFileFilter() {};
	bool matching(const char*);
	void read(QList<data_entry>&, QString);
	int read(cmatrix &, gsimFD &, QList<Array_> &, const char*);
	void write(QList<data_entry>&, QString&);
	void write(const cmatrix&, gsimFD, const char*, QList<Array_>);
	QString short_filename(QString);
	QString name() {return QString("SPINSIGHT spectra or FID");};
	QString extensions() {return QString("data");};
	bool canRead() {return true;};
	bool canWrite() {return true;};
	bool savesToDir() {return true;};
	virtual bool invertAfterFT() {return true;};
	void extract(const data_entry*, QStringList&);
private:
	int read_spinsight_parfile(QString, QHash<QString, QString> &);
	double absmax(const BaseList<complex>&); 
	template <class T> void write_out(const BaseList<complex>& , int, ofstream& );
	void write_data(const cmatrix&, const gsimFD&, const char*);
	void write_parameters(gsimFD, const char *, QList<Array_>,size_t,size_t);
	void write_spinsight(const cmatrix&, gsimFD, const char*, QList<Array_>);
};

class SimpsonFileFilter : public BaseFileFilter 
{
public:
SimpsonFileFilter(DataStore* p=NULL) : BaseFileFilter(p) {};
virtual ~SimpsonFileFilter() {};
	bool matching(const char*);
	void read(QList<data_entry>&, QString);
	int read(cmatrix &, gsimFD &, const char*);
	void read_source(data_entry& dat, QString fname);
	void write(const cmatrix &, gsimFD, const char*);
	QString short_filename(QString);
	QString name() {return QString("SIMPSON spectra or FID");};
	QString extensions() {return QString("*.spe *.fid");};
	bool canRead() {return true;};
	bool canWrite() {return true;};
};

class XwinnmrFileFilter : public BaseFileFilter 
{
public:
XwinnmrFileFilter(DataStore* p=NULL) : BaseFileFilter(p) {};
virtual ~XwinnmrFileFilter() {};
	virtual bool matching(const char*);
//	virtual void read(QList<data_entry>&, QString);
	virtual int read(cmatrix &, gsimFD &, QList<Array_> &, const char*);
//	virtual QStringList extract(); //extract processing parameters
	virtual QString short_filename(QString);
	virtual QString name() {return QString("Bruker XWIN-NMR FID");};
	virtual QString extensions() {return QString("fid ser");};
	virtual bool canRead() {return true;};
	virtual bool invertAfterFT() {return true;};
	virtual void extract(const data_entry*, QStringList&);
protected:
	void read_xwinnmr_array(Array_ &, QFile*);
	void read_xwinnmr_arrays(QList<Array_> &, const char*);
	int read_xwinnmr_parfile(QString, QHash<QString, QString> &);
	int read_xwinnmr(cmatrix &, gsimFD &, const char* );
};

class XwinnmrprocFileFilter : public XwinnmrFileFilter
{
public:
XwinnmrprocFileFilter(DataStore* p=NULL) : XwinnmrFileFilter(p) {};
virtual ~XwinnmrprocFileFilter() {};
	bool matching(const char*);
	int read(cmatrix &, gsimFD &, QList<Array_> &, const char* );
//	QStringList extract(); //extract processing parameters
	QString short_filename(QString);
	QString name() {return QString("Bruker XWIN-NMR 1D spectra");};
	QString extensions() {return QString("*1r");};
	bool canRead() {return true;};
	virtual bool invertAfterFT() {return false;};
	void extract(const data_entry*, QStringList& l) {l.clear();};
};

class VnmrFileFilter : public BaseFileFilter 
{
public:
VnmrFileFilter(DataStore* p=NULL) : BaseFileFilter(p) {};
virtual ~VnmrFileFilter() {};
	bool matching(const char*);
	int read(cmatrix &, gsimFD &, QList<Array_> &, const char*);
	void write(QList<data_entry>&, QString&);
	QString short_filename(QString);
	QString name() {return QString("Varian VNMR FID");};
	QString extensions() {return QString("fid data phasefile");};
	bool canRead() {return true;};
	bool canWrite() {return false;};
private:
	int read_vnmr_parfile(QString, QHash<QString, QString> &);
	int namelength; //the length of the filename(3 /fid/ or 4 /data/)
	void write_data(data_entry&, QString&);
	void write_parfile(data_entry&, QString&);
};

class SpinevolutionFileFilter : public BaseFileFilter 
{
public:
SpinevolutionFileFilter(DataStore* p=NULL) : BaseFileFilter(p) {};
virtual ~SpinevolutionFileFilter() {};
	bool matching(const char*);
//	void read(QList<data_entry>&, QString);
	int read(cmatrix &, gsimFD &, const char*);
	QString short_filename(QString);
	QString name() {return QString("SPINEVOLUTION spectra or FID");};
	QString extensions() {return QString("*.dat");};
	bool canRead() {return true;};
private:
	int read_spinevolution(cmatrix &, gsimFD &, const char*);
};

class CastepData
{
public:
	QStringList availableAtomTypes;
	QStringList allNames;
	QList<int> indexes;
        QList<QString> extralabels; //":" style labels
        QList<int> multiplicity; //peak multiplicities
	List<double> iso;
        QList<QStringList> idenAtoms;//list of atoms which chemical shifts should be averaged out, e.g. protons in CH3.
};

class MatlabFileFilter : public BaseFileFilter 
{
public:
MatlabFileFilter(DataStore* p=NULL) : BaseFileFilter(p) {};
virtual ~MatlabFileFilter() {};
	bool matching(const char*);
	template <class T> void read_data(T&, QList<data_entry>& );
	void read_graphics(matlab_controller::composite& , data_entry& );
	void read_arrays(matlab_controller::composite& , data_entry& );
	void read_params(matlab_controller::composite& , data_entry& );
	void read_integrals(matlab_controller::composite& , data_entry& );
	void read(QList<data_entry>&, QString);
	bool hasData;
//	int read(cmatrix &, gsimFD &, const char*);
//	void read_source(data_entry& dat, QString fname);
	template <class T> List<double> pair(T& a, T& b) {//create a list with two values	
		List<double> temp;
		temp.push_back(double(a));
		temp.push_back(double(b));
		return temp;
	};
//	QString fromListToQString(List<char> string) {
//		QString s;
	//	for (size_t i=0; i<string.size(); i++)
	//		s.push_back(string(i));
//		s=string.vector();
//		s.chop(1);
//		return s;
//	};
	template <class T> List<double> one(T& a) {//create a list with a single value
		List<double> temp;
		temp.push_back(double(a));
		return temp;
	};
	template <class T1,class T2> void read_numbers(T1& ctrl, T2* a=NULL, T2* b=NULL){
		List<double> list;
		ctrl.read(list);
		uint N=list.size();
		if ((a!=NULL) && (N>0))
			*a=T2(list(0));
		if ((b!=NULL) && (N>1))
			*b=T2(list(1));
//		if ((c!=NULL) && (N>2))
//			*c=T2(list(2));
//		if ((d!=NULL) && (N>3))
//			*d=T2(list(3));
	};
	void write(QList<data_entry>&, QString&);
	QString short_filename(QString);
	QString name() {return QString("MATLAB files");};
	QString extensions() {return QString("*.mat");};
	bool canRead() {return true;};
	bool canWrite() {return true;};
};

class CastepFileFilter : public BaseFileFilter 
{
public:
CastepFileFilter(DataStore* p=NULL) : BaseFileFilter(p)  {};
virtual ~CastepFileFilter() {};
	bool matching(const char*);
	void read(QList<data_entry>&, QString);
        void averageShifts();
//	int read(cmatrix &, gsimFD &, const char*);
	QString short_filename(QString);
	QString name() {return QString("Castep MagRes calculation");};
	QString extensions() {return QString("*.magres");};
	bool canRead() {return true;};
protected:
	CastepData castepData;
};

#endif
