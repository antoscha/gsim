
#include "datatable.h"
#include "base.h"

#include <QVariant>
#include <QClipboard>
//#include <QDebug>

DataTable::DataTable(MainForm *parent, Qt::WindowFlags fl)
	:QMainWindow(parent, fl) {

    setupUi(this);

    p=parent;
    setWindowTitle( "Spectral data" );
    setWindowIcon( QPixmap( ":/images/gsim.png" ) );
    setIconSize(QSize(24,24));

    fileExitAction = new QAction( this);
    fileExitAction->setText( tr( "E&xit" ) );
    fileExitAction->setShortcut(QKeySequence(tr("Ctrl+Q")));

    editCopyAction = new QAction( this);
    editCopyAction->setText( tr( "&Copy Ctrl+C" ) );
    editCopyAction->setShortcut(QKeySequence(tr("Ctrl+C")));
    editCopyAction->setIcon( QIcon::fromTheme("edit-copy" ) );

    editPasteAction = new QAction( this);
    editPasteAction->setText( tr( "&Paste Ctrl+V" ) );
    editPasteAction->setShortcut(QKeySequence(tr("Ctrl+V")));
    editPasteAction->setIcon( QIcon::fromTheme("edit-paste" ) );

    toolBar = new QToolBar(this);
    addToolBar(toolBar);
    toolBar->addAction(editCopyAction);
    toolBar->addAction(editPasteAction);

//    menubar = new QMenuBar(this);
   
    fileMenu = menuBar()->addMenu(tr("&File"));
    fileMenu->addAction(fileExitAction);

    editMenu = menuBar()->addMenu(tr("&Edit"));
    editMenu->addAction(editCopyAction);
    editMenu->addAction(editPasteAction);


    connect( fileExitAction, SIGNAL( triggered() ), this, SLOT( on_cancelButton_clicked() ) );
    connect( editCopyAction, SIGNAL( triggered() ), this, SLOT( editCopy() ) );
    connect( editPasteAction, SIGNAL( triggered() ), this, SLOT( editPaste() ) );

    size_t k=p->data->getActiveCurve();
//    QStringList headers;
//    headers<<"Row"<<"Col"<<"Real"<<"Imaginary";
//    dataTable->setHorizontalHeaderLabels(headers);
    int ni=p->data->get_odata(k)->rows();
    int np=p->data->get_odata(k)->cols();
    if (!ni) ni=1;
    int total=ni*np;
    QString x_title=QString("x / %1").arg(p->activePlot()->xaxis_label);
    QString y_title=QString("y / %1").arg(p->activePlot()->yaxis_label);
    QStringList headers;
    headers<<y_title<<x_title<<"Real"<<"Imaginary";
    if (p->data->hasHyperData(k)) {
	headers<<"Hyper Real"<<"Hyper Imaginary";
	dataTable->setColumnCount(6);
    }
    dataTable->setHorizontalHeaderLabels(headers);
    dataTable->setRowCount(total);
    QProgressDialog progress("Building table...", "Abort", 0, total, this);
    for (int i=0; i<ni; i++)
	for (int j=0; j<np; j++) {
		progress.setValue(j+i*np);
		qApp->processEvents();
		if (progress.wasCanceled())
                	return;
		if (ni>1) {
			QTableWidgetItem* item1=new QTableWidgetItem(QString("%1").arg(p->data->get_y(k,i)));
			item1->setBackgroundColor(Qt::lightGray);
			item1->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);
			dataTable->setItem(j+i*np, 0,item1);
			}
		QTableWidgetItem* item2=new QTableWidgetItem(QString("%1").arg(p->data->get_x(k,j)));
		item2->setBackgroundColor(Qt::lightGray);
		item2->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);
		dataTable->setItem(j+i*np, 1, item2);

		dataTable->setItem(j+i*np, 2, new QTableWidgetItem(QString("%1").arg(real(p->data->get_odata(k,i,j)))));
		dataTable->setItem(j+i*np, 3, new QTableWidgetItem(QString("%1").arg(imag(p->data->get_odata(k,i,j)))));
		if (p->data->hasHyperData(k)) {
			dataTable->setItem(j+i*np, 4, new QTableWidgetItem(QString("%1").arg(real(p->data->get_hdata(k,i,j)))));
			dataTable->setItem(j+i*np, 5, new QTableWidgetItem(QString("%1").arg(imag(p->data->get_hdata(k,i,j)))));	
		}
	}
	 progress.setValue(total);
	if (ni<2)
		dataTable->hideColumn(0);
//      dataTable->resizeRowsToContents();
}

/*
 *  Destroys the object and frees any allocated resources
 */
DataTable::~DataTable()
{
    // no need to delete child widgets, Qt does it all for us
}

void DataTable::closeEvent ( QCloseEvent * )
{
	dataTable->clear();
}

void DataTable::on_cancelButton_clicked()
{
	close();
}

void DataTable::on_okButton_clicked()
{
    complex temp(0.0,0.0);
    size_t k=p->data->getActiveCurve();
    size_t np=p->data->get_odata(k)->cols();
    size_t ni=p->data->get_odata(k)->rows();
    if (!ni) ni=1;
    cmatrix mat(ni,np);
    QString error("At least one of the cells contains a non-numerical value");
    bool ok;
    for (size_t i=0; i<ni; i++)
	for (size_t j=0; j<np; j++) {
	if (dataTable->item(j+i*np,2)==NULL || dataTable->item(j+i*np,3)==NULL) {
		QMessageBox::critical(this,"Error", error);
		return;
	}
	//temp.re=dataTable->item(j+i*np,2)->text().toDouble(&ok);
        real(temp,dataTable->item(j+i*np,2)->text().toDouble(&ok));
	if (!ok) {
		QMessageBox::critical(this,"Error", error);
		return;
	}
	//temp.im=dataTable->item(j+i*np,3)->text().toDouble(&ok);
	imag(temp,dataTable->item(j+i*np,3)->text().toDouble(&ok));
	if (!ok) {
		QMessageBox::critical(this,"Error", error);
		return;
	}
	mat(i,j)=temp;
	//p->data->set_odata(k,i,j,temp);
    }
	p->data->set_odata(k,mat);
    if (p->data->hasHyperData(k)) {
    for (size_t i=0; i<ni; i++)
	for (size_t j=0; j<np; j++) {
	if (dataTable->item(j+i*np,4)==NULL || dataTable->item(j+i*np,4)==NULL) {
		QMessageBox::critical(this,"Error", error);
		return;
	}
	//temp.re=dataTable->item(j+i*np,4)->text().toDouble(&ok);
	real(temp,dataTable->item(j+i*np,4)->text().toDouble(&ok));
	if (!ok) {
		QMessageBox::critical(this,"Error", error);
		return;
	}
	//temp.im=dataTable->item(j+i*np,5)->text().toDouble(&ok);
	imag(temp,dataTable->item(j+i*np,5)->text().toDouble(&ok));
	if (!ok) {
		QMessageBox::critical(this,"Error", error);
		return;
	}
	mat(i,j)=temp;
	//p->data->set_odata(k,i,j,temp);
    }
	p->data->set_hdata(k,mat);
	}
	p->viewReal();
	close();
}

void DataTable::editCopy()
{
fromTableToClipboard(dataTable);
}

void DataTable::editPaste()
{
fromClipboardToTable(dataTable);
}

void DataTable::on_dataTable_customContextMenuRequested()
{
	QMenu menu;
	menu.addAction(editCopyAction);
	menu.addAction(editPasteAction);
	menu.exec(QCursor::pos());
}

void DataTable::on_dataTable_itemSelectionChanged()
{
	const int row=dataTable->currentRow();
	const size_t k=p->data->getActiveCurve();
	if (p->data->get_odata(k)->rows()<2){
		p->plot2D->leftbarpos=QPointF(p->data->get_x(k,row), p->data->get_value(k,0,row));
		p->plot2D->is_leftbar=true;
		p->plot2D->update();
	}
	else {
		int i=int(row/p->data->get_odata(k)->cols());
		int j=row-i*p->data->get_odata(k)->cols();
		p->plot3D->leftbarpos=QPointF(p->data->get_x(k,j), p->data->get_y(k,i));
		p->plot3D->is_leftbar=true;
		p->plot3D->update();
	}	
}
