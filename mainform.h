#ifndef MAINFORM_H_
#define MAINFORM_H_

#include <QVariant>
#include <QPixmap>
#include <QMainWindow>
#include <QActionGroup>
#include <QMenu>
//#include <QFtp>
#include <QSettings>
#include <QStatusBar>
#include "ui_mainform.h"
#include "processing.h"
#include "filefilters.h"
#include <QDirModel>
#include <QHeaderView>
#include <QTimer>
#include <QPrinter>
#include <QCompleter>
#include <QStringListModel>
//#include <QFileSystemWatcher>
#include <QDateTime>
#include "ui_modelessdialog.h"
#include "graphics_out.h"
#include "fitdialog.h"
#include "base.h"

// enum FT_MODE {FT_COMPLEX, FT_STATES, FT_TPPI};


struct proc_entry
{
public:
	proc_entry(BaseProcessingFunc* p, bool c, QString o) {pointer=p; checked=c; options=o;};
	~proc_entry() {};
	BaseProcessingFunc* pointer;
	bool checked;
	QString options;
};

//class QAction;
//class QActionGroup;
//class QToolBar;
//class QStatusBar;
//class QMenu;

//typedef void (MainForm::*vfpointer)();

//struct commandPair
//{
//	commandPair(QString n,vfpointer f) {name=n;function=f;};
//	QString name;
//	vfpointer function;
//};

class MainForm : public QMainWindow, public Ui::MainWindow
{
    Q_OBJECT
public:
     MainForm( QWidget* parent = 0, Qt::WindowFlags fl=0 );
    ~MainForm();

    //QStatusBar *statusBar;
    QMenuBar *menubar;
    QMenu *fileMenu;
    	QMenu *fileMoveToMenu;
    QMenu *editMenu;
//	QMenu *invertMenu;
    QMenu *viewMenu;
	QMenu* rulerMenu;
//	QMenu* windowsMenu;
    QMenu* analysisMenu;
    QMenu* procMenu;
    QMenu* windowsMenu;
//    QMenu* optionsMenu;
    QMenu *helpMenu;
    QToolBar *toolBar;
    QToolBar * plotToolBar;
    QToolBar * procToolBar;
    QAction* fileNewWindowAction;
    QAction* fileOpenAction;
//    QAction* fileFtpAction;
    QAction* fileSaveAction;
//    QAction* fileSaveDirAction;
    QAction* fileExportPdfAction;
    QAction* fileExportVectorAction;

    QAction* fileCloseAction;
    QAction* fileExitAction;
    QAction* filePrintAction;
    QAction* filePrintPreviewAction;
    QAction* fileReloadAction;
    QAction* fileWatchAction;

    QAction* editUndoAction;
    QAction* editCopyAction;
    QAction* takeInsetAction;
    QAction* pasteImageAction;
    QAction* editSpectralParsAction;
    QAction* editDataAction;

//    QAction* processBatchAction;
//    QAction* editInvertHorAction;
//    QAction* editInvertVertAction;
    QAction* editFixScalingsAction;
    QAction* helpAboutAction;
    QAction* helpAboutQtAction;
    QAction* helpHomeWebpageAction;
    QAction* helpDocAction;
    QAction* helpQuickstartAction;
    QAction* helpForumAction;

    QAction* viewRealAction;
    QAction* viewImagAction;
    QAction* viewPpmAction;
    QAction* viewHzAction;
    QAction* viewReferencingAction;
//    QAction* viewExtendAction;
    QAction* analysisDeconvAction;
    QAction* analysisArrayAction;
    QAction* analysisAdvFitAction;

    QAction* viewSetRangeAction;
    QAction* viewOptionsAction;
    QAction* viewWindowsAction; //treated separately from other actions

// Plot toolbar actions
    QAction* gridAction;
    QAction* hscaleAction;
    QAction* vscaleAction;
    QAction* moveAction;
    QAction* selectAction;
    QAction* removeMarkersAction;
    QAction* resetPlotAction;
    QAction* drawTextAction;
    QAction* drawLineAction;
    QAction* drawImageAction;

    commandLineWidget* commandLineEdit;
//    QStringListModel* commandList;
//    QCompleter* commandCompleter;

    QList<QAction*> procActions;
    void fileSave(QString&, QString, int flags=SaveUpdateFileName | SaveUpdateWorkingDirectory);
    QDirModel *dirModel;
    DataStore* data;
    Plot2DWidget* plot2D;
    Plot3DWidget* plot3D;
    QStackedWidget* plotarea;
    ModelessDialog* mlDialog; ///simple modeless dialog for different prompts
    QPrinter* printer; ///main system printer

//    bool is_power2(size_t);
//    double lp, rp, lp1, rp1; //zero order (rp's) and first order (lp's) phases in both dimensions
//    double xr, xr1; //reference points for lp's
    void update_info(int);
    QString infoString(const int );
//    QList<commandPair> commandTable;
//    void initCommandTable();
    void initAvailableProcList();
    void initFileFilterList();
    void initColourSchemeList();
    void updateProcTable();
    void closeEvent(QCloseEvent *);
    void readSettings();
    void writeSettings();
    QList<BaseProcessingFunc *> availableProcList;
    QList<BaseProcessingFunc *> selectedProcList;
    QList<BaseProcessingFunc *> storedProcList;
    QList<BaseFileFilter* > fileFilterList;
    QList<ColourScheme* > colourSchemeList;
    Plot2DWidget* activePlot();
    BaseProcessingFunc* findFunc(QString);
    QString working_directory; //contains working directory

    QTimer* watchTimer;
    QString watchFile;
    QDateTime watchLastChange;
//      QFileSystemWatcher* watcher;
    void add_integral_2D();
    void calibrate_integrals_2D();

public slots:
//    virtual void fileOpen(QString, FileFormat f=FFORMAT_AUTO);
    virtual void fileOpen(QString, QString f="Any known fileformats", int flags=OpenUpdateWorkingDirectory | OpenSetActive | OpenFullRestart|OpenAppend);
    virtual void fileOpenDialog();
//    virtual void fileFtpDialog();
    virtual void fileSaveDialog();
//    virtual void fileSaveDir();
    virtual void filePrint();
    virtual void filePrintPreview();
    virtual void createPrintPreview(QPrinter*);
    virtual void fileExit();
//    virtual void editCopy();
    virtual void windowsMenuCalled();

    virtual void helpAbout();
    virtual void helpAboutQt();
    virtual void helpHomeWebpage();
    virtual void helpDoc() {openDocumentation("README_GSIM.pdf");};
    virtual void helpQuickstart() {openDocumentation("quickstart.pdf");};
    virtual void openDocumentation(QString);
    virtual void helpForum();

    virtual void curveSelected(int);
    virtual void filesSelectionChanged();

virtual void on_sticksButton_clicked( );
virtual void on_whitewashButton_toggled(bool);

//virtual void on_gridButton_toggled( bool );
virtual void gridActiontoggled( bool );
virtual void on_contourButton_toggled(bool);
virtual void on_projectionsButton_toggled(bool);
virtual void on_smartSideviewsButton_toggled(bool);
virtual void on_addLegendButton_clicked();

//virtual void on_moveButton_clicked();
virtual void moveActiontriggered(bool);
//virtual void on_hscaleButton_clicked();
virtual void hscaleActiontriggered(bool);
// virtual void on_vscaleButton_clicked();
virtual void vscaleActiontriggered(bool);
//virtual void on_selectButton_clicked();
virtual void selectActiontriggered(bool);
//virtual void on_resetPlotButton_clicked();
virtual void resetPlotActiontriggered(bool);
//virtual void on_removeMarkersButton_clicked();
virtual void removeMarkersActiontriggered(bool);

// virtual void on_drawTextButton_clicked();
// virtual void on_drawLineButton_clicked();
// virtual void on_drawImageButton_clicked();
virtual void drawTextActiontriggered(bool);
virtual void drawLineActiontriggered(bool);
virtual void drawImageActiontriggered(bool);

//virtual void on_drawClearButton_clicked();

    /** Add integral */
    virtual void add_integral();
    virtual void on_calibrateButton_clicked();
    virtual void on_integralScaleButton_clicked();
    virtual void upIntegScaleButtonClicked();
    virtual void downIntegScaleButtonClicked();
    virtual void upIntegShiftButtonClicked();
    virtual void downIntegShiftButtonClicked();
    virtual void lvlChanged(double);
    virtual void tltChanged(double);
    virtual void lvltltChanged();
    virtual void on_ppButton_clicked();
    virtual void on_manppButton_clicked();
    virtual void on_delPpButton_clicked() {deleteAllPp(); activePlot()->update();};
    virtual void on_sidebandsButton_clicked();
    virtual void on_adjustScalesButton_clicked();
    virtual void on_snButton_clicked();
    double calcnoise(size_t ,size_t ,size_t);
    virtual void deleteAllPp();
    virtual void update2d();
    virtual void takeSlice();
    virtual void takeSlice(int);
//    virtual void tabChanged(int);
    virtual void fileNewWindow();
    virtual void windowActivated( QMdiSubWindow *);
    virtual void fileExportPdf();
    virtual void fileExportVector();

    virtual void fileClose(); //delete data and update all views
    virtual void fileDelete(const int); //only data is deleted, no views update
    virtual void fileReload();
    virtual void fileWatch(bool);
    virtual void fileMoveTo();
//    virtual void timerEvent();
    virtual void timerEvent();
    virtual void editUndo();
    virtual void resetAll2d();
    virtual void resetAll3d();
    virtual void callChangeInfo();
    virtual void delIntegralAsked();
    virtual void delIntegrals();
    virtual void delSelectedIntegral();
//    virtual void reverseData();
    virtual void nextSlice();
    virtual void previousSlice();
    virtual void changeSlice(size_t,int,bool);
    virtual void mult2dPressed();
    virtual void div2dPressed();
    virtual void scale2D(double);
    virtual void editSpectralPars();
    virtual void on_extractProcButton_clicked();
    virtual void editCopy();
    virtual void takeInset();
    virtual void pasteImage();
    virtual void editData();
    virtual void viewReal();
    virtual void viewImag();
    virtual void viewReferencing();
//    virtual void viewExtend();
    virtual void viewSetRange();
    virtual void analysisDeconv();
    virtual void analysisArray();
    virtual void analysisAdvFit();
    virtual void fitFinished(int);
    virtual void editOptions();
//    virtual void invertHor();
//    virtual void invertVert();
    virtual void toPpm();
    virtual void toHz();
//            void extrasToPPM(bool);
    double convertionFactor(int, int, double);
    void changeScales(int, int, int, int);
    void cycChangeXAxis();
    void cycChangeYAxis();

    virtual void fixScalings();
    virtual void updateFilesList();
    virtual void on_filesListWidget_itemChanged();
    virtual void on_filesListWidget_customContextMenuRequested();
//    virtual void batchProcessing();

//Processing function
 virtual void on_processButton_clicked();
 virtual void on_reloadProcessButton_clicked();
 virtual void on_editProcButton_clicked();
 virtual void on_saveProcButton_clicked();
 void saveProcFile(QString);
 virtual void on_loadProcButton_clicked();
 void loadProcFile(QString);
 void interpreteProcList(QStringList&);
 void readAttachedProcessing(QStringList&,QStringList&);
 void writeAttachedProcessing(QStringList&,QStringList&);
 void removeAttachedProcessing(QString&);
 void checkAttachedProcessing(QStringList& , QStringList&, QStringList&);
 void updateProcToolBar();
 virtual void disactivateControls(bool);
 virtual void on_dirTreeView_activated(const QModelIndex&);
 void commandLineEdit_returnPressed();


private:
    void init();
    void destroy();
    void Moments(List<double> &, double&, double &, double &, double &);
    double WidthHalfHeght(List<double> &);
    int findIntegral(size_t ,double );
    int findIntegral2D(size_t ,QPointF );
    QVector<QString> smoment(size_t, size_t, size_t);
    
    QMainWindow* datatable;
    QDialog* deconvform;
    QMainWindow* arraymanager;
    fitDialog* fit;
//    QStyle* mainstyle;
    void createPrintout(QPrinter* , Plot2DWidget* );
    QString interpreteVnmrAp(); ///interprete parameter 'ap' from Varian format
};

#endif
