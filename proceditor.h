#ifndef PROCEDITOR_H
#define PROCEDITOR_H

#include <QDialog>
#include <QToolBar>
#include "ui_proceditor.h"
#include "mainform.h"
#include "base.h"

class ProcEditor : public QDialog, public Ui::ProcEditor
{
    Q_OBJECT

public:
    ProcEditor( MainForm* parent = 0, Qt::WindowFlags fl = 0 );
    ~ProcEditor();

public slots:
  void on_upButton_clicked();
  void on_downButton_clicked();
  void on_addButton_clicked();
  void on_removeButton_clicked();
  void on_okButton_clicked();
  void on_cancelButton_clicked();
  void on_addToolButton_clicked();
  void on_delToolButton_clicked();
private:
  MainForm* p;
  DataStore* data;
};

#endif
