#!/usr/bin/python
# Filename : cat.py
import sys

##### Functions #####
def readfile(filename):
    '''Print a file to the standard output.'''
    counter=0
    f = file(filename)
    while True:
        line = f.readline()
        if len(line) == 0:
            break
        counter=counter+1 # The comma is to suppress additional newline.
    f.close()
    print 'File',
    print filename,
    print 'contains',
    print counter,
    print 'non-empty lines'
    return counter

##### Main #####
if len(sys.argv) < 2:
    print 'No action specified.'
    sys.exit()

if sys.argv[1].startswith('--'):
    option = sys.argv[1][2:]
    # Fetch sys.argv[1] and copy the string except for first two characters
    if option == 'version':
        print 'Version 1.00'
    elif option == 'help':
        print '''\
This program prints files to the standard output.
Any number of files can be specified.
Options include:
    --version : Prints the version number and exits
    --help    : Prints this help and exits'''
    else:
        print 'Unknown option.'
    sys.exit()
else:
    totcounter=0;
    for filename in sys.argv[1:]:
	totcounter=totcounter+readfile(filename)
    print 'Total number',
    print totcounter
