#include "plot2dwidget.h"
#include "version.h"
#include <QDebug>
#include <QInputDialog>
#include <QMessageBox>
#include <QTimer>
#include <QMenu>
#include <QColorDialog>
#include <QFontDialog>
#include <QMimeData>

//QColor colour[] = { QColor(0,0,102,255), QColor(0,102,0,255),QColor(102,0,102,255), QColor(0,128,128,255), QColor(153,0,0,255), QColor(214,0,147,255), QColor(0,204,0,255), QColor(255,102,0,255), QColor(255,0,102,255), QColor(0,204,0,255), QColor(0,128,128,255), QColor(51,153,255,255), QColor(51,51,0,255), QColor(102,102,153,255), QColor(255,51,153,255), QColor(0,0,255,255)};

/*
double first_decimal(double d)
{
	char buf[32];
	sprintf(buf, "%.0e", d);
	// code below helps to have a "meaningful" step, say excluding 3, 4, 7 
	if (buf[0]=='3') buf[0]='2';
	if (buf[0]=='4' || buf[0]=='6' ||buf[0]=='7' || buf[0]=='8') buf[0]='5';
	if (buf[0]=='9') { //difficult substitution to 10
		for (size_t i=30; i--;)
			buf[i+1]=buf[i];
		buf[0]='1';
		buf[1]='0';
	}
	d = atof(buf);
	return d;
}
*/

double first_decimal(double d)
{
	QString buf = QString("%1").arg(d,0,'e',0);
	// code below helps to have a "meaningful" step, say excluding 3, 4, 7 
	if (buf.at(0)=='3') buf[0]='2';
	if (buf.at(0)>='4' && buf.at(0)<='8') buf[0]='5';
	if (buf.at(0)=='9') { //substitution to 10
		buf[0]='0';
		buf.prepend('1');
	}
	return buf.toDouble();
}

#ifdef QT_OPENGL_LIB
Plot2DWidget::Plot2DWidget( QWidget *parent ): QGLWidget(parent)
#else
Plot2DWidget::Plot2DWidget( QWidget *parent ): QWidget(parent)
#endif
{
    xglobmax=-1e36;
    yglobmax=-1e36;
    xglobmin=1e36;
    yglobmin=1e36;
    xwinmin=1e36;
    ywinmin=1e36;
    xwinmax=-1e36;
    ywinmax=-1e36;
    point_old.setX(0);
    point_old.setY(0);
    is_leftbar=false;
    is_rightbar=false;
//    is_stick_mode=false;
    visualMode=SPECTRAASLINES;
    whitewash=false;
    is_xaxis_inverted=false;
    is_yaxis_inverted=false;
    for_print=false;
    move_state=MOVEALL;
    mousePressed=false;
    first_click=true;
    colourScheme=whiteScheme();
//    QPalette pal=palette();  //should be simpler but I don't know how to do that (yet)
//    pal.setColor(QPalette::Background, Qt::white);
//    setPalette(pal);
    xaxis_label=QString("");
    yaxis_label=QString("");
    data=NULL;
    is_xgrid=false; //draw always xgrid
    is_ygrid=false;
    labeloffset=0;
    setFocusPolicy(Qt::StrongFocus);
    grabbed_point=0;
    grabbed_object_index=-1;
    grabbed_object_dataset=-1;
	
	deleteGraphObjectAction = new QAction(this);
	deleteGraphObjectAction->setText("Delete");
	deleteGraphObjectAction->setIcon(QIcon::fromTheme("edit-delete"));
	connect(deleteGraphObjectAction, SIGNAL(triggered()),this,SLOT(deleteSelected()));
	changeColourAction =  new QAction(this);
	changeColourAction->setText("Colour");
	changeColourAction->setIcon(QIcon(":/images/colour.png"));
	connect(changeColourAction, SIGNAL(triggered()),this,SLOT(changeColour()));
	changeFontAction =  new QAction(this);
	changeFontAction->setText("Font");
	changeFontAction->setIcon(QIcon(":/images/text.png"));
	connect(changeFontAction, SIGNAL(triggered()),this,SLOT(changeFont()));
	changeAngleAction =  new QAction(this);
	changeAngleAction->setText("Orientation");
	changeAngleAction->setIcon(QIcon(":/images/text_direction.png"));
	connect(changeAngleAction, SIGNAL(triggered()),this,SLOT(changeAngle()));
	
    markerLabel1 = new QLabel(this);
    markerMovie1 = new QMovie(":/images/marker.mng");
    markerMovie1->setCacheMode(QMovie::CacheAll);
    markerLabel1->setMovie(markerMovie1);
    markerMovie1->setSpeed(100);
//    markerMovie1->start();
//     markerLabel1->show();
    markerLabel1->setVisible(false);

    markerLabel2 = new QLabel(this);
    markerMovie2 = new QMovie(":/images/marker.mng");
    markerMovie2->setCacheMode(QMovie::CacheAll);
    markerLabel2->setMovie(markerMovie2);
    markerMovie2->setSpeed(100);
//    markerMovie2->start();
//    markerLabel2->show();
//    markerLabel->setAutoFillBackground(true);
//    markerLabel->setText("text");
   markerLabel2->setVisible(false);
}

Plot2DWidget::~Plot2DWidget()
{
 //   delete picture;
}

void Plot2DWidget::set_global_range()
{
    xglobmin=9e36;
    yglobmin=9e36;
    xglobmax=-9e36;
    yglobmax=-9e36;
    for (size_t k=0; k<data->size(); k++)
    {
	if (!data->get_display(k)->isVisible)
		continue;
	if (!data->global_options.mixFidSpec)
		if (data->get_info(k)->type!=data->get_info(data->getActiveCurve())->type)
			continue;
//	info=data->get_info(k);
	size_t ni=data->get_odata(k)->rows();
	size_t np=data->get_odata(k)->cols();

// operation below is too slow, should be much faster
	if (ni<=1){
	    	double xgmincurve=data->get_x(k,0);
		double xgmaxcurve=data->get_x(k,np-1);
		if (xgmincurve>xgmaxcurve)
			std::swap(xgmincurve,xgmaxcurve);
		if (xglobmin>xgmincurve)
			xglobmin=xgmincurve;
		if (xglobmax<xgmaxcurve)
			xglobmax=xgmaxcurve;
		for (size_t i=np; i--;) {
	    		if (data->get_value(k,0,i)<yglobmin) yglobmin=data->get_value(k,0,i);
	    		if (data->get_value(k,0,i)>yglobmax) yglobmax=data->get_value(k,0,i);
		}
    	}
   }	
}

void Plot2DWidget::set_window_range(double xmin, double xmax, double ymin, double ymax)
{
    xwinmin=xmin;
    xwinmax=xmax;
    ywinmin=ymin;
    ywinmax=ymax;
    if (ywinmin==ywinmax) {
	ywinmin-=5.0;
	ywinmax+=5.0;
	} // it saves from crash if all data is zeros
    xrange=xwinmax-xwinmin;
    yrange=ywinmax-ywinmin;
    xppt=xrange/(width()-1);
    yppt=yrange/(height()-1);
}

void Plot2DWidget::hot_restart()
{
	init_settings();
	set_global_range();
	double xglobmax_here=(is_xaxis_inverted)? xglobmax+(xglobmax-xglobmin)*0.1 : xglobmax;
	double xglobmin_here=(is_xaxis_inverted)?  xglobmin : xglobmin-(xglobmax-xglobmin)*0.1;
	set_window_range(xglobmin_here, xglobmax_here, yglobmin-(yglobmax-yglobmin)*0.1,yglobmax+(yglobmax-yglobmin)*0.1);
	update();
}

void Plot2DWidget::cold_restart(DataStore* p)
{
    if (!p) cerr<<"Hot_restart:data is empty\n";
    data=p;
    if (!p->size()) {
	update();
	return;
    }
    is_leftbar=false;
    is_rightbar=false;
    markerMovie1->stop();
    markerMovie2->stop();
    markerLabel1->hide();
    markerLabel2->hide();
    hot_restart();
}

void Plot2DWidget::Update()
{
	update();
}

void Plot2DWidget::fromreal_toscreen (double x, double y, QPointF & point)
{
    if (is_xaxis_inverted)
	 point.setX(width()-(x-xwinmin)/xppt);
    else 
	point.setX((x-xwinmin)/xppt);
    point.setY(height()-(y-ywinmin)/yppt);
}

template <class T> void Plot2DWidget::fromreal_toscreen (double x, double y, T & point, size_t k)
{
    const Display_* display=data->get_display(k);
    if (is_xaxis_inverted)
        point.setX(width()-(((x*display->xscale-xwinmin)/xppt)+display->xshift/xppt));
    else
	point.setX(((x*display->xscale-xwinmin)/xppt)+display->xshift/xppt);
    if (is_yaxis_inverted)
	point.setY(((y*display->vscale-ywinmin)/yppt)+display->vshift/yppt);
    else
    	point.setY(height()-(((y*display->vscale-ywinmin)/yppt)+display->vshift/yppt));
}

void Plot2DWidget::fromreal_toscreen (const size_t startx, const size_t endx, QPolygon& polygon,const size_t k)
{
//	polygon.reserve(endx-startx+1);
	polygon.clear();
	const Display_* display=data->get_display(k);
	double xslope, xshift, yslope, yshift;
	if (is_xaxis_inverted) {
		xslope=(-1.0*display->xscale)/xppt;
		xshift=width()+(xwinmin-display->xshift)/xppt;
	}
	else {
		xslope=display->xscale/xppt;
		xshift=(display->xshift-xwinmin)/xppt;
	}
	yslope=(-1.0*display->vscale)/yppt;
	yshift=height()+(ywinmin-display->vshift)/yppt;

	int step=lround(fabs(xppt/display->xscale/(data->get_x(k,0)-data->get_x(k,1))));
	if (step<1)
		step=1;
	if (step==1)
		for (size_t i=startx; i<=endx; i+=step) 
			polygon<<QPoint(int(data->get_x(k,i)*xslope+xshift),int(data->get_value(k,0,i)*yslope+yshift));
	else {
		for (size_t i=startx; i<=endx; i+=step) {
			//List<double> tl;
			double maxr=-9e38;
			double  minr=9e38;
			int imax=-1, imin=-1;
			for (size_t j=0; j<step; j++) {
				if (i+j<=endx) {
					if (data->get_value(k,0,i+j)>maxr) {
						maxr=data->get_value(k,0,i+j);
						imax=i+j;
					}
					if (data->get_value(k,0,i+j)<minr) {
						minr=data->get_value(k,0,i+j);
						imin=i+j;
					}
				//	tl.push_back(data->get_value(k,0,i+j));
				}
			}

			int maxscr=int(maxr*yslope+yshift);
			int minscr=int(minr*yslope+yshift);
			int xscr=int(data->get_x(k,i)*xslope+xshift);

			if (maxscr==minscr)
				polygon<<QPoint(xscr,minscr);
			else if (imax>imin) {
				polygon<<QPoint(xscr,minscr);
				polygon<<QPoint(xscr,maxscr);
			}
			else {
				polygon<<QPoint(xscr,maxscr);
				polygon<<QPoint(xscr,minscr);
			}

		}
	}
}

void Plot2DWidget::fromreal_toscreen (const size_t startx, const size_t endx, QPolygonF& polygon,const size_t k)
{
	polygon.reserve(endx-startx+1);
	const Display_* display=data->get_display(k);
	double xslope, xshift, yslope, yshift;
	if (is_xaxis_inverted) {
		xslope=(-1.0*display->xscale)/xppt;
		xshift=width()+(xwinmin-display->xshift)/xppt;
	}
	else {
		xslope=display->xscale/xppt;
		xshift=(display->xshift-xwinmin)/xppt;
	}
	yslope=(-1.0*display->vscale)/yppt;
	yshift=height()+(ywinmin-display->vshift)/yppt;

	for (size_t i=startx; i<=endx; i++)
		polygon<<QPointF(data->get_x(k,i)*xslope+xshift,data->get_value(k,0,i)*yslope+yshift);

}


void Plot2DWidget::fromscreen_toreal (QPoint point, double& x, double& y)
{
    x= (is_xaxis_inverted)? (width()-point.x())*xppt+xwinmin : point.x()*xppt+xwinmin;
    y=(is_yaxis_inverted)? point.y()*yppt+ywinmin : (height()-point.y())*yppt+ywinmin;
//qDebug()<<"Fromscreen_toreal (no k)"<<x<<y;
}

void Plot2DWidget::fromscreen_toreal (QPoint point, double& x, double& y, int k)
{
    const Display_* display=data->get_display(k);	
    x= (is_xaxis_inverted)? ((width()-point.x())*xppt+xwinmin-display->xshift)/display->xscale : (point.x()*xppt+xwinmin-display->xshift)/display->xscale;
    y=(is_yaxis_inverted)? (point.y()*yppt+ywinmin-display->vshift)/display->vscale : ((height()-point.y())*yppt+ywinmin-display->vshift)/display->vscale;
}

void Plot2DWidget::paintEvent( QPaintEvent * )
{
    if (!data->mutex.tryLock())
	return;
    QPainter* paint = new QPainter();
    if (!paint) cerr<<"paintEvent: Cannot create painter\n";
    paint->begin( this );          // paint in the widget
    paint->setBrush(Qt::SolidPattern);
    if (data->global_options.antialiasing && (!mousePressed))
	paint->setRenderHints(paint->renderHints() | QPainter::Antialiasing);
    draw_picture(paint);  // draw picture

    if (!selectionRect.isNull()) {
//Should be taken from colour scheme
	paint->setPen(colourScheme.rectBorderColour);
	paint->setBrush(colourScheme.rectFillColour);
	paint->drawRect(selectionRect);
    }

    paint->end();                       // painting done
    delete paint;
    data->mutex.unlock();
//    plotmutex.unlock();
}

void Plot2DWidget::resizeEvent (QResizeEvent *)
{
    set_window_range(xwinmin, xwinmax, ywinmin, ywinmax);
    update();
}

void  Plot2DWidget::mousePressEvent( QMouseEvent * e)
{
    no_mousemove=true;
    mousePressed=true;
    point_old.setX(e->x());
    point_old.setY(e->y());
    action=e->button();
    if (first_click) //a series of mousePressEvents occurs continuosly if someone holds the mouse button pressed - difficult to find the initial mouse position
	start_point=e->pos();
    first_click=false;
    if (move_state==GRAPHSELECTION) {
		double x,y;
		int ind;
//		const size_t k=data->getActiveCurve();
		foreach (int k, data->selectedCurves) {
			fromscreen_toreal(e->pos(),x,y,k);
			graph_object* gr=find_graph_object(x,y, k, &ind);
//			if (gr==NULL)
//				unselectAll();
			if (gr!=NULL) {
				grabbed_object_index=ind;
				grabbed_object_dataset=k;
//				cout<<"object found underneath\n";
				if (!gr->selected) {
					if (e->modifiers()!=Qt::ShiftModifier) {
						unselectAll();
						gr=find_graph_object(x,y,k, &ind);
						}
//					cout<<"object has been selected\n";
					gr->selected=true;
				}
				if (gr->type==GRAPHLINE && gr->selected) {
//					cout<<"Check grabbing\n";
					QLineF lll=gr->pos.toLineF();
					QPoint sp1, sp2;
					fromreal_toscreen(lll.p1().x(),lll.p1().y(),sp1,k);
					fromreal_toscreen(lll.p2().x(),lll.p2().y(),sp2,k);
					if (QLineF(point_old,sp1).length()<5) {
//						cout<<"Point 1 grabbed\n";
						grabbed_object_index=ind;
						grabbed_object_dataset=k;
						grabbed_point=1;
					}
					else if (QLineF(point_old,sp2).length()<5) {
//						cout<<"Point 2 grabbed\n";
						grabbed_object_index=ind;
						grabbed_object_dataset=k;
						grabbed_point=2;
					}
				}
				else if ((gr->type==GRAPHIMAGE || gr->type==GRAPHVPICTURE) && gr->selected) {
					QPointF ppp=gr->pos.toPointF();
					QPoint sp1;
					fromreal_toscreen(ppp.x(),ppp.y(),sp1,k);
					QSize size;
					if (gr->type==GRAPHIMAGE)
						size=gr->maindata.value<QPixmap>().size()*gr->scale;
					else if (gr->type==GRAPHVPICTURE) {
						QPicture pic;
						pic.setData(gr->maindata.toByteArray().data(),gr->maindata.toByteArray().size());
						size=pic.boundingRect().size()*gr->scale;
					}
					QPoint sp2=sp1+QPoint(size.width(),0);
					QPoint sp3=sp1+QPoint(0,size.height());
					QPoint sp4=sp1+QPoint(size.width(),size.height());
					if (QLineF(sp1,point_old).length()<9) {
						grabbed_object_index=ind;
						grabbed_object_dataset=k;
						grabbed_point=1;
					}
					else if (QLineF(sp2,point_old).length()<9) {
						grabbed_object_index=ind;
						grabbed_object_dataset=k;
						grabbed_point=2;
					}
					else if (QLineF(sp3,point_old).length()<9) {
						grabbed_object_index=ind;
						grabbed_object_dataset=k;
						grabbed_point=3;
					}
					else if (QLineF(sp4,point_old).length()<9) {
						grabbed_object_index=ind;
						grabbed_object_dataset=k;
						grabbed_point=4;
					}
				}
			if (e->button()==Qt::RightButton){//show context menu if right button clickrd
				QMenu menu;
				menu.addAction(deleteGraphObjectAction);
                                if (!selectedGraphContainsImageOnly()) //you cannot change colour for bitmaps
                                    menu.addAction(changeColourAction);
				if (selectedGraphContainsText()) {
					menu.addAction(changeFontAction);
					menu.addAction(changeAngleAction);
					}
				menu.exec(QCursor::pos());
			}
			update();
			return;
			}
		unselectAll();
		}
	}
}

void  Plot2DWidget::mouseReleaseEvent( QMouseEvent * e)
{
    mousePressed=false;
    first_click=true;
    if (no_mousemove){
	if (xlabelrect.contains(e->pos())) {
		emit onXaxisLabelClicked();
		return;
	}
	else if (ylabelrect.contains(e->pos())) {
		emit onYaxisLabelClicked();
		return;
	}
	if (move_state!=GRAPHSELECTION) {
		double x,y;
		fromscreen_toreal(e->pos(),x,y);
		if (action==Qt::LeftButton) {
	    		set_leftbar(x,y);
			emit leftbarSet();
			}
		else if (action==Qt::RightButton) {
	    		set_rightbar(x,y);
			emit rightbarSet();
			}
	}
	update();
    }
    else { //after mouse move needs to remove selection rectangle
		selectionRect=QRect(0,0,0,0);
		update();
	}
	grabbed_point=0;
	grabbed_object_index=-1;
	grabbed_object_dataset=-1;
}

void Plot2DWidget::mouseMoveEvent (QMouseEvent *e)
{
    if (!data->size())
		return;
    no_mousemove=false;
    bool somethinghappens=false;
    double xf=xppt*(point_old.x()-e->x());
    double yf=yppt*(point_old.y()-e->y());

//    size_t k=data->getActiveCurve();
//    Display_ display=*(data->get_display(k));

switch (move_state) {
case VERTSCALING:  //vertical scalings
	if (e->modifiers()!=Qt::ShiftModifier)
	foreach (int k, data->selectedCurves) {
		Display_ display=*(data->get_display(k));
		if (action==Qt::RightButton) { 
			display.vshift=display.vshift-(2*is_yaxis_inverted-1)*yf;
		}
		if (action==Qt::LeftButton){
			display.vscale=display.vscale*(1-(2*is_yaxis_inverted-1)*yf/yrange);
		}
		data->set_display(k, display);
	}
	else //Alt is pressed
	for (size_t i=0; i<data->selectedCurves.size(); i++) {
		size_t k=data->selectedCurves.at(i);
		Display_ display=*(data->get_display(k));
		if (action==Qt::RightButton) { 
			display.vshift=display.vshift-(2*is_yaxis_inverted-1)*yf*double(data->selectedCurves.size()-1-i)/double(data->selectedCurves.size()-1);
		}
		data->set_display(k, display);
	}
break;
case HORSCALING: //horizontal scaling
	if (e->modifiers()!=Qt::ShiftModifier)
	foreach (int k, data->selectedCurves) {
		Display_ display=*(data->get_display(k));
		if (action==Qt::RightButton) {
			display.xshift=display.xshift+(2*is_xaxis_inverted-1)*xf;
		}
		if (action==Qt::LeftButton){
			display.xscale=display.xscale*(1+(2*is_xaxis_inverted-1)*xf/xrange);
		}
		data->set_display(k,display);
	}
	else 
	for (size_t i=0; i<data->selectedCurves.size(); i++) {
		size_t k=data->selectedCurves.at(i);
		Display_ display=*(data->get_display(k));
		if (action==Qt::RightButton) {
			display.xshift=display.xshift+(2*is_xaxis_inverted-1)*xf*double(data->selectedCurves.size()-1-i)/double(data->selectedCurves.size()-1);
		}
		data->set_display(k, display);
	}
break;
case MOVEALL: //simple moving
{
    if (action==Qt::LeftButton) { //Left button, no modifier : scaling
	double ratioX=(double(start_point.x())/double(width()));
	double ratioY=(double(start_point.y())/double(height()));
	if (!is_xaxis_inverted)
		ratioX=1-ratioX;
	if (!is_yaxis_inverted)
		ratioY=1-ratioY;
	set_window_range(xwinmin-2*(1-ratioX)*xf, xwinmax+2*ratioX*xf, ywinmin+2*ratioY*yf, ywinmax-2*(1-ratioY)*yf);
    }
    else if (action==Qt::RightButton) //Right button, no modifier : moving
		set_window_range(xwinmin-(2*is_xaxis_inverted-1)*xf, xwinmax-(2*is_xaxis_inverted-1)*xf, ywinmin+(2*is_yaxis_inverted-1)*yf, ywinmax+(2*is_yaxis_inverted-1)*yf);//shift
}break;
case GRAPHSELECTION:
//	size_t k=data->getActiveCurve();
	foreach (int k, data->selectedCurves) {
		double x,y;
		fromscreen_toreal(e->pos(),x,y);
		graph_object* gr=find_graph_object(x,y,k);
		if (grabbed_point && (grabbed_object_dataset==k)) {
//		cout<<"AQ point is grabbed\n";
			somethinghappens=true;
			gr=data->get_graphobject(k,grabbed_object_index);
			if (gr->type==GRAPHLINE) {
//				cout<<"Grabbed point is from line\n";
				QLineF lll=gr->pos.toLineF();
				fromscreen_toreal(e->pos(),x,y,k);
				if (grabbed_point==1) {
					lll=QLineF(QPointF(x,y),lll.p2());
				}
				else if (grabbed_point==2) {
					lll=QLineF(lll.p1(),QPointF(x,y));
				}
				gr->pos=lll;
				}
			else if (gr->type==GRAPHIMAGE && gr->selected) {
//				cout<<"Grabbed point is from image\n";
				double dif;
				QPointF p=gr->pos.toPointF();
				QSize size=gr->scale*gr->maindata.value<QPixmap>().size();
				int shiftx=e->x()-point_old.x();
				int shifty=e->y()-point_old.y();
				if (grabbed_point==1) { //top left corner
					dif=(fabs(shifty)>fabs(shiftx))? 1.0-double(shifty)/double(size.height()) : 1-double(shiftx)/double(size.width());
					QPointF sp;
					fromreal_toscreen(p.x(),p.y(),sp,k);
//					qDebug()<<"before"<<sp;
					QPointF ep=sp+QPoint(size.width(),size.height()); //this point should be fixed
					sp=ep-dif*QPoint(size.width(),size.height()); //new starting point
//					qDebug()<<"after"<<sp;
					fromscreen_toreal(sp.toPoint(),x,y,k);
					gr->pos=QPointF(x,y);
					gr->scale*=dif;
				}
				else if (grabbed_point==2) {
					dif=(fabs(shifty)>fabs(shiftx))? 1.0-double(shifty)/double(size.height()) : 1+double(shiftx)/double(size.width());
					QPointF sp;
					fromreal_toscreen(p.x(),p.y(),sp,k);
//					qDebug()<<"before"<<sp;
					QPointF ep=sp+QPoint(0,size.height()); //this point should be fixed
					sp=ep-dif*QPoint(0,size.height()); //new starting point
//					qDebug()<<"after"<<sp;
					fromscreen_toreal(sp.toPoint(),x,y,k);
					gr->pos=QPointF(x,y);
					gr->scale*=dif;
				}
				else if (grabbed_point==3) { //top left corner
					dif=(fabs(shifty)>fabs(shiftx))? 1.0+double(shifty)/double(size.height()) :	 1-double(shiftx)/double(size.width());
					QPointF sp;
					fromreal_toscreen(p.x(),p.y(),sp,k);
//					qDebug()<<"before"<<sp;
					QPointF ep=sp+QPoint(size.width(),0); //this point should be fixed
					sp=ep-dif*QPoint(size.width(),0); //new starting point
//					qDebug()<<"after"<<sp;
					fromscreen_toreal(sp.toPoint(),x,y,k);
					gr->pos=QPointF(x,y);
					gr->scale*=dif;
				}
				else if (grabbed_point==4) {
					dif=(fabs(shifty)>fabs(shiftx))? 1.0+double(shifty)/double(size.height()) : 1+double(shiftx)/double(size.width());
					gr->scale*=dif;
				}
			}
//		return;
		}
		else if ((grabbed_object_index>=0)&&(grabbed_object_dataset==k) ) {
			somethinghappens=true;
			moveSelected(xf,yf);
//			return;
		}
	}
if (!somethinghappens) {
	selectionRect=QRect(point_old,e->pos());
	if (!selectionRect.isValid())
	selectionRect=selectionRect.normalized();
	selectByRectangle();
	update();
	return;
}
//here should be code to select and manipulate graphics
break;
}
    point_old.setX(e->x());
    point_old.setY(e->y());
    update();
}

void Plot2DWidget::wheelEvent(QWheelEvent *event)
{
	int numDegrees = event->delta() / 8;
        int numSteps = numDegrees / 15;
        double coeff=0.98;
        if (numSteps<0){
            numSteps=abs(numSteps);
            coeff=1.0/coeff;
        }
        double yf=coeff;
        for (int i=0; i<numSteps; i++)
            yf*=coeff;
        set_window_range(xwinmin, xwinmax, ywinmin*yf, ywinmax*yf);
        update();
}


void Plot2DWidget::selectByRectangle()
{
	unselectAll();
	foreach (int k, data->selectedCurves) {
		QList<graph_object> graphics=data->get_graphics(k);
		for (size_t i=0; i<graphics.size(); i++) {
			if (graphics.at(i).type==GRAPHLINE) {
				QLineF l=graphics.at(i).pos.toLineF();
				QPoint sp1,sp2;
				fromreal_toscreen(l.p1().x(),l.p1().y(),sp1,k);
				fromreal_toscreen(l.p2().x(),l.p2().y(),sp2,k);
				if (selectionRect.contains(sp1) && selectionRect.contains(sp2))
					graphics[i].selected=true;
			}
			else {
				QPointF p=graphics.at(i).pos.toPointF();
				QPoint sp;
				fromreal_toscreen(p.x(),p.y(),sp,k);
				if (selectionRect.contains(sp)) {
					graphics[i].selected=true;
				}
			}
		}
		data->set_graphics(k,graphics);
	}
}

void Plot2DWidget::moveSelected(double xf, double yf)
{
	foreach (int k, data->selectedCurves) {
		QList<graph_object> graphics=data->get_graphics(k);
		for (size_t i=0; i<graphics.size(); i++) {
			if (graphics.at(i).selected) {
				if (graphics.at(i).type==GRAPHLINE){
					QLineF lll=graphics.at(i).pos.toLineF();
					QLineF lll2;
					lll2=QLineF(lll.p1()+QPointF((2*is_xaxis_inverted-1)*xf,-(2*is_yaxis_inverted-1)*yf), lll.p2()+QPointF((2*is_xaxis_inverted-1)*xf,-(2*is_yaxis_inverted-1)*yf));
					graphics[i].pos=lll2;
				}
				else {
					QPointF ppp=graphics.at(i).pos.toPointF();
					ppp+=QPointF((2*is_xaxis_inverted-1)*xf,-(2*is_yaxis_inverted-1)*yf);
					graphics[i].pos=ppp;
				}
			}
		}
		data->set_graphics(k,graphics);
	}
}

void Plot2DWidget::deleteSelected()
{
	foreach (int k, data->selectedCurves) {
		QList<graph_object> g_old=data->get_graphics(k);
		QList<graph_object> g_new;
		for (size_t i=0; i<g_old.size(); i++)
			if (!g_old.at(i).selected)
				g_new.append(g_old[i]);
		data->set_graphics(k,g_new);
	}
	update();
}

void Plot2DWidget::changeColour()
{
	QColor colour=QColorDialog::getColor(Qt::blue,this);
	foreach (int k, data->selectedCurves) {
		QList<graph_object> gr=data->get_graphics(k);
		for (size_t i=0; i<gr.size(); i++)
			if (gr.at(i).selected)
				gr[i].colour=colour;
		data->set_graphics(k,gr);
	}
	update();
}

 bool Plot2DWidget::selectedGraphContainsText()
 {
	bool result=false;
	foreach (int k, data->selectedCurves) {
		QList<graph_object> gr=data->get_graphics(k);
		for (size_t i=0; i<gr.size(); i++)
			if (gr.at(i).type==GRAPHTEXT && gr.at(i).selected)
				result=true;
	}
	return result;
 }

 bool Plot2DWidget::selectedGraphContainsImageOnly()
 {
        bool result=true;
        foreach(int k, data->selectedCurves) {
                QList<graph_object> gr=data->get_graphics(k);
                for (size_t i=0; i<gr.size(); i++)
                    if (gr.at(i).type!=GRAPHIMAGE && gr.at(i).selected)
                                result=false;
        }
        return result;
 }

 void Plot2DWidget::changeFont()
 {
	bool ok;
	QFont f=QFontDialog::getFont(&ok,font(),this);
 	QString font=f.toString();
	if (!ok) 
		return;
	foreach (int k, data->selectedCurves) {
		QList<graph_object> gr=data->get_graphics(k);
		for (size_t i=0; i<gr.size(); i++)
			if (gr.at(i).selected && gr.at(i).type==GRAPHTEXT)
				gr[i].font=font;
		data->set_graphics(k,gr);
	}
	update();
 }

 void Plot2DWidget::changeAngle()
 {
	bool ok;
	double angle=QInputDialog::getDouble(this,"Set orientation","Clockwise angle:",0.0,0.0,360.0,1,&ok);
	if (!ok) 
		return;
	foreach (int k, data->selectedCurves) {
		QList<graph_object> gr=data->get_graphics(k);
		for (size_t i=0; i<gr.size(); i++)
			if (gr.at(i).selected && gr.at(i).type==GRAPHTEXT)
				gr[i].angle=angle;
		data->set_graphics(k,gr);
	}
	update();
 }

 
void 	Plot2DWidget::mouseDoubleClickEvent (QMouseEvent *e)
{
	if (!data->size())
		return;
//	size_t k=data->getActiveCurve();
	foreach (int k, data->selectedCurves) {
		if (move_state==GRAPHSELECTION) {
			double x,y;
			fromscreen_toreal(e->pos(),x,y,k);
			int i=-1;
			find_graph_object(x,y,k,&i);
			if (i<0)
				continue;
			QList<graph_object> g=data->get_graphics(k);
			if (g.at(i).type==GRAPHTEXT) {
				bool ok;
				QString newtext=QInputDialog::getText(this, "Edit label", "Text:",QLineEdit::Normal, g.at(i).maindata.toString(),&ok);
				if (!ok)
					return;
				g[i].maindata=newtext;
				data->set_graphics(k,g);
			}
		}
	}
	update();
}

void 	Plot2DWidget::keyPressEvent ( QKeyEvent * e)
{
	if (move_state!=GRAPHSELECTION) {
		if (e->key()==Qt::Key_Y) {
			data->global_options.hasYaxis=!data->global_options.hasYaxis;
			update();
			}
		else if (e->key()==Qt::Key_X) {
			data->global_options.hasXaxis=!data->global_options.hasXaxis;
			update();
		}
		else if (e->key()==Qt::Key_B) {
			data->global_options.useBold1D=!data->global_options.useBold1D;
			update();
		}
		else
			e->ignore();
	}
	else {
		if (e->key()==Qt::Key_Up) 
			moveSelected(0,yppt);
		else if (e->key()==Qt::Key_Down)
			moveSelected(0,-yppt);
		else if (e->key()==Qt::Key_Left)
			moveSelected(xppt,0);
		else if (e->key()==Qt::Key_Right)
			moveSelected(-xppt,0);
		else if (e->key()==Qt::Key_Delete || e->key()==Qt::Key_Backspace)
			deleteSelected();
		else
			e->ignore();
		update();
	}
}

void Plot2DWidget::draw_axis(QPainter * paint)
{
if (!data->global_options.hasXaxis)
	return;

int extraoffset=3;

hoffset=AXOFFSET;
bool leftlabels=data->global_options.yLeftLabels; //draw labels at the left of the y-axis
if (leftlabels) {
	const double labelspace=fontMetrics().boundingRect(QString("8888888")).width();
	hoffset+=labelspace+TICSLEN;
}

QPen pen = paint->pen();
pen.setColor(colourScheme.axisColour);
pen.setStyle(Qt::SolidLine);
if (for_print)
	pen.setWidth(data->global_options.printLineWidth);
QFont f;
f.fromString(data->global_options.axisFont);
paint->setFont(f);
paint->setPen(pen);


double yaxisline=height()-TICSLEN-fontMetrics().height()-extraoffset;

double startxat=0.0;
if (data->global_options.hasYaxis)
	startxat+=hoffset;

paint->drawLine(QLineF(startxat,yaxisline,width()-labeloffset,yaxisline));

QPoint pp(width()-paint->fontMetrics().width(xaxis_label)-4-labeloffset, height()-extraoffset);
paint->drawText(pp, xaxis_label);
QRect labelRect=fontMetrics().boundingRect(xaxis_label);//save rectangle for the label;
labelRect.moveBottomLeft(pp);

double step=first_decimal(xrange/data->global_options.ticks);
double firstlabel= int(xwinmin/step)*step;
double lastlabel= (int(xwinmax/step)+1)*step;
//qDebug()<<"axis points"<<step<<firstlabel<<lastlabel;
QPointF p;
if (step<0) {
	std::swap(firstlabel,lastlabel);
	step=-step;
}
	
for (double i=firstlabel; i<=lastlabel; i=i+step)
{
    QPointF p1, p2;
    if (fabs(i/step)<1e-10) 
	i=0; //fix rounding error around zero
    fromreal_toscreen(i, 0, p);

	p1.setX(p.x());
	p1.setY(yaxisline);
	p2.setX(p.x());
	if (!is_xgrid)
    		p2.setY(yaxisline+TICSLEN);
	else {
		p2.setY(0);
		pen.setStyle(Qt::DotLine);
		paint->setPen(pen);
	}
    if (p.x()>startxat && p.x()<width()) {
	paint->drawLine(p1,p2);
    }
		if (data->global_options.hasExtraTicks) {
			size_t n=data->global_options.extraTicks;//number of extra tics
			pen.setStyle(Qt::SolidLine);
			paint->setPen(pen);
			double sstep=step/n;
			if (i==firstlabel) //creating minor ticks bakward 
			for (size_t j=0; j<n; j++) {
				fromreal_toscreen(i-sstep*j,0, p1);
				p1.setY(yaxisline);
				if (p1.x()>startxat && p1.x()<width())
					paint->drawLine(p1,p1+QPointF(0.0, 2.0));
			}	
			for (size_t j=0; j<n; j++) {
				fromreal_toscreen(i+sstep*j,0, p1);
				p1.setY(yaxisline);
				if (p1.x()>startxat && p1.x()<width())
					paint->drawLine(p1,p1+QPointF(0.0, 2.0));
			}
		}
    QString lab= QString("%1").arg(i,0, 'g', -1);
    QPoint pp(int(p.x()-paint->fontMetrics().width(lab,-1)/2), height()-extraoffset);
    if ((pp.x()+fontMetrics().width(lab)+2<labelRect.left()) && (pp.x()+fontMetrics().width(lab)/2.0 > hoffset*data->global_options.hasYaxis))
    	paint->drawText(pp,lab);
    xlabelrect=labelRect;
}
}


/*
void Plot2DWidget::draw_yaxis(QPainter * paint)
{
if (!data->global_options.hasYaxis)
	return;
	QSize win=size();
	QPen pen=paint->pen();
	pen.setColor(colourScheme.axisColour);
	if (for_print)
		pen.setWidth(data->global_options.printLineWidth);
	paint->setPen(pen);
	QFont f;
	f.fromString(data->global_options.axisFont);
	paint->setFont(f);
	int length=height();
	if (data->global_options.hasXaxis)
		length-=TICSLEN+fontMetrics().height()+3;//should be synchronised with X-axis position (better way?)
	QLine line(AXOFFSET, 0, AXOFFSET, length);
	paint->drawLine(line);
	double step=first_decimal(yrange/data->global_options.ticks);
	double firstlabel= int(ywinmin/step)*step;
	double lastlabel= int(ywinmax/step)*step;
	QPointF p;
	for (double i=firstlabel; i<=lastlabel; i=i+step)
	{
		QPointF p1,p2;
    		if (fabs(i/step)<1e-10) 
			i=0; //fix rounding error around zero
		fromreal_toscreen(0,i, p);
		p1.setY(p.y());
		p1.setX(AXOFFSET);
		p2.setY(p.y());
		if (!is_ygrid)
			p2.setX(AXOFFSET+TICSLEN);
		else{
			p2.setX(width());
			pen.setStyle(Qt::DotLine);
			paint->setPen(pen);
		}
		if (p1.y()<length) //dont paint tick outside the axis
			paint->drawLine(p1,p2);
		if (data->global_options.hasExtraTicks) {
			size_t n=data->global_options.extraTicks;
			pen.setStyle(Qt::SolidLine);
			paint->setPen(pen);
			double sstep=step/n;
			if (i==firstlabel)
			for (size_t j=0; j<n; j++) {
				fromreal_toscreen(0,i-sstep*j, p1);
				p1.setX(AXOFFSET);
				if (p1.y()<length) //dont paint tick outside the axis
					paint->drawLine(p1,p1+QPointF(2.0, 0.0));
			}
			for (size_t j=0; j<n; j++) {
				fromreal_toscreen(0,i+sstep*j, p1);
				p1.setX(AXOFFSET);
				if (p1.y()<length) //dont paint tick outside the axis
					paint->drawLine(p1,p1+QPointF(2.0, 0.0));
			}
		}	
		QString lab= QString("%1").arg(i,0, 'g', -1);
//		if (lab.contains("e")) {
//			lab.remove("+0");
//			lab.remove("+");
//			lab.replace("e",QString("x10<sup>"));
//			lab.append(QString("<\\sup>"));
//		}
		QPoint pp(AXOFFSET+TICSLEN+2,int(p.y()+paint->fontMetrics().height()/2.5));
		if (willMarkBePrinted(pp))
			paint->drawText(pp,lab); //save space for axis label at the top and x-axis labels at the bottom
	}
	paint->drawText(QPoint(AXOFFSET+TICSLEN+2, paint->fontMetrics().height()+labeloffset), yaxis_label);
	if (yaxis_label.isEmpty())
		ylabelrect=QRect(0,0,-1,-1);
	else {
		ylabelrect=fontMetrics().boundingRect(yaxis_label);
		ylabelrect.moveBottomLeft(QPoint(AXOFFSET+TICSLEN+2, paint->fontMetrics().height()+labeloffset));
	}
} */

void Plot2DWidget::draw_yaxis(QPainter * paint)
{
if (!data->global_options.hasYaxis)
	return;
	QSize win=size();
	QPen pen=paint->pen();
	pen.setColor(colourScheme.axisColour);
	if (for_print)
		pen.setWidth(data->global_options.printLineWidth);
	paint->setPen(pen);
	QFont f;
	f.fromString(data->global_options.axisFont);
	paint->setFont(f);
	int length=height();
	if (data->global_options.hasXaxis)
		length-=TICSLEN+fontMetrics().height()+3;//should be synchronised with X-axis position (better way?)

	hoffset=AXOFFSET;
	bool leftlabels=data->global_options.yLeftLabels; //draw labels at the left of the y-axis
	if (leftlabels) {
		const double labelspace=fontMetrics().boundingRect(QString("8888888")).width();
		hoffset+=labelspace+TICSLEN;
	}

	QLine line(hoffset, 0, hoffset, length);
	paint->drawLine(line);
	double step=first_decimal(yrange/data->global_options.ticks);
	double firstlabel= int(ywinmin/step)*step;
	double lastlabel= int(ywinmax/step)*step;
	QPointF p;
	for (double i=firstlabel; i<=lastlabel; i=i+step)
	{
		QPointF p1,p2;
    		if (fabs(i/step)<1e-10) 
			i=0; //fix rounding error around zero
		fromreal_toscreen(0,i, p);
		p1.setY(p.y());
		p1.setX(hoffset);
		p2.setY(p.y());
		if (!is_ygrid)
			if (!leftlabels)
				p2.setX(hoffset+TICSLEN);
			else
				p2.setX(hoffset-TICSLEN); //draw in oposite direction
		else{//draw as grid
			p2.setX(width());
			pen.setStyle(Qt::DotLine);
			paint->setPen(pen);
		}
		if (p1.y()<length) //dont paint tick outside the axis
			paint->drawLine(p1,p2);
		if (data->global_options.hasExtraTicks) {
			size_t n=data->global_options.extraTicks;
			pen.setStyle(Qt::SolidLine);
			paint->setPen(pen);
			double sstep=step/n;
			if (i==firstlabel)
			for (size_t j=0; j<n; j++) {
				fromreal_toscreen(0,i-sstep*j, p1);
				p1.setX(hoffset);
                                if (p1.y()<length) {//dont paint tick outside the axis
                                    if(!leftlabels)
                                        paint->drawLine(p1,p1+QPointF(2.0, 0.0));
                                    else
                                        paint->drawLine(p1,p1-QPointF(2.0, 0.0));}
			}
			for (size_t j=0; j<n; j++) {
				fromreal_toscreen(0,i+sstep*j, p1);
				p1.setX(hoffset);
                                if (p1.y()<length){ //dont paint tick outside the axis
					if(!leftlabels)
                                            paint->drawLine(p1,p1+QPointF(2.0, 0.0));
					else
                                            paint->drawLine(p1,p1-QPointF(2.0, 0.0));}
			}
		}	
		QString lab= QString("%1").arg(i,0, 'g', 3);
//		if (lab.contains("e")) {
//			lab.remove("+0");
//			lab.remove("+");
//			lab.replace("e",QString("x10<sup>"));
//			lab.append(QString("<\\sup>"));
//		}
		QPoint pp;
		if (!leftlabels)
			pp=QPoint(AXOFFSET+TICSLEN+2,int(p.y()+paint->fontMetrics().height()/2.5));
		else
			pp=QPoint(hoffset-TICSLEN-2-fontMetrics().boundingRect(lab).width(),int(p.y()+paint->fontMetrics().height()/2.5));
		if (willMarkBePrinted(pp))
			paint->drawText(pp,lab); //save space for axis label at the top and x-axis labels at the bottom
	}
	if (yaxis_label.isEmpty())
		ylabelrect=QRect(0,0,-1,-1);
	else {
		ylabelrect=fontMetrics().boundingRect(yaxis_label);
		if (!leftlabels)
			ylabelrect.moveBottomLeft(QPoint(hoffset+TICSLEN+2, paint->fontMetrics().height()+labeloffset));
		else
			ylabelrect.moveBottomRight(QPoint(hoffset-TICSLEN-2, paint->fontMetrics().height()+labeloffset));
	}
	paint->drawText(ylabelrect.bottomLeft(), yaxis_label);
}

bool Plot2DWidget::willMarkBePrinted(QPoint pp)
{
	return pp.y()<height()-AXOFFSET-TICSLEN-fontMetrics().height();
}

#ifdef USE_CUSTOM_POLYLINE
void Plot2DWidget::drawPolyline(QPainter* p, QPolygonF lines) //bypas of Qt 4.0.1 bug
{
	for (size_t i=lines.size()-1; i--;)
	{
		p->drawLine(lines.at(i), lines.at(i+1));
	}
	
}
#endif

void Plot2DWidget::drawPolylineSplit(QPainter* p, QPolygonF lines)
//continious line never exceed certain number of points
{
    QPolygonF limited;
    for (size_t i=0; i<lines.size(); i++)
    {
        if (!((i+1)%1024)){//start new line
            p->drawPolyline(limited);
            limited.clear();
            limited.append(lines.at(i-1));
        }
        limited.append(lines.at(i));
    }
    if (limited.size()) //draw the remaining
        p->drawPolyline(limited);
}

void Plot2DWidget::draw_spectra(QPainter * paint)
{
size_t ncurves=data->size();
QPoint point;
for (size_t k=0; k<ncurves; k++)
{
	if (!data->get_display(k)->isVisible)
		continue;
	if (!data->global_options.mixFidSpec)
		if (data->get_info(k)->type!=data->get_info(data->getActiveCurve())->type)
			continue;
	const size_t ni=data->get_odata(k)->rows();
	if (ni<=1) {
		size_t len=data->size(k);
		QPen pen;
		pen.setColor(get_color(k));
		if (k==data->getActiveCurve() && (!for_print) && data->global_options.useBold1D)
			pen.setWidth(2);//active curve in bold
		else 
			if (for_print)
				pen.setWidth(data->global_options.printLineWidth);
			else
				pen.setWidth(1);
		paint->setPen(pen);
		size_t startx, endx;
		fromreal_toindex(k, QPointF(xwinmin,ywinmin),startx);
		fromreal_toindex(k, QPointF(xwinmax,ywinmax),endx);
		if (endx<startx) 
			swap(startx,endx);
		if (startx>1) 
			startx-=2;
		if (endx<len-1)
			endx++;
		QPoint old(0,0);


		if (visualMode==SPECTRAASLINES || visualMode==SPECTRAASLINESANDPOINTS){
			if (!for_print){
				QPolygon polygon;
				fromreal_toscreen(startx,endx,polygon,k);
				if (whitewash) {
					QPen old=paint->pen();
					QPolygon pol2=polygon;
					QPoint p=pol2[0];
					p.setY(height());
					pol2.prepend(p);
					p=pol2.last();
					p.setY(height());
					pol2.append(p);
					paint->setBrush(QBrush(colourScheme.backgroundColour,Qt::SolidPattern));
					paint->setPen(QPen(Qt::NoPen));
					paint->drawPolygon(pol2);
					paint->setPen(old);
				}
#ifdef USE_CUSTOM_POLYLINE
				drawPolyline(paint, polygon); //custom drawPolyline is faster on Windows than Qt's one
#else
				paint->drawPolyline(polygon);
#endif
			}
			else {//for print
				QPolygonF polygon;
				fromreal_toscreen(startx,endx,polygon,k);
				if (whitewash) {
					QPen old=paint->pen();
					QPolygonF pol2=polygon;
					QPointF p=pol2[0];
					p.setY(height());
					pol2.prepend(p);
					p=pol2.last();
					p.setY(height());
					pol2.append(p);
					paint->setBrush(QBrush(colourScheme.backgroundColour,Qt::SolidPattern));
					paint->setPen(QPen(Qt::NoPen));
					paint->drawPolygon(pol2);
					paint->setPen(old);
				}
                                if (data->global_options.print1Dmode==PRINT1D_SPLIT)
                                    drawPolylineSplit(paint, polygon);
                                else
                                    paint->drawPolyline(polygon);

			}
			if (visualMode==SPECTRAASLINESANDPOINTS && endx-startx<width()) { //resolution allow to draw points
				QPoint p;
				for (int i=startx; i<=endx; i++) {
					fromreal_toscreen(data->get_x(k,i), data->get_value(k,0,i), p, k);
					paint->drawRect(QRectF(p-QPoint(1,1),QSize(3,3)));
				}
			}
		}
		else if (visualMode==SPECTRAASSTICKS) {
			QVector<QPoint> lines;
			for (size_t i=startx; i<=endx; i++)
			{
				QPoint point1, point2;
				fromreal_toscreen(data->get_x(k,i), data->get_value(k,0,i), point1, k);
				fromreal_toscreen(data->get_x(k,i), 0.0, point2, k);
				if (point1!=old) {
					lines<<point1<<point2;
					old=point1;
				}
			}
			paint->drawLines(lines);
		}
	}
}
const Display_* display=data->get_display(data->getActiveCurve());
if (display->xscale!=1.0 || display->xshift!=0 || display->vshift!=0 || display->vscale!=1.0) {
	paint->drawText(2*width()/3,paint->fontMetrics().height(),QString("Shift X: %1  Y: %2").arg(display->xshift).arg(display->vshift));
	paint->drawText(2*width()/3,paint->fontMetrics().height()*2,QString("Scale X: %1  Y: %2 ").arg(display->xscale).arg(display->vscale));
}
}

void Plot2DWidget::draw_bar_lines(QPainter *paint, QPointF barpos)
{
	if (for_print) {
		QPen pen=paint->pen();
		pen.setWidth(data->global_options.printLineWidth);
		paint->setPen(pen);
	}
	QPointF tp;
	fromreal_toscreen(barpos.x(), barpos.y(), tp);
	QLineF line1(tp.x(), 0, tp.x(),height());
	paint->drawLine(line1);
	
	QLineF line2(0,tp.y(),width(),tp.y());
	paint->drawLine(line2);
	if (!data->global_options.animatedMarkers) {
//draw cross
		paint->drawLine(tp+QPointF(4.0,4.0),tp-QPointF(4.0,4.0));
		paint->drawLine(tp+QPointF(4.0,-4.0),tp-QPointF(4.0,-4.0));
	}
	else {
//draw animation
		 QLabel* label = (paint->pen().style()==Qt::SolidLine)? markerLabel1 : markerLabel2;
		 label->move(tp.toPoint()-QPoint(label->width()/2,label->height()/2));
	}
}

void Plot2DWidget::draw_bars(QPainter *paint)
{
	QSize win=size();
	QPen pen;
	size_t index1, index2;
	pen.setColor(colourScheme.markerColour);
	paint->setPen(pen);
	if (is_leftbar) {
		fromreal_toindex(data->getActiveCurve(),leftbarpos,index1,false);
		draw_bar_lines(paint, leftbarpos);
		pen.setColor(colourScheme.textColour);
		paint->setPen(pen);
		paint->drawText(hoffset+50,paint->fontMetrics().height(),QString("Main X:%1 (Pt:%2); Y:%3").arg(leftbarpos.x()).arg(index1+1).arg(leftbarpos.y()));
//		markerLabel1->setVisible(true);
		if (!markerLabel1->isVisible() && data->global_options.animatedMarkers) {
			markerMovie1->start();
			markerLabel1->setVisible(true);
		}
	}
	else if (markerLabel1->isVisible() && data->global_options.animatedMarkers) {
		markerMovie1->stop();
		markerLabel1->setVisible(false);
	}
	if (is_rightbar) {
		fromreal_toindex(data->getActiveCurve(),rightbarpos,index2,false);
		pen.setStyle(Qt::DashLine);
		pen.setColor(colourScheme.markerColour);
		paint->setPen(pen);
		draw_bar_lines(paint, rightbarpos);
		pen.setColor(colourScheme.textColour);
		paint->setPen(pen);
		paint->drawText(hoffset+50,paint->fontMetrics().height()*2,QString("Secondary X:%1 (Pt:%2); Y:%3").arg(rightbarpos.x()).arg(index2+1).arg(rightbarpos.y()));
		if (is_leftbar)
			paint->drawText(hoffset+50,paint->fontMetrics().height()*3,QString("%4 X:%1 (Pts:%2); Y:%3").arg(leftbarpos.x()-rightbarpos.x()).arg(int(index1)-int(index2)).arg(leftbarpos.y()-rightbarpos.y()).arg(QChar(0x0394)));
		if (!markerLabel2->isVisible() && data->global_options.animatedMarkers) {
			markerMovie2->start();
			markerLabel2->setVisible(true);
		}
	}
	else if (markerLabel2->isVisible() && data->global_options.animatedMarkers) {
		markerMovie2->stop();
		markerLabel2->setVisible(false);
	}	
}

void Plot2DWidget::draw_integrals (QPainter * paint)
{
//  const size_t k=data->getActiveCurve();
for (size_t k=0; k<data->size(); k++) {
  if (!data->get_display(k)->isVisible)
	continue;
  size_t ninteg=data->integrals_size(k);
  if (!ninteg) continue;
  if (data->get_odata(k)->rows()>1) continue;
  Array_ intar("empty");
  for (size_t i=0; i<data->get_arrays(k).size(); i++)
	if (data->get_arrays(k).at(i).name==QString("IntegList"))
		intar=data->get_arrays(k)[i];
  if (intar.name==QString("empty"))
	throw Failed("IntegList not found");

  QPointF p1, p2;
  QPen pen=paint->pen();
  pen.setColor(get_color(data->size()));
  pen.setStyle(Qt::SolidLine);

  if (for_print)
	pen.setWidth(data->global_options.printLineWidth);
  paint->setPen(pen);
  const double minv=min(real(*(data->get_odata(k))));
  for (size_t ing=0; ing<ninteg; ing++)
  {
    size_t len=data->integrals_size(k,ing);
    QPolygonF polygon;

	for (size_t i=0; i<len; i++){
		double x1=data->get_integral_x(k,ing,i);
		double y1=data->get_integral_value(k,ing,i);
		double vscale=data->get_integrals(k).integral_vscale;
		double vshift=data->get_integrals(k).integral_vshift;
		fromreal_toscreen(x1, vscale*y1+vshift, p1,k);
		polygon<<p1;
	}
	paint->drawPolyline(polygon);

//	fromreal_toscreen(data->get_integral_x(k,ing,0), 0.1*data->get_integral_value(k,ing,0), p1, k);
//	fromreal_toscreen(data->get_integral_x(k,ing,len-1), 0.1*data->get_integral_value(k,ing,len-1), p2, k); 

	fromreal_toscreen(data->get_integral_x(k,ing,0), minv, p1, k);
	fromreal_toscreen(data->get_integral_x(k,ing,len-1), minv, p2, k); 

	p1+=QPointF(0.0,15.0);
	p2.setY(p1.y());	
	paint->drawLine(p1,p1-QPointF(0.0,5.0));
	paint->drawLine(p2,p2-QPointF(0.0,5.0));
	paint->drawLine(p1,p2);
	p1.setX(p1.x()+fabs(p2.x()-p1.x())/2);
	paint->drawLine(p1,p1+QPointF(0.0,5.0));
	p1+=QPointF(0.0,5.0);
	QMatrix matrix;
	matrix.rotate(90.0);
	QPointF t=matrix.inverted().map(p1);
	t.setX(t.x()+3);
	t.setY(t.y()+paint->fontMetrics().height()/4);
	paint->setMatrix(matrix);
	double val=intar.data(ing)*data->get_integral_scale(k);
	paint->drawText(t, QString("%1").arg(val,0,'g',3));
	paint->resetMatrix();
  }
}
}

/*
void Plot2DWidget::draw_peaks (QPainter * paint) {
	for (size_t k=data->size(); k--;) {
	if (!data->get_display(k)->isVisible)
		continue;
	if (data->get_odata(k)->rows()>1)
		continue;
	if (!data->get_display(k)->hasPeaks)
		continue;
	QList<Array_> arrays=data->get_arrays(k);
	size_t i=0;
	while (arrays.at(i).name!=QString("PeaksPos")) {
		i++;
		if (i>=arrays.size())
			return;
	}
	paint->setPen(Qt::black);
	double prev_point=-100.0;

	double max=-9e36;
	for (size_t l=0; l<data->get_odata(k)->cols(); l++)
		if (max<data->get_value(k,0,l))
			max=data->get_value(k,0,l); //find max value on the spectrum

	for (size_t j=0; j<arrays.at(i).data.size(); j++) {
		QPointF top;
		QPointF start;

		double val=arrays.at(i).data(j);
		size_t ind;
		fromreal_toindex(k, QPointF(val,0.0),ind,false);
		fromreal_toscreen(val, data->get_value(k,0,ind), start, k);
		if ((start.x()<0) || (start.x()>width()))
			continue; // don't draw peaks outside the sreen area
		fromreal_toscreen(val, max, top, k);
		start.setY(start.y()-3); //start at the start of the line
		top.setY(top.y()-33); //top at the end of the line
		if ((top.x()<prev_point+paint->fontMetrics().height()))
			top.setX(prev_point+paint->fontMetrics().height()); //correct top point if overlapping
		prev_point=top.x();

		if (top.y()<65)
			top.setY(65);
		if (start.y()-top.y()<33)
			start.setY(top.y()+33); //don't allow the labels to move outside the screen

		QPointF end(start.x(),top.y()+30);
		QPolygonF polygon;
		polygon<<start<<end<<top+QPointF(0,6)<<top;
		paint->drawPolyline(polygon);

		QMatrix matrix;
		matrix.rotate(-90.0);
		QPointF t=matrix.inverted().map(top);
		t.setX(t.x()+3);
		t.setY(t.y()+paint->fontMetrics().height()/4);
		paint->setMatrix(matrix);
		paint->drawText(t, QString("%1").arg(val,0,'f',3));
		paint->resetMatrix();
	}
	}
}
*/

 
void Plot2DWidget::draw_peaks (QPainter * paint) {
//standard checks
	for (size_t k=data->size(); k--;) {
	if (!data->get_display(k)->isVisible)
		continue;
	if (data->get_odata(k)->rows()>1)
		continue;
	if (!data->get_display(k)->hasPeaks)
		continue;
	QList<Array_> arrays=data->get_arrays(k);
	size_t i=0;
	while (arrays.at(i).name!=QString("PeaksPos")) {
		i++;
		if (i>=arrays.size())
			return;
	}
//i contains the peak position at the exit
	QPen pen=paint->pen();
	pen.setColor(colourScheme.axisColour);
	if (for_print)
		pen.setWidth(data->global_options.printLineWidth);
	paint->setPen(pen);
//	double prev_point=-100.0;

	double max=-9e36;
	for (size_t l=0; l<data->get_odata(k)->cols(); l++)
		if (max<data->get_value(k,0,l))
			max=data->get_value(k,0,l); //find max value on the spectrum


	const int Npeaks=arrays.at(i).data.size();
	List<QPoint> start(Npeaks); //contains starting points (x and y)
	List<int> top(Npeaks); //contains ending x-points (corrected for overlappings) (x only)
//create initial values
	for (size_t j=0; j<Npeaks; j++) {
		double val=arrays.at(i).data(j);
		size_t ind;
		fromreal_toindex(k, QPointF(val,0.0),ind,false);
		fromreal_toscreen(val, data->get_value(k,0,ind), start(j), k);
		top(j)=int(start(j).x()); //initial parameters are the same
	}
	const int twidth=paint->fontMetrics().height(); //text width
//corrects top value
	for (size_t j=1; j<Npeaks; j++) {
		if (start(j-1).x()<0 || start(j).x()>width())
			continue;//don't correct peaks outside the sreen area
		if (top(j)-top(j-1)<twidth) { //shift is needed
			int cc=j-1;
			top(cc)-=twidth/2;
			top(j)=top(cc)+twidth;
			while (cc && (top(cc)-top(cc-1)<twidth)) {
				top(cc-1)=top(cc)-twidth;
				cc--;
			}
		}
	}
//draw everything
#define OFFSETFROMPEAK 5
#define MINHEIGHT 33
#define SMALLLINELOW 3
#define SMALLLINEHIGH 6
#define TEXTOFFSET 3
#define MINTEXTFIELD 65
	QPointF tempp; //will contains the maximal intensity in screen coordinate
	fromreal_toscreen(0, max, tempp, k);
	int upper_point=int(tempp.y())-MINHEIGHT-OFFSETFROMPEAK;//the proposed highest point
	if (upper_point<MINTEXTFIELD) //the highest possible upper point, needed to place label
		upper_point=MINTEXTFIELD;
	for (size_t j=0; j<Npeaks; j++) {
		if (start(j).x()<0 || start(j).x()>width())
			continue; //don't draw peaks outside the sreen area
//This part will use long line for peak label (user Pongpong didn't like it)
//		start(j).setY(start(j).y()-OFFSETFROMPEAK); //shift start point 3 pixels up
//		if (start(j).y()-upper_point<MINHEIGHT)
			start(j).setY(upper_point+MINHEIGHT); //don't allow the labels to move outside the screen
		QPolygonF polygon;
		polygon<<start(j)<<QPointF(start(j).x(),upper_point+MINHEIGHT-SMALLLINELOW)<<QPointF(top(j),upper_point+SMALLLINEHIGH)<<QPointF(top(j),upper_point);
		paint->drawPolyline(polygon);
		QMatrix matrix;
		matrix.rotate(-90.0);
		QPointF t=matrix.inverted().map(QPointF(top(j),upper_point));
		t.setX(t.x()+TEXTOFFSET);
		t.setY(t.y()+paint->fontMetrics().height()/4);
		paint->setMatrix(matrix);
		paint->drawText(t, QString("%1").arg(arrays.at(i).data(j),0,'f',3));
		paint->resetMatrix();
	}
}
}

void Plot2DWidget::init_settings()
{
	size_t k=data->getActiveCurve();
	if (data->get_info(k)->type==FD_TYPE_FID) {
		is_xaxis_inverted=false;
		switch (data->global_options.xtimeunits) {
			case UNITS_US:
				xaxis_label=QString("us");
				break;
			case UNITS_MS:
				xaxis_label=QString("ms");
				break;
			case UNITS_S:
				xaxis_label=QString("s");
				break;
			case UNITS_TPTS:
                                xaxis_label=QString("pts");
				break;
			default:
				xaxis_label=QString("");
				break;
		}
	}
	else {
		is_xaxis_inverted=true;
		switch (data->global_options.xfrequnits) {
			case UNITS_PPM:
				xaxis_label=QString("ppm");
				break;
			case UNITS_HZ:
				xaxis_label=QString("Hz");
				break;
			case UNITS_KHZ:
				xaxis_label=QString("kHz");
				break;
			case UNITS_FPTS:
				xaxis_label=QString("pts (frq)");
                                is_xaxis_inverted=false;
				break;
			default:
				xaxis_label=QString("");
				break;
		}
	}
}

void Plot2DWidget::draw_picture(QPainter *paint)
{
if (data!=NULL){
if (data->size()) {
	init_settings();
	draw_extra_graph(paint);
//Draw actual spectra/FIDs
	draw_spectra(paint);
//draw bars if present
	draw_bars(paint);
//draw integrals if present
	draw_integrals(paint);
	draw_peaks(paint);
	draw_axis(paint);
	draw_yaxis(paint);
}
else
	draw_logo(paint);
}}

void Plot2DWidget::draw_logo(QPainter *paint)
{
	static int countdown=9;
	double scaler=1.0;
//	qDebug()<<"Counter"<<countdown;
	if (countdown>0) {
		scaler=(10.0-countdown)/10.0;
		QMatrix mat;
		mat.scale(scaler, scaler);
		paint->setMatrix(mat);
	}
	else {
		paint->resetMatrix();
	}
	const QImage logo(":/images/startlogo.png");
	QPointF start((width()/scaler-logo.width())/2, (height()/scaler-logo.height())/2);
	paint->drawImage(start, logo);
	if (countdown>0) {
		countdown--;
		QTimer::singleShot(30,this,SLOT(update()));
	}
	else {
	QString version=QString("(c) Vadim Zorin; Version: %1").arg(VERSION);
	QFontMetrics metrics=paint->fontMetrics();
	QPointF starttext(width()-metrics.boundingRect(version).width()-5, height()-metrics.boundingRect(version).height());
	paint->drawText(starttext, version);
	}
}

void Plot2DWidget::draw_extra_graph(QPainter * paint)
{
	const QFont defaultFont=paint->font();
	for (size_t k=0; k<data->size(); k++) {
		if (!data->get_display(k)->isVisible)
			continue;
		if ((data->get_odata(k)->rows()>1)!=(data->get_odata(data->getActiveCurve())->rows()>1))
			continue;
		QList<graph_object> graphics=data->get_graphics(k);
		size_t n=graphics.size();
		if (!n)
			continue;
//		paint->setPen(get_color(k));
		for (size_t i=0; i<n; i++) {
			(graphics.at(i).colour==Qt::transparent)? paint->setPen(get_color(k)) : paint->setPen(graphics.at(i).colour);
			if (graphics.at(i).type==GRAPHTEXT) {
				QPointF pos=graphics.at(i).pos.toPointF();//position in real coordinates
				QPointF spos;//screen position
				QFont font;
				if (font.fromString(graphics.at(i).font))
					paint->setFont(font);
				fromreal_toscreen(pos.x(), pos.y(), spos, k);
				if (graphics.at(i).angle) {//rotate text
					QMatrix matrix;
					matrix.rotate(graphics.at(i).angle);
					spos=matrix.inverted().map(spos);
					paint->setMatrix(matrix);
				}
				paint->drawText(spos, graphics.at(i).maindata.toString());
				if (graphics.at(i).selected) {
					QPen pen=paint->pen();
					QRect rect=paint->fontMetrics().boundingRect(graphics.at(i).maindata.toString());
					rect.moveTo(spos.toPoint()-QPoint(0,rect.height()));
					paint->setPen(Qt::DotLine);
					paint->setBrush(Qt::NoBrush);
					paint->drawRect(rect);
					paint->setPen(pen);
					paint->setPen(Qt::SolidLine);
					paint->setBrush(Qt::SolidPattern);
					paint->drawRect(QRect(spos.x()-1,spos.y()-1,2,2));
				}
				paint->setFont(defaultFont);
				if (graphics.at(i).angle) {
					paint->resetMatrix();
				}
			}
			else if (graphics.at(i).type==GRAPHLINE) {
				QLineF pos=graphics.at(i).pos.toLineF();
				QPointF p1, p2;
				fromreal_toscreen(pos.x1(), pos.y1(), p1, k);
				fromreal_toscreen(pos.x2(), pos.y2(), p2, k);
				paint->drawLine(p1,p2);
				if (graphics.at(i).selected) {
					QPen pen=paint->pen();
					paint->setPen(Qt::green);
					paint->setBrush(Qt::green);
					paint->drawRect(int(p1.x())-3,int(p1.y())-3,7,7);
					paint->drawRect(int(p2.x())-3,int(p2.y())-3,7,7);
					paint->setPen(pen);
				}
			}
			else if (graphics.at(i).type==GRAPHIMAGE) {
				QPointF pos=graphics.at(i).pos.toPointF();
				QPointF spos;
				QRect rect;
				fromreal_toscreen(pos.x(), pos.y(), spos, k);
				if (graphics.at(i).scale==1.0) {
					paint->drawPixmap(spos, graphics.at(i).maindata.value<QPixmap>());
					rect=graphics.at(i).maindata.value<QPixmap>().rect();
				}
				else {
					QPixmap px=graphics.at(i).maindata.value<QPixmap>();
					QPixmap pxn;
					if (data->global_options.smoothTransformations)
						pxn=px.scaled(graphics.at(i).scale*px.size(), Qt::KeepAspectRatio, Qt::SmoothTransformation);
					else	
						pxn=px.scaled(graphics.at(i).scale*px.size(), Qt::KeepAspectRatio, Qt::FastTransformation);
					paint->drawPixmap(spos, pxn);
					rect=pxn.rect();
				}
				if (graphics.at(i).selected) {
					QPen pen=paint->pen();
					rect.moveTo(spos.toPoint());
					paint->setPen(Qt::DotLine);
					paint->setBrush(Qt::NoBrush);
					paint->drawRect(rect);
					paint->setPen(Qt::green);
					paint->setBrush(Qt::green);
					int x1,x2,y1,y2;
					rect.getCoords(&x1,&y1,&x2,&y2);
					paint->drawRect(x1,y1,7,7);
					paint->drawRect(x2-7,y1,7,7);
					paint->drawRect(x1,y2-7,7,7);
					paint->drawRect(x2-7,y2-7,7,7);
					paint->setPen(pen);
				}
			}
			else if (graphics.at(i).type==GRAPHVPICTURE) {
				QPointF pos=graphics.at(i).pos.toPointF();
				QPointF spos; //screen position
				QRect rect; //contains bounding rect in local coordinates (not screen coords)
				fromreal_toscreen(pos.x(), pos.y(), spos, k);
				spos/=graphics.at(i).scale; //scaled position
				QMatrix mat;
				mat.scale(graphics.at(i).scale,graphics.at(i).scale);
				paint->setMatrix(mat);
				QByteArray bdata=graphics.at(i).maindata.toByteArray();
				QPicture pic;
				pic.setData(bdata.data(),bdata.size());
				paint->drawPicture(spos, pic);
				rect=pic.boundingRect();

				if (graphics.at(i).selected) {
					QPen pen=paint->pen();
					rect.moveTo(spos.toPoint());
					paint->setPen(Qt::DotLine);
					paint->setBrush(Qt::NoBrush);
					paint->drawRect(rect);
					paint->setPen(Qt::green);
					paint->setBrush(Qt::green);
					int x1,x2,y1,y2;
					rect.getCoords(&x1,&y1,&x2,&y2);
					int corner_size=int(7/graphics.at(i).scale);
					paint->drawRect(x1,y1,corner_size,corner_size);
					paint->drawRect(x2-corner_size,y1,corner_size,corner_size);
					paint->drawRect(x1,y2-corner_size,corner_size,corner_size);
					paint->drawRect(x2-corner_size,y2-corner_size,corner_size,corner_size);
					paint->setPen(pen);
				}
				paint->resetMatrix();
			}
		}
	}
}

void Plot2DWidget::unselectAll()
{
	for (size_t k=0; k<data->size(); k++) {
		QList<graph_object> graphics=data->get_graphics(k);
		for (size_t i=0; i<graphics.size(); i++)
			graphics[i].selected=false;
		data->set_graphics(k,graphics);
	}
	update();
}

graph_object* Plot2DWidget::find_graph_object(double x, double y, int k, int* index)
{
	QPoint pt;
		if (!data->get_display(k)->isVisible)
			return NULL;
		if ((data->get_odata(k)->rows()>1)!=(data->get_odata(data->getActiveCurve())->rows()>1))
			return NULL;
		QList<graph_object> graphics=data->get_graphics(k);
		size_t n=graphics.size();
		if (!n)
			return NULL;
		for (size_t i=0; i<n; i++) {
			if (graphics.at(i).type==GRAPHTEXT) {
				QFont font;
				QFontMetrics metrics=fontMetrics();
				if (font.fromString(graphics.at(i).font))
					metrics=QFontMetrics(font);
				fromreal_toscreen(x,y,pt,k);
				QRect rect=metrics.boundingRect(graphics.at(i).maindata.toString());
				QPointF pos=graphics.at(i).pos.toPointF();
				QPoint spos;
				fromreal_toscreen(pos.x(), pos.y(), spos, k);
				spos-=QPoint(0,rect.height());
				rect.moveTo(spos);
				QPolygon poly(rect,true);
				if (graphics.at(i).angle) {//correct polygon according to matrix 
					poly.clear();
					spos+=QPoint(0,rect.height());
					double cs=cos(graphics.at(i).angle/180.0*M_PI);
					double sn=sin(graphics.at(i).angle/180.0*M_PI);
					QPoint rgBt=spos+QPoint(cs*rect.width(),sn*rect.width());
					QPoint lfTp=spos+QPoint(sn*rect.height(),-cs*rect.height());
					QPoint rgTp=lfTp+QPoint(cs*rect.width(),sn*rect.width());
					poly<<spos<<rgBt<<rgTp<<lfTp;
					//qDebug()<<rect<<poly;
				}
				if (poly.containsPoint(pt,Qt::OddEvenFill)) {
					if (index)
						*index=i;
					return data->get_graphobject(k,i);
				}
			}
			else if (graphics.at(i).type==GRAPHLINE) {
				fromreal_toscreen(x,y,pt,k);
				QLineF pos=graphics.at(i).pos.toLineF();
				QPointF p1, p2;
				fromreal_toscreen(pos.x1(), pos.y1(), p1, k);
				fromreal_toscreen(pos.x2(), pos.y2(), p2, k);
				QRectF lrec=QRectF(p1.x(),p1.y(),p2.x()-p1.x(),p2.y()-p1.y()).normalized();
				lrec.setTopLeft(lrec.topLeft()-QPointF(5,5));
				lrec.setBottomRight(lrec.bottomRight()+QPointF(5,5)); //a bit extended rectangle
				if (!lrec.contains(pt))
					continue;
				//calculation of distance from point to line is taken from http://mathworld.wolfram.com/Point-LineDistance2-Dimensional.html
				double dist=fabs((p2.x()-p1.x())*(p1.y()-pt.y())-(p1.x()-pt.x())*(p2.y()-p1.y()))/sqrt((p2.x()-p1.x())*(p2.x()-p1.x())+(p2.y()-p1.y())*(p2.y()-p1.y()));
				if (dist<5) {
//				if ((QLineF(pt,p1).length()+QLineF(pt,p2).length())<(QLineF(p1,p2).length())+10) {
					if (index)
						*index=i;
					return data->get_graphobject(k,i);
				}
			}
			else if (graphics.at(i).type==GRAPHIMAGE) {
//				qDebug()<<"checking image";
				fromreal_toscreen(x,y,pt,k);
				QPointF pos=graphics.at(i).pos.toPointF();
				QPoint spos;
				fromreal_toscreen(pos.x(), pos.y(), spos, k);
				QSize actSize=graphics.at(i).maindata.value<QPixmap>().size()*graphics.at(i).scale;
				if (QRect(spos, actSize).contains(pt)) {
					if (index) 
						*index=i;
					return data->get_graphobject(k,i);
				}
			}
		}
	if (index)
		*index=-1;
	return NULL;
}

void Plot2DWidget::fromreal_toindex(size_t k, QPointF pos, size_t& xi, bool with_shifts)
{ //pos in real coordinates
	if (!data->size())
		return;
	size_t xsize=data->get_odata(k)->cols();
	if (with_shifts) 
		pos.setX((pos.x()-data->get_display(k)->xshift)/data->get_display(k)->xscale);
	int xi_=lround ((pos.x()-data->get_x(k,0))/(data->get_x(k,xsize-1)-data->get_x(k,0))*(xsize-1));
	if (xi_<0) xi_=0;
	xi=size_t(xi_);
	if (xi>=xsize) xi=xsize-1;
}

void Plot2DWidget::dragEnterEvent(QDragEnterEvent *event)
   {
	QStringList f=event->mimeData()->formats();
	for (size_t i=f.size(); i--;)
	    if (event->mimeData()->hasFormat("text/uri-list") ){
            event->acceptProposedAction();
	}
  }

void Plot2DWidget::dropEvent(QDropEvent *event)
{
	QImage image;
	if (event->mimeData()->hasUrls()) {
		QList<QUrl> urls=event->mimeData()->urls();
        	event->acceptProposedAction();
		for (size_t i=urls.size(); i--;) {
			bool itIsImage=false;
			QString s=urls.at(i).toLocalFile();
			if (s.isEmpty())
				continue;
			itIsImage=image.load(s);
			if (!itIsImage)
				emit openFileAsked(s);
			else 
				image_dropped(event,image);
		}
	}
	else if (event->mimeData()->hasImage()) {
		if (!data->size())
			return; //check that at least 1 file is opened
		event->acceptProposedAction();
		image = qvariant_cast<QImage>(event->mimeData()->imageData());
		image_dropped(event,image);
	}
}

void Plot2DWidget::image_dropped(QDropEvent *event, QImage& image)
{
		if (!data->size()) {
			QMessageBox::critical(this, "Cannot drop image","You should open at least 1 dataset before you can attach an image");
			return;
		}
		graph_object obj;
		obj.type=GRAPHIMAGE;
		obj.maindata=QPixmap::fromImage(image);
		double x,y;
		fromscreen_toreal(event->pos(),x,y);
		obj.pos=QPointF(x,y);
		if (image.width()>width() || image.height()>height()) {
			obj.scale=(double(image.width())/double(width())>double(image.height())/double(height()))? 0.2*double(width())/double(image.width()): 0.2*double(height())/double(image.height()); //rescale image if it is too big
//			qDebug()<<"Scaler"<<obj.scale;
		}
		QList<graph_object> graphs=data->get_graphics(data->getActiveCurve());
		graphs.push_back(obj);
		data->set_graphics(data->getActiveCurve(),graphs);
		update();
}

QColor Plot2DWidget::get_color(size_t i)
{
	while (i>=colourScheme.plot2DColour.size())
		i-=colourScheme.plot2DColour.size();
	return colourScheme.plot2DColour[i];
}
