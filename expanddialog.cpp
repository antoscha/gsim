#include "expanddialog.h"
#include <QMessageBox>

expandDialog::expandDialog(Plot2DWidget* parent, Qt::WindowFlags fl)
	:QDialog(parent, fl) {
    setupUi(this);
    setWindowIcon( QPixmap( ":/images/gsim.png" ) );
    plot=parent;
    // signals and slots connections
    readSettings();
}


expandDialog::~expandDialog()
{
    writeSettings();
    // no need to delete child widgets, Qt does it all for us
}

void expandDialog::readSettings()
{
        QSettings settings("GSim", "GSim");
        settings.beginGroup("ExpandDialog");
        resize(settings.value("size", QSize(437, 150)).toSize());
        move(settings.value("pos", QPoint(200, 200)).toPoint());
        settings.endGroup();
}

void expandDialog::writeSettings()
{
	QSettings settings("GSim", "GSim");
        settings.beginGroup("ExpandDialog");
        settings.setValue("size", size());
        settings.setValue("pos", pos());
        settings.endGroup();
}

void expandDialog::on_markersButton_clicked()
{
	if ((!plot->is_leftbar) || (!plot->is_rightbar)) {
		QMessageBox::critical(this, "Expansion impossible", "Please, set both markers first", "Abort");
		return;
	}
	double xmin=plot->leftbarpos.x();
	double xmax=plot->rightbarpos.x();
	double ymin=plot->leftbarpos.y();
	double ymax=plot->rightbarpos.y();
	if (xmin>xmax) 
		std::swap(xmin,xmax);
	if (ymin>ymax) 
		std::swap(ymin,ymax);
	xstartSpinBox->setValue(xmin);
	xendSpinBox->setValue(xmax);
	ystartSpinBox->setValue(ymin);
	yendSpinBox->setValue(ymax);
}
