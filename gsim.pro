###################################################################################################
#                  	Change this part according to your system configuration                   #
###################################################################################################

#Uncomment next line if you want to have a system console running alongside GSim on Windows
#win32: CONFIG +=console

#Uncomment next line if you want to use OpenGL
CONFIG+=use_opengl

#Uncomment next line if you want to use EMF output
#EMF output requires libEMF or Wine on X11 systems and MAC(?)
CONFIG+=use_emf

#DO NOT UNCOMMENT NEXT LINE. The scripting possibilty won't work at the moment
#CONFIG+=use_script

# Change directories locations for libcmatrix, MinUIT and muParser libraries
unix:INCLUDEPATH += /usr/include/libcmatrix /usr/include

win32:INCLUDEPATH += "C:\gsim\libcmatrixR3_qt4\include" "C:\gsim\Minuit2_qt4\include" "C:\gsim\muparser_qt4\include"

unix:LIBS += -lcmatrix -lMinuit2 -lmuparser -lEMF

win32:LIBS += -lcmatrix -L"C:\gsim\libcmatrixR3_qt4\lib" -lMinuit2 -L"C:\gsim\Minuit2_qt4\lib" -L"C:\gsim\muparser_qt4\lib" -lmuparser

#Change directories location for libEMF (if used)
use_emf {
	DEFINES+=USE_EMF_OUTPUT Q_WS_X11
unix:LIBS += -lcmatrix -lMinuit2 -lmuparser -lEMF
	win32:LIBS +=-lgdi32
}

#################################################################################################
#                             Don't change anything below this line                             #
#################################################################################################

CONFIG += qt debug_and_release
QT += svg printsupport

use_opengl {
	QT+= opengl
}

use_script {
	QT += script
}

FORMS +=spectralparsdialog.ui \
        datatable.ui \
        deconvform.ui \
	phasingform.ui \
        arraymanager.ui \
        mainform.ui \
        proceditor.ui \
	modelessdialog.ui \
	optionsDialog.ui \
	castepdialog.ui \
	expanddialog.ui \
	fitdialog.ui \
	plotarea.ui

HEADERS += version.h \
           plot2dwidget.h \
           mainform.h \
           base.h \
	   plot3dwidget.h \
	   aboutform.h \
	   phasedialog.h \
	   spectralparsdialog.h \
	   datatable.h \
	   deconvform.h \
	   arraymanager.h \
	   processing.h \
           proceditor.h \
	   optionsDialog.h \
	   graphics_out.h \
	   filefilters.h \
	   castepdialog.h \
	   expanddialog.h \
	   fitdialog.h \
	   plotarea.h \
	   widgets.h \
	   arrayplot.h


SOURCES += main.cpp \
            plot2dwidget.cpp \
           mainform.cpp \
	   mainformsignals.cpp \
	   base.cpp \ 
	   plot3dwidget.cpp \
	   aboutform.cpp \
	   phasedialog.cpp \
	   spectralparsdialog.cpp \
	   deconvform.cpp \
	   datatable.cpp \
	   arraymanager.cpp \
	   processing.cpp \
	   proceditor.cpp \
	   optionsDialog.cpp \
	   graphics_out.cpp \
	   filefilters.cpp \
	   castepdialog.cpp \
	   expanddialog.cpp \
	   fitdialog.cpp \
	   plotarea.cpp \
	   widgets.cpp \
	   arrayplot.cpp

RESOURCES += images.qrc 

TEMPLATE =app

win32:RC_FILE = icon.rc

#changing this variables can be wrong for compilers other than GCC
contains (QMAKE_CXX, g++) {
	QMAKE_CXXFLAGS_WARN_ON +=  -Wno-sign-compare
	QMAKE_CFLAGS_WARN_ON +=  -Wno-sign-compare
	QMAKE_CFLAGS_ISYSTEM=''

        #because of the bug in MinGW with gcc 3.4 O2 produces a faulty code for MatLAB
        win32 {QMAKE_CXXFLAGS_RELEASE = -O1
              QMAKE_CFLAGS_RELEASE = -O1}
}
